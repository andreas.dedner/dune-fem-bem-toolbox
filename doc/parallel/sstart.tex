\input{talk}
%\documentclass[a4paper,14pt,twoside]{article}
%\input{packages}
\usepackage{versions}
\usepackage{ifthen}
\usepackage{epsfig}
\usepackage{mathrsfs}
\usepackage{natbib}
\usepackage{graphics}
\usepackage{graphicx}
%\usepackage{hyperref}

\setcounter{secnumdepth}{0}
\newcounter{eqnsection}
%\numberwithin{equation}{eqnsection}
\alph{eqnsection}
\newboolean{slides}
\setboolean{slides}{true}
\newcommand{\nn}{\nonumber\\}
\newcommand{\cxy}{\cos\left(x y\right)}
\newcommand{\sxy}{\sin\left(x y\right)}
\newcommand{\ignore}[1]{}
\newcommand{\slidetitle}{\frametitle}
\newcommand{\sectiontitle}{\ignore}

\newcommand{\slidesmall}{\small}
\newcommand{\slidetiny}{\tiny}

\newcommand{\fo}{ (FEM only) Convergence with $\alpha$}
\newcommand{\sy}{ (Sym. FEM - BEM) Convergence with $\alpha$}
\newcommand{\ns}{ (Non - Sym. FEM - BEM) Convergence with $\alpha$}
\newcommand{\st}{ - Structured Mesh}
\newcommand{\us}{ - Unstructured Mesh}
%
\newenvironment{slide}{\begin{frame}}{\end{frame}}
%
%\newenvironment{slide}{\begin{large}}{\end{large}}
%
\includeversion{pdfoff}
\excludeversion{slioff}

\newboolean{piccys}
\setboolean{piccys}{false}

% Commands for diagrams
\renewcommand{\#}{$}
%\renewcommand{\%}{\Gamma}
\renewcommand{\_}{_}
%\newcommand{/d}{\Omega}

\begin{document}

\titlepage


\begin{slide}
\slidetitle{FEM-BEM Coupling Schemes}

\begin{itemize}
 \item FEM: large, sparse system matrices;
 \item BEM: small, dense system matrices.
\end{itemize}


Direct coupling:

\begin{itemize}
 \item Single system matrix;
 \item Single solution process;
 \item Hard to precondition;
 \item Solver optimization difficult.
\end{itemize}


Iterative coupling:

\begin{itemize}
 \item Seperate system matrices;
 \item Iterative updating of solution between sub-problems;
 \item Specialist preconditioning for each system;
 \item Optimized solvers for each sub-problem.
\end{itemize}

\end{slide}


 \begin{slide}
  \slidetitle{FEM-FEM First Example}

  Governing equations:
  \vspace{-10pt}

\begin{subequations}
\begin{align}
FEM:\hspace{20pt}&
-\Delta\,u+u
&=
f
&\ \ \ &in\ \Omega
\nonumber
\\
Rob:\hspace{20pt}&
u-v+\frac{\partial u}{\partial n}
&=
0
&\ \ \ &on\ \Gamma
\nonumber
\\
FEM:\hspace{20pt}&
-\Delta_{\Gamma}\,v+v
+\frac{\partial u}{\partial n}
&=
g
&\ \ \ &on\ \Gamma
\nonumber
\end{align}
\label{rannereqns}
\end{subequations}

  \vspace{-13pt}
Analytic (example) solution):
  \vspace{-20pt}


\begin{eqnarray}
u(x,y,z)
&=&
exp\left[-x\left(x-1\right)-y\,\left(y-1\right)\right]
\nonumber
\\
v(x,y,z)
&=&
\left[1+x\,\left(1-2\,x\right)+y\left(1-2\,y\right)\right]
\nonumber
\\&&
\times exp\left[-x\left(x-1\right)-y\,\left(y-1\right)\right]
\nonumber
\end{eqnarray}

  \vspace{-10pt}
Weak formulation:
  \vspace{-10pt}

\begin{eqnarray}
  \int_\Omega \left(\nabla u\cdot \nabla\eta + u\eta \right)
      + \int_\Gamma u\eta
    &=&
 \int_\Omega f\eta
      + \int_\Gamma v\eta
\nonumber
\\
  \int_\Gamma \left(\nabla_{\Gamma} v\cdot \nabla_{\Gamma} \xi + v\,\xi\right)
      +\int_\Gamma v\xi
    &=&
 \int_\Gamma g\xi
      + \int_\Gamma u\xi
\nonumber
\end{eqnarray}


 \end{slide}



 \begin{slide}
  \slidetitle{FEM-FEM First Example}


\begin{table}[h!]
  \centering
  \caption{Convergence rates for bulk-surface example.}
  \label{femfemconvergencetable}
  \begin{tabular}{ccccc}
    %\toprule
    Refinement & Iteration & Error & Time & Convergence \\
    Level      & Count     &       & (secs) & Order \\
    %\toprule
    \input{../../tests/bulkSurfFemFem/source/convergence}
    %\toprule
  \end{tabular}
\end{table}


 \end{slide}



 \begin{slide}
  \slidetitle{FEM-BEM Scattering Example}

\begin{subequations}
\begin{align}
FEM:\hspace{20pt}&
\Delta\,u
+
n\left({\bf{x}}\right)k^2u
&=
0
&\ \ \ &in\ \Omega
\label{scatemgovone}
\\
BEM:\hspace{20pt}&
\Delta\,v
+
k^2 v
&=
0
&\ \ \ &in\ \mathbb{R}^3\backslash \Omega
\label{scatemgovtwo}
\\
Dir:\hspace{20pt}&
u-v
&=
0
&\ \ \ &in\ \Gamma
\label{scatemgovthr}
\\
Neu:\hspace{20pt}&
\frac{\partial u}{\partial\,n}
-
\frac{\partial v}{\partial\,n}
&=0
&\ \ \ &in\ \Gamma
\label{scatemgovfou}
\end{align}
\label{emscateqns}
\end{subequations}


\begin{equation}
 n\left({\bf{x}}\right)
=
\frac{
1-0.5\exp\left(-||{\bf{x}}||^2_{\infty}\right)
}{
1-0.5\exp\left(-0.25\right)
}
\end{equation}

 \end{slide}


 \begin{slide}
  \slidetitle{FEM-BEM Scattering Example}

\begin{figure}[t]
\centering
\resizebox{0.9\textwidth}{!}{
\includegraphics{../../tests/fembem/source/snapshot}
}
\caption{Real component of EM wave scattering solution on the surface of the box corresponding to $\Omega$ (see text), and projected onto a surrounding plane by the exterior representation formulae together with multi-core grid partitioning examples.}
\label{emscat}
\end{figure}

 \end{slide}


 \begin{slide}
  \slidetitle{FEM-BEM+FEM Charged Droplet Deformation}

%
\begin{subequations}
%
\begin{align}
FEM:\hspace{20pt}&
-
\mu\,\Delta\,{\bf{u}}
+
{\bf{u}}
&=&
\hspace{20pt}
L\left({\bf{u}}^-\right)
&\ \ \ &in\ \Omega  \label{momint}
\\
FEM:\hspace{20pt}&
-
k\,\Delta\,v
+
v
&=&
\hspace{20pt}
v^-
&\ \ \ &on\ \Gamma  \label{mcf}
\\
Rob:\hspace{20pt}&
\frac{\partial{\bf{u}}}{\partial\,n}
-
{\bf{a}}\,v
+
{\bf{b}}\,w^2
&=&
\hspace{20pt}
0
&\ \ \ &on\ \Gamma  \label{forcing}
\\
BEM:\hspace{20pt}&
\Delta\,w
&=&
\hspace{20pt}
0
&\ \ \ &\in\ \mathbb{R}^3\backslash \Omega  \label{charge}
\end{align}
\label{dropproblem}
\end{subequations}


\begin{equation}
\mu=\frac{\tau\,\nu\,\theta}{\rho}\hspace{20pt} k=C\tau\hspace{20pt} {\bf{a}} = 2\,\gamma\,{\bf{n}}\hspace{20pt} {\bf{b}} = \frac{1}{2\,\epsilon_0}\,{\bf{n}}
\nonumber
\end{equation}


\begin{equation}
L\left({\bf{u}}\right)
=
 {\bf{u}}
-
\left(\frac{\tau\,\nu}{\rho}-\mu\right)\Delta\,{\bf{u}}
+
\left({\bf{u}}\cdot\nabla\right){\bf{u}}
+
\frac{\nabla\,p}{\rho}
\end{equation}

 \end{slide}

 \begin{slide}
  \slidetitle{FEM-BEM+FEM Charged Droplet Deformation}

\begin{figure}[t]
\centering
\resizebox{0.9\textwidth}{!}{
\includegraphics{../../tests/droplet/output/array}
}
\caption{Non-dimensional surface charge concentration on the deformed droplet at various times of extension (top row) and relaxation (bottom row).}
\label{emscat}
\end{figure}

 \end{slide}

 \begin{slide}
  \slidetitle{Relevant Experience}

  \begin{itemize}
   \item Many years of software development/management;
   \item Finite element implementations;
   \item Fluid dynamics applications;
   \item Advanced C++ programming (using DUNE);
   \item High performance parallel (MPI/OMP/multithreading) calculations;
   \item Peer reviewed Journal publishing.
  \end{itemize}


 \end{slide}

\end{document}

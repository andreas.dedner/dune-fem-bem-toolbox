\documentclass[a4paper,10pt]{letter}

\usepackage{graphics}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage[utf8x]{inputenc}
% \signature{Alastair J. Radcliffe}
\address{Dr. Andreas S. Dedner \\ Dr. Alastair J. Radcliffe \\ Warwick University \\  Coventry \\ CV4 7AL \\ United Kingdom}

\makeatletter
\let\@@texttop\relax
\makeatother

\addtolength{\textheight}{6cm}
\addtolength{\topmargin}{-4cm}
\usepackage{color}
%\addtolength{\textwidth}{-1.1cm}
%\addtolength{\oddsidemargin}{-3cm}

\newcommand{\Emph}[1]{\emph{\color{red}#1}}

\begin{document}
\begin{letter}
{}

\opening{Dear Sir/Madam,}

\vspace{10pt}

RE: Authors' responses to reviews of: Asynchronous evaluation within parallel environments of coupled finite and boundary element schemes for the simulation of multiphysics problems.

We would like to thank the referees for their helpful comments and
suggestions. We have tried to incorporate all the suggestions. Major
changes and additions are highlighted in red in the text.

\vspace{10pt}

Please find below a summary of the authors' responses to each of the reviewers comments.

% ------------------------------------------------------

{\bf\large Reviewer A}

\begin{enumerate}
\item The discussion on the solution of the coupled system and the presented
solution strategies are not up to date. The presented solvers are very
expensive and some may fail to guarantee convergence. Some details:
  \begin{enumerate}
  \item It is straightforward and efficient to solve the coupled block system by
  iterative solvers (as CG, BiCGStab, GMRES) and appropriated preconditioners
  are available.
  \item I do not agree on the statements at the top of page 4:
  A parallel, iterative block solver requires matrix vector multiplications of
  the blocks and of the preconditioners only.
  \item  In my opinion, the  presented interpretation of the coupled solvers is
  misleading. The presented block Gauss--Seidel and Jacobi solvers are
  Richardson iterations with the preconditioners
\end{enumerate}

\Emph{We agree with the reviewer that the formulation of the text was misleading
  and have made suitable corrections. In the original version we already
  included both the more traditional iterative methods based on a form of
  Richardson iteration as well as an approach based on a Krylov solver. In
  fact the only difference to the approach suggested by Feischl, F{\"u}hrer, Praetorius, and Stephan
  is that we formulate the problem for a "Shur" complement type reduction of
  the system. Within the framework it would be possible to apply the GMRes
  solver directly to the full block system with minor changes to the 
  {\tt GMResCouplingScheme}. All this has now been clarified in Section 2 of
  the revised manuscript.
  The changes have been highlighted in Section 2.}

\item In the introduction, any discussion and citations are missing on
  concepts, data-structure, and techniques to couple a shared-memory model to
  a distributed memory model. This is important as this coupling is the core
  contribution of the manuscript.

\Emph{We have added some more references and remarks in the introduction,
    these mostly cover ''hierarchical'' parallelization approaches within a
    single library. We have not been able to find literature on coupling two software
    packages which are based on different parallelization paradigms as required in
    this work.}

\item In the opinion of the reviewer, many of the statements in the
  introduction are not adequate and up to date:
  \begin{enumerate}
  \item I do not understand the first paragraph of page 2 at all. The main
  drawbacks of BEM are the limitation to a small class of problems and the
  huge effort for a fast implementation compared to FEM.
  \item page 2, 2nd paragraph: A recent, important development on the analysis of
  the non-symmetric coupling is missing, see [Sayas 2009,2013] and related
  publications. Such a kind of coupling is used in Section 6.2. 
  \item page 2: The use of traditional BEM with dense matrices is not up to date
  anymore. Data-sparse, fast BEM is standard nowadays. BEM++ provides an H
  matrix implementation and the authors state in the middle of page 8 that
  they use it. I do not mind if a student implements a traditional BEM. But
  considering the high level of the involved libraries and the aspect that we
  talk about parallelization, I think it is not appropriate to consider
  traditional BEM. I suggest to refer to fast BEM in the discussion. I do not
  mind if the authors switch a traditional BEM in some of the examples, as the
  scalability of BEM++ is not their business.
  \item I do not agree on the statements of the last paragraph of page 2:
  Efficient parallelization of traditional BEM is around for decades and
  distributed-memory parallelization scale perfectly for small numbers of
  processes (as the 64 cores mentioned for shared memory), as long as the
  whole mesh is available on each node. If you talked about the
  parallelization of fast BEM, I would agree that a shared memory
  parallelization is a lot easier than an scalable MPI parallelization (but
  even these exist).
  \item page 2; page 4: I do not agree on the motivation to consider iterative
  procedures based on the alternating solution of the FEM system and BEM
  system. See discussion of 1).
  \end{enumerate}

\Emph{We added more details on the boundary element method in the
  introduction taking all the comments of the reviewer into account.}

\item The scaling results are not quite satisfying. In big parts this seems to
be due to the native parallelization of BEM++ and maybe due to the hardware
(Details on the hardware are missing).

\Emph{We added the missing details on the hardware used at the top of
  Section 6. As mentioned it is
  mostly caused by the parallelization of the BEM++ package and possibly
  due to the size of the problems studied here.}
 
\end{enumerate}

{\bf Minor issues}:

\begin{enumerate}
\item  page 6, last equation: What are $\alpha_\Omega$, $\alpha_\Gamma$
  (coefficients?, operators?). Please provide the conditions for FEM and BEM
  separately and make the quantities explicit.

\Emph{These refer to some generic coefficients given by the problem
  formulation. As stated before that equation we are only considering
  linear coupling in this version. We have made this more explicit.}

\item page 9, line 4: What does GMResCouplingScheme refer to? Coupled system (7) ?

\Emph{This refers to a choice of Krylov solver for the system (7) which is
  detailed at the end of Section 2}

\item Section 6: Please provide more detailed information on the hardware/CPU
  to allow for an evaluation of the scaling results.

\Emph{The details of the processor used is now given right at the beginning of section 6.}

\item page 13, Fig. 1 and Fig. 2: Your example seems to be really small. A good
  fast BEM implementation should outperform a traditional one for medium size
  problems and single core computations easily.

\Emph{On fewer processors the H-matrix does out-perform the traditional version but with
  Bempp on this mesh the parallel efficiency is worse for the H-matrix version. We added
  the size of the mesh now (about 11500). A significant refinement of this mesh led to
  problems with memory for the traditional version on our machine.}

\item page 13, line -4: What does direct coupling and indirect coupling mean
  here? How do these match the schemes on page 4?

\Emph{Direct coupling refers to the coupling discussed in Section 2 while in the
  droplet problem the surface solution influences the bulk problem in a more indirect manner
  by moving the boundary. This is now sketched in the introduction to Section 6 (highlighted)
  and explained in Section 6.3 in detail.}

\item Some remarks on Sect. 6.1: \\
a) The positioning of Fig. 3 in front of the Section is confusing. \\
\Emph{Fixed} \\
b) A description of the left diagram of Fig. 3 is confusing, (left) and a
reference to Section 6.3 are missing. \\
\Emph{Fixed: section references has also been added to caption} \\
c) page 14, last line: Which iterative coupling are you referring to? \\
\Emph{Fixed} \\
d) Even the efficiency of the FEM FEM coupling drops to 51\%. 
   Are there some significant limitations by the hardware? \\
\Emph{The caption in Fig 3 was unclear and has been corrected. For the fem-fem coupling
  the lowest efficiency was 68 and not 51\%. Also hardware details have now been added to
  the introduction of Section 6.} \\
e) Which problem size is used for the scaling tests (Fig.3 right)?  \\
\Emph{Finite element diameters are now mentioned in captions and text}

\item The information on the example pf Sect. 6.2 is quite incomplete: \\
a) Why do you use (15e) instead of the standard radiation condition for the
   Helmholtz equation? \\
\Emph{We have modified (15e) to standard ratiation conditions which was used in the test} \\
b) Fig. 5: Do the diagrams on the right-hand side belong to (15)? \\
\Emph{The correct references have been added to the caption} \\
c) Which value of the wave number $k$ is used? \\
\Emph{The two different wave-numbers used are now mentioned in the captions and the text} \\
d) Fig 5 (left): The caption refers to a simple Poisson example while the
   text at the end of the 4th paragraph of page 19 refers to a FEM only
  calculation. This is inconsistent. \\
\Emph{The pure FEM test referred to a simple Poisson example. This is now clarified in the text} \\
e) page 17, after Fig. 5: you tell that $P_1$ basis functions are used in the
   BEM part while $P_1, P_2$, and $P_3$ basis functions are used for the FEM part.
   For the coupling matrix $C$ in (19) you do not address the possible
   disagreement of the polynomial degree. How do handle this issue? \\
\Emph{Bem++ only allows you to apply the operators to $P_1$ functions, so the
  higher order functions in the bulk are projected onto $P_1$ Lagrange functions
  on the boundary grid. This has been clarified in the text} \\
f) Which solver of Sect. 2 is used? \\
\Emph{Explained now in the text} \\
g) How many iterations are needed? \\
\Emph{Iterations required to get demonstration solution are now mentioned in text} \\
h) Which problem size is used for the scaling tests? \\
\Emph{Indications of all problem sizes now included in the captions and the text }


\item The installation did not work out of the box. In particular, the BEM++
  installation failed to install several external packages. The installation
  was successful after installing these packages in advance.
  I suggest to provide a list of packages which are recommended to install in
  advance in the installation notes. 

\Emph{So far we were only aware of Cython (which was mentioned) and in one instance TBB
     (which is now also mentioned in Appendix B). Other than those we have not
     encountered additional dependencies of BEM++ which are not taken care of by the 
     BEM++ installation script. BEM++ downloads Eigen if not found but does not test
     the version number of a previously installed Eigen package. The required version
     is now mentioned in the appendix.
     }


\end{enumerate}

{\bf Typos:} have been corrected.

%\newpage
% ------------------------------------------------------

{\bf\large Reviewer B}

\Emph{We now provide more details of the version for the software, e.g., boost version
1.57 with which we know the package to work correctly.
The module is licenced under GPL v2 (as is dune-fem). A COPYRIGHT file has been added and it has been mentioned at the start of Appendix B}

\begin{enumerate}
\item Module needs a specific range of Eigen, which seems not explcitily checked:

\Emph{We have added more information on compatible versions for required packages}

\item buildsystem wants to link tbb\_debug and tbbmalloc\_debug although they are not
 present on my machine.

\Emph{Has been fixed in the repository}

\item p. 14: 'The forcings f and g must, of course'

\Emph{Fixed} 

\item p. 15: invalid citation: [something]

\Emph{Fixed} 

\item figure 1 (right): efficiency and total runtime numbers overlay, not readable

\Emph{Fixed} 

\item dune/fem\_bem\_toolbox/bem\_objects/bemoperator.hh unconditionally includes eigen,
  seemingly the build-system does not the correct include paths for Eigen to compile flags

\Emph{There is a problem with the versioning and include paths when bempp uses a pre
  installed Eigen. This has been mentioned in the installation notes.}

- Errors with enabled SuperLU support:
  dune-istl/dune/istl/supermatrix.hh:273:67: error: ‘create’ is not a member of
  ‘Dune::SuperMatrixCreateSparseChooser’
  static\_cast(GetSuperLUType::type), SLU\_GE);

\Emph{We were not able to reproduce this issue. There must be some problem with the 2.3
release of dune-istl causing this in combindation with the superlu version
    installed on the system.}

\item FindGrape in provided dune-grid calls unavailable macro/function
  check\_include\_files
 CMake version issue?

\Emph{This seems to be a problem with the old dune release 2.3 and newer
    CMake versions. A patch has been added to the build script.}

\item Install notes should mention cython 0.21 is minimally needed

\Emph{Fixed}

\item This section could contain an short overview of how and where the mega.build
 installs and builds the dependencies

\Emph{Fixed}

\item p. 26: 'pip install -user cython' -> 'pip install --user cython'

\Emph{Fixed}

\item References
  
\Emph{Fixed}

\end{enumerate}

{\bf Typos:} have been corrected.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%  SIAM Journal on Numerical Analysis, 2009, Vol. 47, No. 5 : pp. 3451-3463
% The Validity of Johnson–Nédélec's BEM–FEM Coupling on Polygonal Interfaces
% Francisco-Javier Sayas
% (doi: 10.1137/08072334X) 
% 
%  SIAM Review, 2013, Vol. 55, No. 1 : pp. 131-146
% The Validity of Johnson--Nédélec's BEM--FEM Coupling on Polygonal Interfaces
% Francisco-Javier Sayas
% (doi: 10.1137/120892283)


% ------------------------------------------------------


\closing{Yours sincerely, \\
 \fromname{A. S. Dedner and A. J. Radcliffe}
}
\vspace{-40pt}
% \encl{pdf}
\end{letter}
\end{document}

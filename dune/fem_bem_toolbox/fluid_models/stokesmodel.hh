#ifndef STOKES_MODEL_HH
#define STOKES_MODEL_HH

#include <cassert>
#include <cmath>

#include <dune/fem/io/parameter.hh>
#include <dune/fem/function/common/gridfunctionadapter.hh>

#include <dune/fem_bem_toolbox/fem_objects/probleminterface.hh>
#include <dune/fem_bem_toolbox/fem_objects/model.hh>

template< class FunctionSpace, class GridPart >
struct StokesModel : public DiffusionModel<FunctionSpace,GridPart>
{
  typedef DiffusionModel<FunctionSpace,GridPart> BaseType;
  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef ProblemInterface< FunctionSpaceType > ProblemType ;
  static const int dimDomain = RangeType::dimension-1;

  //! constructor
  StokesModel( const ProblemType& problem,
             const GridPart &gridPart )
    : BaseType(problem,gridPart),
      stab_( Dune::Fem::Parameter::getValue< double >( "stokes.stability", 1e-4 ) )
  {
  }

  template< class Entity, class Point >
  void source ( const Entity &entity,
                const Point &x,
                const RangeType &value,
                const JacobianRangeType &gradient,
                RangeType &flux ) const
  {
    linSource( value, entity, x, value, gradient, flux );
  }

  // the linearization of the source function
  template< class Entity, class Point >
  void linSource ( const RangeType& uBar,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    flux = 0.0;

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      // try conventional incompressibility condition = - nabla dot u times q in dimdomain(=pressure)  row of aphi
      flux[ dimDomain ] -= gradient[ localRow ][ localRow ];

      // integration by parts of conventional incompressibility constraint (as above) to allow inclusion within adphi
      // adphi[ dimDomain ][ localRow ] -= phi[ localCol ][ localRow ];
    }
  }

  // the linearization of the advective term
  template< class Entity, class Point >
  void advectionSource ( const Dune::FieldVector<double,dimDomain> &velo,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    const DomainType xGlobal = entity.geometry().global( coordinate( x ) );
    RangeType n;
    problem_.n(xGlobal,n);

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      for( unsigned int localCol = 0; localCol < dimDomain; ++localCol )
        flux[ localRow ] -= n[ localRow ] * velo[ localCol ] * gradient[ localRow ][ localCol ];
    }
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void diffusiveFlux ( const Entity &entity,
                       const Point &x,
                       const RangeType &value,
                       const JacobianRangeType &gradient,
                       JacobianRangeType &flux ) const
  {
    linDiffusiveFlux( value, gradient, entity, x, value, gradient, flux );
  }

  // linearization of diffusiveFlux
  template< class Entity, class Point >
  void linDiffusiveFlux ( const RangeType& uBar,
                          const JacobianRangeType& gradientBar,
                          const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          const JacobianRangeType &gradient,
                          JacobianRangeType &flux ) const
  {
    flux = 0.0;

    static const int dimDomain = RangeType::dimension-1;

    const auto &geometry = entity.geometry();
    // const double scaling = std::pow( geometry.volume() , 1.0 / 3.0 );

    const int numcorn = geometry.corners();
    double longest = 0.0;
    for( int i = 0; i < numcorn; ++i )
    {
        double leng = ( geometry.corner((i+1)%numcorn) - geometry.corner((i+2)%numcorn) ).two_norm();
        if( leng > longest ) longest = leng;
    };
    for( int i = 0; i < 2; ++i )
    {
        double leng = ( geometry.corner((i+1)%numcorn) - geometry.corner((i+3)%numcorn) ).two_norm();
        if( leng > longest ) longest = leng;
    };

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      // velocity equations -- localRow = 0, 1, 2 = dimDomain - 1
      flux[ localRow ][ localRow ] -= value[ dimDomain ];

      // pressure equation -- localRow = dimdomain

      // stabilization term = - k * nabla p dot nabla q  where p and q are pressure shape functions
      flux[ dimDomain ][ localRow ] -= stab_ * longest * gradient[ dimDomain ][ localRow ];

      for( unsigned int localCol = 0; localCol < dimDomain; ++localCol )
      {
        // extra transpose of gradient term
        flux[ localRow ][ localCol ] += vdot( uBar )[ localCol ] * gradient[ localCol ][ localRow ];
      }
    }
  }

  //! exact some methods from the problem class
  bool hasDirichletBoundary () const
  {
    return BaseType::hasDirichletBoundary() ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  bool isDirichletPoint( const DomainType& x ) const
  {
    return BaseType::isDirichletPoint(x) ;
  }

  template< class Entity, class Point >
  void g( const RangeType& uBar,
          const Entity &entity,
          const Point &x,
          RangeType &u ) const
  {
    BaseType::g(uBar,entity,x,u);
  }

  // return Fem::Function for Dirichlet boundary values
  typename BaseType::DirichletBoundaryType dirichletBoundary( ) const
  {
    return BaseType::dirichletBoundary();
  }

protected:
  using BaseType::problem_;
  bool implicit_;
  double stab_;

};
#endif // #ifndef STOKES_MODEL_HH

#ifndef EIGENVECTOR_HH
#define EIGENVECTOR_HH
#if HAVE_BEMPP
#include <dune/fem/storage/vector.hh>
#include <dune/fem/solver/pardg.hh>

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <mpi.h>
#include <dune/fem_bem_toolbox/bem_objects/bempp.hh>

// using namespace boost::interprocess;

template< class SharedObjectType, class Field >
struct SharedMemory
{
  static void clearSharedMemory( const char *name)
  {
    boost::interprocess::shared_memory_object::remove(name);
  }

  SharedMemory( const char *name, unsigned int size)
  {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    {
      Field field;
      unsigned int bytesPerSizeUnit = sizeof(field);

      std::cout << "Setting-up shared memory of size " << size << " (" << size*bytesPerSizeUnit << " bytes)" << " with name " << name << " ..." << std::endl;

      // creating our first shared memory object.
      std::cout << "before  boost::interprocess::shared_memory_object" << std::endl;
      boost::interprocess::shared_memory_object sharedmem1(boost::interprocess::open_or_create, name, boost::interprocess::read_write);
      // boost::interprocess::shared_memory_object sharedmem1(boost::interprocess::create_only, name, boost::interprocess::read_write);
      std::cout << "after  boost::interprocess::shared_memory_object" << std::endl;

      // setting the size of the shared memory
      if (rank == 0)
        sharedmem1.truncate (size*bytesPerSizeUnit);
      std::cout << "after truncate" << std::endl;

      // map the shared memory to current process
      region_ = boost::interprocess::mapped_region(sharedmem1, boost::interprocess::read_write);

      // access the mapped region using get_address
      std::cout << "AAA " << rank << "  " << sharedmem1.get_name() << '\n';

      boost::interprocess::offset_t sized;
      if (sharedmem1.get_size(sized))
        std::cout << "BBB " << rank << "  " << sized << '\n';
    }
    sharedObject_ = new SharedObjectType ( (Field*)(region_.get_address()),size );
    assert( sharedObject_ );
  }
  SharedObjectType *get()
  {
    return static_cast<SharedObjectType* >(region_.get_address());
  }
  SharedObjectType *getso()
  {
    return sharedObject_;
  }
  private:
  SharedObjectType* sharedObject_;
  boost::interprocess::mapped_region region_;
};

namespace Dune
{
  namespace Fem
  {
    template< class Field >
    class EigenVector
    : public VectorDefault< Field, EigenVector< Field > >
    {
      typedef EigenVector< Field > ThisType;
      typedef VectorDefault< Field, ThisType > BaseType;

    public:
      //! field type of the vector
      typedef Field FieldType;
      typedef Field field_type;

      using BaseType :: assign;

    protected:
      // typedef Bempp::Vector< FieldType > DofStorageType;
      typedef Bempp::Vector< FieldType > BaseDofStorageType;
      typedef Eigen::Map< BaseDofStorageType > DofStorageType;
      DofStorageType* fields_;

    public:

      static int getMemId()
      {
        static int memoryId = 0;
        // std::cout << "Memory id = " << memoryId << std::endl;
        return memoryId++;
      }

      void randomString(int size, char* output, int fiddle) // pass the destination size and the destination itself
      {
        std::srand(std::time(0)); // seed with time

          char src[size];
          // size = std::rand() % size; // this randomises the size (optional)

          src[size] = '\0'; // start with the end of the string...

          int extra = getMemId() + fiddle;

          // ...and work your way backwards
          while(--size > -1)
            src[size] = ((rand()+ extra) % 94) + 32; // generate a string ranging from the space character to ~ (tilde)

          strcpy(output, src); // store the random string
      }

      DofStorageType* createFields(const bool shared, unsigned int siz)
      {
        if(!shared)
        {
          notshared_ = new FieldType[siz];
          return new DofStorageType(notshared_,siz);
        }

        int fiddle = 0;

        while( fiddle++ < 10 )
	      {
          try
          {
            int rank,size;
            MPI_Comm_rank(MPI_COMM_WORLD,&rank);
            MPI_Comm_size(MPI_COMM_WORLD,&size);

            char memName[] = "1234567890";
            const int root=0;

            if(rank == root)
            {
              // create a unique shared memory identification name
              // std::cout << "memName is " << memName << " changed to ";
              randomString(10,memName,fiddle);
              // std::cout << memName << std::endl;

              std::cout << "starting: " << rank << "/" << size << "  name is " << memName << std::endl;
              // clear any previous instances of shared objects with the same name
              SharedMemory<DofStorageType,FieldType>::clearSharedMemory(memName);

              // create (or just refer to) a new shared memory of the given name
              memoryPtr_ = new SharedMemory<DofStorageType,FieldType>(memName,siz);
            }

            /* everyone calls bcast, data is taken from root and ends up in everyone's buf */
            MPI_Bcast(&memName, 10, MPI_CHAR, root, MPI_COMM_WORLD);

            if(rank != root)
            {
              // create (or just refer to) a new shared memory of the given name
              memoryPtr_ = new SharedMemory<DofStorageType,FieldType>(memName,siz);
            }

            std::cout << "returned: " << rank << "/" << size << std::endl;

            break;

          } catch (boost::interprocess::interprocess_exception& e) {
            std::cout << e.what() << std::endl;
            std::cout << "problem with shared memory, trying again ..." << std::endl;
          }
        }
        if( fiddle > 9 )
	{
          std::cout << "terminal problem with shared memory allocation, aborting." << std::endl;
          abort();
        }
          
        assert( memoryPtr_ );
        return memoryPtr_->getso();
      }

      //! Constructor setting up a shared vector of a specified size
      inline explicit EigenVector ( unsigned int size = 0, const bool shared = false )
        : fields_( createFields(shared,size) )
      {
        if(shared)
          std::cout << "Requested a shared dof vector of size " << size << " units\n" << std:: endl;
        else
          std::cout << "Requested a private dof vector of size " << size << " units\n" << std:: endl;
      }

      //! Constructor setting up a vector iniitialized with a constant value
      inline EigenVector ( unsigned int size,
                               const FieldType s )
      : fields_( new DofStorageType(size) )
      {
        assign( s );
      }

      //! Copy constructor setting up a vector with the data of another one
      template< class T >
      inline EigenVector ( const VectorInterface< T > &v )
      : fields_( new DofStorageType() )
      {
        assign( v );
      }

      //! Copy constructor setting up a vector with the data of another one (of the same type)
      inline EigenVector ( const ThisType &v )
      : fields_( new DofStorageType() )
      {
        assign( v );
      }

      //! Assign another vector to this one
      template< class T >
      inline ThisType &operator= ( const VectorInterface< T > &v )
      {
        assign( v );
        return *this;
      }

      //! Assign another vector (of the same type) to this one
      inline ThisType &operator= ( const ThisType &v )
      {
        assign( v );
        return *this;
      }

      //! Initialize all fields of this vector with a scalar
      inline ThisType &operator= ( const FieldType s )
      {
        assign( s );
        return *this;
      }

      inline const FieldType &operator[] ( unsigned int index ) const
      {
        return (*fields_)( index );
      }

      inline FieldType &operator[] ( unsigned int index )
      {
        return (*fields_)( index );
      }

      /*
      template< class T >
      inline void assign ( const VectorInterface< T > &v )
      {
        fields_.assign( v );
      }
      */

      inline const DofStorageType &coefficients () const
      {
        return *fields_;
      }
      inline DofStorageType &coefficients ()
      {
        return *fields_;
      }

      inline const FieldType *leakPointer () const
      {
        return fields_->memptr();
      }

      inline FieldType *leakPointer ()
      {
        return fields_->memptr();
      }

      inline void reserve ( unsigned int newSize )
      {
        fields_->resize( newSize );
      }

      inline void resize ( unsigned int newSize )
      {
        fields_->resize( newSize );
      }

      inline void resize ( unsigned int newSize,
                           const FieldType defaultValue )
      {
        fields_->resize( newSize ); assign( defaultValue );
      }

      inline unsigned int size () const
      {
        return fields_->size();
      }
    private:
      SharedMemory<DofStorageType,FieldType>* memoryPtr_;
      FieldType* notshared_;
    };

    template< class DomainFunctionSpace, class RangeFunctionSpace >
    class ParDGOperator< ManagedDiscreteFunction< VectorDiscreteFunction< DomainFunctionSpace, 
                           EigenVector<typename DomainFunctionSpace::FunctionSpaceType::RangeFieldType> > >, 
                         ManagedDiscreteFunction< VectorDiscreteFunction< RangeFunctionSpace, 
                           EigenVector<typename RangeFunctionSpace::FunctionSpaceType::RangeFieldType> > >
                       >
    : public PARDG::Function 
    {
      typedef ManagedDiscreteFunction< VectorDiscreteFunction< DomainFunctionSpace, 
                EigenVector<typename DomainFunctionSpace::FunctionSpaceType::RangeFieldType> > >
              DomainFunctionType;
      typedef ManagedDiscreteFunction< VectorDiscreteFunction< RangeFunctionSpace, 
                EigenVector<typename RangeFunctionSpace::FunctionSpaceType::RangeFieldType> > >
              RangeFunctionType;
      typedef ParDGOperator< DomainFunctionType, RangeFunctionType > ThisType;
    public:
      typedef Operator< DomainFunctionType, RangeFunctionType > OperatorType;

      typedef typename DomainFunctionType::DiscreteFunctionSpaceType DomainFunctionSpaceType;
      typedef typename RangeFunctionType::DiscreteFunctionSpaceType RangeFunctionSpaceType;

      ParDGOperator ( const OperatorType &op, const DomainFunctionSpaceType &domainSpace, const RangeFunctionSpaceType &rangeSpace )
      : operator_( op ),
        domainSpace_( domainSpace ),
        rangeSpace_( rangeSpace )
      {}

      void operator() ( const double *u, double *w, int i = 0 )
      {
        DomainFunctionType uFunction( "ParDGOperator u", domainSpace_, u );
        RangeFunctionType wFunction( "ParDGOperator w", rangeSpace_, w );
        operator_( uFunction, wFunction );
      }
      
      int dim_of_argument( int i = 0 ) const
      { 
        assert( i == 0 );
        return domainSpace_.size();
      }

      int dim_of_value ( int i = 0 ) const
      { 
        assert( i == 0 );
        return rangeSpace_.size();
      }

    private:
      const OperatorType &operator_;
      const DomainFunctionSpaceType &domainSpace_;
      const RangeFunctionSpaceType &rangeSpace_;
    };

  }
}
#endif
#endif // EIGENVECTOR_HH

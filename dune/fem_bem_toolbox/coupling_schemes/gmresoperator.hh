#ifndef GMRESOPERATOR_HH
#define GMRESOPERATOR_HH

#include <dune/common/fmatrix.hh>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/stencil.hh>

#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>

#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>


template< class SchemeOne, class SchemeTwo, class DomainFunction >
struct GMResOperator
: public virtual Dune::Fem::Operator< DomainFunction,DomainFunction >
{
  typedef DomainFunction   DiscreteFunctionType;

  GMResOperator ( SchemeOne &schemeOne, SchemeTwo &schemeTwo )
  : schemeTwo_(schemeTwo)
  , schemeOne_(schemeOne)
  {
  }
  ~GMResOperator()
  {
  }

  virtual void
  operator() ( const DomainFunction &u, DomainFunction &w ) const
  {
    schemeOne_.clearRhs();
    schemeTwo_.clearRhs();
    schemeTwo_.couple(u);
    schemeTwo_.solve(false);
    schemeOne_.couple(schemeTwo_.solution());
    schemeOne_.solve(false);
    w.assign(u);
    w.axpy(-1., schemeOne_.solution());
  }
private:
  SchemeOne &schemeOne_;
  SchemeTwo &schemeTwo_;
};

#endif

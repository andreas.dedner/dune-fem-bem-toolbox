#ifndef GMRESSCHEME_HH
#define GMRESSCHEME_HH

// iostream includes
#include <iostream>

// include discrete function space
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/finitevolume.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/common/adaptmanager.hh>

// include discrete function
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>

#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
/*********************************************************/

#include <dune/fem/special_solvers/istlinverseoperators.hh>

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

// #include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
// #include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>
#include "gmresoperator.hh"

template < class SchemeOne, class SchemeTwo >
class GMResCouplingScheme 
{
public:
  typedef typename SchemeOne::DiscreteFunctionType DiscreteFunctionTypeOne;
  typedef typename SchemeTwo::DomainFunctionType DomainFunctionTypeTwo;
  typedef typename SchemeTwo::DiscreteFunctionType DiscreteFunctionTypeTwo;
  //! define Laplace operator
  //! define Laplace operator
  typedef GMResOperator< SchemeOne, SchemeTwo, DiscreteFunctionTypeOne> GMResOperatorType;
  typedef Dune::Fem::ISTLInverseOperator< DiscreteFunctionTypeOne, Dune::Fem::ISTLRestartedGMRes > LinearInverseOperatorType;

  GMResCouplingScheme( SchemeOne &schemeOne, SchemeTwo &schemeTwo )
    : schemeOne_(schemeOne)
    , schemeTwo_(schemeTwo)
    , solution_( "solution_rhs", schemeOne.discreteSpace() )
    , rhs_( "gmres_rhs", schemeOne.discreteSpace() ) // !!! call domain space
    , fbop_( schemeOne,schemeTwo )
    , maxIt_( Dune::Fem::Parameter::getValue< int >( "coupling.maxit", 1000 ) )
    , tol_( Dune::Fem::Parameter::getValue< double >( "coupling.solvereps", 1e-7 ) )
    , verbose_( Dune::Fem::Parameter::getValue< double >( "coupling.verbose", false ) )
    , n_(0)
  {
    rhs_.clear();
    solution_.clear();
  }

  void clearRhs() 
  {
    rhs_.clear();
  }

  //! set-up the rhs for the coupling values
  void prepare( )
  {
    // forcing from scheme 2 (D^{-1}g)
    schemeTwo_.prepare();
    schemeTwo_.solve( true );
    // forcing from scheme 1
    schemeOne_.prepare();
    solution_.assign(schemeOne_.solution()); // get  dirichlet data into initial guess
    // add coupling term
    schemeOne_.couple( schemeTwo_.solution() );
    schemeOne_.solve( true );
    // this gives the rhs for our system
    rhs_.assign( schemeOne_.solution() );
  }

  //! solve the system
  void solve ( bool assemble = false )
  {
    // on-the-fly version (does not work with ISTL solvers)
    LinearInverseOperatorType solver( fbop_, tol_, tol_, maxIt_, verbose_ );
    // solve system
    solver( rhs_, solution_ );
    // copy into femscheme to make original output still work
    schemeOne_.solution().assign( solution_ );

    // reconstruct solution from scheme 2
    schemeTwo_.prepare();
    schemeTwo_.couple( solution_ );
    schemeTwo_.solve(false);

    n_ = solver.iterations();
  }

  int n() 
  {
    return n_;
  }

  std::pair<double,double> error ()
  {
    // calculate standard error
    // select norm for error computation
    typedef typename SchemeOne::ModelType::GridPartType GridPartType;
    typedef typename SchemeTwo::ModelType::GridPartType GrydPartType;
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    typedef Dune::Fem::L2Norm< GrydPartType > NurmType;
    NormType norm( schemeOne_.discreteSpace().gridPart() );
    NurmType nurm( schemeTwo_.discreteSpace().gridPart() );
    auto gridExactSolution = schemeOne_.model().exactSolution();
    auto grydExactSolution = schemeTwo_.model().exactSolution();

    double errorOne = norm.distance( gridExactSolution, schemeOne_.solution() );
    double errorTwo = nurm.distance( grydExactSolution, schemeTwo_.solution() );

    return std::make_pair(errorOne,errorTwo);
  }

protected:
  SchemeOne &schemeOne_;
  SchemeTwo &schemeTwo_;
  DiscreteFunctionTypeOne solution_;
  DiscreteFunctionTypeOne rhs_;
  GMResOperatorType fbop_;
  const int maxIt_;
  const double tol_;
  const bool verbose_;
  int n_;
};

#endif

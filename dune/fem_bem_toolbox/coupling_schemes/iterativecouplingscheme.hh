#ifndef ITERATIVECOUPLINGSCHEME_HH
#define ITERATIVECOUPLINGSCHEME_HH

// iostream includes
#include <iostream>

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

template < class SchemeOne, class SchemeTwo >
class IterativeCouplingSchemeBase
{
public:
  IterativeCouplingSchemeBase( SchemeOne &schemeOne, SchemeTwo &schemeTwo )
    : schemeOne_(schemeOne)
    , schemeTwo_(schemeTwo)
    , oldSolutionOne_("tmp old solution one",schemeOne.discreteSpace() )
    , oldSolutionTwo_("tmp old solution one",schemeTwo.discreteSpace() )
    , n_(0)
    , maxIt_( Dune::Fem::Parameter::getValue< int >( "coupling.maxit", 1000 ) )
    , tol2_( std::pow( Dune::Fem::Parameter::getValue< double >( "coupling.tol", 1e-7 ),2.0) )
    , verbose_( Dune::Fem::Parameter::getValue< double >( "coupling.verbose", false ) )
  {
  }

  //! set-up the rhs for the coupling values
  void prepare( )
  {}

  void clearRhs() 
  {
    schemeOne_.clearRhs();
    schemeTwo_.clearRhs();
  }

  double updateMagnitude()
  {
    double diff = std::numeric_limits<double>::max();
    if (n_>0)
    {
      oldSolutionOne_ -= schemeOne_.solution();
      oldSolutionTwo_ -= schemeTwo_.solution();
      auto diffOne = oldSolutionOne_.scalarProductDofs( oldSolutionOne_ );
      auto diffTwo = oldSolutionTwo_.scalarProductDofs( oldSolutionTwo_ );
      diff = std::real( diffOne + diffTwo );
    }
    oldSolutionOne_.assign( schemeOne_.solution() );
    oldSolutionTwo_.assign( schemeTwo_.solution() );
    return diff;
  }

  int n()
  {
    return n_;
  }

  std::pair<double,double> error ()
  {
    // calculate standard error
    // select norm for error computation
    typedef typename SchemeOne::ModelType::GridPartType GridPartType;
    typedef typename SchemeTwo::ModelType::GridPartType GrydPartType;
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    typedef Dune::Fem::L2Norm< GrydPartType > NurmType;
    NormType norm( schemeOne_.discreteSpace().gridPart() );
    NurmType nurm( schemeTwo_.discreteSpace().gridPart() );
    auto gridExactSolution = schemeOne_.model().exactSolution();
    auto grydExactSolution = schemeTwo_.model().exactSolution();

    double errorOne = norm.distance( gridExactSolution, schemeOne_.solution() );
    double errorTwo = nurm.distance( grydExactSolution, schemeTwo_.solution() );

    return std::make_pair(errorOne,errorTwo);
  }

protected:
  SchemeOne &schemeOne_;
  SchemeTwo &schemeTwo_;
  typename SchemeOne::DiscreteFunctionType oldSolutionOne_;
  typename SchemeTwo::DiscreteFunctionType oldSolutionTwo_;
  int n_;
  int maxIt_;
  double tol2_;
  bool verbose_;
};

#endif

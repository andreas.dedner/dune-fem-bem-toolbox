#ifndef JACOBISCHEME_HH
#define JACOBISCHEME_HH

// iostream includes
#include <iostream>

#include "iterativecouplingscheme.hh"

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

template < class SchemeOne, class SchemeTwo >
struct JacobiCouplingScheme : public IterativeCouplingSchemeBase< SchemeOne, SchemeTwo >
{
  typedef IterativeCouplingSchemeBase< SchemeOne, SchemeTwo > BaseType;
  typedef typename SchemeOne::DiscreteFunctionType DiscreteFunctionTypeOne;
  typedef typename SchemeTwo::DiscreteFunctionType DiscreteFunctionTypeTwo;

  JacobiCouplingScheme( SchemeOne &schemeOne, SchemeTwo &schemeTwo )
    : BaseType(schemeOne,schemeTwo)
  {}

  //! solve the system
  void solve ( bool assemble = false )
  {
    // iterate a certain number of times
    for( n_ = 0; n_ <= maxIt_; ++n_ )
    {
      // setup the right hand side
      schemeOne_.prepare();
      schemeOne_.couple(schemeTwo_.solution());
      schemeTwo_.prepare();
      schemeTwo_.couple(schemeOne_.solution());

      // solve once
      schemeOne_.solve( n_ == 0 || assemble );
      schemeTwo_.solve( n_ == 0 || assemble );

      // check for convergence
      double mag = BaseType::updateMagnitude();
      if (Dune::Fem::Parameter::verbose() && n_>0 && BaseType::verbose_)
      {
        std::cout << "Jacobi coupling: at iteration " << n_ << " size of update " 
          << std::sqrt(mag) << std::endl;
      }
      if ( mag<tol2_ )
        break;
    }
  }

protected:
  using BaseType::schemeOne_;
  using BaseType::schemeTwo_;
  using BaseType::n_;
  using BaseType::maxIt_;
  using BaseType::tol2_;
};

#endif

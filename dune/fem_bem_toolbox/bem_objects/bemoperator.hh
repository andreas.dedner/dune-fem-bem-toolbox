#ifndef BEMOPERATOR_HH
#define BEMOPERATOR_HH

#include <boost/lexical_cast.hpp>

#include <dune/common/fmatrix.hh>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/stencil.hh>

#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>

#include <dune/fem_bem_toolbox/bem_objects/bempp.hh>
#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>
#include <dune/fem/special_solvers/istlinverseoperators.hh>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/IterativeLinearSolvers>

#include "bempp.hh"

template <class ModelType, class RT>
struct ConstructBemOperators;
template <class ModelType>
struct ConstructBemOperators<ModelType,double>
{
  typedef double RT;
  typedef double BFT;
  typedef Bempp::Matrix<RT> EigenMatrix;
  // typedef decltype(std::declval<EigenMatrix>().llt()) Factorization;
  typedef Eigen::PartialPivLU<EigenMatrix> Factorization;
  template <class Op>
  using Solver = Eigen::ConjugateGradient < Op, Eigen::Lower|Eigen::Upper, Eigen::IdentityPreconditioner >;
  static void factorize( const boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &slpOpAss_,
               std::shared_ptr<Factorization>& decomp)
  {
    decomp = std::make_shared<Factorization>( slpOpAss_->asMatrix() ); // .llt() );
  }

  static void get( const ModelType &model,
                   Bempp::PiecewiseLinearContinuousScalarSpace<BFT> &pwiseLinears,
                   Bempp::PiecewiseConstantScalarSpace<BFT> &pwiseConstants,
                   boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &slpOpAss_, 
                   boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &dlpOpAss_, 
                   boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &idOpAss_,
                   Bempp::Context<BFT,RT>& context,
                   const bool doDoubleLayer )
  {
    auto slpOp = Bempp::laplaceSingleLayerBoundaryOperator<BFT, RT,RT>(
                Bempp::GlobalParameters::parameterList(),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants));
    slpOpAss_ = slpOp->assembleWeakForm( context );
    if(doDoubleLayer)
    {
      auto dlpOp = Bempp::laplaceDoubleLayerBoundaryOperator<BFT, RT,RT>(
                Bempp::GlobalParameters::parameterList(),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseConstants));
      dlpOpAss_ = dlpOp->assembleWeakForm( context );
    }
    else
      dlpOpAss_ = 0;
    auto idOp = Bempp::identityOperator<BFT, RT>(
                Bempp::GlobalParameters::parameterList(),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseConstants));
    idOpAss_ = idOp->assembleWeakForm( context );
  }
};
template <class ModelType>
struct ConstructBemOperators<ModelType, std::complex<double> >
{
  typedef std::complex<double> RT;
  typedef double BFT;
  typedef Bempp::Matrix<RT> EigenMatrix;
  // typedef decltype(std::declval<EigenMatrix>().lu()) Factorization;
  typedef Eigen::PartialPivLU<EigenMatrix> Factorization;
  template <class Op>
  using Solver = Eigen::BiCGSTAB < Op, Eigen::IdentityPreconditioner >;
  static void factorize( const boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &slpOpAss_,
               std::shared_ptr<Factorization>& decomp)
  {
    // decomp = std::make_shared<Factorization>( slpOpAss_->asMatrix().lu() );
    decomp = std::make_shared<Factorization>( slpOpAss_->asMatrix() ); 
  }
  static void get( const ModelType &model,
                   Bempp::PiecewiseLinearContinuousScalarSpace<BFT> &pwiseLinears,
                   Bempp::PiecewiseConstantScalarSpace<BFT> &pwiseConstants,
                   boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &slpOpAss_, 
                   boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &dlpOpAss_, 
                   boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > &idOpAss_,
                   Bempp::Context<BFT,RT>& context,
                   const bool doDoubleLayer )
  {
    auto slpOp = Bempp::helmholtzSingleLayerBoundaryOperator<BFT>(
                Bempp::GlobalParameters::parameterList(),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                std::complex<double>( model.omega() ,0. ) );
    slpOpAss_ = slpOp->assembleWeakForm( context );
    if(doDoubleLayer)
    {
      auto dlpOp = Bempp::helmholtzDoubleLayerBoundaryOperator<BFT>(
                Bempp::GlobalParameters::parameterList(),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseConstants),
                std::complex<double>( model.omega(), 0.) );
      dlpOpAss_ = dlpOp->assembleWeakForm( context );
    }
    else
      dlpOpAss_ = 0;
    auto idOp = Bempp::identityOperator<BFT, RT>(
                Bempp::GlobalParameters::parameterList(),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseConstants));
    idOpAss_ = idOp->assembleWeakForm( context );
  }
};
template< class DomainFunction, class RangeFunction, class Model >
struct BemOperator : public Dune::Fem::Operator<RangeFunction,RangeFunction >
{
  typedef BemOperator<DomainFunction,RangeFunction,Model> ThisType;
  typedef DomainFunction   DiscreteFunctionType;
  typedef Model            ModelType;

  typedef typename DiscreteFunctionType::FunctionSpaceType::RangeFieldType RT;
  typedef double BFT;

  public:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;
  typedef typename GridPartType::GridType GridType;

  typedef Dune::GridSelector::GridType GT;

  typedef RangeFunction RangefunctionType;

  const ModelType& model_;
  GridType &dunegrid;
  boost::shared_ptr<Bempp::Grid> grid;
  Bempp::PiecewiseLinearContinuousScalarSpace<BFT> pwiseLinears;
  Bempp::PiecewiseConstantScalarSpace<BFT> pwiseConstants;
  bool useHMatrix;

  Bempp::AccuracyOptions accuracyOptions;
  static Bempp::AccuracyOptions &setAccuracyOptions( Bempp::AccuracyOptions &accuracyOptions )
  {
    accuracyOptions.doubleRegular.setRelativeQuadratureOrder(2);
    accuracyOptions.singleRegular.setRelativeQuadratureOrder(2);
    return accuracyOptions;
  }
  Bempp::NumericalQuadratureStrategy<BFT, RT> quadStrategy;
  static Bempp::AssemblyOptions &setAssemblyOptions( bool useHMatrix, Bempp::AssemblyOptions &assemblyOptions )
  {
    if (! useHMatrix)
      assemblyOptions.switchToDenseMode();
    else
      assemblyOptions.switchToHMatMode();
    return assemblyOptions;
  }
  Bempp::AssemblyOptions assemblyOptions;
  Bempp::EvaluationOptions evaluationOptions;
  Bempp::Context<BFT, RT> context;
  boost::shared_ptr< Bempp::DiscreteBoundaryOperator<RT> > slpOpAss_, dlpOpAss_, idOpAss_;

  typedef ConstructBemOperators<ModelType,RT> Construction;
  mutable std::shared_ptr<typename Construction::Factorization> decompMatrix;

  double solverEps_;
  int numThreads_;
public:
  //! contructor
  BemOperator ( const GridPartType &gridPart, const ModelType &model )
  : model_( model )
  , dunegrid( const_cast<GridType&>(gridPart.grid()) )
  , grid( new Bempp::ConcreteGrid<GridType>(&dunegrid,Bempp::GridParameters::TRIANGULAR,false) )
  , pwiseLinears( grid )
  , pwiseConstants( grid )
  , useHMatrix( Dune::Fem::Parameter::getValue<bool>("bempp.hmatrix",false) )
  , quadStrategy(setAccuracyOptions( accuracyOptions) )
  , assemblyOptions( setAssemblyOptions(useHMatrix, assemblyOptions) )
  , context(make_shared_from_ref(quadStrategy), assemblyOptions)
  , decompMatrix(0)
  , solverEps_( Dune::Fem::Parameter::getValue< double >( "poisson.solvereps", 1e-8 ) )
  , numThreads_(0)
  {
    char *p_num_threads = std::getenv("BEMPP_NUM_THREADS");
    if (p_num_threads != nullptr)
      numThreads_ = boost::lexical_cast<unsigned int>(p_num_threads);
    assemble();
  }
  void assemble()
  {
    Bempp::AccuracyOptions accuracyOptions;
    accuracyOptions.doubleRegular.setRelativeQuadratureOrder(2);
    accuracyOptions.singleRegular.setRelativeQuadratureOrder(2);
    Bempp::NumericalQuadratureStrategy<BFT, RT> quadStrategy(accuracyOptions);
    Bempp::AssemblyOptions assemblyOptions;
    Bempp::Context<BFT, RT> context(make_shared_from_ref(quadStrategy), assemblyOptions);
    Construction::get(model_,
                      pwiseLinears,pwiseConstants,
                      slpOpAss_,dlpOpAss_,idOpAss_,context,
                      model_.doDoubleLayer());

    if ( !useHMatrix )
    {
      Eigen::initParallel();
      Eigen::setNbThreads(numThreads_);
      auto n = Eigen::nbThreads( );
      Construction::factorize( slpOpAss_, decompMatrix );
    }
    Eigen::setNbThreads(1);
  }
  ~BemOperator()
  {
  }

  // apply mass matrix to dirichlet data ( i.e. P^1 -> dual(P^0) )
  void project( const DomainFunction &u, RangeFunction &w ) const 
  {
    auto& dirichletDataDofs = u.dofVector().coefficients();
    auto& rhsDataDofs = w.dofVector().coefficients();
    idOpAss_->apply(Bempp::NO_TRANSPOSE, dirichletDataDofs, rhsDataDofs, 1., 1. );
  }
  void prepareRhs( const DomainFunction &u, RangeFunction &w ) const 
  {
    auto& dirichletDataDofs = u.dofVector().coefficients();
    auto& rhsDataDofs = w.dofVector().coefficients();
    if(dlpOpAss_)
      dlpOpAss_->apply(Bempp::NO_TRANSPOSE, dirichletDataDofs, rhsDataDofs, 1. , 1.);
    idOpAss_->apply(Bempp::NO_TRANSPOSE, dirichletDataDofs, rhsDataDofs, model().idOpCoeff(), 1. );
  }

  template <class Points, class Values>
  void fill( const DomainFunction &dirichlet, const RangeFunction &neuman,
             const Points &points, Values &values,double)
  {
    auto& dirichletDataDofs = dirichlet.dofVector().coefficients();
    auto& neumannDataDofs = neuman.dofVector().coefficients();
    Bempp::laplaceSingleLayerPotentialOperator<double,double>( make_shared_from_ref(pwiseConstants),
        points, 
        Bempp::GlobalParameters::parameterList()
        )->apply( Bempp::NO_TRANSPOSE, neumannDataDofs, values, -1. , 1.);
    Bempp::laplaceDoubleLayerPotentialOperator<double,double>( make_shared_from_ref(pwiseLinears),
        points, 
        Bempp::GlobalParameters::parameterList()
        )->apply(Bempp::NO_TRANSPOSE, dirichletDataDofs, values, 1. , 1.);
  }
  template <class Points, class Values>
  void fill( const DomainFunction &dirichlet, const RangeFunction &neuman,
             const Points &points, Values &values,std::complex<double>) const
  {
    auto& dirichletDataDofs = dirichlet.dofVector().coefficients();
    auto& neumannDataDofs = neuman.dofVector().coefficients();
    auto omega = std::complex<double>( model().omega() ,0. );
    Bempp::helmholtzSingleLayerPotentialOperator<double>( make_shared_from_ref(pwiseConstants),
        points, omega, 
        Bempp::GlobalParameters::parameterList()
        )->apply( Bempp::NO_TRANSPOSE, neumannDataDofs, values, -1. , 1.);
    Bempp::helmholtzDoubleLayerPotentialOperator<double>( make_shared_from_ref(pwiseLinears),
        points, omega, 
        Bempp::GlobalParameters::parameterList()
        )->apply(Bempp::NO_TRANSPOSE, dirichletDataDofs, values, 1. , 1.);
  }

  //! application operator
  virtual void
  operator() ( const RangeFunction &u, RangeFunction &w ) const;

protected:
  const ModelType &model () const { return model_; }

  struct MainOp 
  : public virtual Dune::Fem::Operator< RangeFunction,RangeFunction >
  {
    MainOp( const BemOperator< DomainFunction, RangeFunction, Model > &op) 
     : op_(op) {}
    virtual void
    operator() ( const RangeFunction &u, RangeFunction &w ) const
    {
      auto& uDofs = u.dofVector().coefficients();
      auto& wDofs = w.dofVector().coefficients();
      op_.slpOpAss_->apply(Bempp::NO_TRANSPOSE, uDofs, wDofs, 1. , 0.);
    }
    const BemOperator< DomainFunction, RangeFunction, Model > &op_;
  };
};

template<class BemOp, typename Rhs>
class MainEigenOpProduct;
template< class BemOp >
struct MainEigenOp : public Eigen::EigenBase< MainEigenOp<BemOp> >
{
  typedef typename BemOp::RangeFunctionType RangeFunctionType;
  MainEigenOp( const BemOp &op) 
   : op_(op) {}
  virtual void
  operator() ( const RangeFunctionType &u, RangeFunctionType &w ) const
  {
    auto& uDofs = u.dofVector().coefficients();
    auto& wDofs = w.dofVector().coefficients();
    op_.slpOpAss_->apply(Bempp::NO_TRANSPOSE, uDofs, wDofs, 1. , 0.);
  }
  const BemOp &op_;

  typedef typename RangeFunctionType::FunctionSpaceType::RangeFieldType Scalar;
  typedef double RealScalar;
  enum {
    ColsAtCompileTime = Eigen::Dynamic,
    RowsAtCompileTime = Eigen::Dynamic,
    MaxColsAtCompileTime = Eigen::Dynamic,
    MaxRowsAtCompileTime = Eigen::Dynamic
  };
  size_t rows() const { return op_.pwiseConstants.globalDofCount(); }
  size_t cols() const { return op_.pwiseConstants.globalDofCount(); }
  void resize(size_t a_rows, size_t a_cols)
  {
    // This method should not be needed in the future.
    assert(a_rows==0 && a_cols==0 || a_rows==rows() && a_cols==cols());
  }
  template<typename Rhs>
  MainEigenOpProduct<BemOp,Rhs> operator*(const Eigen::MatrixBase<Rhs>& x) const 
  {
    return MainEigenOpProduct<BemOp,Rhs>(*this, x.derived());
  }
};
template<class BemOp,typename Rhs>
class MainEigenOpProduct : public Eigen::ReturnByValue<MainEigenOpProduct<BemOp,Rhs> > 
{
public:
  typedef size_t Index;
  
  // The ctor store references to the matrix and right-hand-side object (usually a vector).
  MainEigenOpProduct(const MainEigenOp<BemOp>& matrix, const Rhs& rhs)
    : m_matrix(matrix), m_rhs(rhs)
  {}
  
  Index rows() const { return m_matrix.rows(); }
  Index cols() const { return m_rhs.cols(); }
  // This function is automatically called by Eigen. It must evaluate the product of matrix * rhs into y.
  template<typename Dest>
  void evalTo(Dest& y) const
  {
    y.setZero(m_matrix.rows());
    m_matrix.op_.slpOpAss_->apply(Bempp::NO_TRANSPOSE, m_rhs, y, 1. , 0.);
  }
protected:
  const MainEigenOp<BemOp>& m_matrix;
  typename Rhs::Nested m_rhs;
};
namespace Eigen { namespace internal {
  template< class BemOp >
  struct traits< MainEigenOp<BemOp> > :  
          Eigen::internal::traits<Eigen::SparseMatrix<typename BemOp::RangeFunctionType::FunctionSpaceType::RangeFieldType> >
  {};
  template < class BemOp, class Rhs >
  struct traits< MainEigenOpProduct<BemOp,Rhs> > 
  {
    // The equivalent plain objet type of the product. This type is used if the product needs to be evaluated into a temporary.
    typedef Eigen::Matrix<typename Rhs::Scalar, Eigen::Dynamic, Rhs::ColsAtCompileTime> ReturnType;
  };
} }


template< class DomainFunction, class RangeFunction, class Model >
void BemOperator< DomainFunction, RangeFunction, Model >
  ::operator() ( const RangeFunction &u, RangeFunction &w ) const
{
  auto& dirichletDataDofs = u.dofVector().coefficients();
  auto& neumannDataDofs = w.dofVector().coefficients();

  if ( !useHMatrix )
  {
    assert(decompMatrix);
    neumannDataDofs = decompMatrix->solve( dirichletDataDofs );
  }
  else
  {
    MainEigenOp<ThisType> op(*this);
    typename Construction::template Solver< MainEigenOp<ThisType> > solver;
    solver.setTolerance( solverEps_ );
    solver.compute(op);
    neumannDataDofs = solver.solveWithGuess(dirichletDataDofs, neumannDataDofs);
  }
}

#endif // #ifndef ELLIPTIC_HH

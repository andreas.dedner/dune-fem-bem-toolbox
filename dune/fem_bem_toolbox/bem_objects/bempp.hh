#ifndef BEMPP_HH
#define BEMPP_HH
// Copyright (C) 2011 by the BEM++ Authors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <dune/fem/io/parameter.hh>

#if HAVE_BEMPP
#define NOPYTHON
#include "bempp/assembly/discrete_boundary_operator.hpp"
#include "bempp/assembly/evaluation_options.hpp"
#include "bempp/assembly/numerical_quadrature_strategy.hpp"
#include "bempp/assembly/surface_normal_and_domain_index_dependent_function.hpp"
#include "bempp/assembly/discrete_boundary_operator.hpp"
#include "bempp/common/global_parameters.hpp"


#include "bempp/operators/sparse_operators.hpp"
#include "bempp/operators/helmholtz_operators.hpp"
#include "bempp/operators/laplace_operators.hpp"

#include "bempp/common/boost_make_shared_fwd.hpp"

#include "bempp/grid/grid.hpp"
#include "bempp/grid/concrete_grid.hpp"
#include "bempp/grid/grid_factory.hpp"

#include "bempp/space/piecewise_linear_continuous_scalar_space.hpp"
#include "bempp/space/piecewise_constant_scalar_space.hpp"

#include "bempp/common/eigen_support.hpp"

#include <iostream>
#include <fstream>

#endif
#endif

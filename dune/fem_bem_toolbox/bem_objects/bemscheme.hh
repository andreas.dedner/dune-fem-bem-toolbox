#ifndef ELLIPT_BEMSCHEME_HH
#define ELLIPT_BEMSCHEME_HH

// iostream includes
#include <iostream>

// include discrete function space
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/finitevolume.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/common/adaptmanager.hh>

// include discrete function
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>

#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>
#include <dune/fem/special_solvers/solver.hh>

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
/*********************************************************/

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include "bemoperator.hh"

struct ExternalDataOutputParameters
: public Dune::Fem::LocalParameter< Dune::Fem::DataOutputParameters, ExternalDataOutputParameters >
{
  ExternalDataOutputParameters ( const int step, const std::string &preprefix = "" )
    : step_( step ),
      pre_(preprefix)
  {}

  ExternalDataOutputParameters ( const ExternalDataOutputParameters &other )
  : step_( other.step_ )
  {}

  std::string prefix () const
  {
    std::stringstream s;
    s << pre_ << "-external-" << step_ << "-";
    return s.str();
  }

private:
  int step_;
  std::string pre_;
};

// BemScheme
//----------
template < class Model >
class BemScheme
{
public:
  //! type of the mathematical model
  typedef Model ModelType ;

  //! grid view (e.g. leaf grid view) provided in the template argument list
  typedef typename ModelType::GridPartType GridPartType;

  //! type of underyling hierarchical grid needed for data output
  typedef typename GridPartType::GridType GridType;

  //! type of function space (scalar functions, f: \Omega -> R)
  typedef typename ModelType :: FunctionSpaceType   FunctionSpaceType;

  //! choose type of discrete function space
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPartType, 1 > P1DiscreteFunctionSpaceType;
  typedef Dune::Fem::FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0 > DiscreteFunctionSpaceType;

  typedef typename Solvers<P1DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType P1DiscreteFunctionType;
  typedef typename Solvers<DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType DiscreteFunctionType;

  typedef P1DiscreteFunctionType DomainFunctionType;

  //! type of Coupling constraints
  typedef Dune::CouplingConstraints< ModelType, P1DiscreteFunctionSpaceType, false > ConstraintsType;

  //! define Bem operator
  typedef BemOperator< P1DiscreteFunctionType, DiscreteFunctionType, ModelType > BemOperatorType;

  BemScheme( GridPartType &gridPart,
             const ModelType& bemModel, const std::string prefix="", bool constructOperator=true )
    : bemModel_( bemModel ),
      gridPart_( gridPart ),
      p1Space_( gridPart_ ),
      p0Space_( gridPart_ ),
      solution_( prefix+"solution", p0Space_ ),
      dirichlet_( "dirichlet", p1Space_ ),
      rhs_( "rhs", p0Space_ ),
      bemOperator_(0),
      constraints_( std::make_shared<ConstraintsType>( bemModel_, p1Space_, true ) ),
      constraintsSetup_(false)
  {
    if (constructOperator)
      bemOperator_ = std::make_shared<BemOperatorType>( gridPart_, bemModel_ );
    // set all DoF to zero
    solution_.clear();
    rhs_.clear();
    dirichlet_.clear();
  }

  const ModelType& model() const
  {
    return bemModel_;
  }

  DiscreteFunctionType &solution()
  {
    return solution_;
  }
  const DiscreteFunctionType &solution() const
  {
    return solution_;
  }

  P1DiscreteFunctionType &dirichlet() 
  {
    return dirichlet_;
  }
  const P1DiscreteFunctionType &dirichlet() const
  {
    return dirichlet_;
  }

  const DiscreteFunctionType &rhs() const
  {
    return rhs_;
  }
  DiscreteFunctionType &rhs() 
  {
    return rhs_;
  }

  DiscreteFunctionSpaceType &discreteSpace()
  {
    return p0Space_;
  }
  const DiscreteFunctionSpaceType &discreteSpace() const
  {
    return p0Space_;
  }

  void clearRhs() 
  {
    rhs_.clear();
  }

  //! set-up the rhs for the coupling values
  template <class OtherDiscreteFunctionType>
  void couple(const OtherDiscreteFunctionType &otherSolution)
  {
    if (!constraintsSetup_)
    {
      constraints().updateCouplingDofs();
      constraintsSetup_ = true;
    }
    dirichlet_.clear();

    // apply coupling constraints
    constraints()( dirichlet_, otherSolution, dirichlet_ );
    dirichlet_.communicate();
    // apply rhsOp to rhs_
    prepareRhs();
  }

  //! set-up the rhs for the coupling values
  void prepare()
  {
    prepare(dirichlet_,rhs_);
  }

  //! solve the system
  void solve ( bool assemble=true )
  {
    bemOperator().operator()(rhs_,solution_);
  }

  template <class DF>
  void fill(DF &extDF, bool useIncident=true) const
  {
    fillExternal(extDF,dirichlet(),solution(),useIncident);
  }
  template <class ExternalGrid>
  void outputExternal(ExternalGrid &grid, bool useIncident=true) const
  {
    outputExternal(grid,dirichlet(),solution(),useIncident);
  }
  void outputExternal( const std::string& name, int level, bool useIncident) const
  {
    if(level<0) return;
    outputExternal(name,level,dirichlet(),solution(),useIncident);
  }

protected:
  virtual void prepareRhs()
  {
    bemOperator().prepareRhs(dirichlet_, rhs_);
  }

  template <class DDF, class NDF>
  void prepare(DDF &dirichlet, NDF &rhs)
  {
    rhs.clear();

    typedef Dune::Fem::GridFunctionAdapter< typename ModelType::IncidentData, GridPartType > GridIncidentType;
    GridIncidentType gridIncident("grid incident", model().incidentData(), dirichlet_.space().gridPart(), 5 );
    Dune::Fem::Interpolation<P1DiscreteFunctionType>::apply(gridIncident, dirichlet);
    bemOperator().project(dirichlet, rhs);

    typedef Dune::Fem::GridFunctionAdapter< typename ModelType::DirichletData, GridPartType > GridRhsType;
    GridRhsType gridRhs("grid rhs", model().dirichletData(), dirichlet_.space().gridPart(), 5 );
    Dune::Fem::Interpolation<P1DiscreteFunctionType>::apply(gridRhs, dirichlet);

    bemOperator().prepareRhs(dirichlet, rhs);
  }
  template <class DF, class DDF, class NDF>
  void fillExternal(DF &extDF, const DDF &dirichlet, const NDF &neumann,
                    bool useIncident) const
  {
    const int dim = DF::GridPartType::dimension;
    Bempp::Matrix<double> evaluationPoints( 3, extDF.gridPart().indexSet().size(dim) );
    const auto end = extDF.space().gridPart().template end<dim>();
    auto it = extDF.space().gridPart().template begin<dim>();
    for (; it!=end; ++it )
    {
      evaluationPoints(0,extDF.space().gridPart().indexSet().index(*it)) = it->geometry().corner(0)[0];
      evaluationPoints(1,extDF.space().gridPart().indexSet().index(*it)) = it->geometry().corner(0)[1];
      evaluationPoints(2,extDF.space().gridPart().indexSet().index(*it)) = it->geometry().corner(0)[2];
    }
    if ( extDF.dofVector().coefficients().size() != evaluationPoints.cols() )
    {
      std::cout << "Something is going wrong with dimensions in the fill method!" << std::endl;
      assert(0);
    }

    if (useIncident)
    {
      typedef Dune::Fem::GridFunctionAdapter< typename ModelType::IncidentData, typename DF::GridPartType > GridIncidentType;
      GridIncidentType gridIncident("grid incident", model().incidentData(), extDF.gridPart(), 5 );
      Dune::Fem::Interpolation<DF>::apply(gridIncident, extDF);
    }
    else
      extDF.clear();

    bemOperator().fill( dirichlet, neumann, evaluationPoints, extDF.dofVector().coefficients(),
                        typename DF::FunctionSpaceType::RangeFieldType() );
  }
  template <class ExternalGrid, class DDF, class NDF>
  void outputExternal(ExternalGrid &grid, const DDF &dirichlet, const NDF &neumann,
      bool useIncident) const
  {
    typedef Dune::Fem::LeafGridPart< ExternalGrid > LeafGridPart;
    LeafGridPart leafGridPart(grid);

    typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, LeafGridPart, 1 > DiscreteFunctionSpaceType;
    typedef typename Solvers<DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType DiscreteFunctionType;
    DiscreteFunctionSpaceType space( leafGridPart );
    DiscreteFunctionType solution( "u_inf", space );

    fillExternal( solution,dirichlet,neumann,useIncident );

    typedef Dune::tuple< DiscreteFunctionType* > IOTupleType;
    typedef Dune::Fem::DataOutput< ExternalGrid, IOTupleType > DataOutputType;
    IOTupleType ioTuple( &solution );
    DataOutputType dataOutput( grid, ioTuple, ExternalDataOutputParameters( 0 ) );
    dataOutput.write();
  }

  template <class DDF, class NDF>
  void outputExternal(const std::string& name, int level, const DDF &dirichlet, const NDF &neumann,
      bool useIncident) const
  {
    try
    {
      // first try a 2,3 grid
      typedef Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> ExternalGrid;
      Dune::GridPtr< ExternalGrid > gridPtr( name, Dune::MPIHelper::getLocalCommunicator() );
      ExternalGrid &grid = *gridPtr;
      grid.globalRefine( level );
      outputExternal( grid, dirichlet, neumann, useIncident );
    }
    catch (...)
    {
      // output bem solution on an extended grid
      typedef Dune::ALUGrid<3,3,Dune::cube,Dune::nonconforming> ExternalGrid;
      Dune::GridPtr< ExternalGrid > gridPtr( name, Dune::MPIHelper::getLocalCommunicator() );
      ExternalGrid &grid = *gridPtr;
      grid.globalRefine( level );
      outputExternal( grid, dirichlet, neumann, useIncident );
    }
  }

  virtual BemOperatorType &bemOperator () const { return *bemOperator_; }

  const ModelType& bemModel_;      // the mathematical model
  const ConstraintsType &constraints () const { return *constraints_; }

  GridPartType  &gridPart_;             // grid part(view), e.g. here the leaf grid the discrete space is build with

  DiscreteFunctionSpaceType p0Space_;   // discrete function space
  DiscreteFunctionType solution_,rhs_;       // the unknown

  P1DiscreteFunctionSpaceType p1Space_; // discrete function space
  P1DiscreteFunctionType dirichlet_;

  std::shared_ptr<ConstraintsType> constraints_;
  std::shared_ptr<BemOperatorType> bemOperator_;    // the bem operator
  double constraintsSetup_;
};

#endif // end #if ELLIPT_BEMSCHEME_HH

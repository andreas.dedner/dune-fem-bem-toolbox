#ifndef DROP_BEMSCHEME_HH
#define DROP_BEMSCHEME_HH

#include <dune/fem_bem_toolbox/bem_objects/sharedbemscheme.hh>

/*********************************************************/

#include "bemoperator.hh"
#include "../../../case_studies/charged_droplet/source/wallidentify.hh"
#include <dune/fem_bem_toolbox/coupling_schemes/dofrankpad.hh>
#include <string>
#include <boost/lexical_cast.hpp>

using boost::lexical_cast;
using std::string;

// DropBemScheme
//----------

/*******************************************************************************
 * template arguments are:
 * - GridPart: the part of the grid used to tessellate the
 *             computational domain
 * - Model: description of the data functions and methods required for the
 *          elliptic operator (massFlux, diffusionFlux)
 *     Model::ProblemType boundary data, exact solution,
 *                        and the type of the function space
 *******************************************************************************/
template < class Model >
class DropBemScheme : public SharedBemScheme<Model>
{
  typedef SharedBemScheme<Model> BaseType;
public:
  //! type of the mathematical model
  typedef Model ModelType ;

  //! grid view (e.g. leaf grid view) provided in the template argument list
  typedef typename ModelType::GridPartType GridPartType;

  //! type of underyling hierarchical grid needed for data output
  typedef typename GridPartType::GridType GridType;

  //! type of function space (scalar functions, f: \Omega -> R)
  typedef typename ModelType :: FunctionSpaceType   FunctionSpaceType;

  //! choose type of discrete function space
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPartType, 1 > P1DiscreteFunctionSpaceType;
  typedef Dune::Fem::FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0 > DiscreteFunctionSpaceType;

  typedef typename Solvers<P1DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType P1DiscreteFunctionType;
  typedef typename Solvers<DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType DiscreteFunctionType;

  // special shared eigenvector for vector surface node movements
  typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, 3 > NudgeVectorFunctionSpaceType;
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< NudgeVectorFunctionSpaceType, GridPartType, 1 > NudgeVectorP1DiscreteFunctionSpaceType;
  typedef typename Solvers<NudgeVectorP1DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType NudgeVectorP1DiscreteFunctionType;

  typedef DiscreteFunctionType BemDiscreteFunctionType;
  typedef DiscreteFunctionSpaceType BemDiscreteFunctionSpaceType;

  /*********************************************************/

  template< class LoadBalancer >
  DropBemScheme( GridPartType &gridPart, GridPartType &fullGridPart,
             const ModelType& bemModel, const LoadBalancer &loadbal, const std::string prefix="" )
    : BaseType(gridPart,fullGridPart,bemModel,loadbal,prefix)
    , fullP1NudgeSpace_( fullGridPart )
    , sharedVectorP1Data_("sharedNudgeDirData", true, fullP1NudgeSpace_ )
    , drift_( 0 )
    , badNode_( Dune::FieldVector<double,3>(0) )
    , walled_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) > -400.0 )
    , fixid_( {0,0,0,0,0} )
    , spun_( 0 )
    , avesiz_( 0.0 ) 
    , fixtype_( true )
    , tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01 ) )
  {
  }

  template <class DF , class IOFT>
  void oii(DF &solution,  IOFT &iof)
  {
    iof << solution << std::endl;
  }
  
  template <class DF , class IOFT>
  void ioo(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stod(line,&sz);
  }
  
  template <class DF , class IOFT>
  void iooI(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stoi(line,&sz);
  }
  
  NudgeVectorP1DiscreteFunctionType &fullVectorDirData()
  {
    return sharedVectorP1Data_;
  }
  const NudgeVectorP1DiscreteFunctionType &fullVectorDirData() const
  {
    return sharedVectorP1Data_;
  }

  template <class DF , class IOFT>
  void oi(unsigned int num, DF &solution,  IOFT &iof)
  {
    unsigned int span = DF::RangeType::dimension;
    typename DF::DofIteratorType end = solution.dend(); unsigned int i = 0;
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git, ++i )
    {
      iof << num << "  " << std::floor(float(i)/float(span))+1 << " w " << 1+i%span << " xxx " << lexical_cast<string>(*git) << std::endl;
    };
  }
  
  template <class DF , class IOFT>
  void io(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    
    typename DF::DofIteratorType end = solution.dend();
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git )
    {
      getline(iof,line); *git = std::stod(line.erase(0,line.rfind("xxx")+4),&sz);
    };
  }
  
  void dump(const int snap)
  {
    std::ifstream isnapfile, jsnapfile; std::ofstream osnapfile, psnapfile;
    if( snap < -1 ) osnapfile.open("snip.bem"); else if( snap > 0 ) osnapfile.open("snap.bem"); else if( snap < 0 ) isnapfile.open("snap.bem");
    if( snap < -1 ) psnapfile.open("snip.bim"); else if( snap > 0 ) psnapfile.open("snap.bim"); else if( snap < 0 ) jsnapfile.open("snap.bim");

    if( snap > 0 ) // writing data out
    {
      for( unsigned int i = 0; i < 4; ++i )
      oii( drift_[ i ], psnapfile );
      for( unsigned int i = 0; i < 3; ++i )
      for( unsigned int j = 0; j < 3; ++j )
      oii( badNode_[ i ][ j ], psnapfile );
      oii( spun_, psnapfile );
      oii( avesiz_, psnapfile );
      for( unsigned int i = 0; i < 5; ++i )
      oii( fixid_[ i ], psnapfile );

      oi( 1, BaseType::dofRank_, osnapfile );
      // oi( 2, solution_, osnapfile );
      // oi( 3, rhs_, osnapfile );
      oi( 4, sharedVectorP1Data_, osnapfile );
      oi( 5, BaseType::dirichlet_, osnapfile );
      oi( 6, BaseType::sharedDirichlet_, osnapfile ); 
      // oi( 7, BaseType::sharedSolution_, osnapfile );
      // oi( 8, BaseType::sharedRhs_, osnapfile );
    }
    else if( snap < 0 ) // reading data in
    {
      for( unsigned int i = 0; i < 4; ++i )
      ioo( drift_[ i ], jsnapfile );
      for( unsigned int i = 0; i < 3; ++i )
      for( unsigned int j = 0; j < 3; ++j )
      ioo( badNode_[ i ][ j ], jsnapfile );
      ioo( spun_, jsnapfile );
      ioo( avesiz_, jsnapfile );
      for( unsigned int i = 0; i < 5; ++i )
      iooI( fixid_[ i ], jsnapfile );

      io( BaseType::dofRank_, isnapfile );
      solution_.clear();                             // io( solution_, isnapfile );
      rhs_.clear();                                  // io( rhs_, isnapfile );
      io( sharedVectorP1Data_, isnapfile );
      io( BaseType::dirichlet_, isnapfile );
      io( BaseType::sharedDirichlet_, isnapfile ); 
      BaseType::sharedSolution_.clear();             // io( BaseType::sharedSolution_, isnapfile );
      BaseType::sharedRhs_.clear();                  // io( BaseType::sharedRhs_, isnapfile );
    };
    
    if( snap > 0 ) osnapfile.close(); else if( snap < 0 ) isnapfile.close();
    if( snap > 0 ) psnapfile.close(); else if( snap < 0 ) jsnapfile.close();
  }
  
  int wall( const int &plane, double bias )
  {
    rhs_.clear();

    if( plane == 0 ) { badNode_[0] = Dune::FieldVector<double,3>(0); for( unsigned int i = 4; i > 0; --i ) fixid_[i] = fixid_[i-1]; fixid_[0] = 0; fixtype_ = !fixtype_; };
    
    // if there is a bad node (degenerated element) and the plane fitting has just done plane number three
    // then set plane fitting up to fix degenerated element
    bool fixing = ( badNode_[0].two_norm() > tol_ ) && ( ( plane == 3 ) || ( plane == 4 ) );

    if( fixing )
    {
      // dud element the same as two fixes ago (for same fixtype), so fix obviously still not working, so try fixing 3rd node of duff element instead
      if( fixid_[0] == fixid_[4] ) badNode_[0] = badNode_[2];
      // dud element the same as last time (for same fixtype), so first fix obviously did not work, so try 2nd node of duff element instead
      else if( fixid_[0] == fixid_[2] ) badNode_[0] = badNode_[1];
      // otherwise, this is a new dud element, so just try fixing it's first node coordinates already held in badNode_[0]
    }
    
    typedef WallIdentify< DiscreteFunctionType > WallIdentifyType;
    typedef Dune::Fem::LocalFunctionAdapter< WallIdentifyType > WallIdentifyFunctionType;
    WallIdentifyType wallIdentify( fixing, fixid_[0], solution_, badNode_, spun_, avesiz_, fixtype_, bias );
    WallIdentifyFunctionType wallIdentifyFunction( "wallidentify", wallIdentify, gridPart_ );

    Dune::Fem::LagrangeInterpolation
          < WallIdentifyFunctionType, DiscreteFunctionType > interpolationWall;

    interpolationWall( wallIdentifyFunction, rhs_ );

    if( walled_ ) drift_[ 3 ] = wallIdentify.area(); // contact area passed through to nudge via drift

    // calculate the new spun
    wallIdentify.span();
    
    int retval = 0;
    if(fixing) retval = 1;
    // std::cout << "DBS " << retval << "  ==  " << badNode_ << "  ++  " << plane << std::endl;
    return retval;
  }

  const double spun() const
  {
    // std::cout << "WWW " << spun_ * 180.0 / M_PI << std::endl;
    return spun_;
  }
  
  void zapSpun()
  {
    spun_ = 0.0;
  }
  
  //! assemble a new bem operator
  void prepare()
  {
    MPI_Barrier(MPI_COMM_WORLD);
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      typedef typename BaseType::BemOperatorType BemOperatorType;
      BaseType::bemOperator_->assemble();
    }
    BaseType::prepare();
  }

  template <class OtherDiscreteFunctionType>
  //! sotup the right hand side and physical boundaries
  void collate(OtherDiscreteFunctionType &otherSolution)
  {
    // need to pad out the dofRank to look like a vector
    typedef DofRankPad< P1DiscreteFunctionType, NudgeVectorP1DiscreteFunctionType > DofRankPadType;
    typedef Dune::Fem::LocalFunctionAdapter< DofRankPadType > DofRankPadFunctionType;
    DofRankPadType dofrankpad( BaseType::dofRank_ );
    DofRankPadFunctionType dofrankpadFunction( "dofrankpad", dofrankpad, BaseType::fullGridPart_ );

    // set coupling values for solution
    // BaseType::constraints()( func, funk, u );
    BaseType::fullConstraints()( dofrankpadFunction, otherSolution, sharedVectorP1Data_ );

    // ensure all rhs data has been collected together first ...
    MPI_Barrier(MPI_COMM_WORLD);
  }

  Dune::FieldVector<double,4> &drift()
  {
    return drift_;
  }
  const Dune::FieldVector<double,4> &drift() const
  {
    return drift_;
  }

protected:

  using BaseType::model;
  using BaseType::fullGridPart_;
  using BaseType::rhs_;
  using BaseType::gridPart_;
  using BaseType::solution_;
  const NudgeVectorP1DiscreteFunctionSpaceType fullP1NudgeSpace_;
  mutable NudgeVectorP1DiscreteFunctionType sharedVectorP1Data_;
  // keep track of any "drifting"
  Dune::FieldVector<double,4> drift_;
  Dune::FieldVector<Dune::FieldVector<double,3>,3> badNode_;
  // measure of how much drop has spun about y-axis
  double spun_;
  double avesiz_;
  int fixid_[5];
  const bool walled_;
  // is fixing due to skewedness or small element-ness
  bool fixtype_;
  const double tol_;
};

#endif

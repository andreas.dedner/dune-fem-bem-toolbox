#ifndef SHAREDBEMSCHEME_HH
#define SHAREDBEMSCHEME_HH

#include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>

/*********************************************************/

// SharedBemScheme
//----------

template < class Model >
class SharedBemScheme : public BemScheme<Model>
{
  typedef BemScheme<Model> BaseType;
public:
  //! type of the mathematical model
  typedef Model ModelType ;

  //! grid view (e.g. leaf grid view) provided in the template argument list
  typedef typename ModelType::GridPartType GridPartType;

  //! type of underyling hierarchical grid needed for data output
  typedef typename GridPartType::GridType GridType;

  //! type of function space (scalar functions, f: \Omega -> R)
  typedef typename ModelType :: FunctionSpaceType   FunctionSpaceType;

  //! choose type of discrete function space
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPartType, 1 > P1DiscreteFunctionSpaceType;
  typedef Dune::Fem::FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0 > DiscreteFunctionSpaceType;

  typedef typename Solvers<P1DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType P1DiscreteFunctionType;
  typedef typename Solvers<DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType DiscreteFunctionType;

  /*********************************************************/

  //! type of Dirichlet constraints
  typedef Dune::CouplingConstraints< ModelType, P1DiscreteFunctionSpaceType, false > ConstraintsType;
  typedef Dune::CouplingConstraints< ModelType, P1DiscreteFunctionSpaceType, true > SpecialConstraintsType;

  //! define Laplace operator
  typedef BemOperator< P1DiscreteFunctionType, DiscreteFunctionType, ModelType > BemOperatorType;

  template< class LoadBalancer >
  SharedBemScheme( GridPartType &gridPart, GridPartType &fullGridPart,
		   const ModelType& bemModel, const LoadBalancer &loadbal, const std::string prefix="" )
    : BaseType(gridPart,bemModel,prefix,false),
      fullGridPart_( fullGridPart ),
      fullP1Space_( fullGridPart_ ),
      sharedDirichlet_("sharedDirData", true, fullP1Space_ ),
      dofRank_("dofRank", fullP1Space_ ),
      fullP0Space_( fullGridPart_ ),
      sharedSolution_("sharedNeuData", true, fullP0Space_ ),
      sharedRhs_("sharedRhs", true, fullP0Space_ ),
      partConstraints_( std::make_shared<SpecialConstraintsType>( bemModel, p1Space_ ) ),
      fullConstraints_( std::make_shared<SpecialConstraintsType>( bemModel, fullP1Space_ ) ),
      // which rank is allowed to fill in a particular dof (of the whole surface mesh)
      // the elliptic operator (implicit)
      bemOperator_(0)
  {
    partConstraints().updateCouplingDofs();
    fullConstraints().updateCouplingDofs();
    sharedSolution_.clear();

    if( Dune::Fem::MPIManager::rank() == 0 )
      bemOperator_ = std::make_shared<BemOperatorType>( fullGridPart_, model() );

    // initialize all full surface grid solution dofs to be matched up to surface section solution dofs by rank 0
    dofRank_.clear();

    // set up which ranks are allowed to fill in which full surface solution dof
    std::cout << "Full surface area = " << setDofRank( loadbal ) << std::endl;
  }

  P1DiscreteFunctionType &dofRank()
  {
    return dofRank_;
  }
  const P1DiscreteFunctionType &dofRank() const
  {
    return dofRank_;
  }
  P1DiscreteFunctionType &fullDirData()
  {
    return sharedDirichlet_;
  }
  const P1DiscreteFunctionType &fullDirData() const
  {
    return sharedDirichlet_;
  }
  DiscreteFunctionType &fullSolution()
  {
    return sharedSolution_;
  }
  const DiscreteFunctionType &fullSolution() const
  {
    return sharedSolution_;
  }

  DiscreteFunctionType &fullRhs()
  {
    return sharedRhs_;
  }
  const DiscreteFunctionType &fullRhs() const
  {
    return sharedRhs_;
  }

  void clearRhs() 
  {
    rhs_.clear();
    if( Dune::Fem::MPIManager::rank() == 0 )
      sharedRhs_.clear();
  }

  //! set-up the rhs for the coupling values
  virtual void prepareRhs()
  {
    // form full rhs vector from rhs vector on different surface sections
    fullConstraints()( dofRank(), dirichlet_, sharedDirichlet_ );
    MPI_Barrier(MPI_COMM_WORLD);

    // ensure all rhs data has been collected together first ...
    if( Dune::Fem::MPIManager::rank() == 0 )
      bemOperator().prepareRhs(sharedDirichlet_, sharedRhs_);
    MPI_Barrier(MPI_COMM_WORLD);
  }

  void prepare()
  {
    if( Dune::Fem::MPIManager::rank() == 0 )
      BaseType::prepare(sharedDirichlet_,sharedRhs_);
    MPI_Barrier(MPI_COMM_WORLD);
  }

  template <class DF>
  void fill(DF &extDF, bool useIncident=true) const
  {
    if( Dune::Fem::MPIManager::rank() == 0 )
      BaseType::fillExternal(extDF,sharedDirichlet_,sharedSolution_,useIncident);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  template <class ExternalGrid>
  void outputExternal(ExternalGrid &grid, bool useIncident=true) const
  {
    if( Dune::Fem::MPIManager::rank() == 0 )
      BaseType::outputExternal(grid,sharedDirichlet_,sharedSolution_,useIncident);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  void outputExternal( const std::string& name, int level, bool useIncident) const
  {
    if (level < 0)
      return;
    if( Dune::Fem::MPIManager::rank() == 0 )
      BaseType::outputExternal(name,level, sharedDirichlet_,sharedSolution_, useIncident);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  //! solve the system
  void solve ( bool assemble=true )
  {
    if( Dune::Fem::MPIManager::rank() == 0 )
      bemOperator().operator()(sharedRhs_,sharedSolution_);
    MPI_Barrier(MPI_COMM_WORLD);

    solution_.clear();
    dirichlet_.clear();
    // no copying over of solution until it is ready
    // bring back the solution on to the local grid part
    partConstraints()( dirichlet_, sharedSolution_, solution_ );
    MPI_Barrier(MPI_COMM_WORLD);
  }

protected:
  // set up which ranks are allowed to fill in which full surface solution dof
  template< class LoadBalancer >
  double setDofRank( const LoadBalancer &loadbal )
  {
    typedef typename GridPartType::template Codim<0>::IteratorType IteratorType;

    // initialize an area checker
    double result = 0.0;

    // run through all the elements of the full (un-partitioned) surface grid
    const IteratorType end = fullGridPart_.template end<0>();
    for( IteratorType it = fullGridPart_.template begin<0>(); it != end; ++it )
    {
      // keep track of the total surface area as a check
      result += it->geometry().volume();

      // determine the rank the load balancer would have given this element
      int destRank = loadbal.operator()( *it );
      setDof( *it, destRank );
    }
    return result;
  }

  //! set the entity dofs to the value of the rank (if the greatest)
  template< class EntityType >
  void setDof( const EntityType &entity, const int dest )
  {
    typedef typename P1DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
    typedef typename P1DiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename P1DiscreteFunctionSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename P1DiscreteFunctionSpaceType :: RangeType RangeType;

    // get local functions of result
    LocalFunctionType wLocal = dofRank_.localFunction( entity );

    const LagrangePointSetType &lagrangePointSet = dofRank_.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    int localDof = 0;
    const unsigned int localBlockSize = dofRank_.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    dofRank_.space().blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( unsigned int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
      RangeType phi( dest );

      // store result to dof vector
      for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
      {
        // whichever rank is the greatest gets to set the dof
        if( std::abs(phi[ l ]) > std::abs(wLocal[ localDof ]) ) wLocal[ localDof ] = phi[ l ];
      }
    }
  }

protected:

  using BaseType::model;
  using BaseType::p1Space_; // discrete function space
  using BaseType::constraints;
  using BaseType::solution_;       // the unknown
  using BaseType::rhs_; // ,
  using BaseType::dirichlet_;

  const SpecialConstraintsType &partConstraints () const { return *partConstraints_; }
  const SpecialConstraintsType &fullConstraints () const { return *fullConstraints_; }

  GridPartType &fullGridPart_;
  P1DiscreteFunctionSpaceType fullP1Space_;
  P1DiscreteFunctionType sharedDirichlet_, dofRank_;
  DiscreteFunctionSpaceType fullP0Space_;   
  DiscreteFunctionType sharedSolution_, sharedRhs_;     
  std::shared_ptr<SpecialConstraintsType> fullConstraints_;
  std::shared_ptr<SpecialConstraintsType> partConstraints_;

  std::shared_ptr<BemOperatorType> bemOperator_;    // the bem operator
  BemOperatorType &bemOperator () const { return *bemOperator_; }
};

#endif // end #if SHAREDBEMSCHEME_HH

#ifndef BEMERFOPERATOR_HH
#define BEMERFOPERATOR_HH

#include "bemoperator.hh"

// Bemerfoperator
// ----------------
  using namespace Bempp;

template< class DomainFunction, class RangeFunction, class Model >
struct BemerfOperator
: public BemOperator< DomainFunction, RangeFunction, Model >
{
  typedef BemOperator< DomainFunction, RangeFunction, Model > Basetype;

  typedef DomainFunction   DiscreteFunctionType;
  typedef Model            ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;
  typedef typename GridPartType::GridType GridType;

  typedef Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GridPartType FixedGridPartType;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GrydPartType GrydPartType;

public:

  // the points at which the erf will be needed
  Bempp::Matrix<CT> points;
  // the incident field (if any) at these points
  Bempp::Matrix<RT> iStore ;

  //! contructor
  BemerfOperator ( const ModelType &model, const DiscreteFunctionSpaceType &space )
    : BaseType(model, space)
  {
  }

  // set-up the points array for the exterior representation formulae
  template <class FillDiscreteFunctionType>
  void erfSetup( const FillDiscreteFunctionType &u )
  {
    std::cout << "Shouldn't call erfSetup" << std::endl;
    abort();
    typedef typename FillDiscreteFunctionType::DiscreteFunctionSpaceType FillDiscreteFunctionSpaceType;

    typedef typename FillDiscreteFunctionSpaceType::IteratorType IteratorType;
    typedef typename IteratorType::Entity       EntityType;

    // get discrete function space
    const FillDiscreteFunctionSpaceType &erfSpace = u.space();

    // array to store previously computed erf values for efficiency
    const int blocks = erfSpace.blockMapper().size() ;

    points.resize(3, blocks);
    iStore.resize(blocks);

    // iterate over grid
    const IteratorType end = erfSpace.end();
    for( IteratorType it = erfSpace.begin(); it != end; ++it )
    {
      // get entity (here element)
      const EntityType &entity = *it;

      fillPoints( erfSpace, entity );
    }

    // create and assemble the single and double layer operators to be used for the ERF calculations at the points just found
    assert( !slPotOpAss );
    slPotOpAss = new AssembledPotentialOperator<BFT, RT> (
      slPotOp.assemble( make_shared_from_ref(pwiseConstants),
                        make_shared_from_ref(points),
                        quadStrategy, evaluationOptions ) );
    assert( !dlPotOpAss );
    dlPotOpAss = new AssembledPotentialOperator<BFT, RT> (
      dlPotOp.assemble( make_shared_from_ref(pwiseLinears),
                        make_shared_from_ref(points),
                        quadStrategy, evaluationOptions ) );
  }

  //! fill up the points array with the coordinates of the points where the ERF will need to be evaluated
  template< class FillDiscreteSpaceType, class EntityType >
  void fillPoints( const FillDiscreteSpaceType &erfSpace, const EntityType &entity )
  {
    typedef typename FillDiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename EntityType::Geometry GeometryType;

    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    const LagrangePointSetType &lagrangePointSet = erfSpace.lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    int localDof = 0;
    const int localBlockSize = FillDiscreteSpaceType :: localBlockSize ;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    erfSpace.blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
        auto where = geometry.global( Dune::Fem::coordinate( lagrangePointSet[ localBlock ] ) );

        CT x = where[0];
        CT y = where[1];
        CT z = where[2];

        points[ 0, globalBlockDofs[ localBlock ] ] = x;
        points[ 1, globalBlockDofs[ localBlock ] ] = y;
        points[ 2, globalBlockDofs[ localBlock ] ] = z;

        // determine the incident field at these points
        IncidentData incident;
        IncidentData::ValueType values;

        incident.evaluate( x, y, z, values );

        // std::cout << "EP " << x << "  " << y << "  " << z << "  " << std::sqrt(x*x+y*y+z*z) << "  " << values << std::endl;

        iStore[ globalBlockDofs[ localBlock ] ] = values;

        // std::cout << "PE " << x << "  " << y << "  " << z << "  " << std::sqrt(x*x+y*y+z*z) << std::endl;
    }
  }

  // apply the exterior representation formulae
  template <class DirichletFunction, class NeumannFunction, class FillDiscreteFunctionType>
  void filling( const DirichletFunction &func, const NeumannFunction &funk, FillDiscreteFunctionType &u )
  {
    typedef typename FillDiscreteFunctionType::DiscreteFunctionSpaceType FillDiscreteFunctionSpaceType;

    typedef typename FillDiscreteFunctionSpaceType::IteratorType IteratorType;
    typedef typename IteratorType::Entity       EntityType;

    // get discrete function space
    const FillDiscreteFunctionSpaceType &dfSpace = u.space();

    // array to store previously computed erf values for efficienty
    const int blocks = dfSpace.blockMapper().size() ;

    typedef typename FillDiscreteFunctionType :: DiscreteFunctionSpaceType FillDiscreteSpaceType;
    typedef typename FillDiscreteSpaceType :: RangeType RangeType;

    std::vector< RangeType > store;

    store.resize( blocks );

    // set-up Dirichlet and Neumann data to be used in the filling
    auto& dirichletDataDofs = func.dofVector().coefficients();
    auto& neumannDataDofs = funk.dofVector().coefficients();

    GridFunction<BFT, RT> dirichletData(
              make_shared_from_ref(context),
              make_shared_from_ref(pwiseLinears),
              dirichletDataDofs);

    GridFunction<BFT, RT> neumannData(
              make_shared_from_ref(context),
              make_shared_from_ref(pwiseConstants),
              neumannDataDofs);

    // calculate the new erf values without redoing all the potential operators
    Bempp::Matrix<RT> sStore = slPotOpAss->apply( neumannData );
    Bempp::Matrix<RT> dStore = dlPotOpAss->apply( dirichletData );

    for( int i=0; i<blocks; ++i )
    {
      store[ i ] = 1000.0;
      // arma::Mat<RT> feeld = sStore(i) + dStore(i);
      // store[ i ] = sStore(i) + dStore(i) + iStore(i); // feeld(0, 0);
    }

    // iterate over grid
    const IteratorType end = dfSpace.end();
    for( IteratorType it = dfSpace.begin(); it != end; ++it )
    {
      // get entity (here element)
      const EntityType &entity = *it;

      erfApplication( entity, func, funk, u, dirichletData, neumannData, store );
    }
  }

  //! set the rhs dirichlet point values to those required for the coupling
  template< class EntityType, class DirichletGridFunctionType, class NeumannGridFunctionType, class FillDiscreteFunctionType, class RangeType >
  void erfApplication( const EntityType &entity,
                              const DirichletGridFunctionType& u,
                              const NeumannGridFunctionType& v,
                       FillDiscreteFunctionType &w, GridFunction<BFT, RT> &dirichletData, GridFunction<BFT, RT> &neumannData, std::vector< RangeType > &store )
  {
    typedef typename FillDiscreteFunctionType :: DiscreteFunctionSpaceType FillDiscreteSpaceType;
    typedef typename FillDiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename FillDiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename EntityType::Geometry GeometryType;

    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    // get local functions of result
    LocalFunctionType wLocal = w.localFunction( entity );

    const LagrangePointSetType &lagrangePointSet = w.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    int localDof = 0;
    const int localBlockSize = FillDiscreteSpaceType :: localBlockSize ;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    w.space().blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
        // Construct the array 'evaluationPoints' containing the coordinate
        // of the point where the solution should be evaluated

        assert( globalBlockDofs[ localBlock ] < store.size() );

        if( std::abs(store[globalBlockDofs[ localBlock ]].two_norm()) > 500.0 )
        {

        const int dimWorld = 3;
        Bempp::Matrix<CT> evaluationPoints(dimWorld, 1);

        auto where = geometry.global( Dune::Fem::coordinate( lagrangePointSet[ localBlock ] ) );

        CT x = where[0];
        CT y = where[1];
        CT z = where[2];

        evaluationPoints(0, 0) = x;
        evaluationPoints(1, 0) = y;
        evaluationPoints(2, 0) = z;

        // determine the incident field at these points
        IncidentData incident;
        IncidentData::ValueType values;

        incident.evaluate( x, y, z, values );

        // Use the Green's representation formula to evaluate the solution
        Bempp::Matrix<RT> field =
            - slPotOp.evaluateAtPoints(neumannData, evaluationPoints,
                                       quadStrategy, evaluationOptions) +
             dlPotOp.evaluateAtPoints(dirichletData, evaluationPoints,
                                      quadStrategy, evaluationOptions) + values;

        // std::cout << "EP " << x << "  " << y << "  " << z << "  " << std::sqrt(x*x+y*y+z*z) << "  " << store[globalBlockDofs[ localBlock ]] << "  " << field(0, 0) << "  " << std::endl;

        store[globalBlockDofs[ localBlock ]] = field(0, 0);

        // put the solution into the scheme's solution
        }

        RangeType phi = store[ globalBlockDofs[ localBlock ] ];


        // store result to dof vector
        for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
        {
          wLocal[ localDof ] = phi[ l ];
        }
    }
  }

protected:

};

#endif

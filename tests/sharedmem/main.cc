/***********************************************
 * Some bits taken from
 *    http://www.ibm.com/developerworks/aix/library/au-concurrent_boost
 * Compile with
 *    mpiCC -o main main.cc -lrt
 * Run with
 *    mpiexec -np n ./main
 *
 * with mpimanager from dune fem:
 * mpiCC -I/storage/maths/masnaj/07.07.15/bem/dune/dune-fem -I/storage/maths/masnaj/07.07.15/bem/mod/bempp-build/external/include -o main main.cc -lrt -DHAVE_MPI -ldunecommon -L/storage/maths/masnaj/07.07.15/bem/mod/bempp-build/external/lib64 -DUSE_SMP_PARALLEL -DMPI_2
 **********************************************/

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <mpi.h>

#include <iostream>

#include <dune/fem/misc/mpimanager.hh>


using namespace boost::interprocess;

template< class SharedObjectType >
struct SharedMemory
{
  static void clearSharedMemory( const char *name)
  {
    shared_memory_object::remove(name);
  }

  SharedMemory( const char *name)
  {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    // (rank == 0)
    {
      std::cout << "setup shared memory..." << std::endl;
      // creating our first shared memory object.
      // shared_memory_object::remove("Hello");
      shared_memory_object sharedmem1(open_or_create, name, read_write);
      // setting the size of the shared memory
      sharedmem1.truncate (256);
      // map the shared memory to current process
      region_ = mapped_region(sharedmem1, read_write);
      // access the mapped region using get_address
      std::cout << rank << "  " << sharedmem1.get_name() << '\n';
      offset_t size;
      if (sharedmem1.get_size(size))
        std::cout << rank << "  " << size << '\n';
    }
  }
  SharedObjectType *get()
  {
    return static_cast<SharedObjectType* >(region_.get_address());
  }
  private:
  mapped_region region_;
};


int main(int argc, char* argv[ ])
{
  try {
    MPI_Init(&argc, &argv);
    // Dune::Fem::MPIManager::initialize( argc, argv );
    int rank,size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    std::cout << "starting: " << rank << "/" << size << std::endl;

    // clear any previous instances of shared objects with the same name
    SharedMemory<int>::clearSharedMemory("Hallo");

    // sync processes
    MPI_Barrier(MPI_COMM_WORLD);

    // create (or just refer to) a new shared memory of the given name
    SharedMemory<int> memory("Hallo");

    std::cout << "returned: " << rank << "/" << size << std::endl;

    // at the moment only rank zero can fill the buffer
    if (rank > *memory.get())
    {
      std::cout << "Replacing " << *memory.get() << " with " << rank << std::endl;
      *memory.get() = rank;
    }
    // std::strcpy(memory.get(), "Hello World!");

    std::cout << "Process zero fills the memory..." << std::endl;

    // sync processes
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "...waiting" << std::endl;

    int str1 = *memory.get();
    std::cout << "rank=" << rank << " " << str1 << std::endl;

    MPI_Finalize();
    } catch (interprocess_exception& e) {
    // .. .  clean up
    }
}

#if 0

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <cstring>
#include <cstdlib>
#include <iostream>

int main(int argc, char *argv[ ])
{
      using namespace boost::interprocess;
      try {
      // opening an existing shared memory object
      shared_memory_object sharedmem2 (open_only, "Hello", read_only);

      // map shared memory object in current address space
      mapped_region mmap (sharedmem2, read_only);

      // need to type-cast since get_address returns void*
      char *str1 = static_cast<char*> (mmap.get_address());
      std::cout << str1 << std::endl;
      } catch (interprocess_exception& e) {
          std::cout << e.what( ) << std::endl;
      }
      return 0;
}

#endif





#ifndef POISSON_PROBLEMS_HH
#define POISSON_PROBLEMS_HH

#include <cassert>
#include <cmath>

#include <dune/fem_bem_toolbox/fem_objects/probleminterface.hh>

// -laplace u = f with zero Dirichlet boundary conditions on domain [0,1]^d
// Exsct solution is u(x_1,...,x_d) = sin(2*pi*x_1)...sin(2*pi*x_d)
template <class FunctionSpace>
class Stokes : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    double r = x.two_norm();
    phi[0] = 1.0 / r  + x[0] * x[ 0 ] / std::pow(r, 3.0);
    phi[1] =          + x[1] * x[ 0 ] / std::pow(r, 3.0);
    phi[2] =          + x[2] * x[ 0 ] / std::pow(r, 3.0);
    phi[3] = 2*x[0] / std::pow(r, 3.0);
    phi /=( 8*M_PI );
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(0);
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true;
  }
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }
};

#endif // #ifndef POISSON_HH

// always include this file
#include <config.h>

// C++ includes
#include <cmath>
#include <iostream>
#include <string>

// dune-common includes
#include <dune/common/exceptions.hh>                // adds Dune::Exception

// dune-fem includes
#include <dune/fem/gridpart/filteredgridpart.hh>    // adds Dune::Fem::FilteredGridPart
#include <dune/fem/gridpart/leafgridpart.hh>        // adds Dune::Fem::LeafGridPart
#include <dune/fem/io/file/dataoutput.hh>           // adds Dune::Fem::DataOutput
#include <dune/fem/io/parameter.hh>                 // adds Dune::Fem::Parameter
#include <dune/fem/misc/mpimanager.hh>              // adds Dune::Fem::MPIManager
#include <dune/fem/quadrature/cachingquadrature.hh> // adds Dune::Fem::CachingQuadrature
#include <dune/fem/space/common/adaptmanager.hh>    // adds Dune::Fem::GlobalRefine
#include <dune/fem/io/file/dataoutput.hh>

// local includes
#include "function.hh"                              // adds QuadraticFunction
#include "radialfilter.hh"                          // adds RadialFilter

// MyDataOutputParameters
// ----------------------

struct MyDataOutputParameters
: public Dune::Fem::LocalParameter< Dune::Fem::DataOutputParameters, MyDataOutputParameters >
{
  MyDataOutputParameters ( const int step )
  : step_( step )
  {}

  MyDataOutputParameters ( const MyDataOutputParameters &other )
  : step_( other.step_ )
  {}

  virtual int startcounter () const
  {
    return step_;
  }

private:
  int step_;
};

// compute the size of the domain and the average of the exact solution
template< class HGridType >
double algorithm ( HGridType &grid, int step )
{
  // return value is some meassure for the error
  double error = 0;
  // use a scalar function space
  typedef Dune::Fem::FunctionSpace< double, double, HGridType::dimensionworld, 1 > FunctionSpaceType;

  // construct an instance of the problem
  typedef School::QuadraticFunction< FunctionSpaceType > FunctionType;
  FunctionType function;

  // create host grid part consisting of leaf level elements
  typedef Dune::Fem::LeafGridPart< HGridType > HostGridPartType;
  HostGridPartType hostGridPart( grid );

  // create filter
  typedef School::RadialFilter< HostGridPartType > FilterType;
  typename FilterType::GlobalCoordinateType center( 0.5 );
  typename FilterType::ctype radius( .25 );
  FilterType filter( hostGridPart, center, radius );

  // create filtered grid part, only a subset of the host grid part's entities are contained
  typedef Dune::Fem::FilteredGridPart< HostGridPartType, FilterType > GridPartType;
  GridPartType gridPart( hostGridPart, filter );
  const double exactTotalVolume = std::pow(radius*radius*M_PI,GridPartType::dimension/2.0) /
                                  tgamma(GridPartType::dimension/2.0+1.);

  // convert the continuously given problem into a grid function
  typedef Dune::Fem::GridFunctionAdapter< FunctionType, GridPartType > GridExactSolutionType;
  GridExactSolutionType exact( "exact solution", function, gridPart, 5 );

  // obtain type definitions
  // note: the keywords "typename" and "template" are necessary (and allowed)
  //       only, because this function is itself a template
  typedef typename GridPartType::template Codim< 0 >::IteratorType IteratorType;
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
  typedef typename GridPartType::template Codim< 0 >::GeometryType GeometryType;

  // obtain range type of exact solution
  typedef typename FunctionType::RangeType RangeType;

  // initialize average and totalVolume
  double totalVolume = 0;
  RangeType average( 0 );

  // iterate over the grid
  const IteratorType end = gridPart.template end< 0 >();
  for( IteratorType it = gridPart.template begin< 0 >(); it != end; ++it )
  {
    // obtain a reference to the entity
    const EntityType &entity = *it;

    // obtain the geometry (note: this is not a reference, but the object itself)
    const GeometryType geometry = entity.geometry();

    RangeType value;

    // obtain the volume
    const double volume = geometry.volume();
    // update totalVolume
    totalVolume += volume;

    // update average
    /***** Simple one point quadrature:
           simply use a midpoint quadrature to compute int_E u
    function.evaluate( geometry.center(), value );
    average.axpy( volume, value );
    *****/
    /**** Use a higher order quadrauture ****/
    const int quadOrder = 2;
    Dune::Fem::CachingQuadrature< GridPartType, 0 > quadrature( entity, quadOrder );
    for( std::size_t p = 0; p < quadrature.nop(); ++p )
    {
      // evaluate function
      function.evaluate( geometry.global( quadrature.point( p ) ), value );

      // obtain weight of quadrature point
      const double weight = geometry.integrationElement( quadrature.point( p ) ) * quadrature.weight( p );

      // update average and totalVolume
      average.axpy( weight, value );

    }
  }

  // finalize compuatation of average
  average /= totalVolume;
  // the error is the difference between the exact domain size
  error = std::abs( totalVolume - exactTotalVolume );

  // just print it to the console
  std::cout << "Domain size: " << totalVolume << std::endl;
  std::cout << "Average: " << average << std::endl;
  // setup tuple of functions to output (discrete solution and exact solution)
  typedef Dune::tuple< GridExactSolutionType * > IOTupleType;
  IOTupleType ioTuple( &exact );

  // write data
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  DataOutputType dataOutput( grid, ioTuple, MyDataOutputParameters( step ) );
  dataOutput.write();

  // later we will return an approximation error
  return error;
}

// Main function
// -------------

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType HGridType;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType &grid = *gridPtr;

  // do initial load balance
  grid.loadBalance();

  // read parameter: initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "intro.level" );

  // read parameter: number of EOC loops
  const int repeats = Dune::Fem::Parameter::getValue< int >( "intro.repeats", 0 );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // globally refine grid such that the grid width is halfed level times
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );

  // calculate first step
  double oldError = algorithm( grid, 0 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine grid globally (such that grid width is halfed)
    // note: This function correctly adjusts memory for discrete functions, etc.
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    const double newError = algorithm( grid, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }

  return 0;
}
catch( const Dune::Exception &exception )
{
  // handle Dune exceptions by simply displaying their message
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

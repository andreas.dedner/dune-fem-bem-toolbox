
      program fe2d

      include "fe2dnag.inc"

      use CONTROL, only : tol, fitaa, fitbb, skyline, cflr, step, rho, sphereish, pi, disgal, axisym, bres, VolumeUpdate
      use CONTROL, only : fita, fitb, dura, coun, symt, pard, omeg, fitn, fish, prefix, anal, legendre, splines, oblatexpt
      use CONTROL, only : inviscid, blob, air, control_load, paramzap, relaxing, setup, oblatejump, heat, masscheck, drag
      use CONTROL, only : firsttime, maxvel, bemo, rifsym, o, vortcheck, resist, perfect, face
      use radrop_util, only : ch, nch
      use SIZES, only : set, width, c3maxnod, maxnod, maxele, omaxnod, maxsid
      use PLACE, only : rc,cc,rr,oor, jumpcheck, ppalcheck, HX, HY, isvroot, isproot, noderoot
      use PLACE, only : skyinit
      use BOUNDARY, only : SplineSetup
      use TEMPERATURE, only : setuptemp, loadtemp
      use RESISTANCE, only : dragSetup, dragCalculate

      implicit none

      ! Set-up some NaNs
      integer   iNaN;         real rNaN;   double precision dNaN

      integer  maxnid, i, j, nods, eles, nuds, emes, nids, enes, ii, jj
      integer  err, er1, er2, er3, er4, er5, xnd, ynd
      integer  nomat
      real sx, sy
      character choice*1
           
      integer,     allocatable::Connect(:,:),List(:),Jist(:),Pist(:),Zist(:)
      real,        allocatable::X(:),Y(:),Mesh(:,:),iX(:),iY(:)
      double precision, allocatable, target :: S(:)
      double precision, allocatable         :: T(:,:),Z(:,:),CP(:,:),R(:,:),L(:)

      ! Single Layer Potential BEM Matrices
      double precision, allocatable::Vxx(:,:),Vxy(:,:),Vyy(:,:)

      ! First Double Layer Potential BEM Matrices
      double precision, allocatable::Kxx(:,:),Kxy(:,:),Kyy(:,:)

      ! Second Double Layer Potential BEM Matrices
      double precision, allocatable::Lxx(:,:),Lxy(:,:),Lyy(:,:)

      ! Hyper-Singular Operator BEM Matrices
      double precision, allocatable::Wxx(:,:),Wxy(:,:),Wyy(:,:)

      ! Pressure BEM Matrices
      double precision, allocatable::Px(:,:),Py(:,:),Qx(:,:),Qy(:,:)

      ! Pressure Compatibility Row Vectors
      double precision, allocatable::Ex(:),Ey(:)

      ! Solution Vector
      double precision, allocatable::P(:)

      ! Mesh Velocity Vector
      double precision, allocatable::Q(:)

      integer, allocatable:: sortord(:)
      real, allocatable:: sortcrit(:)

      ! Node Type Array
      character*1, allocatable::Type(:), iType(:)

      double precision area, totarea, volu, totvolu, kine, totkine, surener, coulene
      logical abortproc, verbose

      integer(kind=2) errorcode

      save
      
      !call ATTACH@('C:\Users\Orange\Documents\blob\d.NEW.RESIST_0.120',errorcode)

      call timey('ALL','BEG')

      inquire(file='verbose.msh',exist=verbose)

      iNaN = 9*10**8; rNaN = 9.9e28; dNaN = 9.9e28

      open(58,file='results.dat')

      call setup()

      ! load any non-default values from the control.dat file
      call control_load

! ### ! Allocate Arrays needed for Initial Mesh Retrieval
      allocate(iX(-maxsid+1:maxnod),iY(-maxsid+1:maxnod),Mesh(11,maxele),L(3*maxnod),iType(-maxsid+1:maxnod),  &  !stat=err)
               sortcrit(maxele),sortord(maxele),stat=err)
      if(err.ne.0) then
        print*,'Insufficient Memory for Initial Mesh Retrieval!'
        stop
      else
        iX = rNaN; iY = rNaN; Mesh = rNaN; L = 0.0d0; iType = 'U'
      endif
  
!  A  ! Retrieve the Initial Mesh from the Input File
      call Mesh_Retrieve(iX(1),iY(1),L,Mesh,nomat,sortord,sortcrit)
      
      if(nomat.lt.0) then
        ! Flag for a FEM only problem
        nomat = -nomat
        call paramzap()
        choice = 'D'
      else if(nomat.ne.0) then
        ! Regular FEM/BEM coupling problem
        choice='I'
      else
        ! If no materials present, must be a BEM only problem
        choice='G'
      endif

! ### ! Allocate Arrays needed for Mesh Construction   
      allocate(Connect(maxele,10),R(3,maxele),stat=err)
      if(err.ne.0) then
        print*,'Insufficient Memory for Mesh Construction!'
        stop
      else
        Connect = iNaN; R = dNaN
      endif

!  B  ! Generate the Main Mesh Used by the Program
      call Mesh_Generator(Connect,iX,iY,Mesh,L,R,iType,choice)

! ### ! Allocate Arrays dependent on the Actual Number of Unknowns (maxnod)
      if(skyline) then
        allocate(S(3*maxnod*width),stat=er1)
      else
        allocate(S(3*maxnod*3*maxnod),stat=er1)
      end if
      allocate(P(-3*maxsid+1:3*maxnod),Q(2*maxnod),List(maxnod),Jist(maxnod),Pist(maxnod),stat=er2)
      allocate(X(-maxsid+1:maxnod),Y(-maxsid+1:maxnod),Type(-maxsid+1:maxnod),stat=er3)
      if(er1+er2+er3.ne.0) then
        print*,'Insufficient Memory for System Matrix!'
        stop
      end if

      ! Place node positions where they belong
    8 fitaa = 0.0; fitbb = 0.0
      do err = -maxsid+1, maxnod
        if(firsttime) then
          X(err) = iX(err); Y(err) = iY(err); Type(err) = iType(err)
        end if
        if(err.gt.0) then
          if(X(err).lt.tol) fitaa = max(fitaa,Y(err))
          if(Y(err).lt.tol) fitbb = max(fitbb,X(err))
        end if
      end do
      ! Flag when close to the sphere (not outgoing prolates)
      if(heat.or.(.not.relaxing).or.drag) then
        sphereish = .false.
      else if((fitaa.gt.tol).and.(fitbb.gt.tol)) then
        sphereish = max( fitaa / fitbb , fitbb / fitaa ) .lt. 1.2
      else
        sphereish = .false.
      end if
      ! Turn off any Legendering when too prolate
      legendre = legendre .and. (fitaa.lt.1.2*fitbb)

      ! Set-up solution vector P with the solution at the previous timestep if applicable
      if(choice.eq.'D') then
        rho = - abs(rho)
      else if(choice.eq.'G') then
        rho = 0.0d0
      else
        call maxVelocity(L,P(1),Q,Connect,firsttime)

        ! Set-up timestep using specified CFL ratio and the maximum velocity
        maxvel = sqrt(maxvel)
        step = dble(cflr * bres) / max(maxvel,0.1d0)
!        write(*,'(a11,4f18.9)') 'INFO: CFL: ',cflr,bres,maxvel

      end if

      ! Create / clean skyline storage arrays if applicable
      call skyinit(firsttime)

!  I  ! Check History Output
      !call historyOutput(omaxnod,maxnod,maxsid,maxele,X,Y,P,Connect,R)
      
      ! Initialize arrays
      S = 0.0d0; L = 0.0d0; List = -1; Jist = -1; Pist = -1

!  #  ! Deallocate the Arrays with Initial maxnod Sizes
      !deallocate(Mesh,iX,iY,iType)

      if(firsttime) then
        call ppalcheck()!,Type(1),X(1),Y(1))
        if(verbose) call Output(X,Y,P,Connect,Type,List,Jist,0,Pist,40,40,R)
      end if

      ! Communicate expected number of traction coefficients to drag routines
      if((drag.or.resist).and.(.not.bemo)) call dragSetup(1000)

!  C  ! Create Diagnostic Output for the Mesh                        
      call timey('FEM','BEG')

      totarea = 0.0d0; totvolu = 0.0d0; totkine = 0.0d0

!  D  ! Embed the Finite Elements
      do i=1, maxele
        if(R(1,i).gt.599) cycle
        !print*,'System Matrix is ',sysym(3*maxnod,S,maxnod,Type(1)),' % Symmetric before embedding element ',i
        call Embed_Element(X(1),Y(1),Type(1),Connect,R,P,Q,L,S,i,area,volu,kine)
        totarea = totarea + area
        totvolu = totvolu + volu
        totkine = totkine + kine
      end do

      ! Total kinetic energy as would appear for the full (not half) droplet divided by the surface energy for the sphere
      ! totkine = ( 0.5 * rho * 2 pi * totkine ) * 2 / ( 4 pi / ohne )
      totkine = totkine / 2.0d0

      ! Print out velocity divergence (of previous iteration) if requested
      if(masscheck.or.vortcheck) then
        call Output(X,Y,P,Connect,Type,List,Jist,0,Pist,60,60,R)
      end if

      call timey('FEM','END')

! === ! Deallocate Arrays used for Mesh Retrieval/Construction
      !deallocate(R,P)

      ! Add contributions to the RHS from traction integrals around Dirichlet boundaries (if any)
      !if(disgal) then
      !  call Catch('P','Q',' ',' ',maxnod,X(1),Y(1),Type(1),List,nods,eles,.false.)     ! List: Boundary Velocity
      !else
      !  call Catch('D','E',' ',' ',maxnod,X(1),Y(1),Type(1),List,nods,eles,.false.)     ! List: Boundary Velocity
      !endif

      !call Integral_RHS(maxnod,nods,X(1),Y(1),Type(1),L,List)

      if(axisym) then
        call Catch(' ',' ','X',' ',X(1),Y(1),Type(1),List,nods,eles,.false.)
        xnd = List(1)
        call Catch(' ',' ',' ','Y',X(1),Y(1),Type(1),List,nods,eles,.false.)
        ynd = List(1)
      end if

      ! Update the volume for the contact angle stuff
      if(xnd.ne.-1) call VolumeUpdate(totvolu,P(rr(1,xnd)))

      ! FEM or FEM/BEM
      if(disgal) then
        call Catch('J','K','X','Y',X(1),Y(1),Type(1),List,nods,eles,.true.)     ! List: Boundary Velocity
      else
        call Catch('H','I','X','Y',X(1),Y(1),Type(1),List,nods,eles,.false.)     ! List: Boundary Velocity
      endif

      ! BEM only
      if(List(1).eq.-1) then
        call Catch('H','W',' ',' ',X(1),Y(1),Type(1),List,nods,eles,.false.)  ! List: Boundary Velocity
        call Catch('T',' ',' ',' ',X(1),Y(1),Type(1),Jist,nuds,emes,.false.)  ! Jist: Boundary Traction
      else
        call Catch('T','W',' ',' ',X(1),Y(1),Type(1),Jist,nuds,emes,.false.)  ! Jist: Boundary Velocities for BEM
        if(disgal) call Catch('G',' ',' ',' ',X(1),Y(1),Type(1),Pist,nids,enes,.false.)  ! Pist: Traction Step Functions
      endif

      ! Ellipsoid fit parameter calculation
      if((choice.ne.'D').and.(choice.ne.'G')) call abnfit(nods,List,X(1),Y(1))
      if(splines.and.(choice.ne.'D').and.(choice.ne.'G')) call SplineSetup(nods,List,X(1),Y(1))

      ! Set number of boundary nodes
      if((nods.gt.nuds).and.(.not.inviscid).and.air) then
        print*,'More Boundary Velocity Nodes than Traction Nodes!!!!'
        call Output(X,Y,P,Connect,Type,List,Jist,nids,Pist,40,40,R)
        stop
      else
        maxnid=nods
        if(rifsym) o = 3 - maxnid
        if(nods.ne.nuds) then
          do i = nods+1, nuds
            j = Jist(i)
            Type(j) = 'D'
            !do ii = 1, 3
            !  do jj = 1, maxnod
            !    S( rc(1,j,ii,jj) ) = 0.0d0; S( rc(2,j,ii,jj) ) = 0.0d0; S( rc(3,j,ii,jj) ) = 0.0d0
            !  end do
            !end do
            S( rc(1,j,1,j) ) = 1.0d0; S( rc(2,j,2,j) ) = 1.0d0; S( rc(3,j,3,j) ) = 1.0d0
            L( cc(1,j)     ) = 0.3d0; L( cc(2,j)     ) = 0.3d0; L( cc(3,j)     ) = 0.3d0
          end do
        endif

        ! Set the Normals to the New Boundary Location
        call Normal(-10001.0,-10001.0)
        do i = o, maxnid
          ii = i
          call qmap(maxnid,ii,sx,sy)
          call Normal(20000.0+sx*X(List(ii)),20000.0+sy*Y(List(ii)))
        end do

        ! Output Diagnostics Data
        !call Output(X,Y,P,Connect,Type,List,Jist,nids,Pist,40,40,R)
        if(verbose.and.(.not.firsttime)) call Output(X,Y,P,Connect,Type,List,Jist,maxnid,List,59,59,Ex)
      endif

! ### ! Allocate Space for the Exterior Surface Integral Matrices
      if(firsttime) then
        allocate(Vxx(o:maxnid,o:maxnid),Vxy(o:maxnid,o:maxnid),Vyy(o:maxnid,o:maxnid),stat=er1)     
        allocate(Kxx(o:maxnid,o:maxnid),Kxy(o:maxnid,o:maxnid),Kyy(o:maxnid,o:maxnid),stat=er2)     
        allocate(Lxx(o:maxnid,o:maxnid),Lxy(o:maxnid,o:maxnid),Lyy(o:maxnid,o:maxnid),stat=er3)     
        allocate(Wxx(o:maxnid,o:maxnid),Wxy(o:maxnid,o:maxnid),Wyy(o:maxnid,o:maxnid),stat=er4)
        allocate(Px(o:maxnid,o:maxnid),Py(o:maxnid,o:maxnid),Qx(o:maxnid,o:maxnid),Qy(o:maxnid,o:maxnid),stat=er5)
        allocate(T(o:maxnid,o:maxnid),Z(o:maxnid,o:maxnid),CP(o:maxnid,o:maxnid),Ex(o:maxnid),Ey(o:maxnid),stat=err)
      endif
      if(firsttime.and.(er1+er2+er3+er4+er5+err.ne.0)) then
        print*,'Insufficient Memory for BEM Matrices!'     
        stop
      else
        Vxx = 0.0d0; Kxx = 0.0d0; Lxx = 0.0d0; Wxx = 0.0d0
        Vxy = 0.0d0; Kxy = 0.0d0; Lxy = 0.0d0; Wxy = 0.0d0
        Vyy = 0.0d0; Kyy = 0.0d0; Lyy = 0.0d0; Wyy = 0.0d0; T  = 0.0d0
        Px  = 0.0d0; Py  = 0.0d0; Qx  = 0.0d0; Qy  = 0.0d0; Z  = 0.0d0; CP  = 0.0d0
        if(firsttime.or.perfect) then
          Ex = 0.0d0; Ey = 0.0d0
        end if
        ! For imperfect conductors, wipe the mesh velocity vector here, as will be hijacked to form the surface normal velocity
        if(.not.perfect) Q = 0.0d0
      endif

      ! Grab the free boundary temperatures (for heated expts) just before old solution is wiped
      if(heat) then
        if(firsttime) then
          call setuptemp(maxnid)
        else
          call Catch('H','I',' ',' ',X(1),Y(1),Type(1),Pist,nods,eles,.false.) 
          do i = 1, nods
            call loadtemp(i+1, P(rr(4,Pist(i))) )
          end do
        end if
      end if

      !if(control('fast').ne.0.0) then PP = 0.0d0 goto 777 endif

      call timey('BEM','BEG')

!  E  ! FEM/BEM or BEM only
      if((choice.ne.'D').and.((.not.drag).or.bemo)) &
      call Integral_Element(maxnid,X(1),Y(1),Type(1),L,List,Jist,Pist,Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy, &
                                                                    Wxx,Wxy,Wyy,T,Px,Py,Qx,Qy,Z,CP,Q(1),Q(maxnod+1),.False.)
      call timey('BEM','END')

! === ! Dealloacte the Load Vector
      ! deallocate(L)

      ! Collect ids of actual Non-Conforming Basis-Functions where things are to be interpolated
      if(disgal) call Catch('H','I',' ',' ',X(1),Y(1),Type(1),List,nods,eles,.false.)     ! List: Boundary Velocity

      ! Grab the normal velocity of the surface before it gets scrubbed for imperfect conductor case
      if((.not.perfect).and.(.not.firsttime)) then
        do i = 2, nods + 1
          Q(i)        = Q(i)        * P(rr(1,List(i-1)))
          Q(i+maxnod) = Q(i+maxnod) * P(rr(2,List(i-1)))
          Ey(i)       = dble(face) * ( Ey(i) * ( Q(i) + Q(i+maxnod) ) )
        end do
      end if

! ### ! Allocate Arrays for the Solution
      P = 0.0d0
      do i = 1, 3*maxnod
        P(i) = L(i)
      end do
    
!  F  ! Interpolate the Dense BEM Matrices into the Main Sparse System Matrix
      if((.not.inviscid).and.air.and.(choice.ne.'D')) &
      call Inter(maxnid,List,Jist,Pist,S,Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy,Wxx,Wxy,Wyy,T,Px,Py,Qx,Qy,Z,P(1),xnd,ynd)

      if(firsttime) allocate(Zist(maxnid+1),stat=er1)
      if(firsttime.and.(er1.ne.0)) then
        print*,'Insufficient Memory for extra list !!!'
        stop
      end if

      if(disgal) then
        Jist = List
        call Catch('J','K','X','Y',X(1),Y(1),Type(1),List,nods,eles,.true.)

        do i = 1, maxnid + 1
          Zist(i) = Pist(i)
        end do

        call Catch('T','W',' ',' ',X(1),Y(1),Type(1),Pist,nids,enes,.false.)

      endif

      ! Put in Surface Tension Forces to the RHS
      if((choice.ne.'D').and.(.not.drag)) then
        call Tension(maxnid,List,Jist,Pist,Zist,Kxx,Kxy,Kyy,T,Wxx,Wxy,Wyy,Qx,Qy,Z,CP,X(1),Y(1),P(1),Ex,Ey,xnd,ynd, &
                                                                                                          surener,coulene)
      else
        surener = 0.0d0; coulene = 0.0d0
      end if

      ! Diagnostic Output
      ! call Matrix_Write(maxnod,Type(1),S)!,P(1))

      ! Check the edge jumps
!      call jumpcheck(X(1),Y(1))!,P(1))

      call timey('FAC','BEG')

      if((pard.ne.0.0).or.(symt.ne.0.0)) then

        call paradiso(S,P(1),L)

      else

!  G    ! Factorize the System Matrix
        call LU_FactorizationLight(c3maxnod,S)

        call timey('FAC','END')
        call timey('SOL','BEG')

!  H    ! Back-Substitute for the Solution
        call SolverLight(c3maxnod,S,P(1),1)

      endif

      call timey('SOL','END')

      ! Collect ids of actual Non-Conforming Basis-Functions where things are to be interpolated
      if(disgal.and.resist) call Catch('H','I',' ',' ',X(1),Y(1),Type(1),List,nods,eles,.false.)     ! List: Boundary Velocity

      if(drag.or.resist) call dragCalculate(P(1),totkine,surener,coulene,nods,List,Ey)

      ! Divergence output file for actual ...              and    example solution
      !open(99,file='soldiv.pos')                             ;    open(98,file='exdiv.pos')
      !write(99,'(a29)')'View "Solution Divergence" {'        ;    write(98,'(a29)')'View "Example Divergence" {'

      ! Output some results
      if(choice.ne.'D') then
        ! Invert areas and volumes if drag
        if(drag) then
          totarea = dble(2.0*fitaa*fitbb) - totarea
          totvolu = dble(fitaa*fitbb*fitbb) - totvolu
          write(*,'(a5,f12.5)') 'SA = ', totkine
          totkine = 0.0d0
        end if
        totarea = 100.0d0 * totarea / (pi/4.0d0); totvolu = 100.0d0 * totvolu / (1.0d0/3.0d0)
        write(58,'("I = ",i5," T = ",f9.4, &
      &" A = ",f5.3," B = ",f5.3," A/B = ",f5.3," N = ",f5.3," AR = ",f6.2," % VL = ",f6.2," % Q = ",f6.1, &
      &" E = ",f12.5," + ",f12.5," + ",f12.5," = ",f12.5," MV = ",f12.3 &
       &          )') &
                       nint(coun),        dura, &
        fitaa,fitbb,fita/fitb,fitn,totarea,totvolu,omeg,totkine,surener,coulene,totkine+surener+coulene,maxvel
      end if

      ! Calculate the Norms or Check mass conservation enfor
      do i=1, maxele
        if(R(1,i).gt.599) cycle
        if((.not.blob).or.(choice.eq.'D').or.drag) then
          call Element_Norm(X(1),Y(1),Connect,P(1),i)
        else
          !call Check_Element(X(1),Y(1),Connect,P(1),i)
        end if
      end do

      !write(99,'(a2)')'};'                                   ;    write(98,'(a2)')'};'
      !write(99,'(a1)')' '                                    ;    write(98,'(a1)')' '
      !close(99)                                              ;    close(98)

      if((.not.blob).or.(choice.eq.'D').or.drag) call Element_Norm(X,Y,Connect,P(1),0)

      ! Calculate the Pressure Constant
      ! call constant(maxnod,maxnid,List,Jist,Zist,Ex,Ey,X(1),Y(1),P(1))

      call timey('SPE','BEG')

      ! Fill in the "Specials"
      call Integral_Element(maxnid,X(1),Y(1),Type(1),P(1),List,Jist,Pist, &
                                                    Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy,Wxx,Wxy,Wyy,T,Px,Py,Qx,Qy,Z,CP,Ex,Ey,.True.)
      call timey('SPE','END')

!  I  ! Produce Numeric Solution Output
      if(verbose) then
        if(drag.or.heat) then
          call Output(X,Y,P,Connect,Type,List,Jist,nids,Pist,41,44,R)
        else
          call Output(X,Y,P,Connect,Type,List,Jist,nids,Pist,42,42,R)
        end if
      end if

      ! Looped problem
      if((anal.ge.5.0).and.(anal.le.8.0).and.(choice.ne.'D').and.(.not.drag)) then

        ! Nudge the mesh points to their new positions (not for FEM only cases)
        if(choice.ne.'D') &
        call Nudge(Connect,List,Jist,Pist,X,Y,L,P,Q,Type(1),R,totvolu)

        if(firsttime) firsttime = .false.

        ! Turn off perfect conductor after initial movement if requested
        if(resist.and.perfect.and.(fitaa.gt.-1.1*fitbb)) perfect = .false.

        coun = coun - 1.0
        dura = dura - real(step)

        inquire(file='stop.msh',exist=abortproc)

        if(abortproc) then
          print*,'Program aborted at request.'
          close(58)
          open(58,file='stop.msh')
          close(58,status='delete')
        else if((coun.eq.0.0 ).or.( dura.le.0.0)) then
          print*,'Program successfully completed.'
          close(58)
        else
          
          ! Adjust the output file prefixer
          prefix = ch(-real(floor(coun)))

          ! Organise the oblate fissility jumps
          call oblatejump(fish)

          ! Otherwise print-out graphics files every * iterations
          if(oblatexpt) then
            fish = fish .or. ((modulo(nint(coun),250).eq.0).or.(nint(coun).eq.1))
          else
            fish = fish .or. ((modulo(nint(coun),100).eq.0).or.(nint(coun).eq.1))
          end if

          ! Print-out the iteration completed to the screen
          write (*, 10, advance='no') floor(coun)
   10     format (i5)
     
         !     Newline to complete
         !      write (*, 10)

          ! Loop round for a new iteration
          goto 8
        end if
      else

!  J    ! Produce Analytic Solution Output
        call Output(X,Y,P,Connect,Type,List,Jist,nids,Pist,45,47,R)

      end if
      
! === ! Deallocate All Remaining Arrays
      !deallocate(X,Y,Type,Connect,Order,P)

      ! Terminate the Program
      print*,'The End.'

      call timey('ALL','END')

      stop
      end
     
           
      ! Alternative Solver
      !allocate(Order(3*maxnod),stat=err)
      !call ludcmp(S,3*maxnod,Order)
      !read*,i
      !call Diagonal_Write(3*maxnod,S,P)   
      !call lubksb(S,3*maxnod,Order,P)

                      
      subroutine Inter(maxnid,List,Jist,Pist,S,Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy,Wxx,Wxy,Wyy,T,Px,Py,Qx,Qy,Z,P, &
                                                                                                                       xnod,ynod)

      use CONTROL, only : disgal, free
      use PLACE, only : rr, rc
      use SIZES, only : maxnod

      implicit none
      integer maxnid, i, j, k, inod, jnod, inud, jnud, inid, jnid, ined, jned, List(maxnod), Jist(maxnod), Pist(maxnod)
      integer ione, itwo, jone, jtwo, xnod, ynod
      double precision S(3*maxnod*3*maxnod), T(maxnid,maxnid), Z(maxnid,maxnid), P(3*maxnod)
      double precision Vxx(maxnid,maxnid),Vxy(maxnid,maxnid),Vyy(maxnid,maxnid)
      double precision Kxx(maxnid,maxnid),Kxy(maxnid,maxnid),Kyy(maxnid,maxnid)
      double precision Lxx(maxnid,maxnid),Lxy(maxnid,maxnid),Lyy(maxnid,maxnid)
      double precision Wxx(maxnid,maxnid),Wxy(maxnid,maxnid),Wyy(maxnid,maxnid)
      double precision Px(maxnid,maxnid),Py(maxnid,maxnid),Qx(maxnid,maxnid),Qy(maxnid,maxnid)
      logical dummey

      ! Nothing to do if no BEM Matrices to Interpolate ...
      if((List(1).eq.-1).or.(Jist(1).eq.-1)) return

      if(.not.disgal) then
        ! Double-up the Existing System Matrix and Load Vector
        S = 2.0d0 * S; P = 2.0d0 * P
      endif
      
      ! Loop over Rows of BEM Matrices
      do 20 i = 1, maxnid

        ! Identify Corresponding Rows of System Matrix
        inod=List(i); inud=Jist(i); inid=Pist(i); ione=Pist(1); itwo=Pist(2)

        if(free) then
          ! Locate exterior pressures on Traction Boundary
          ined = inud
        else
          ! Locate exterior pressures on Velocity Boundary
          ined = inod
        endif

        ! Flag for dummey pressure node (on traction boundary)
        if(disgal) then
          dummey = .false.
        else
          dummey = Z(i,i).eq.0.0
        endif

        ! Pin the Pressure at non-dummey "Traction Nodes" to that (calculated) at "Velocity Nodes" to avoid a singular system
        if((.not.free).and.(.not.dummey)) then
          S(  rc(3,inud,3,inud)  ) = + 1.0d8
          S(  rc(3,inud,3,inod)  ) = - 1.0d8
        endif

        ! Wipe boundary pressure rows
        if(.not.dummey) then
          do k = 1, 3
            do j = 1, maxnod
              S(  rc(3,ined,k,j) ) = 0.0d0
            end do
          end do
          P(  rr(3,ined)     ) = 0.0d0
        endif

        ! Loop over Columns of BEM Matrices
        do 10 j = 1, maxnid

          ! Identify Corresponding Columns of System Matrix
          jnod=List(j); jnud=Jist(j); jnid=Pist(j); jone=Pist(1); jtwo=Pist(2)

          if(free) then
            ! Locate exterior pressures on Traction Boundary
            jned = jnud
          else
            ! Locate exterior pressures on Velocity Boundary
            jned = jnod
          endif

          ! Interpolate the Hyper-Singular Operator Matrix (W)
          if(disgal) then
            S(  rc(1,inud,1,jnud)  ) = - Wxx(i,j)
            S(  rc(1,inud,2,jnud)  ) = - Wxy(i,j)
            S(  rc(2,inud,1,jnud)  ) = - Wxy(i,j)
            S(  rc(2,inud,2,jnud)  ) = - Wyy(i,j)
          end if
          if((inod.ne.ynod).and.(.not.disgal)) then
            S(  rc(1,inod,1,jnod)  ) = S(  rc(1,inod,1,jnod)  ) - Wxx(i,j)
            S(  rc(1,inod,2,jnod)  ) = S(  rc(1,inod,2,jnod)  ) - Wxy(i,j)
          end if
          if((inod.ne.xnod).and.(.not.disgal)) then
            S(  rc(2,inod,1,jnod)  ) = S(  rc(2,inod,1,jnod)  ) - Wxy(i,j)
            S(  rc(2,inod,2,jnod)  ) = S(  rc(2,inod,2,jnod)  ) - Wyy(i,j)
          endif

          ! Interpolate the Adjoint Double Layer Potential Matrix (-I+K')
          if(disgal) then
            S(  rc(1,inud,1,jnid)  ) = + Lxx(i,j) + T(j,i)
            S(  rc(1,inud,2,jnid)  ) = + Lxy(i,j)
            S(  rc(2,inud,1,jnid)  ) = + Lxy(i,j)
            S(  rc(2,inud,2,jnid)  ) = + Lyy(i,j) + T(j,i)
          end if
          if((inod.ne.ynod).and.(.not.disgal)) then
            S(  rc(1,inod,1,jnud)  ) = + Lxx(i,j) - T(i,j)
            S(  rc(1,inod,2,jnud)  ) = + Lxy(i,j)
          end if
          if((inod.ne.xnod).and.(.not.disgal)) then
            S(  rc(2,inod,1,jnud)  ) = + Lxy(i,j)
            S(  rc(2,inod,2,jnud)  ) = + Lyy(i,j) - T(i,j)
          endif

          ! Interpolate the Double Layer Potential Matrix (-I+K)
          if(disgal) then
            S(  rc(1,inid,1,jnud)  ) = + Kxx(i,j) + T(i,j)
            S(  rc(1,inid,2,jnud)  ) = + Kxy(i,j)
            S(  rc(2,inid,1,jnud)  ) = + Kxy(i,j)
            S(  rc(2,inid,2,jnud)  ) = + Kyy(i,j) + T(i,j)
          end if
          if((inod.ne.ynod).and.(.not.disgal)) then
            S(  rc(1,inud,1,jnod)  ) = + Kxx(i,j) - T(i,j)
            S(  rc(1,inud,2,jnod)  ) = + Kxy(i,j)
          end if
          if((inod.ne.xnod).and.(.not.disgal)) then
            S(  rc(2,inud,1,jnod)  ) = + Kxy(i,j)
            S(  rc(2,inud,2,jnod)  ) = + Kyy(i,j) - T(i,j)
          endif

          ! Interpolate the Dudu matrix
          if(disgal) then
            S(  rc(1,inod,1,jnid)  ) = - Z(j,i)
            S(  rc(2,inod,2,jnid)  ) = - Z(j,i)

            S(  rc(1,inid,1,jnod)  ) = - 2.0 * Z(i,j)
            S(  rc(2,inid,2,jnod)  ) = - 2.0 * Z(i,j)

            ! Stuff for finding the constants
            S(  rc(1,inud,3,jone)  ) = S(  rc(1,inud,3,jone)  ) + T(j,i)
            S(  rc(2,inud,3,jtwo)  ) = S(  rc(2,inud,3,jtwo)  ) + T(j,i)

            S(  rc(3,ione,1,jnud)  ) = S(  rc(3,ione,1,jnud)  ) + T(i,j)
            S(  rc(3,itwo,2,jnud)  ) = S(  rc(3,itwo,2,jnud)  ) + T(i,j)

            ! Avoid singular system by giving pressures on (step) traction nodes something to be ...
            if((i.gt.2).and.(inid.eq.jnid)) S(  rc(3,inid,3,jnid)  ) = 1.0d0
          endif

          ! Interpolate the Single Layer Potential Matrix (V)
          if(disgal) then
            S(  rc(1,inid,1,jnid)  ) = - Vxx(i,j)
            S(  rc(1,inid,2,jnid)  ) = - Vxy(i,j)
            S(  rc(2,inid,1,jnid)  ) = - Vxy(i,j)
            S(  rc(2,inid,2,jnid)  ) = - Vyy(i,j)
          end if
          if((inod.ne.ynod).and.(.not.disgal)) then
            S(  rc(1,inud,1,jnud)  ) = - Vxx(i,j)
            S(  rc(1,inud,2,jnud)  ) = - Vxy(i,j)
          else if((inud.eq.jnud).and.(.not.disgal)) then
            S(  rc(1,inud,1,jnud)  ) = 1.0d0
          end if
          if((inod.ne.xnod).and.(.not.disgal)) then
            S(  rc(2,inud,1,jnud)  ) = - Vxy(i,j)
            S(  rc(2,inud,2,jnud)  ) = - Vyy(i,j)
          else if((inud.eq.jnud).and.(.not.disgal)) then
            S(  rc(2,inud,2,jnud)  ) = 1.0d0
          endif

          ! If not a dummey pressure node
          if(.not.dummey) then

            ! Interpolate the Pressure Double Layer Potential (Q)
            if(disgal) then
              S(  rc(3,ined,1,jnud)  ) = Qx(i,j)
              S(  rc(3,ined,2,jnud)  ) = Qy(i,j)
            else
              S(  rc(3,ined,1,jnod)  ) = Qx(i,j)
              S(  rc(3,ined,2,jnod)  ) = Qy(i,j)
            end if

            ! Interpolate the Pressure Single Layer Potential (P)
            if(disgal) then
              S(  rc(3,ined,1,jnid)  ) = Px(i,j)
              S(  rc(3,ined,2,jnid)  ) = Py(i,j)
            else
              S(  rc(3,ined,1,jnud)  ) = Px(i,j)
              S(  rc(3,ined,2,jnud)  ) = Py(i,j)
            endif

            ! Interpolate the Boundary Mass Matrix (Z)
            if(disgal) then
              S(  rc(3,ined,3,jned)  ) = T(j,i)
            else
              S(  rc(3,ined,3,jned)  ) = Z(i,j)
            endif

          else

            ! Take Pressure Interpolation Factors for Traction Boundary from those for Velocity Boundary
            S(  rc(3,inud,3,jnud)  ) = S(  rc(3,inod,3,jnod)  )

          endif

   10   continue

   20 continue

      return
      end

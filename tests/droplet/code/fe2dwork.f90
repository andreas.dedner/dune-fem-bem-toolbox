
      real function U_ana(x,y)

      use CONTROL, only : oneOverFourPi, anal, drag, flow

      implicit none
      real x, y, a, t, r, s, c

      a = 0.15

      if(drag) then
        ! http://www.kirbyresearch.com/index.cfm/wrap/textbook/microfluidicsnanofluidicsse46.html
        a = 1.0
        t = atan2(x,-y); r = sqrt(x*x+y*y); s = sin(t); c = cos(t)
        U_ana = flow * s * c * 0.75 * ( a**3/r**3 - a/r )
      else if(anal.eq.-1.0) then
        ! x-axis orientated point force Stokeslet
        U_ana = oneOverFourPi  * ( log(1.0/sqrt(x*x+y*y)) + x*x/(x*x+y*y) )
      else if(anal.eq.0.0) then
        U_ana = x/11.0 !0.0
      else if(anal.eq.1.0) then
        U_ana =  -1.11
      else if(anal.eq.2.0) then
        U_ana = y / (x*x + y*y )
      else if(anal.eq.3.0) then
        U_ana = -x*sin(x*y)
      else if(anal.eq.4.0) then
        U_ana = y * cos(a/(x*x+y*y)) / (x*x + y*y )
      else if(anal.eq.16.0) then
        t = atan2(y,x); r = sqrt(x*x+y*y); s = sin(t); c = cos(t)
        U_ana = - r * ( (3.0*s*s-1.0) * c - 3.0*s*c*s )
        !write(*,'(a6,7f12.5)') 'U_ana ',x,y,t,r,s,c,U_ana
      else if(anal.eq.5.0) then
        U_ana = - a * x
!        U_ana = - a * x * y
        U_ana = + a * x * x * x + U_ana
      else
        U_ana = 0.0
      endif

      return
      end 
   


      real function UU_ana(x,y)

      use CONTROL, only : axisym, oneOverFourPi, oneOverEightPi

      implicit none
      real x, y, r

      if(axisym) then
        r = sqrt(x*x+y*y)
        UU_ana = oneOverEightPi  * ( 1.0 / r + x*x/(r*r*r) )
      else
        UU_ana = oneOverFourPi  * ( log(1.0/sqrt(x*x+y*y)) + x*x/(x*x+y*y) )
      end if

      return
      end 
   
      
      
      real function V_ana(x,y)

      use CONTROL, only : oneOverFourPi, anal, drag, flow

      implicit none
      real x, y, a, t, r, s, c

      a = 0.15

      if(drag) then
        ! http://www.kirbyresearch.com/index.cfm/wrap/textbook/microfluidicsnanofluidicsse46.html
        a = 1.0
        t = atan2(x,-y); r = sqrt(x*x+y*y); s = sin(t); c = cos(t)
        V_ana = - flow * ( c * c * ( 1.0 - 1.50 * a/r + 0.50 * a**3/r**3 ) &
                         + s * s * ( 1.0 - 0.75 * a/r - 0.25 * a**3/r**3 ) )
      else if(anal.eq.-1.0) then
        ! x-axis orientated point force Stokeslet
        V_ana = oneOverFourPi  * ( x*y/(x*x+y*y) )
      else if(anal.eq.0.0) then
        V_ana = 0.0
      else if(anal.eq.1.0) then
        V_ana = -2.22
      else if(anal.eq.2.0) then
        V_ana = -x / (x*x + y*y )
      else if(anal.eq.3.0) then
        V_ana = y*sin(x*y)
      else if(anal.eq.4.0) then
        V_ana = -x * cos(a/(x*x+y*y)) / (x*x + y*y )
      else if(anal.eq.16.0) then
        t = atan2(y,x); r = sqrt(x*x+y*y); s = sin(t); c = cos(t)
        V_ana = - r * ( (3.0*s*s-1.0) * s + 3.0*s*c*c )
        !write(*,'(a6,7f12.5)') 'V_ana ',x,y,t,r,s,c,V_ana
      else if(anal.eq.5.0) then
        V_ana = + 2.0 * a * y
!        V_ana = +       a * y * y
        V_ana = - 4.0 * a * x * x * y + V_ana
      else
        V_ana = 0.0
      endif

      return
      end 
   
   
      
      real function VV_ana(x,y)

      use CONTROL, only : axisym, oneOverFourPi, oneOverEightPi

      implicit none
      real x, y, r

      if(axisym) then
        r = sqrt(x*x+y*y)
        VV_ana = oneOverEightPi  * ( x*y/(r*r*r) )
      else
        VV_ana = oneOverFourPi  * ( x*y/(x*x+y*y) )
      end if

      return
      end 
   


      real function P_ana(x,y)

      use CONTROL, only : oneOverTwoPi, anal, scal, zero, drag, flow

      implicit none
      real x, y, r, R0, a, t, c

      a = 0.3

      if(drag) then
        ! http://www.kirbyresearch.com/index.cfm/wrap/textbook/microfluidicsnanofluidicsse46.html
        a = 1.0
        t = atan2(x,-y); r = sqrt(x*x+y*y); c = cos(t)
        P_ana = - flow * c * 1.50 * a/r**2
      else if(anal.eq.-1.0) then
        P_ana = oneOverTwoPi  * x / (x*x+y*y)
      else if(anal.eq.0.0) then
        P_ana = 0.0
      else if(anal.eq.1.0) then
        P_ana =-3.33
      else if(anal.eq.2.0) then!0.76
        R0 = sqrt(2.0*1.16*1.16) * scal
        r = sqrt(x*x + y*y )
        if ( r .lt. R0 ) then
          P_ana = R0 - r
        else
          P_ana = 0.0
        endif
      else if(anal.eq.3.0) then
        P_ana = cos(x*y)
      else if(anal.eq.4.0) then
        R0 = sqrt(2.0*0.76*0.76) * scal
        r = sqrt(x*x + y*y )
        if ( r .lt. R0 ) then
          P_ana = ( R0 - r ) * cos(a/(x*x+y*y))
        else
          P_ana = 0.0
        endif
      else if(anal.eq.16.0) then
        !t = atan2(y,x); r = sqrt(x*x+y*y)!; s = sin(t)
        P_ana = 0.0 !r * r * (3.0*s*s-1.0) / 2.0
      else if(anal.eq.5.0) then
        P_ana = 0.0
!        P_ana = 2.0 * a * y
        P_ana = a * ( 4.0 * x * x - 8.0 * y * y )
      else
        P_ana = 0.0
      endif

      return
      end 
   



      real function PP_ana(x,y)

      use CONTROL, only : oneOverTwoPi

      implicit none
      real x, y

      PP_ana = oneOverTwoPi  * x / (x*x+y*y)

      return
      end 
   



      real function TT_ana(x,y)

      use CONTROL, only : pi

      implicit none
      real x, y, xn, yn

      xn = x
      yn = y
      call Normal(xn,yn)

      TT_ana = (1.0/pi)*x / (x*x+y*y)

      return
      end



      real function Tx_ana(x,y)

      use CONTROL, only : pi, anal
      implicit none
      real x, y, xn, yn, uxdx, uxdy, uydx, uydy, p, a

      a = 0.3

      xn = x
      yn = y
      call Normal(xn,yn)
      
      if(anal.eq.-1.0) then
        Tx_ana = -(1.0/pi) * ( x*x*(x*xn+y*yn) ) / (x*x+y*y)**2
        return
      else if(anal.eq.0.0) then
        uxdx = 0.0; uxdy = 0.0; uydx = 0.0; uydy = 0.0
        p = 0.0
      else if(anal.eq.2.0) then
        uxdx =  -2.0*x*y / ((x*x+y*y)**2)
        uxdy = (x*x-y*y) / ((x*x+y*y)**2)
        uydx = (x*x-y*y) / ((x*x+y*y)**2)
        uydy =   2.0*x*y / ((x*x+y*y)**2)
        p    =   0.0
      else if(anal.eq.3.0) then
        uxdx = sin(x*y) + x*y*cos(x*y)
        uxdy = x*x*cos(x*y)
        uydx = -y*y*cos(x*y)
        uydy = -sin(x*y) -x*y*cos(x*y)
        p    = -cos(x*y)
      else if(anal.eq.4.0) then
        uxdx = 0.2D1 * y * x * (sin(a / (x ** 2 + y ** 2)) * a - cos(a / (x &
               ** 2 + y ** 2)) * x ** 2 - cos(a / (x ** 2 + y ** 2)) * y ** 2) / &
               (x ** 2 + y ** 2) ** 3
        uxdy = (cos(a / (x ** 2 + y ** 2)) * x ** 4 - cos(a / (x ** 2 + y  &
               ** 2)) * y ** 4 + 0.2D1 * y ** 2 * sin(a / (x ** 2 + y ** 2)) * a) &
               / (x ** 2 + y ** 2) ** 3
        uydx = -(-cos(a / (x ** 2 + y ** 2)) * x ** 4 + cos(a / (x ** 2 + y &
               ** 2)) * y ** 4 + 0.2D1 * x ** 2 * sin(a / (x ** 2 + y ** 2)) * a &
               ) / (x ** 2 + y ** 2) ** 3
        uydy = -0.2D1 * y * x * (sin(a / (x ** 2 + y ** 2)) * a - cos(a / ( &
               x ** 2 + y ** 2)) * x ** 2 - cos(a / (x ** 2 + y ** 2)) * y ** 2) &
               / (x ** 2 + y ** 2) ** 3
        p    =   0.0
      else
        uxdx = 0.0; uxdy = 0.0; uydx = 0.0; uydy = 0.0; p = 0.0
      endif

      Tx_ana = 2.0 * ( uxdx - 0.5 * p ) * xn + ( uxdy + uydx ) * yn

      return
      end





      real function TTx_ana(x,y)

      use CONTROL, only : axisym, pi

      implicit none
      real x, y, xn, yn, r

      xn = x
      yn = y
      call Normal(xn,yn)

      if(axisym) then
        r = sqrt(x*x+y*y)
        TTx_ana = -(3.0/(4.0*pi)) * ( x*x*(x*xn+y*yn) ) / (r*(x*x+y*y)**2)
      else
        TTx_ana = -(1.0/pi) * ( x*x*(x*xn+y*yn) ) / (x*x+y*y)**2
      end if

      return
      end




      real function Txx_ana(x,y)

      use CONTROL, only : pi

      implicit none
      real x, y

      Txx_ana = -(1.0/pi) * ( x*x*(x) ) / (x*x+y*y)**2

      return
      end




      real function Txy_ana(x,y)

      use CONTROL, only : pi

      implicit none
      real x, y

      Txy_ana = -(1.0/pi) * ( x*x*(y) ) / (x*x+y*y)**2

      return
      end



      real function Ty_ana(x,y)

      use CONTROL, only : pi, anal
      implicit none
      real x, y, xn, yn, uxdx, uxdy, uydx, uydy, p, a

      a = 0.3

      xn = x
      yn = y
      call Normal(xn,yn)

      if(anal.eq.-1.0) then
        Ty_ana = -(1.0/pi) * ( x*y*(x*xn+y*yn) ) / (x*x+y*y)**2
        return
      else if(anal.eq.0.0) then
        uxdx = 0.0; uxdy = 0.0; uydx = 0.0; uydy = 0.0
        p = 0.5
      else if(anal.eq.2.0) then
        uxdx =  -2.0*x*y / ((x*x+y*y)**2)
        uxdy = (x*x-y*y) / ((x*x+y*y)**2)
        uydx = (x*x-y*y) / ((x*x+y*y)**2)
        uydy =   2.0*x*y / ((x*x+y*y)**2)
        p    =   0.0
      else if(anal.eq.3.0) then
        uxdx = sin(x*y) + x*y*cos(x*y)
        uxdy = x*x*cos(x*y)
        uydx = -y*y*cos(x*y)
        uydy = -sin(x*y) -x*y*cos(x*y)
        p    = -cos(x*y) + 0.5
      else if(anal.eq.4.0) then
        uxdx = 0.2D1 * y * x * (sin(a / (x ** 2 + y ** 2)) * a - cos(a / (x &
               ** 2 + y ** 2)) * x ** 2 - cos(a / (x ** 2 + y ** 2)) * y ** 2) / &
               (x ** 2 + y ** 2) ** 3
        uxdy = (cos(a / (x ** 2 + y ** 2)) * x ** 4 - cos(a / (x ** 2 + y  &
               ** 2)) * y ** 4 + 0.2D1 * y ** 2 * sin(a / (x ** 2 + y ** 2)) * a) &
               / (x ** 2 + y ** 2) ** 3
        uydx = -(-cos(a / (x ** 2 + y ** 2)) * x ** 4 + cos(a / (x ** 2 + y &
               ** 2)) * y ** 4 + 0.2D1 * x ** 2 * sin(a / (x ** 2 + y ** 2)) * a &
               ) / (x ** 2 + y ** 2) ** 3
        uydy = -0.2D1 * y * x * (sin(a / (x ** 2 + y ** 2)) * a - cos(a / ( &
               x ** 2 + y ** 2)) * x ** 2 - cos(a / (x ** 2 + y ** 2)) * y ** 2) &
               / (x ** 2 + y ** 2) ** 3
        p    =   0.0
      else
        uxdx = 0.0; uxdy = 0.0; uydx = 0.0; uydy = 0.0; p = 0.0
      endif

      Ty_ana = ( uydx + uxdy ) * xn + 2.0 * ( uydy - 0.5 * p ) * yn

      return
      end



      real function TTy_ana(x,y)

      use CONTROL, only : axisym, pi

      implicit none
      real x, y, xn, yn, r

      xn = x
      yn = y
      call Normal(xn,yn)

      if(axisym) then
        r = sqrt(x*x+y*y)
        TTy_ana = -(3.0/(4.0*pi)) * ( x*y*(x*xn+y*yn) ) / (r*(x*x+y*y)**2)
      else
        TTy_ana = -(1.0/pi) * ( x*y*(x*xn+y*yn) ) / (x*x+y*y)**2
      end if

      return
      end



      real function Tyx_ana(x,y)

      use CONTROL, only : pi

      implicit none
      real x, y

      Tyx_ana = -(1.0/pi) * ( x*y*(x) ) / (x*x+y*y)**2

      return
      end



      real function Tyy_ana(x,y)

      use CONTROL, only : pi

      implicit none
      real x, y

      Tyy_ana = -(1.0/pi) * ( x*y*(y) ) / (x*x+y*y)**2

      return
      end


      real function Fx_ana(x,y)

      use CONTROL, only : anal, scal
      implicit none
      real x, y, a, R
      intrinsic sqrt

      a = 0.3

      if(anal.eq.0.0) then
        Fx_ana = 0.0
      else if(anal.eq.1.0) then
        Fx_ana = 1.0
      else if(anal.eq.2.0) then
        Fx_ana = -x/sqrt(x*x+y*y)
      else if(anal.eq.3.0) then
        Fx_ana = 2.0*y*cos(x*y) -(y+x*y*y+x*x*x)*sin(x*y)
      else if(anal.eq.4.0) then
        R = sqrt(2.0*0.76*0.76) * scal

        Fx_ana = (-cos(a / (x ** 2 + y ** 2)) * x ** 9 - 0.4D1 * x ** 7 * cos(&
                 a / (x ** 2 + y ** 2)) * y ** 2 - 0.2D1 * x ** 7 * sin(a / (x ** 2&
                  + y ** 2)) * a + 0.2D1 * x ** 5 * sin(a / (x ** 2 + y ** 2)) * a &
                 * sqrt(x ** 2 + y ** 2) * R - 0.6D1 * x ** 5 * cos(a / (x ** 2 + y&
                  ** 2)) * y ** 4 - 0.6D1 * x ** 5 * sin(a / (x ** 2 + y ** 2)) * a&
                  * y ** 2 - 0.6D1 * x ** 3 * sin(a / (x ** 2 + y ** 2)) * a * y **&
                  4 + 0.4D1 * x ** 3 * sin(a / (x ** 2 + y ** 2)) * a * sqrt(x ** 2&
                  + y ** 2) * R * y ** 2 - 0.4D1 * x ** 3 * cos(a / (x ** 2 + y ** &
                 2)) * y ** 6 + 0.8D1 * y * sin(a / (x ** 2 + y ** 2)) * a * sqrt(x&
                  ** 2 + y ** 2) * x ** 2 - 0.2D1 * x * sin(a / (x ** 2 + y ** 2)) &
                 * a * y ** 6 + 0.2D1 * x * sin(a / (x ** 2 + y ** 2)) * a * sqrt(x&
                  ** 2 + y ** 2) * R * y ** 4 - x * cos(a / (x ** 2 + y ** 2)) * y &
                 ** 8 + 0.4D1 * y * cos(a / (x ** 2 + y ** 2)) * a ** 2 * sqrt(x **&
                  2 + y ** 2) + 0.8D1 * y ** 3 * sin(a / (x ** 2 + y ** 2)) * a * &
                 sqrt(x ** 2 + y ** 2)) * (x ** 2 + y ** 2) ** (-0.9D1 / 0.2D1)
      else
        Fx_ana = 0.0
      endif

      return
      end 
   


      real function Fy_ana(x,y)

      use CONTROL, only : anal, scal
      implicit none
      real x, y, a, R
      intrinsic sqrt

      a = 0.3

      if(anal.eq.0.0) then
        Fy_ana = 0.0
      else if(anal.eq.1.0) then
        Fy_ana = 1.0
      else if(anal.eq.2.0) then
        Fy_ana = -y/sqrt(x*x+y*y)
      else if(anal.eq.3.0) then
        Fy_ana = -2.0*x*cos(x*y) -(x-y*x*x-y*y*y)*sin(x*y)
      else if(anal.eq.4.0) then
        R = sqrt(2.0*0.76*0.76) * scal

        Fy_ana = (-y * cos(a / (x ** 2 + y ** 2)) * x ** 8 - 0.4D1 * x ** 6 * &
                 y ** 3 * cos(a / (x ** 2 + y ** 2)) - 0.2D1 * x ** 6 * sin(a / (x &
                 ** 2 + y ** 2)) * a * y - 0.6D1 * x ** 4 * sin(a / (x ** 2 + y ** &
                 2)) * a * y ** 3 - 0.6D1 * x ** 4 * y ** 5 * cos(a / (x ** 2 + y &
                 ** 2)) + 0.2D1 * x ** 4 * sin(a / (x ** 2 + y ** 2)) * a * y * sqrt&
                 (x ** 2 + y ** 2) * R - 0.8D1 * sin(a / (x ** 2 + y ** 2)) * a * &
                 sqrt(x ** 2 + y ** 2) * x ** 3 + 0.4D1 * x ** 2 * sin(a / (x ** 2 +&
                  y ** 2)) * a * y ** 3 * sqrt(x ** 2 + y ** 2) * R - 0.4D1 * x ** &
                 2 * y ** 7 * cos(a / (x ** 2 + y ** 2)) - 0.6D1 * x ** 2 * sin(a /&
                  (x ** 2 + y ** 2)) * a * y ** 5 - 0.8D1 * x * sin(a / (x ** 2 + y&
                  ** 2)) * a * sqrt(x ** 2 + y ** 2) * y ** 2 - 0.4D1 * x * cos(a /&
                  (x ** 2 + y ** 2)) * a ** 2 * sqrt(x ** 2 + y ** 2) - 0.2D1 * sin&
                 (a / (x ** 2 + y ** 2)) * a * y ** 7 - y ** 9 * cos(a / (x ** 2 + &
                 y ** 2)) + 0.2D1 * sin(a / (x ** 2 + y ** 2)) * a * y ** 5 * sqrt(&
                 x ** 2 + y ** 2) * R) * (x ** 2 + y ** 2) ** (-0.9D1 / 0.2D1)
      else if(anal.eq.5) then
        Fy_ana = 0.0
      else
        Fy_ana = 0.0
      endif

      return
      end 
   



      real function Fp_ana(x,y)

      use CONTROL, only : anal
      implicit none
      real x, y

      if(anal.eq.0.0) then
        Fp_ana = 0.0
      else if(anal.eq.1.0) then
        Fp_ana = 1.0
      else if(anal.eq.2.0) then
        Fp_ana = 0.0
      else if(anal.eq.3.0) then
        Fp_ana = 0.0
      else
        Fp_ana = 0.0
      endif

      return
      end 




      subroutine constant(maxnod,maxnid,List,Jist,Zist,Ex,Ey,X,Y,P)

      use PLACE, only : rr
      use CONTROL, only : disgal

      implicit none
      integer maxnod, maxnid, i, List(maxnid+1), Jist(maxnid), Zist(maxnid)
      real X(maxnod), Y(maxnod)
      double precision Ex(maxnid), Ey(maxnid), P(3*maxnod), sum, sim, ref

      if(maxnid.lt.1) return

      sum = 0.0d0; sim = 0.0d0; ref = 0.0d0

      do i = 1, maxnid
        ref = ref + sqrt ( ( X(List(i)) - X(List(i+1)) ) ** 2 + ( Y(List(i)) - Y(List(i+1)) ) ** 2 )
        sim = sim + Ex(i) * P(rr(1,List(i))) + Ey(i) * P(rr(2,List(i)))
        if(disgal) then
          sum = sum + Ex(i) * P(rr(1,Zist(i))) + Ey(i) * P(rr(2,Zist(i)))
        else
          sum = sum + Ex(i) * P(rr(1,Jist(i))) + Ey(i) * P(rr(2,Jist(i)))
        endif
      end do

      write(*,'(a16,e11.3,a22,e11.3,a11,e11.3)') 'Compatibility = ',sum/ref,'  Incompressibility = ',sim/ref,'  Length = ',ref

      return
      end


      subroutine Tension(maxnid,List,Jist,Pist,Zist,Kxx,Kxy,Kyy,T,Wxx,Wxy,Wyy,Qx,Qy,Z,CP,X,Y,P,Ex,Ey,xnod,ynod,surener,coulene)

      use PLACE, only : rr
      use CONTROL, only : disgal, odv, ccaa, omeg, inviscid, stan, axisym, tol, air, prefix, anal, free, fish, fitHK
      use CONTROL, only : pionfour, step, fita, fitb, relaxing, sphereish, twopi, rho, fourpi, splines, fitaa, fitbb
      use CONTROL, only : legendre, monocharge, EquilibriumCircle, heat, perfect, step, cond, bulk
      use BOUNDARY, only : splinesmooth
      use TEMPERATURE, only : radiation
      use SIZES, only : maxnod

      ! Calculates the Surface Tension based on the Formulaee in Primo2000.
      
      implicit none

      integer maxnid, List(maxnid), Jist(maxnid), Pist(maxnid), Zist(maxnid),i,j,g(5),s,ij,jj, xnod, ynod
      integer span, spanstart, spanstop
      real X(maxnod), Y(maxnod), U_inf, V_inf, sx(5), sy(5)
      double precision P(3*maxnod), T(maxnid,maxnid), CP(maxnid,maxnid)!, xx, yy, r1, r2, mc, gc
      double precision Kxx(maxnid,maxnid), Kxy(maxnid,maxnid), Kyy(maxnid,maxnid)
      double precision Wxx(maxnid,maxnid), Wxy(maxnid,maxnid), Wyy(maxnid,maxnid)
      double precision Qx(maxnid,maxnid), Qy(maxnid,maxnid), Z(maxnid,maxnid)
      double precision Ex(maxnid), Ey(maxnid), sum, ref, chk, surener, coulene
      double precision nxx, nyy, exx, eyy, nxxx, nyyy, exxx, eyyy, nk, ek, dSdr, d2Sdr2, snx, sny, sex, sey
      double precision X_g(5), Y_g(5), dcross
      logical vertical, nearvertical
      external dcross

      ! Initialize energy accumulators
      if(perfect) Ex = 0.0d0
      chk = 0.0d0; surener = 0.0d0; coulene = 0.0d0

      if(anal.ne.5.0) return

      ! For axisym disgal first row + column of Coulomb Potential matrix will be empty ...
      if(disgal.and.axisym) then
        s = 2; CP(1,1) = 1.0d0; Ex(1) = 1.0d0
      else
        s = 1
      end if

      ! If there is surface charge, then invert Coulomb matrix to get its distribution
      ! if a perfect conductor, otherwise calculate the surface electric field for use
      ! in the charge evolution equation
      if((omeg.ne.0.0).and.(ccaa.eq.0.0)) then
        ! Initialization stuff for non-perfect conductor
        if(.not.perfect) then
          ! Build-up Crank-Nicolson matrix inside the T array
          CP = 0.0d0; CP(1,1) = 1.0d0; Ex(1) = 0.0d0
          do i = s,  maxnid
            ! Take the interface deformation term and surface divergence terms combined in Ey
            ref = Ey(i)
            ! Restore the charge back from its squared and divided by two value and place in Ey
            Ey(i) = sqrt( 2.0d0 * Ex(i) )
            ! Interface deformation term = 2 H q n.V
            Ex(i)   = ( 1.0d0 + 0.5d0 * step * ref ) * Ey(i)
            CP(i,i) = ( 1.0d0 - 0.5d0 * step * ref )
          end do
          ! Plot some output for debugging
          !if(fish) then
          !  call gmshOpen(56,'field ')
          !  call gmshOpen(57,'potential ')
          !  call gmshOpen(64,'charge ')
          !end if
        end if
        do i = s, maxnid
          ! Initialize some accumulators for plotting the electric field computed from the charge distribution
          !if((.not.perfect).and.fish) then
          !  nxx = 0.0d0; nyy = 0.0d0; dSdr = 0.0d0; sum = 0.0d0
          !end if
          do j = s, maxnid
            ! Original stuff for perfect conductor
            if(perfect) then
            ! Add reference to unknown surface charge potential for each charge calculation row
            if(disgal) then
              Ex(i) = Ex(i) + Z(i,j)
              surener = surener + Z(i,j)
            else
              Ex(i) = Ex(i) + T(i,j)
              surener = surener + T(i,j)
            end if
            ! Charge evolution equation for imperfect conductor
            else
              ! Bulk conductivity contribution
              Ex(i)   = Ex(i)   - dble(bulk) * 0.5d0 * step * dble(cond) * ( ( 0.5d0 * Z(i,j) - Qx(i,j) ) / Z(i,i) ) * Ey(j)
              CP(i,j) = CP(i,j) + dble(bulk) * 0.5d0 * step * dble(cond) * ( ( 0.5d0 * Z(i,j) - Qx(i,j) ) / Z(i,i) )
              ! Accumulate the fields etc for plotting
              !if(fish) then
              !  nxx = nxx + Qx(i,j) ! * Ey(j)
              !  nyy = nyy + 0.5d0 * Z(i,j) ! Qy(i,j) * Ey(j)
              !  dSdr = dSdr + CP(i,j) * Ey(j); sum = sum + Z(i,j)
              !end if
            end if
          end do
          !if((.not.perfect).and.fish) then
          !  exx = Y(List(i))-Y(List(i-1)); eyy = X(List(i-1))-X(List(i))
          !  sey = sqrt(exx*exx+eyy*eyy)
          !  exx = exx / sey; eyy = eyy / sey
          !  nxxx = exx * nxx / sum
          !  nyyy = eyy * nxx / sum
          !  call gmshVP(56,(X(List(i))+X(List(i-1)))/2.0,(Y(List(i))+Y(List(i-1)))/2.0,nxxx,nyyy)
          !  nxxx = - eyy * nyy / sum
          !  nyyy = + exx * nyy / sum
          !  call gmshVP(56,(X(List(i))+X(List(i-1)))/2.0,(Y(List(i))+Y(List(i-1)))/2.0,nxxx,nyyy)
          !  call gmshSLd(57,X(List(i)),Y(List(i)),X(List(i-1)),Y(List(i-1)),dSdr/sum)
          !  call gmshSLd(64,X(List(i)),Y(List(i)),X(List(i-1)),Y(List(i-1)),Ey(i))
          !end if
        end do
        !if((.not.perfect).and.fish) then
        !  call gmshClose(64)
        !  call gmshClose(57)
        !  call gmshClose(56)
        !end if

        ! Keep a record of the RHS vector for the areas of the axisymmetric rings
        !Ey = Ex

        ! Solve for the distribution of surface charges with a constant unit surface potential
        call LU_Factorization(maxnid,CP)
        call Solver(maxnid,CP,Ex,1)

        ! Slight fiddle to get the prolates going
        if(monocharge) then
          if(fita.lt.fitb) then
            do i = 1, maxnid
              Ex(i) = - Ex(i)
            end do
            call dheapsort(maxnid,Pist,Ex)
            do i = 1, maxnid
              Ex(i) = - Ex(i)
            end do
          else
            call dheapsort(maxnid,Pist,Ex)
          end if
        end if

        ! Determine how much charge there was with the given potential of V = 1
        nxx = 0.0d0
        do i = s, maxnid
          nxx = nxx + Ex(i) * Z(i,i)
        end do

        ! How much charge should there be
        nyy = dble( omeg / twopi )

        ! Correction required to get correct amount of surface charge = required surface potential
        nxx = nyy / nxx

        do i = s, maxnid
          ! Apply correction
          Ex(i) = Ex(i) * nxx
          ! Square the charge and divide by two as required in the surface force balance formula
          Ex(i) = Ex(i) * Ex(i) / 2.0d0
        end do

        ! Calculate the Coulomb energy = Q * V / 2 as would appear on the full (not half) droplet
        ! Note that the potential arising from the hemisphere is half that of the whole sphere
        ! coulene = 2 ( 0.5 * omeg * ( 2 pot ) ) / ( 4 pi / ohne )
        coulene = nxx * dble( omeg / ( twopi * rho ) )

      else
        ! Still need surface energy even if no surface charge
        do i = s, maxnid
          do j = s, maxnid
            if(disgal) then
              surener = surener + Z(i,j)
            else
              surener = surener + T(i,j)
            end if
          end do
        end do
        Ex = 0.0d0
        coulene = 0.0d0

      endif

      ! Surface energy as would appear on the full (not half) droplet
      ! surener = 2.0d0 * dble ( twopi * stan ) * surener / ( 4 pi / ohne )

!      if(fish) then
!        open(43,file=prefix//'nummP'//'.pos')
!        write(43,'(a29)')'View "All" {'
!      end if

      sum = 0.0d0; chk = 0.0d0; ref = 0.0d0

      ! call gmshOpen(13,'eqcircle ')

      ! call gmshOpen(77,'tension ')

      ! Loop (actual) boundary nodes extra "+1" bit for removing pressure constant only
      do i = 1, maxnid !+ 1

        ! One fewer boundary edges than boundary nodes in axisym case
        if(disgal.and.axisym.and.(i.eq.1)) cycle

        ! Determine pressure constant introduced by surface tension
        if(i.eq.maxnid+1) sum = sum / ref

        ! Loop ALL the boundary nodes as they would appear with the quadrant symmetries expanded out
        do j = 1, maxnid

          ! Only the diagonal of the boundary mass matrix Z has anything in it for the non-conforming case
          if(disgal.and.(j.ne.i)) cycle

          ! One fewer boundary edges than boundary nodes in axisym case
          if(disgal.and.axisym.and.(j.eq.1)) cycle

          ! Initialize the Accumulators for the edge valued things
          X_g = 0.0d0; Y_g = 0.0d0; spanstart = 1; spanstop = 10; vertical = .false.; nearvertical = .false.

          ! Loop over previous and current boundary nodes to get edge values too ...
          do s = -1, 0, +1

            if((.not.disgal).and.(s.ne.0)) cycle

            ! Set the span for the jumps to be used in the stencil based on how colinear the nodes are
            do span = spanstart, spanstop

              ! Determine the 5 Nodes to be used for the Polynomial Interpolation
              jj = j + s - 2 * span; call qmap(maxnid,jj,sx(1),sy(1)); g(1) = List( jj )
              jj = j + s - 1 * span; call qmap(maxnid,jj,sx(2),sy(2)); g(2) = List( jj )
              ij = j + s           ; call qmap(maxnid,ij,sx(3),sy(3)); g(3) = List( ij )
              jj = j + s + 1 * span; call qmap(maxnid,jj,sx(4),sy(4)); g(4) = List( jj )
              jj = j + s + 2 * span; call qmap(maxnid,jj,sx(5),sy(5)); g(5) = List( jj )

              ! Determine mid-points for element edge ("e") excitations
              do jj = 1, 5
                nxx = dble(sx(jj) * X(g(jj))); nyy = dble(sy(jj) * Y(g(jj)))
                ! If droplet on a substrate, correct coordinates lying within the substrate
                call EquilibriumCircle(nxx,nyy)
                X_g(jj) = X_g(jj) + 0.5d0 * nxx; Y_g(jj) = Y_g(jj) + 0.5d0 * nyy
              end do

              ! If on second round nothing to check
              if(s.eq.0) then
                cycle
              ! First make sure haven't hit spanend
              else if(span.eq.spanstop) then
                ! If the stencil is almost vertical ...
                if(10.0*abs(X_g(1)-X_g(5)).lt.abs(Y_g(1)-Y_g(5))) then
                  ! .. flag for a vertical stencil (just uses one radius of curvature)
                  vertical = .true.
                  exit
                else
                  ! ... assume the stencil is near vertical ...
                  nearvertical = .true.
                  exit
                end if
              ! Test for colinearity (not intended to be too frequent ...)
              else if(abs(dcross(X_g(5)-X_g(3),Y_g(5)-Y_g(3),X_g(1)-X_g(3),Y_g(1)-Y_g(3))).lt.0.05d0) then
                ! Reinitialize the mid-point coordinate accumulators
                X_g = 0.0d0; Y_g = 0.0d0
              ! Test for indented upper mid-node
              else if(abs(dcross(X_g(5)-X_g(4),Y_g(5)-Y_g(4),X_g(3)-X_g(4),Y_g(3)-Y_g(4))).le.0.0d0) then
                ! Reinitialize the mid-point coordinate accumulators
                X_g = 0.0d0; Y_g = 0.0d0
              ! Test for indented lower mid-node
              else if(abs(dcross(X_g(3)-X_g(2),Y_g(3)-Y_g(2),X_g(1)-X_g(2),Y_g(1)-Y_g(2))).le.0.0d0) then
                ! Reinitialize the mid-point coordinate accumulators
                X_g = 0.0d0; Y_g = 0.0d0
              else
                exit
              end if

            end do

            ! All clear, the s = -1 loop can use the same span
            spanstart = span; spanstop = span

            ! Actual Contributions only Added at the End of the Smoothing Loop
            if(s.ne.0) cycle

            ! Fitsmooth mid-points for element edge ("e") excitations
            if(sphereish.or.splines.or.legendre) then
              do jj = 1, 5
                if(legendre) then
                  call legfit(X_g(jj),Y_g(jj))
                else if(sphereish) then
                  call fitsmooth(X_g(jj),Y_g(jj))
                else
                  call splinesmooth(exx,eyy)
                end if
              end do
            end if

            ! Determine appropriate curvatures inside kinks
            ! call dekink(X_g,Y_g)

            ! Constructions for the Normal Calculation
            nxx  = (1.0d0/6.0d0) * ( sx(1) * X(g(1)) - 8.0d0 * sx(2) * X(g(2)) + 8.0d0 * sx(4) * X(g(4)) - sx(5) * X(g(5)) )
            nyy  = (1.0d0/6.0d0) * ( sy(1) * Y(g(1)) - 8.0d0 * sy(2) * Y(g(2)) + 8.0d0 * sy(4) * Y(g(4)) - sy(5) * Y(g(5)) )

            ! Constructions for the Normal Calculation
            exx  = (1.0d0/6.0d0) * ( X_g(1)  - 8.0d0 * X_g(2)  + 8.0d0 * X_g(4)  - X_g(5)  )
            eyy  = (1.0d0/6.0d0) * ( Y_g(1)  - 8.0d0 * Y_g(2)  + 8.0d0 * Y_g(4)  - Y_g(5)  )

            ! The Surface Normal
            snx = + nyy / sqrt( nxx * nxx + nyy * nyy )
            sny = - nxx / sqrt( nxx * nxx + nyy * nyy )
            sex = + eyy / sqrt( exx * exx + eyy * eyy )
            sey = - exx / sqrt( exx * exx + eyy * eyy )

            ! Special Constructions for the Surface Curvature Calculation
            nxxx = (1.0d0/3.0d0) * (-sx(1)*X(g(1)) +16.0d0*sx(2)*X(g(2)) -30.0d0*sx(3)*X(g(3)) +16.0d0*sx(4)*X(g(4)) -sx(5)*X(g(5)))
            nyyy = (1.0d0/3.0d0) * (-sy(1)*Y(g(1)) +16.0d0*sy(2)*Y(g(2)) -30.0d0*sy(3)*Y(g(3)) +16.0d0*sy(4)*Y(g(4)) -sy(5)*Y(g(5)))

            ! Special Constructions for the Surface Curvature Calculation
            exxx = (1.0d0/3.0d0) * (-      X_g(1)  +16.0d0*      X_g(2)  -30.0d0*      X_g(3)  +16.0d0*      X_g(4)  -      X_g(5) )
            eyyy = (1.0d0/3.0d0) * (-      Y_g(1)  +16.0d0*      Y_g(2)  -30.0d0*      Y_g(3)  +16.0d0*      Y_g(4)  -      Y_g(5) )

            ! Regular (normal) curvature for non-axisym probs, Mean Surface Curvature otherwise
            if(.not.axisym) then
              ! Surface Curvature Formula (with Weight)
              nk = ( ( nxx * nxx + nyy * nyy ) ** (3.0d0/2.0d0) ) / ( nxx * nyyy - nyy * nxxx )
              ! Accumulate for an Average Surface Curvature / Normals over an Edge
              ek = ( ( exx * exx + eyy * eyy ) ** (3.0d0/2.0d0) ) / ( exx * eyyy - eyy * exxx )
              ! Reciprocal Surface Curvature defines the Traction Jump
              nk = 1.0d0 / nk; ek = 1.0d0 / ek
            ! Mean curvature from Legendre polynomial fitting
            else if(legendre) then
              call meanH(X_g(3),Y_g(3),ek)
              ek = 2.0d0 * ek
            ! Use spheroid analytic form when close to a sphere
            else if(sphereish) then
              ! Get spheroid Mean and Gaussian curvatures
              call fitHK(X_g(3),ek,Ex(j))
              ek = 2.0d0 * ek; Ex(j) = 2.0d0 * Ex(j) * ( omeg / fourpi ) ** 2
            else
              if(vertical) then
                ek = 1.0d0 / X_g(3)
              else if(nearvertical) then
                dSdr = eyy / exx
                ek = dSdr * dSdr
                ek = sqrt( 1.0d0 + ek )
                ek = - dSdr / ( X_g(3) * ek )
              ! Wikipedia axi-symmetric mean curvature formula
              else if(abs(exx).lt.10.0*tol) then
                ! Use Giglio simplification when close to equator
                ek = - exxx / ( eyy * eyy )
                ek = ek + 1.0d0 / X_g(3)
              else
                dSdr   =           eyy               / exx       ! First  Order Surface Derivative
                d2Sdr2 = ( exx * eyyy - eyy * exxx ) / exx**3    ! Second Order Surface Derivative
                ek = dSdr * dSdr
                ek = sqrt( 1.0d0 + ek )
                ek = - d2Sdr2 / ( ek * ek * ek )  - dSdr / ( X_g(3) * ek )
              end if
            end if

            ! Record the mean curvature
            Ey(j) = ek

            ! Construct the surface tension + charge terms
            if(disgal.and.(j.eq.maxnid)) then
              exxx = + step * ( stan * ek - Ex(j) ) * sex
              eyyy = + step * ( stan * ek - Ex(j) ) * sey
            else if(disgal) then
              exxx = + step * ( stan * ek - Ex(j) ) * sex
              eyyy = + step * ( stan * ek - Ex(j) ) * sey
            else if(j.eq.maxnid) then
              nxxx = - step * ( stan * nk - Ex(j) ) * snx
              nyyy = - step * ( stan * nk - Ex(j) ) * sny
            else
              nxxx = - step * ( stan * nk - Ex(j) ) * snx
              nyyy = - step * ( stan * nk - Ex(j) ) * sny
            endif

            ! Record competing surface tension and charge forces for plotting
            ! if(i.eq.maxnid) then
              ! Analytic equivalents for ellipse only - using Taylor Formulae
!              xx = X_g(3); yy = Y_g(3)
!              r1 = fitb * fitb; r2 = fita * fita
!              mc = r1 * r2; gc = r1
!              r1 = xx * xx / ( r1 * r1 ); r2 = yy * yy / ( r2 * r2 )
!              r2 = sqrt( r1 + r2 )
!              ! Principal radii of Curvature
!              r1 = mc * r2 * r2 * r2
!              r2 = gc * r2
!              ! Principle Curvatures
!              r1 = 1.0d0 / r1; r2 = 1.0d0 / r2
!              ! Mean Curvature - the Algebraic Mean
!              mc = ( r1 + r2 ) / 2.0d0
!              ! Gaussian Curvature - the Geometric Mean
!              gc = sqrt( r1 * r2 )

            ! Check what the stencil looks like
            !do jj = 1, 4
            !  call gmshSLd(77,real(X_g(jj)),real(Y_g(jj)),real(X_g(jj+1)),real(Y_g(jj+1)),stan*ek)    
            !end do
            !call gmshSP(77,real(X_g(5)),real(Y_g(5)),Ex(j))

              ! For testing the calculated mean curvature - should follow the Mean Curvature
              !Ex(j) = mc * stan

              ! For testing the calculated dSdr
              !Ex(j) = - fita * fita * dble(X_g(3)) / ( fitb * fitb * dble(Y_g(3)) )

              ! For testing the calculated d2Sdr2
              !Ex(j) = - fita**4 / ( fitb * fitb * dble(Y_g(3))**3 )

              ! For testing the calculated charge density - should follow the Gaussian Curvature
              !Ey(j) = gc * stan
            ! end if

            ! Remove pressure constant introduced by surface tension
            if(i.eq.maxnid+1) then
              if(disgal) then
                P(rr(1,Jist(i))) = P(rr(1,Jist(i))) + sum * sex
                P(rr(2,Jist(i))) = P(rr(2,Jist(i))) + sum * sey
              endif
              cycle
            endif

            ! Accumulate the Traction Forcings due to Surface Tension (N.B.: for disgal sys. matrix ISN'T doubled in interpolate)
            if(disgal.and.axisym) then
              P(rr(1,Jist(i-1))) = P(rr(1,Jist(i-1))) - Z(i,j) * exxx !/ twopi
              P(rr(2,Jist(i-1))) = P(rr(2,Jist(i-1))) - Z(i,j) * eyyy !/ twopi
              if(heat) P(rr(4,Jist(i-1))) = P(rr(4,Jist(i-1))) - Z(i,j) * radiation(j)
            else if(disgal) then
              P(rr(1,Jist(i))) = P(rr(1,Jist(i))) - 1.0 * Z(i,j) * exxx
              P(rr(2,Jist(i))) = P(rr(2,Jist(i))) - 1.0 * Z(i,j) * eyyy
            end if
            if((List(i).ne.ynod).and.(.not.disgal)) then
              P(rr(1,List(i))) = P(rr(1,List(i))) - 2.0 * T(i,j) * nxxx
            end if
            if((List(i).ne.xnod).and.(.not.disgal)) then
              P(rr(2,List(i))) = P(rr(2,List(i))) - 2.0 * T(i,j) * nyyy
            endif

            ! Pressure constant arising from surface tension
            if(.false.) then !disgal
              ref = ref + Z(i,j)
              !if(i.eq.j) chk = chk + sqrt ( ( X(List(i)) - X(List(i+1)) ) ** 2 + ( Y(List(i)) - Y(List(i+1)) ) ** 2 )
              sum = sum - ( Ex(i) * sex + Ey(i) * sey ) * stan * ek * Z(i,j)
            endif

            ! Pressure Jump due to Surface Tension added to Pressure's RHS
            if(free) then
              if(disgal.and.air) then
                P(rr(3,Pist(i))) = P(rr(3,Pist(i))) + stan * nk * Z(i,j)
              else if((.not.inviscid).and.(.not.disgal)) then
                P(rr(3,Jist(i))) = P(rr(3,Jist(i))) + stan * nk * Z(i,j)
              endif
            else
              P(rr(3,List(i))) = P(rr(3,List(i))) + stan * nk * Z(i,j)
            endif

            ! Incident Flow Definition - per node (n) and per edge (e)
            if(.false.) then
            nxx = U_inf(sx(3)*X(List(j)),sy(3)*Y(List(j)))
            nyy = V_inf(sx(3)*X(List(j)),sy(3)*Y(List(j)))
            jj  = j+1
            call qmap(maxnid,jj,sx(4),sy(4))
            exx = U_inf( ( sx(3)*X(List(j)) + sx(4)*X(List(jj)) ) / 2.0 , ( sy(3)*Y(List(j)) + sy(4)*Y(List(jj)) ) / 2.0 )
            eyy = V_inf( ( sx(3)*X(List(j)) + sx(4)*X(List(jj)) ) / 2.0 , ( sy(3)*Y(List(j)) + sy(4)*Y(List(jj)) ) / 2.0 )
            end if

            ! Add the Incident Flow to the Boundary Velocity R.H.S.
            !if(disgal.and.air.and.(.false.)) then
            !  P(rr(1,Pist(i))) = P(rr(1,Pist(i))) - ( Wxx(i,j) * nxx + Wxy(i,j) * nyy )
            !  P(rr(2,Pist(i))) = P(rr(2,Pist(i))) - ( Wxy(i,j) * nxx + Wyy(i,j) * nyy )
            !else if((.not.inviscid).and.(.not.disgal).and.(.false.)) then
            !  P(rr(1,List(i))) = P(rr(1,List(i))) - ( Wxx(i,j) * nxx + Wxy(i,j) * nyy )
            !  P(rr(2,List(i))) = P(rr(2,List(i))) - ( Wxy(i,j) * nxx + Wyy(i,j) * nyy )
            !endif

            ! Add the Incident Flow to the Boundary Traction R.H.S.
            if(disgal.and.(.false.)) then
              P(rr(1,Zist(i))) = P(rr(1,Zist(i))) + ( ( Kxx(i,j) + T(i,j) ) * nxx + Kxy(i,j) * nyy )
              P(rr(2,Zist(i))) = P(rr(2,Zist(i))) + ( Kxy(i,j) * nxx + ( Kyy(i,j) + T(i,j) ) * nyy )

              P(rr(1,Zist(i))) = P(rr(1,Zist(i))) - 2.0 * Z(i,j) * exx
              P(rr(2,Zist(i))) = P(rr(2,Zist(i))) - 2.0 * Z(i,j) * eyy

              P(rr(3,Zist(1))) = P(rr(3,Zist(1))) + T(i,j) * nxx
              P(rr(3,Zist(2))) = P(rr(3,Zist(2))) + T(i,j) * nyy

            else if((.not.inviscid).and.(.false.)) then
              P(rr(1,Jist(i))) = P(rr(1,Jist(i))) + ( ( Kxx(i,j) - T(i,j) ) * nxx + Kxy(i,j) * nyy )
              P(rr(2,Jist(i))) = P(rr(2,Jist(i))) + ( Kxy(i,j) * nxx + ( Kyy(i,j) - T(i,j) ) * nyy )
            endif

            ! Add the Incident Flow to the Boundary Pressure R.H.S.
            if(free.and.(.false.)) then
              if(disgal) then
                P(rr(3,Pist(i))) = P(rr(3,Pist(i))) - ( Qx(i,j) * nxx + Qy(i,j) * nyy )
              else if(.not.inviscid) then
                P(rr(3,Jist(i))) = P(rr(3,Jist(i))) - ( Qx(i,j) * nxx + Qy(i,j) * nyy )
              endif
            else if(.false.) then
              P(rr(3,List(i))) = P(rr(3,List(i))) - ( Qx(i,j) * nxx + Qy(i,j) * nyy )
            endif

          ! End of Smoothing Loop
          end do

        end do
      end do

      ! call gmshClose(77)

      return
      end




      real function U_inf(x,y)

      use CONTROL, only : anal, omeg, ccaa
      implicit none
      real x, y

      if(anal.eq.5.0) then
        U_inf = y * ( 1.0 + omeg ) * ccaa / 2.0
      else
        U_inf = 0.0
      endif

      return
      end 
   


      real function V_inf(x,y)

      use CONTROL, only : anal, omeg, ccaa
      implicit none
      real x, y

      if(anal.eq.5.0) then
        V_inf = x * ( 1.0 - omeg ) * ccaa / 2.0
      else
        V_inf = 0.0
      endif

      return
      end 


      subroutine swap(i,j)

      implicit none

      integer i, j, k

      k = i
      i = j
      j = k

      end subroutine swap


      subroutine Normal(x,y)

      implicit none
      integer i, n, j, k, g(4)
      real, intent(inout) :: x, y
      real xx, yy, table(5000,2), m(4), test
      data n /0/
      save table, n

      ! Normal defined from the mesh boundary point locations 
      if((x.lt.-10000.0).and.(y.lt.-10000.0)) then
        n = 0
      else if((x.gt.10000.0).and.(y.gt.10000.0)) then
        n = n + 1
        if(n.gt.5000) then
          print*,'Normal table over-run.'
          stop
        endif
        table(n,1) = x-20000.0; table(n,2) = y-20000.0
      else
        m = 99999.9; g = 0

        ! Find closest four vertex nodes
        do i = 1, n
          test = (table(i,1)-x)**2 + (table(i,2)-y)**2
          do j = 1, 4
            if(test.lt.m(j)) then
              do k = 4, j+1, -1
                g(k) = g(k-1); m(k) = m(k-1)
              end do
              g(j) = i; m(j) = test
              exit
            endif
          end do
        end do

        ! Check for problems ...
        if(n.eq.0) then

          ! No vertex nodes in the first place
          x = 0.0; y = 0.0

        else 
          do i = 1, 4
            do j = 1, 4
              if((i.ne.j).and.(g(i).eq.g(j))) then
                print*,'Error in Normal: Identical interpolation nodes!'
                x = 0.0; y = 0.0
                return
              endif
            end do
          end do

          ! Loop over the two ("inner" and "outer") pairs of nodes
          do i = 0, 2, 2

            ! If either pair ( g(1) , g(2) ) or ( g(3) , g(4) ) bridge the gap ...
            if(abs(g(i+1)-g(i+2)).gt.nint(real(n)/2.0)) then

              ! ... then the smaller of the pair gets to be "node 4" ( or "node 5" )
              if(g(i+2).lt.g(i+1)) call swap(g(i+1),g(i+2))
                
            ! Otherwise ...
            else

              ! ... it is the bigger of them
              if(g(i+2).gt.g(i+1)) call swap(g(i+1),g(i+2))
            endif
          end do

          xx = (1.0/6.0) * ( table(g(4),1) - 8.0 * table(g(2),1) + 8.0 * table(g(1),1) - table(g(3),1) )
          yy = (1.0/6.0) * ( table(g(4),2) - 8.0 * table(g(2),2) + 8.0 * table(g(1),2) - table(g(3),2) )

          x  =  yy / sqrt(xx*xx+yy*yy)
          y  = -xx / sqrt(xx*xx+yy*yy)
        endif
      endif
     
      return
      end


      subroutine testElliptic()

      implicit none

      integer i, j
      double precision a, b, c, EllipticK, EllipticE
      external EllipticK, EllipticE

      open(17,file='elliptic.dat')
      do i = 1, 1000
        j = i*i
        a = 1.0d0 - real(j) * 1.0d-6
        b = EllipticK(a)
        c = EllipticE(a)
        write(17,'(3f18.8)') a, b, c
      end do
      close(17)

      end subroutine testElliptic




      subroutine axikernal(ker,px,ppy,qx,qqy,xm,ym,xn,yn,self)

      use CONTROL, only : bemo, perfect

      implicit none
      
      double precision, intent(out) :: ker(20)
      double precision, intent(in)  :: px, qx, xn, yn, xm, ym, ppy, qqy
      logical self

      ! Local variables
      ! integer i
      double precision R, S, Z
      double precision py, qy, a, b, aaa, aaaaa
      double precision I00, I20, I02
      double precision J00, J20, J02, J40, J04
      double precision K00, K20, K02, K40, K04, K22, K60, K06, K42, K24, L40, L04
      
      ! Elliptic functions
      double precision I100, I120, I102, I300, I320, I302, I340, I304, I500, I520, I502, I522, I540, I504, I542, I524, I560, I506
      external         I100, I120, I102, I300, I320, I302, I340, I304, I500, I520, I502, I522, I540, I504, I542, I524, I560, I506

      double precision EllipticE, EllipticK
      external EllipticE, EllipticK

      ! Check the Elliptic Integrals
      ! open(55,file='elliptic.dat')
      ! do i = 1, 199
      !   b = dble(i) / 200.0d0
      !   write(55,'(30f10.3)') b, I100(b), I120(b), I102(b), I300(b), I320(b), I302(b), I340(b), I304(b), &
      !                            I500(b), I520(b), I502(b), I522(b), I540(b), I504(b), I542(b), I524(b), I560(b), I506(b)
      ! end do
      ! close(55)

      ! Give default values for all kernals
      ker = 0.0d0

      if(self) then
        qy = qqy - ppy
        py = 0.0d0
      else
        py = ppy
        qy = qqy
      end if

      ! Abscissa Difference
      R = px - qx

      ! Coordinate Distance
      Z = py - qy

      ! R and R' (= S) constructions
      S = px + qx

      ! Angular Constants
      a = S * S + Z * Z

      ! The "b" in the integral of 1 / ( 1 - b sin^2(u) )^(1/2)
      b = 4.0d0 * px * qx / a

      if((b.le.0.0d0).or.(b.ge.1.0d0)) then
        print*,'ERROR in axikernal: bad b !!! ',px,py,qx,qy,a,b
        stop
      else if(bemo.and.self) then
        return
      end if

      ! Divisors
      a = sqrt( a )
      aaa = a * a * a
      if(.not.bemo) goto 10
      aaaaa = a * a * aaa / 6.0d0

      ! Constructions for the axi-symmetric single layer operators
      I20 = I120(b); J20 = Z*R*I320(b); J40 = 0.5d0*R*R*I340(b); I00 = 0.5d0    *I100(b)
      I02 = I102(b); J02 = Z*S*I302(b); J04 = 0.5d0*S*S*I304(b); J00 = 3.0d0*Z*Z*I300(b)

      ! Single Layer Potential (V) Kernal Function Values
      ker( 1) = ( I20 - I02 ) / a + ( J40 - J04 ) / aaa
      ker( 2) =                     ( J20 + J02 ) / aaa
      ker(13) =                     ( J20 - J02 ) / aaa
      ker( 3) =      I00      / a +      J00      / aaa

      ! Constructions for the axi-symmetric double layer operators
      K20 = Z*Z*R*I520(b); K40 = 0.5d0*Z*R*R*I540(b); K60 = R*R*R*I560(b); K42 = R*R*S*I542(b); K22 = 2.0d0*Z*R*S*I522(b)
      K02 = Z*Z*S*I502(b); K04 = 0.5d0*Z*S*S*I504(b); K06 = S*S*S*I506(b); K24 = R*S*S*I524(b); K00 = 0.5d0*Z*Z*Z*I500(b)
      L40 = 2.0d0*K40; L04 = 2.0d0*K04

      ! First Double Layer Potential (K) Kernal Function Values
      ker( 4) = (  ( K60 - K42 - K24 + K06 ) * xn + ( K40 - K04 ) * yn ) / aaaaa
      ker( 5) = (  ( K40             - K04 ) * xn + ( K20 + K02 ) * yn ) / aaaaa
      ker(14) = (  ( L40    - K22    + L04 ) * xn + ( K20 - K02 ) * yn ) / aaaaa
      ker( 6) = (  ( K20             - K02 ) * xn +      K00      * yn ) / aaaaa

      ! Second Double Layer Potential (K') Kernal Function Values
      ker( 7) = (  ( K60 + K42 - K24 - K06 ) * xm + ( K40 - K04 ) * ym ) / aaaaa
      ker( 8) = (  ( K40     + K22   + K04 ) * xm + ( K20 - K02 ) * ym ) / aaaaa
      ker(15) = (  ( L40             - L04 ) * xm + ( K20 - K02 ) * ym ) / aaaaa
      ker( 9) = (  ( K20             + K02 ) * xm +      K00      * ym ) / aaaaa

      ! Hyper-Singular Operator Kernal (W) Function Values
      ker(10) = ( I20 - I02 ) / a + ( J40 - J04 ) / aaa
      ker(11) =                     ( J20 + J02 ) / aaa
      ker(16) =                     ( J20 - J02 ) / aaa
      ker(12) =      I00      / a +      J00      / aaa

      ! Coulomb Potential Operator Kernal (CP) Function Values
   10 if(b.lt.1.0d0) then
        ! fv=4*s*a*ellipK/d;   // POTENTIAL
        ker(20) = I100(b) / a

        ! d*=(dr*dr+dz*dz);
        aaa = a * ( R * R + Z * Z )

        ! Need kernals for electric field if imperfect conductor
        if((.not.perfect).and.(b.lt.0.9999999d0)) then
          ! Use special simplified expressions for self element case
          if(self) then
            !ker(18) = ( ( ( -4.0d0 * px * ( px + k * qy ) - kk * qy * qy ) / ( 2.0d0 * kkk * px ) ) * EllipticE(b) &
            !                + ( ( qy * qy + ( 2.0d0 * px + k * qy ) ** 2 ) / ( 2.0d0 * kkk * px ) ) * EllipticK(b) ) / aaa

            ! //------------ GN  ---------
            ! factor = (  nr*2*(sr*dr+dz2)/rq  +  nz*4*dz  ) / (dr*dr+dz2);
            aaaaa = ( xm * 0.5d0 * ( S * (-R) + Z * Z ) + ym * (-Z) * px )
            ! gn=s*a*factor*ellipE/d;
            ker(18) = ( aaaaa * EllipticE(b) / aaa -   xm  * 0.5d0 * ker(20) ) / px

            ! //-----------  GT  ----------
            ! factor = ( tr*2*(sr*dr+dz2)/rq  +  tz*4*dz  ) / (dr*dr+dz2);
            ! aaaaa = ( (-ym) * 0.5d0 * ( S * (-R) + Z * Z ) / px + xm * (-Z) ) / ( R * R + Z * Z )
            ! gt=s*a*factor*ellipE/d - 2.0/z;  // remove singularity at z=0
            ! ker(19) = aaaaa * EllipticE(b) / a - 0.5d0 / ( qy * (-ym/xm) )   - (-ym) * 0.5d0 * ker(20) / px

          else if(.not.self) then
            ! Well seperated boundary elements
            !I00 = I300(b); I02 = I302(b);
            !ker(18) = ( ( S * I00 - 2.0d0 * qx * I02 ) * xm + Z * I00 * ym ) / aaa

            ! //---------------------GN-------------------
            ! factor=(nr*2*(sr*dr+dz2)+nz*4*dz*rq);
            aaaaa = ( xm * 0.5d0 * ( S * (-R) + Z * Z ) + ym * (-Z) * px )
            ! gn=(s*a*factor*ellipE/d-nr*0.5*fv)/rq;
            ker(18) = ( aaaaa * EllipticE(b) / aaa -   xm  * 0.5d0 * ker(20) ) / px

            !//---------------------GT-------------------
            ! factor=(tr*2*(sr*dr+dz2)+tz*4*dz*rq);
            ! aaaaa = ( (-ym) * 0.5d0 * ( S * (-R) + Z * Z ) + xm * (-Z) * px )
            ! gt=(s*a*factor*ellipE/d-tr*0.5*fv)/rq;
            ! ker(19) = ( aaaaa * EllipticE(b) / aaa - (-ym) * 0.5d0 * ker(20) ) / px
          end if
        end if
      else
        ker(20) = 0.0d0
      end if

      ! Add radial sweeps
      ker = ker * px * qx

      return
      end



      subroutine  kernal(ker,p,q,xm,ym,xn,yn,cauchy)

      use CONTROL, only : twoonpi, twopi

      implicit none
      double precision, intent(out) :: ker(20)
      real, intent(in) :: p(13), q(13)
      real, intent(in) :: xn, yn, xm, ym
      real px, py, qx, qy, r, rx, ry, rn, rm
      real r2, r4
      real rxrx, rxry, ryry
      real logr, cauchy
      intrinsic log

      ! Avoid corrupting the Input Values
      px=p(1); py=p(2); qx=q(1); qy=q(2)

      ! Abscissa Difference
      rx = px - qx

      ! Coordinate Distance
      ry = py - qy

      ! Distance between Observation and Integration Points
      r2 = rx*rx + ry*ry
      if(r2.lt.cauchy*cauchy) then
        ker = 0.0d0
        return
      end if
      r = sqrt(r2)
      r4 = r2*r2

      ! Observation Normal Component
      rm = rx*xm + ry*ym

      ! Integration Normal Component
      rn = rx*xn + ry*yn

      ! Some combinations
      rxrx = rx*rx
      rxry = rx*ry
      ryry = ry*ry

      ! evaluate the log only once
      logr = log( r )

      ! Single Layer Potential (V) Kernal Function Values
      ker( 1) = ( -logr + rxrx / ( r2 ) ) / ( twopi )
      ker( 2) = (         rxry / ( r2 ) ) / ( twopi )
      ker( 3) = ( -logr + ryry / ( r2 ) ) / ( twopi )

      ! First Double Layer Potential (K) Kernal Function Values
      ker( 4) = (        rxrx * rn / ( r4 ) ) * ( twoonpi )
      ker( 5) = (        rxry * rn / ( r4 ) ) * ( twoonpi )
      ker( 6) = (        ryry * rn / ( r4 ) ) * ( twoonpi )

      ! Second Double Layer Potential (K') Kernal Function Values
      ker( 7) = (      - rxrx * rm / ( r4 ) ) * ( twoonpi )
      ker( 8) = (      - rxry * rm / ( r4 ) ) * ( twoonpi )
      ker( 9) = (      - ryry * rm / ( r4 ) ) * ( twoonpi )

      ! Hyper-Singular Operator Kernal (W) Function Values
      ker(10) = ( -logr + rxrx / ( r2 ) ) * ( twoonpi )
      ker(11) = (         rxry / ( r2 ) ) * ( twoonpi )
      ker(12) = ( -logr + ryry / ( r2 ) ) * ( twoonpi )

      ! Single Layer Pressure Potential (P) Kernal Function Values
      ker(13) =                  rx / ( r2 )
      ker(14) =                  ry / ( r2 )

      ! Double Layer Pressure Potential (Q) Kernal Function Values (Requiring Regularization)
      ker(15) =                  xn / ( r2 )
      ker(16) =                  yn / ( r2 )

      ! Double Layer Pressure Potential (Q) Kernal Function Values (Not Requiring Regularization)
      ker(17) =       2.0 * rx * rn / ( r4 )
      ker(18) =       2.0 * ry * rn / ( r4 )

      ! Second Hyper-Singular Pressure Regularization Corrector Kernal Function Values
      ker(19) =                  xn / r
      ker(20) =                  yn / r

      return
      end

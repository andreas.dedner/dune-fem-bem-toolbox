
      subroutine paradiso(S,P,L)

      use SIZES, only : width, c3maxnod
      use PLACE, only : associateIAJA

      implicit none
      integer i, j
      integer*8 posn
      integer, pointer :: pia(:)
      integer, pointer :: pja(:)
      real*8, target :: S(c3maxnod*c3maxnod)
      real*8 P(c3maxnod), L(c3maxnod)

        ! Local pointers to sort lists
        integer, pointer :: plist(:)
        real*8,  pointer :: pval(:)

        ! Collect the skyline storage arrays
        call associateIAJA(pia,pja)
        ! Initialize the current insertion position
        posn = 0
        ! For each row of the entire system matrix
        do i = 1, c3maxnod
          ! Identify the elements of row "i"
          plist => pja(1+(i-1)*width:pia(i)+(i-1)*width)
          pval  =>  S(1+(i-1)*width:pia(i)+(i-1)*width)
          ! Sort the row based on the column indices
          call sortheap(pia(i),plist,pval)
          ! Place the sorted elements just above the last lot
          do j = 1, pia(i)
            pja(posn+j) = pja(j+(i-1)*width)
            S(  posn+j) = S(  j+(i-1)*width)
          end do
          j = pia(i)
          ! Record the position of the element that started the row
          pia(i) = posn + 1
          ! Update the total number of non-zero elements
          posn = posn + j
        end do
        ! Finalize the ia array
        pia(c3maxnod+1) = posn + 1
        ! Call the super-solver
        CALL paradiso_unsym(c3maxnod,posn,S,P,L,pia,pja)

      return
      end




            ! CALL paradiso_unsym(m, n, S,P,L,ia,ja)
        subroutine paradiso_unsym(nr,nd,a,b,x,ia,ja)

        IMPLICIT NONE

        integer*8 nd
        integer nr
        
!       Internal solver memory pointer for 64-bit architectures
!       INTEGER*8 pt(64)
!       Internal solver memory pointer for 32-bit architectures
!       INTEGER*4 pt(64)
!       This is OK in both cases.
        INTEGER*8 pt(64)

!       All other variables 

        INTEGER maxfct, mnum, mtype, phase, n, nrhs, error, msglvl
        INTEGER iparm(64)
        INTEGER ia(nr+1) 
        INTEGER ja(nd)
        REAL*8  a(nd) 
        REAL*8  b(nr)
        REAL*8  x(nr)

        INTEGER i, idum
        REAL*8  waltime1, waltime2, ddum

        ! these lines will be used if we compile with -openmp
        !$ integer omp_get_max_threads
        !$ external omp_get_max_threads

        DATA nrhs /1/, maxfct /1/, mnum /1/, msglvl /1/, mtype /11/

        n = nr

      !   ||||||||||        SETTING OF PARDISO PARAMETERS         |||||||||||||||


!  .. Setup Pardiso control parameters und initialize the solvers     
!     internal adress pointers. This is only necessary for the FIRST   
!     call of the PARDISO solver.                                     
!     
      iparm(1) = 0

   !   iparm(1) = 1
   !   iparm(60)=1

   !   iparm(11)=1
   !   iparm(13)=1
   !   iparm(2)=1
   !   iparm(10) = 10

      CALL pardisoinit(pt, mtype, iparm)

!      print*,'||| PARDISO INIT   ',mtype,'|',pt,'|',iparm

      ! Indicate user supplied iparm values
      iparm(1)=1
      iparm(2)=3
      ! iparm(3)=mkl_get_max_threads()
      iparm(4)=0
      iparm(5)=0

      ! Overwrite load with solution vector
      iparm(6)=1
      iparm(8)=-9
      iparm(10)=7
      iparm(11)=1
      iparm(13)=1
      iparm(18)=-1
      iparm(19)=0
      iparm(20)=0
      iparm(27)=0


      iparm(28)=0
      iparm(35)=0


!  .. Numbers of Processors ( value of OMP_NUM_THREADS )
      iparm(3) = 1
      ! the next line will be used if we compile with -openmp
      !$ iparm(3) = omp_get_max_threads()

!      write(*,*) '*************************'
!      write(*,*) iparm(3)
!      write(*,*) '*************************'

    
     ! iparm(1) = 1
     ! iparm(60)=0



      !   ||||||||||        CALLS OF PARDISO ROUTINES         |||||||||||||||


!     Reordering and Symbolic Factorization, This step also allocates
!     all memory that is necessary for the factorization 
      phase     = 11
      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, a, ia, ja, idum, nrhs, iparm, msglvl, ddum, ddum, error)
!      WRITE(*,*) 'PARDISO reordering completed ... '
      IF (error .NE. 0) THEN
        WRITE(*,*) 'The following PARDISO ERROR was detected: ', error
        STOP
      END IF

!      WRITE(*,*) 'Number of nonzeros in factors   = ',iparm(18)
!      WRITE(*,*) 'Number of factorization MFLOPS  = ',iparm(19)

      ! L-U Factorization.
      phase     = 22
      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, a, ia, ja, idum, nrhs, iparm, msglvl, ddum, ddum, error) 
!      WRITE(*,*) 'PARDISO factorization completed ...  '
      IF (error .NE. 0) THEN
         WRITE(*,*) 'The following PARDISO ERROR was detected: ', error
        STOP
      ENDIF 

      call timey('FAC','END')
      call timey('SOL','BEG')

      ! Back substitution and iterative refinement
      phase     = 33
      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, a, ia, ja, idum, nrhs, iparm, msglvl, b, x, error) 
!      WRITE(*,*) 'PARDISO solve completed ... '
      IF (error .NE. 0) THEN
         WRITE(*,*) 'The following PARDISO ERROR was detected: ', error
        STOP
      ENDIF

      ! Write out a few values of the solution to see how sensible it is
!      DO i = 1, 10
!        if(i.lt.10) WRITE(*,*) 'PARDISO solution(',i,') = ', b(i), ' No. Refinement Steps = ',iparm(7)
!      END DO

      ! Release all internal memory
      phase     = -1
      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, ddum, idum, idum, idum, nrhs, iparm, msglvl, ddum, ddum, error)
!      WRITE(*,*) 'PARDISO internal memory freed ... '
      IF (error .NE. 0) THEN
         WRITE(*,*) 'The following PARDISO ERROR was detected: ', error
        STOP
      ENDIF

      return
      END


    
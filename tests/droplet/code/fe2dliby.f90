
      SUBROUTINE jacobi(a,n,np,d,v,nrot)
      INTEGER n,np,nrot,NMAX
      double precision a(np,np),d(np),v(np,np)
      PARAMETER (NMAX=10)
      ! Computes all eigenvalues and eigenvectors of a real symmetric matrix a, which is of size n
      ! by n, stored in a physical np by np array. On output, elements of a above the diagonal are
      ! destroyed. d returns the eigenvalues of a in its first n elements. v is a matrix with the same
      ! logical and physical dimensions as a, whose columns contain, on output, the normalized
      ! eigenvectors of a. nrot returns the number of Jacobi rotations that were required.
      INTEGER i,ip,iq,j
      double precision c,g,h,s,sm,t,tau,theta,tresh,b(NMAX),z(NMAX)
      do ip=1,n ! Initialize to the identity matrix.
        do iq=1,n
          v(ip,iq)=0.0d0
        enddo
        v(ip,ip)=1.0d0
      enddo
      do ip=1,n
        b(ip)=a(ip,ip) ! Initialize b and d to the diagonal of a.
        d(ip)=b(ip)
        z(ip)=0.0d0 ! This vector will accumulate terms of the form tapq
      enddo ! as in equation (11.1.14).
      nrot=0
      do i=1,50
        sm=0.0d0
        do ip=1,n-1 ! Sum off-diagonal elements.
          do iq=ip+1,n
            sm=sm+abs(a(ip,iq))
          enddo
        enddo
        if(sm.eq.0.0d0)return ! The normal return, which relies on quadratic conver
        if(i.lt.4)then ! gence to machine underflow.
          tresh=0.2d0*sm/n**2 ! ...on the first three sweeps.
        else
          tresh=0.0d0 ! ...thereafter.
        endif
        do ip=1,n-1
          do iq=ip+1,n
            g=100.0d0*abs(a(ip,iq))
            ! After four sweeps, skip the rotation if the off-diagonal element is small.
            if((i.gt.4).and.(abs(d(ip))+g.eq.abs(d(ip))).and.(abs(d(iq))+g.eq.abs(d(iq))))then
              a(ip,iq)=0.0d0
            else if(abs(a(ip,iq)).gt.tresh)then
              h=d(iq)-d(ip)
              if(abs(h)+g.eq.abs(h))then
                t=a(ip,iq)/h ! t = 1/(2 theta)
              else
                theta=0.5d0*h/a(ip,iq) ! Equation (11.1.10).
                t=1.0d0/(abs(theta)+sqrt(1.0d0+theta**2))
                if(theta.lt.0.0d0)t=-t
              endif
              c=1.0d0/sqrt(1+t**2)
              s=t*c
              tau=s/(1.0d0+c)
              h=t*a(ip,iq)
              z(ip)=z(ip)-h
              z(iq)=z(iq)+h
              d(ip)=d(ip)-h
              d(iq)=d(iq)+h
              a(ip,iq)=0.0d0
              do j=1,ip-1 ! Case of rotations 1  j < p.
                g=a(j,ip)
                h=a(j,iq)
                a(j,ip)=g-s*(h+g*tau)
                a(j,iq)=h+s*(g-h*tau)
              enddo
              do j=ip+1,iq-1 ! Case of rotations p < j < q.
                g=a(ip,j)
                h=a(j,iq)
                a(ip,j)=g-s*(h+g*tau)
                a(j,iq)=h+s*(g-h*tau)
              enddo
              do j=iq+1,n ! Case of rotations q < j  n.
                g=a(ip,j)
                h=a(iq,j)
                a(ip,j)=g-s*(h+g*tau)
                a(iq,j)=h+s*(g-h*tau)
              enddo
              do j=1,n
                g=v(j,ip)
                h=v(j,iq)
                v(j,ip)=g-s*(h+g*tau)
                v(j,iq)=h+s*(g-h*tau)
              enddo
              nrot=nrot+1
            endif
          enddo
        enddo
        do ip=1,n
          b(ip)=b(ip)+z(ip)
          d(ip)=b(ip) ! Update d with the sum of tapq ,
          z(ip)=0.0d0 ! and reinitialize z.
        enddo
      enddo
      print*,' too many iterations in jacobi'
      return
      END


      SUBROUTINE jacobi90(n,a,d,v,nrot)
      
      ! USE nrutil, ONLY : assert_eq,get_diag,nrerror,unit_matrix,upper_triangle
      
      IMPLICIT NONE
      ! integer, intent(in) :: n
      INTEGER, INTENT(OUT) :: nrot
      double precision, INTENT(OUT) :: d(n)
      double precision, INTENT(INOUT) :: a(n,n)
      double precision, INTENT(OUT) :: v(n,n)
      
      ! Computes all eigenvalues and eigenvectors of a real symmetric N x N matrix a. On output,
      ! elements of a above the diagonal are destroyed. d is a vector of length N that returns the
      ! eigenvalues of a. v is an N x N matrix whose columns contain, on output, the normalized
      ! eigenvectors of a. nrot returns the number of Jacobi rotations that were required.
      
      INTEGER :: i,ip,iq,n, j , k
      double precision :: c,g,h,s,sm,t,tau,theta,tresh
      double precision :: b(n),z(n)

      ! n=assert_eq((/size(a,1),size(a,2),size(d),size(v,1),size(v,2)/),jacobi)
      v = 0.0d0
      do i = 1, n
        ! call unit_matrix(v(:,:)) ! Initialize v to the identity matrix.
        v(i,i) = 1.0d0
        ! b(:)=get_diag(a(:,:)) ! Initialize b and d to the diagonal of a.
        b(i) = a(i,i)
        d(i) = b(i)
      end do
      z=0.0d0 ! This vector will accumulate terms of nrot=0 the form tapq as in eq. (11.1.14).
      do i=1,50
        ! sm=sum(abs(a),mask=upper_triangle(n,n)) ! Sum off-diagonal elements.
        sm = 0.0d0
        do j = 1, n
          do k = j+1, n
            sm = sm + a(j,k)
          end do
        end do
        if (sm == 0.0) RETURN
        ! The normal return, which relies on quadratic convergence to machine underflow.
        !tresh=merge(0.2d0*sm/n**2,0.0d0, i < 4 )
        if( i < 4 ) then
          tresh=0.0d0
        else
          tresh=0.2d0*sm/n**2
        end if
        ! On the first three sweeps, we will rotate only if tresh exceeded.
        do ip=1,n-1
          do iq=ip+1,n
            g=100.0d0*abs(a(ip,iq))
            ! After four sweeps, skip the rotation if the off-diagonal element is small.
            if ((i > 4) .and. (abs(d(ip))+g == abs(d(ip))) .and. (abs(d(iq))+g == abs(d(iq)))) then
              a(ip,iq)=0.0
            else if (abs(a(ip,iq)) > tresh) then
              h=d(iq)-d(ip)
              if (abs(h)+g == abs(h)) then
                t=a(ip,iq)/h ! t = 1/(2 theta)
              else
                theta=0.5d0*h/a(ip,iq) ! Equation (11.1.10).
                t=1.0d0/(abs(theta)+sqrt(1.0d0+theta**2))
                if (theta < 0.0) t=-t
              end if
              c=1.0d0/sqrt(1+t**2)
              s=t*c
              tau=s/(1.0d0+c)
              h=t*a(ip,iq)
              z(ip)=z(ip)-h
              z(iq)=z(iq)+h
              d(ip)=d(ip)-h
              d(iq)=d(iq)+h
              a(ip,iq)=0.0

              call jrotate(n,a(1:ip-1,ip),a(1:ip-1,iq),s,tau)
              ! Case of rotations 1  j < p.
              call jrotate(n,a(ip,ip+1:iq-1),a(ip+1:iq-1,iq),s,tau)
              ! Case of rotations p < j < q.
              call jrotate(n,a(ip,iq+1:n),a(iq,iq+1:n),s,tau)
              ! Case of rotations q < j  n.
              call jrotate(n,v(:,ip),v(:,iq),s,tau)
              nrot=nrot+1
            end if
          end do
        end do
        b(:)=b(:)+z(:)
        d(:)=b(:) ! Update d with the sum of tapq,
        z(:)=0.0d0 ! and reinitialize z.
      end do
      print*,'too many iterations in jacobi'
      END SUBROUTINE jacobi90


      SUBROUTINE jrotate(n,a1,a2,s,tau)
      implicit none
      integer n
      double precision, INTENT(INOUT) :: a1(n),a2(n)
      double precision :: wk1(n)
      double precision s, tau
      wk1(:)=a1(:)
      a1(:)=a1(:)-s*(a2(:)+a1(:)*tau)
      a2(:)=a2(:)+s*(wk1(:)-a2(:)*tau)
      END SUBROUTINE jrotate






      subroutine rswap(a,b)

      real a, b, c

      c = a
      a = b
      b = c

      end subroutine rswap

      subroutine dswap(a,b)

      double precision a, b, c

      c = a
      a = b
      b = c

      end subroutine dswap

!--------------------------------------------------------------------------------------
!   lgndr computes the legendre polynomial by an upward reccurence relation.
!   It returns both the legendre polynomial and its derivative. See Abramowitz chapter 8.
!    input:    n     polynomial order
!        x    polynamial argument part
!    output:    pn    polynomial value
!--------------------------------------------------------------------------------------

	  subroutine lgndr(n,x,pn,dpn)
      integer n, i, nmax
      double precision x, pn, dpn, a, b, p0, p1, p2, u, ak(0:21)

      p0 = 1.0d0
      p1 = x
      
      ! Get Legendre polynomials pn and pn-1 by recurance
      do i = 1, n - 1
        a  = dble(2*i+1) / dble(i+1)
        b  = dble(  i)   / dble(i+1)
        p2 = a * x * p1 - b * p0
        p0 = p1  ! p0 == pn-1
        p1 = p2  ! p1 == pn
      end do

      if(n.eq.0) then
        pn  = 1
        dpn = 0
      else
        pn = p1
        if(abs(x*x-1.0d0).gt.0.05/dble(n)) then
          dpn = dble(n) * ( x * p1 - p0 ) / ( x * x - 1.0d0 )
        else
          ! Uses hyper-geometric expansion around 1
          nmax = MIN( n , 20 )
          u = 0.5d0 * ( 1.0d0 - abs(x) )
          ak(0) = -0.5d0
          ak(1) =  0.5d0 * dble(n*(n+1))
          do i = 1, nmax - 1
            ak(i+1) = ( dble((-n+i)*(n+i+1)) / dble((i+1)*i) ) * ak(i)
          end do
          p2 = ak(nmax)
          do i = nmax - 1, 1, -1
            p2 = ak(i) + p2 * u
          end do
          if(x.gt.0) then
            dpn = p2
          else
            dpn = dpn
            if(modulo(n,2).eq.0) then
              ! n even, dpn/dx antisymetric
              dpn = -p2
            else
              ! n odd,  dpn/dx symetric
              dpn = p2
            end if
          end if
        end if
      end if

	  return
      end subroutine lgndr





	      FUNCTION plgndr(l,m,x)
	      INTEGER l,m
	      double precision plgndr,x
	      INTEGER i,ll
	      double precision fact,pll,pmm,pmmp1,somx2
!	      if(m.lt.0.or.m.gt.l.or.abs(x).gt.1.) then
!            print*,'bad arguments in plgndr'
!            stop
!          end if
	      pmm=1.0d0
	      if(m.gt.0) then
	        somx2=sqrt((1.0d0-x)*(1.0d0+x))
	        fact=1.0d0
	        do i=1,m
	          pmm=-pmm*fact*somx2
	          fact=fact+2.0d0
            end do
	      endif
	      if(l.eq.m) then
	        plgndr=pmm
	      else
	        pmmp1=x*dble(2*m+1)*pmm
	        if(l.eq.m+1) then
	          plgndr=pmmp1
	        else
	          do  ll=m+2,l
	            pll=(x*dble(2*ll-1)*pmmp1-dble(ll+m-1)*pmm)/dble(ll-m)
	            pmm=pmmp1
	            pmmp1=pll
              end do
	          plgndr=pll
	        endif
	      endif
	      return
	      END

	


	      SUBROUTINE splint(xa,ya,y2a,n,x,y)
	      INTEGER n
	      DOUBLE PRECISION x,y,xa(n),y2a(n),ya(n)
	      INTEGER k,khi,klo
	      DOUBLE PRECISION a,b,h
	      klo=1
	      khi=n
	1     if (khi-klo.gt.1) then
	        k=(khi+klo)/2.0D0
	        if(xa(k).gt.x)then
	          khi=k
	        else
	          klo=k
	        endif
	        goto 1
	      endif
	      h=xa(khi)-xa(klo)
	      if(h.eq.0.0D0) then
            print*, 'bad xa input in splint'
            stop
          end if
	      a=(xa(khi)-x)/h
	      b=(x-xa(klo))/h
	      y=a*ya(klo)+b*ya(khi)+((a**3.0D0-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h*h)/6.0D0
	      return
	      END



	      SUBROUTINE spline(x,y,n,yp1,ypn,y2)
	      INTEGER n,NMAX
	      DOUBLE PRECISION yp1,ypn,x(n),y(n),y2(n)
	      PARAMETER (NMAX=500)
	      INTEGER i,k
	      DOUBLE PRECISION p,qn,sig,un,u(NMAX)
	      if (yp1.gt.0.99d30) then
	        y2(1)=0.0d0
	        u(1)=0.0d0
	      else
	        y2(1)=-0.5d0
	        u(1)=(3.0d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
	      endif
	      do i=2,n-1
	        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
	        p=sig*y2(i-1)+2.0d0
	        y2(i)=(sig-1.0d0)/p
            u(i)=(y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1))
	        u(i)=(6.0d0*u(i)/(x(i+1)-x(i-1))-sig*u(i-1))/p
          end do
	      if (ypn.gt.0.99d30) then
	        qn=0.0d0
	        un=0.0d0
	      else
	        qn=0.5d0
	        un=(3.0d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
	      endif
	      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.0d0)
	      do k=n-1,1,-1
	        y2(k)=y2(k)*y2(k+1)+u(k)
          end do
	      return
	      END



      subroutine gmshSP(fil,x1,y1,z1)

      ! Writes-out a GMSH scalar point (SP)

      integer fil
      real x1, y1
      double precision z1

      write(fil,'(a2)')'SP'
      write(fil,'(a1)')'('
      write(fil,'(e18.10,2(a1,e19.10))') x1,',', y1,',',z1
      write(fil,'(a1)')
      write(fil,'(a1)')')'
      write(fil,'(a1)')'{'
      write(fil,'(e18.10)') z1
      write(fil,'(a2)')'};'

      end subroutine gmshSP


      subroutine gmshSL(fil,x1,y1,x2,y2)

      ! Writes-out a GMSH scalar line segment (SL)

      integer fil
      real x1, y1, x2, y2

      write(fil,'(a2)') 'SL'
      write(fil,'(a1)') '('
      write(fil,'(e18.10,2(a1,e19.10))') x1,',',y1,',',0.0
      write(fil,'(a1)') ','
      write(fil,'(e18.10,2(a1,e19.10))') x2,',',y2,',',0.0
      write(fil,'(a1)') ')'
      write(fil,'(a1)') '{'
      write(fil,'(e18.10)') 0.0
      write(fil,'(a1)') ','
      write(fil,'(e18.10)') 0.0
      write(fil,'(a2)') '};'

      end subroutine gmshSL


      subroutine gmshVP(fil,x,y,v,w)

      ! Writes-out a GMSH scalar line segment (SL)

      integer fil
      real x, y
      double precision v, w

      write(fil,'(a2)') 'VP'
      write(fil,'(a1)') '('
      write(fil,'(e18.10,2(a1,e19.10))') x,',',y,',',0.0
      write(fil,'(a1)') ')'
      write(fil,'(a1)') '{'
      write(fil,'(e18.10)') v
      write(fil,'(a1)') ','
      write(fil,'(e18.10)') w
      write(fil,'(a1)') ','
      write(fil,'(e18.10)') 0.0
      write(fil,'(a2)') '};'

      end subroutine gmshVP


      subroutine gmshSLd(fil,x1,y1,x2,y2,z)

      ! Writes-out a GMSH scalar line segment (SL) with height and end node

      integer fil
      real x1, y1, x2, y2
      double precision z

      write(fil,'(a2)') 'SL'
      write(fil,'(a1)') '('
      write(fil,'(e18.10,2(a1,e19.10))') x1,',',y1,',',z
      write(fil,'(a1)') ','
      write(fil,'(e18.10,2(a1,e19.10))') x2,',',y2,',',z
      write(fil,'(a1)') ')'
      write(fil,'(a1)') '{'
      write(fil,'(e18.10)') z
      write(fil,'(a1)') ','
      write(fil,'(e18.10)') z
      write(fil,'(a2)') '};'

      write(fil,'(a2)')'SP'
      write(fil,'(a1)')'('
      write(fil,'(e18.10,2(a1,e19.10))') x1,',', y1,',',z
      write(fil,'(a1)')
      write(fil,'(a1)')')'
      write(fil,'(a1)')'{'
      write(fil,'(e18.10)') z
      write(fil,'(a2)')'};'

      end subroutine gmshSLd




      subroutine gmshSQ(fil,x1,y1,x2,y2,h)

      ! Writes-out a line segment as a GMSH scalar Quadrangle with height h

      implicit none

      integer fil
      real x1, y1, x2, y2, s, nx, ny
      double precision h

      nx = y2 - y1; ny = x1 - x2
      s = 50.0 * sqrt( nx*nx + ny*ny )
      nx = nx / s; ny = ny / s

      write(fil,'(a2)') 'SQ'
      write(fil,'(a1)') '('
      write(fil,'(e18.10,2(a1,e19.10))') x1,',',y1,',',1000.0*real(h)
      write(fil,'(a1)') ','
      write(fil,'(e18.10,2(a1,e19.10))') x2,',',y2,',',1000.0*real(h)
      write(fil,'(a1)') ','
      write(fil,'(e18.10,2(a1,e19.10))') x2 + nx,',',y2 + ny,',',1000.0*real(h)
      write(fil,'(a1)') ','
      write(fil,'(e18.10,2(a1,e19.10))') x1 + nx,',',y1 + ny,',',1000.0*real(h)
      write(fil,'(a1)') ')'
      write(fil,'(a1)') '{'
      write(fil,'(e18.10)') 1000.0*real(h)
      write(fil,'(a1)') ','
      write(fil,'(e18.10)') 1000.0*real(h)
      write(fil,'(a1)') ','
      write(fil,'(e18.10)') 1000.0*real(h)
      write(fil,'(a1)') ','
      write(fil,'(e18.10)') 1000.0*real(h)
      write(fil,'(a2)') '};'

      end subroutine gmshSQ



      subroutine gmshST(fil,x1,y1,z1,x2,y2,z2,x3,y3,z3,flat)

      ! Writes-out a GMSH scalar triangle (ST)

      integer fil
      real x1, y1, x2, y2, x3, y3
      double precision z1, z2, z3
      logical flat

      write(fil,'(a2)')'ST'
      write(fil,'(a1)')'('
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x1,',', y1,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x1,',', y1,',',z1
      end if
      write(fil,'(a1)')','
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x2,',', y2,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x2,',', y2,',',z2
      end if
      write(fil,'(a1)')','
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x3,',', y3,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x3,',', y3,',',z3
      end if
      write(fil,'(a1)')')'
      write(fil,'(a1)')'{'
      write(fil,'(e18.10)') z1
      write(fil,'(a1)')','
      write(fil,'(e18.10)') z2
      write(fil,'(a1)')','
      write(fil,'(e18.10)') z3
      write(fil,'(a2)')'};'

      end subroutine gmshST



      subroutine gmshSTd(fil,x0,y0,z0,x1,y1,z1,x2,y2,z2,x3,y3,z3,flat)

      ! Writes-out a GMSH scalar triangle (ST)

      integer fil, i
      double precision x0, y0, x1, y1, x2, y2, x3, y3
      double precision z0, z1, z2, z3, x, y, z
      logical flat

      do i = 1, 3
        if(i.eq.1) then
          x = x1; y = y1; z = z1
        else if(i.eq.2) then
          x = x2; y = y2; z = z2
        else
          x = x3; y = y3; z = z3
        end if
      write(fil,'(a2)')'SL'
      write(fil,'(a1)')'('
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x0,',', y0,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x0,',', y0,',',z0
      end if
      write(fil,'(a1)')','
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x,',', y,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x,',', y,',',z
      end if
      write(fil,'(a1)')')'
      write(fil,'(a1)')'{'
      write(fil,'(e18.10)') z0
      write(fil,'(a1)')','
      write(fil,'(e18.10)') z
      write(fil,'(a2)')'};'

      end do

      write(fil,'(a2)')'ST'
      write(fil,'(a1)')'('
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x1,',', y1,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x1,',', y1,',',z1
      end if
      write(fil,'(a1)')','
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x2,',', y2,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x2,',', y2,',',z2
      end if
      write(fil,'(a1)')','
      if(flat) then
        write(fil,'(e18.10,2(a1,e19.10))') x3,',', y3,',',0.0d0
      else
        write(fil,'(e18.10,2(a1,e19.10))') x3,',', y3,',',z3
      end if
      write(fil,'(a1)')')'
      write(fil,'(a1)')'{'
      write(fil,'(e18.10)') z1
      write(fil,'(a1)')','
      write(fil,'(e18.10)') z2
      write(fil,'(a1)')','
      write(fil,'(e18.10)') z3
      write(fil,'(a2)')'};'

      end subroutine gmshSTd


      subroutine gmshOpen(fil,name)

      ! Starts a GMSH output file

      use CONTROL, only : prefix

      integer fil, posn
      character name*(*)

      posn = index(name,' ') - 1

      open(fil,file=prefix//name(1:posn)//'.pos')
      write(fil,'(a29)')'View "'//prefix//name(1:posn)//'" {'

      end subroutine gmshOpen



      subroutine gmshClose(fil)

      ! Terminates a GMSH output file

      integer fil
      
      write(fil,'(a2)')'};'
      write(fil,'(a1)')' '
      close(fil)

      end subroutine gmshClose



      real function cross(x1,y1,x2,y2)

      implicit none
      real x1, y1, x2, y2

      cross = ( x1 * y2 - x2 * y1 ) / ( sqrt(x1*x1+y1*y1) * sqrt(x2*x2+y2*y2) )

      end function cross




      real function crossLight(x1,y1,x2,y2)

      implicit none
      real x1, y1, x2, y2

      crossLight = x1 * y2 - x2 * y1

      end function crossLight



      double precision function dcross(x1,y1,x2,y2)

      implicit none
      double precision x1, y1, x2, y2

      dcross = ( x1 * y2 - x2 * y1 ) / ( sqrt(x1*x1+y1*y1) * sqrt(x2*x2+y2*y2) )

      end function dcross


      double precision function I100(m)

      implicit none

      double precision m, EllipticK

      I100 = EllipticK(m)

      end function I100



      double precision function I120(m)

      implicit none

      double precision m, mm, EllipticK, EllipticE

      mm = 1.0d0 - m

      I120 =   EllipticE(m)      / m  &
             - EllipticK(m) * mm / m

      end function I120



      double precision function I102(m)

      implicit none

      double precision m, EllipticK, EllipticE

      I102 = - EllipticE(m) / m  &
             + EllipticK(m) / m

      end function I102



      double precision function I300(m)

      implicit none

      double precision m, EllipticE

      I300 = EllipticE(m) / ( m - 1.0d0 )

      end function I300




      double precision function I500(m)

      implicit none

      double precision m, mm, EllipticK, EllipticE

      mm = 1.0d0 - m

      I500 =   EllipticE(m) * 2.0d0 * ( mm + 1.0d0 ) / ( 3.0d0 * mm * mm )  &
             - EllipticK(m)                          / ( 3.0d0      * mm )

      end function I500



      
      double precision function I502(m)

      implicit none

      double precision m, mm, EllipticK, EllipticE

      mm = 1.0d0 - m

      I502 =   EllipticE(m) * ( m + 1.0d0 ) / ( 3.0d0 * mm * mm * m )  &
             - EllipticK(m)                 / ( 3.0d0 * mm      * m )

      end function I502



      double precision function I520(m)

      implicit none

      double precision m, mm, EllipticK, EllipticE

      mm = 1.0d0 - m

      I520 =   EllipticE(m) * ( 2.0d0 * m - 1.0d0 ) / ( 3.0d0 * mm * m )  &
             + EllipticK(m)                         / ( 3.0d0      * m )

      end function I520




      double precision function I522(m)

      implicit none

      double precision m, I322, I304, I320

      I522 = ( I304(m) + I322(m) - I320(m) ) / ( 3.0d0 * m )

      end function I522




      double precision function I542(m)

      implicit none

      double precision m, I322, I340

      I542 = ( 3.0D0 * I322(m) - I340(m) ) / ( 3.0d0 * m )

      end function I542



      double precision function I524(m)

      implicit none

      double precision m, I304, I322

      I524 = ( I304(m) - 3.0D0 * I322(m) ) / ( 3.0d0 * m )

      end function I524




      double precision function I322(m)

      implicit none

      double precision m, EllipticK, EllipticE

      I322 = - EllipticE(m) *   2.0d0       / ( m * m )  &
             + EllipticK(m) * ( 2.0d0 - m ) / ( m * m )

      end function I322





      double precision function I304(m)

      implicit none

      double precision m, mm, EllipticK, EllipticE

      mm = 1.0d0 - m

      I304 =   EllipticE(m) * ( mm + 1.0d0 ) / ( mm * m * m )  &
             - EllipticK(m) * 2.0d0          / (      m * m )

      end function I304





      double precision function I340(m)

      implicit none

      double precision m, mm, EllipticK, EllipticE

      mm = 1.0d0 - m

      I340 =   EllipticE(m) * ( mm + 1.0d0 ) / ( m * m )  &
             - EllipticK(m) * 2.0d0 * mm     / ( m * m )

      end function I340





      double precision function I320(m)

      implicit none

      double precision m, EllipticK, EllipticE

      I320 = - EllipticE(m) / m  &
             + EllipticK(m) / m

      end function I320




      double precision function I302(m)

      implicit none

      double precision m, EllipticK, EllipticE

      I302 = EllipticE(m) / ( m - m * m ) &
           - EllipticK(m) / m

      end function I302



      double precision function I540(m)

      implicit none

      double precision m, I520, I522

      I540 = I520(m) - I522(m)

      end function I540




      double precision function I504(m)

      implicit none

      double precision m, I502, I522

      I504 = I502(m) - I522(m)

      end function I504



      double precision function I560(m)

      implicit none

      double precision m, I540, I542

      I560 = I540(m) - I542(m)

      end function I560




      double precision function I506(m)

      implicit none

      double precision m, I504, I524

      I506 = I504(m) - I524(m)

      end function I506



      !---------------------------------------------------------------
      !Computes complete elliptic integral of the second kind, E(m)
      !---------------------------------------------------------------

      double precision function EllipticE(m)

      use CONTROL, only : zero

      implicit none

      double precision m, rf, rd, sm, se
      save sm, se
      data sm /10.0d0/, se /0.0d0/

      if(abs(m-sm).lt.zero) then
        EllipticE = se
      else
      EllipticE = rf(0.0d0,1.0d0-m,1.0d0) - rd(0.0d0,1.0d0-m,1.0d0) * m / 3.0d0
        sm = m; se = EllipticE
      end if

      end function EllipticE





      !---------------------------------------------------------------
      !Computes complete elliptic integral of the first kind, K(m)
      !---------------------------------------------------------------

      double precision function EllipticK(m)

      use CONTROL, only : zero

      implicit none

      double precision m, rf, sm, sk
      save sm, sk
      data sm /10.0d0/, sk /0.0d0/

      if(abs(m-sm).lt.zero) then
        EllipticK = sk
      else
      ellipticK = rf(0.0d0,1.0d0-m,1.0d0)
        sm = m; sk = EllipticK
      end if

      end function EllipticK




	      FUNCTION rd(x,y,z)
	      double precision rd,x,y,z,ERRTOL,TINY,BIG,C1,C2,C3,C4,C5,C6
	      PARAMETER (ERRTOL=2.5d-7,TINY=1.5d-58,BIG=3.d57,C1=3.0d0/14.0d0,C2=1.0d0/6.0d0,      &
	                                                          C3=9.0d0/22.0d0,C4=3.0d0/26.0d0,C5=0.25d0*C3,C6=1.5d0*C4)
	      DOUBLE PRECISION alamb,ave,delx,dely,delz,ea,eb,ec,ed,ee,fac,sqrtx,sqrty,sqrtz,sum,xt,yt,zt
	      if((min(x,y).lt.0.0D0).or.(min(x+y,z).lt.TINY).or.(max(x,y,z).gt.BIG)) THEN
            print*,'invalid arguments in rd'
            stop
          end if
	      xt=x
	      yt=y
	      zt=z
	      sum=0.0d0
	      fac=1.0d0
	1     continue
	        sqrtx=sqrt(xt)
	        sqrty=sqrt(yt)
	        sqrtz=sqrt(zt)
	        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
	        sum=sum+fac/(sqrtz*(zt+alamb))
	        fac=0.25d0*fac
	        xt=0.25d0*(xt+alamb)
	        yt=0.25d0*(yt+alamb)
	        zt=0.25d0*(zt+alamb)
	        ave=0.2d0*(xt+yt+3.*zt)
	        delx=(ave-xt)/ave
	        dely=(ave-yt)/ave
	        delz=(ave-zt)/ave
	      if(max(abs(delx),abs(dely),abs(delz)).gt.ERRTOL) goto 1
	      ea=delx*dely
	      eb=delz*delz
	      ec=ea-eb
	      ed=ea-6.0d0*eb
	      ee=ed+ec+ec
	      rd=3.0d0*sum+fac*(1.0d0+ed*(-C1+C5*ed-C6*delz*ee)+delz*(C2*ee+delz*(-C3*ec+delz*C4*ea)))/(ave*sqrt(ave))
	      return
	      END


	      FUNCTION rf(x,y,z)
	      double precision rf,x,y,z,ERRTOL,TINY,BIG,THIRD,C1,C2,C3,C4
	      PARAMETER(ERRTOL=2.5d-7,TINY=1.5d-58,BIG=3.d57,THIRD=1.0d0/3.0d0, &
                             C1=1.0d0/24.0d0,C2=0.1d0,C3=3.0d0/44.0d0,C4=1.0d0/14.0d0)
	      DOUBLE PRECISION alamb,ave,delx,dely,delz,e2,e3,sqrtx,sqrty,sqrtz,xt,yt,zt
!	      if((min(x,y,z).lt.0.0d0).or.(min(x+y,x+z,y+z).lt.TINY).or.(max(x,y,z).gt.BIG)) then
!            print*,'invalid arguments in rf'
!            stop
!          end if
	      xt=x
	      yt=y
	      zt=z
	1     continue
	        sqrtx=sqrt(xt)
	        sqrty=sqrt(yt)
	        sqrtz=sqrt(zt)
	        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
	        xt=0.25d0*(xt+alamb)
	        yt=0.25d0*(yt+alamb)
	        zt=0.25d0*(zt+alamb)
	        ave=THIRD*(xt+yt+zt)
	        delx=(ave-xt)/ave
	        dely=(ave-yt)/ave
	        delz=(ave-zt)/ave
	      if(max(abs(delx),abs(dely),abs(delz)).gt.ERRTOL) goto 1
	      e2=delx*dely
	      e3=e2*delz
	      e2=e2-delz**2
	      rf=(1.0d0+(C1*e2-C2-C3*e3)*e2+C4*e3)/sqrt(ave)
	      return
	      END




! ----------------------------------------------------------
!                     repgauss.f
!       Stand-alone Gauss quadrature subroutine
!                   Implemented by
!                  Matthias Maischak
!      email: maischak@ifam.uni-hannover.de
!              Version 1.0.0  (18.4.1997)
! ----------------------------------------------------------
 
 subroutine Gauss(gqn,gqk,gqw)

 use CONTROL, only : gint

 implicit none
 integer gqn,i,re
 double precision gqk(-32:32),gqw(-32:32)

 ! Initialize the positions and weights
 gqk = 0.0d0; gqw = 0.0d0

 ! Check for over-riding user given quadrature order
 gqn = max(gqn,nint(gint))

 ! Set the "re" for the filling
 re = floor(real(gqn+2)/2.0)

 if (gqn.eq.1) then
  gqk(re-1)= 0.0
  gqw(re-1)= 2.0
 else if (gqn.eq.2) then
  gqk(re-1)= 0.577350269189625620
  gqw(re-1)= 1.0
 else if (gqn.eq.3) then
  gqk(re-1)= 0.774596669241483293
  gqk(re-2)= 0.0
 
  gqw(re-1)= 0.555555555555555555
  gqw(re-2)= 0.888888888888888888
 else if (gqn.eq.4) then
  gqk(re-1)= 0.861136311594052350
  gqk(re-2)= 0.339981043584856257
 
  gqw(re-1)= 0.347854845137453850
  gqw(re-2)= 0.652145154862545984
 else if (gqn.eq.5) then
  gqk(re-1)= 0.906179845938664075
  gqk(re-2)= 0.538469310105683108
  gqk(re-3)= 0.0
 
  gqw(re-1)= 0.236926885056188752
  gqw(re-2)= 0.478628670499366859
  gqw(re-3)= 0.568888888888888888
 else if (gqn.eq.6) then
  gqk(re-1)= 0.932469514203151828
  gqk(re-2)= 0.661209386466264815
  gqk(re-3)= 0.238619186083196849
 
  gqw(re-1)= 0.171324492379170384
  gqw(re-2)= 0.360761573048138051
  gqw(re-3)= 0.467913934572691148
 else if (gqn.eq.7) then
  gqk(re-1)= 0.949107912342758486
  gqk(re-2)= 0.741531185599394238
  gqk(re-3)= 0.405845151377397073
  gqk(re-4)= 0.0
 
  gqw(re-1)= 0.129484966168869536
  gqw(re-2)= 0.279705391489276756
  gqw(re-3)= 0.381830050505118757
  gqw(re-4)= 0.417959183673470069
 else if (gqn.eq.8) then
  gqk(re-1)= 0.960289856497536176
  gqk(re-2)= 0.796666477413626950
  gqk(re-3)= 0.525532409916329102
  gqk(re-4)= 0.183434642495650141
 
  gqw(re-1)= 0.101228536290375815
  gqw(re-2)= 0.222381034453374871
  gqw(re-3)= 0.313706645877887436
  gqw(re-4)= 0.362683783378362490
 else if (gqn.eq.9) then
  gqk(re-1)= 0.968160239507626308
  gqk(re-2)= 0.836031107326636325
  gqk(re-3)= 0.613371432700590025
  gqk(re-4)= 0.324253423403809193
  gqk(re-5)= 0.0
 
  gqw(re-1)= 0.812743883615742180E-01
  gqw(re-2)= 0.180648160694857840
  gqw(re-3)= 0.260610696402935771
  gqw(re-4)= 0.312347077040002585
  gqw(re-5)= 0.330239355001259838
 else if (gqn.eq.10) then
  gqk(re-1)= 0.973906528517172188
  gqk(re-2)= 0.865063366688984536
  gqk(re-3)= 0.679409568299024325
  gqk(re-4)= 0.433395394129247102
  gqk(re-5)= 0.148874338981631021
 
  gqw(re-1)= 0.666713443086878466E-01
  gqw(re-2)= 0.149451349150581003
  gqw(re-3)= 0.219086362515982319
  gqw(re-4)= 0.269266719309995850
  gqw(re-5)= 0.295524224714753536
 else if (gqn.eq.11) then
  gqk(re-1)= 0.978228658146056973
  gqk(re-2)= 0.887062599768095428
  gqk(re-3)= 0.730152005574049467
  gqk(re-4)= 0.519096129206811696
  gqk(re-5)= 0.269543155952345015
  gqk(re-6)= 0.0
 
  gqw(re-1)= 0.556685671161738088E-01
  gqw(re-2)= 0.125580369464904501
  gqw(re-3)= 0.186290210927734762
  gqw(re-4)= 0.233193764591990760
  gqw(re-5)= 0.262804544510246763
  gqw(re-6)= 0.272925086777901227
 else if (gqn.eq.12) then
  gqk(re-1)= 0.981560634246719466
  gqk(re-2)= 0.904117256370474798
  gqk(re-3)= 0.769902674194304804
  gqk(re-4)= 0.587317954286617705
  gqk(re-5)= 0.367831498998180295
  gqk(re-6)= 0.125233408511468802
 
  gqw(re-1)= 0.471753363865118000E-01
  gqw(re-2)= 0.106939325995318371
  gqw(re-3)= 0.160078328543346332
  gqw(re-4)= 0.203167426723065897
  gqw(re-5)= 0.233492536538355083
  gqw(re-6)= 0.249147045813402523
 else if (gqn.eq.13) then
  gqk(re-1)= 0.984183054718588246
  gqk(re-2)= 0.917598399222978034
  gqk(re-3)= 0.801578090733310100
  gqk(re-4)= 0.642349339440340117
  gqk(re-5)= 0.448492751036446979
  gqk(re-6)= 0.230458315955134857
  gqk(re-7)= 0.0
 
  gqw(re-1)= 0.404840047653160576E-01
  gqw(re-2)= 0.921214998377277300E-01
  gqw(re-3)= 0.138873510219787749
  gqw(re-4)= 0.178145980761945599
  gqw(re-5)= 0.207816047536888288
  gqw(re-6)= 0.226283180262897482
  gqw(re-7)= 0.232551553230874147
 else if (gqn.eq.14) then
  gqk(re-1)= 0.986283808696812425
  gqk(re-2)= 0.928434883663573740
  gqk(re-3)= 0.827201315069765131
  gqk(re-4)= 0.687292904811685479
  gqk(re-5)= 0.515248636358154100
  gqk(re-6)= 0.319112368927890022
  gqk(re-7)= 0.108054948707343446
 
  gqw(re-1)= 0.351194603317515619E-01
  gqw(re-2)= 0.801580871597602773E-01
  gqw(re-3)= 0.121518570687903338
  gqw(re-4)= 0.157203167158194185
  gqw(re-5)= 0.185538397477937378
  gqw(re-6)= 0.205198463721295549
  gqw(re-7)= 0.215263853463158183
 else if (gqn.eq.15) then
  gqk(re-1)= 0.987992518020485377
  gqk(re-2)= 0.937273392400706062
  gqk(re-3)= 0.848206583410427206
  gqk(re-4)= 0.724417731360170181
  gqk(re-5)= 0.570972172608538608
  gqk(re-6)= 0.394151347077563718
  gqk(re-7)= 0.201194093997434681
  gqk(re-8)= 0.0
 
  gqw(re-1)= 0.307532419961175710E-01
  gqw(re-2)= 0.703660474881077774E-01
  gqw(re-3)= 0.107159220467171745
  gqw(re-4)= 0.139570677926154629
  gqw(re-5)= 0.166269205816994003
  gqw(re-6)= 0.186161000015561739
  gqw(re-7)= 0.198431485327110607
  gqw(re-8)= 0.202578241925561203
 else if (gqn.eq.16) then
  gqk(re-1)= 0.989400934991649939
  gqk(re-2)= 0.944575023073232711
  gqk(re-3)= 0.865631202387832088
  gqk(re-4)= 0.755404408355002999
  gqk(re-5)= 0.617876244402643660
  gqk(re-6)= 0.458016777657227148
  gqk(re-7)= 0.281603550779258804
  gqk(re-8)= 0.950125098376374960E-01
 
  gqw(re-1)= 0.271524594117547209E-01
  gqw(re-2)= 0.622535239386470193E-01
  gqw(re-3)= 0.951585116824925220E-01
  gqw(re-4)= 0.124628971255534918
  gqw(re-5)= 0.149595988816576736
  gqw(re-6)= 0.169156519395003035
  gqw(re-7)= 0.182603415044922862
  gqw(re-8)= 0.189450610455068197
 else if (gqn.eq.17) then
  gqk(re-1)= 0.990575475314417475
  gqk(re-2)= 0.950675521768767906
  gqk(re-3)= 0.880239153726986134
  gqk(re-4)= 0.781514003896801257
  gqk(re-5)= 0.657671159216690504
  gqk(re-6)= 0.512690537086476605
  gqk(re-7)= 0.351231763453876189
  gqk(re-8)= 0.178484181495847827
  gqk(re-9)= 0.0
 
  gqw(re-1)= 0.241483028685475776E-01
  gqw(re-2)= 0.554595293739875497E-01
  gqw(re-3)= 0.850361483171787474E-01
  gqw(re-4)= 0.111883847193404121
  gqw(re-5)= 0.135136368468525642
  gqw(re-6)= 0.154045761076810533
  gqw(re-7)= 0.168004102156449758
  gqw(re-8)= 0.176562705366992201
  gqw(re-9)= 0.179446470356206478
 else if (gqn.eq.18) then
  gqk(re-1)= 0.991565168420931231
  gqk(re-2)= 0.955823949571397602
  gqk(re-3)= 0.892602466497555924
  gqk(re-4)= 0.803704958972522809
  gqk(re-5)= 0.691687043060353113
  gqk(re-6)= 0.559770831073947206
  gqk(re-7)= 0.411751161462842408
  gqk(re-8)= 0.251886225691505372
  gqk(re-9)= 0.847750130417353337E-01
 
  gqw(re-1)= 0.216160135264829786E-01
  gqw(re-2)= 0.497145488949696929E-01
  gqw(re-3)= 0.764257302548905643E-01
  gqw(re-4)= 0.100942044106287279
  gqw(re-5)= 0.122555206711478751
  gqw(re-6)= 0.140642914670649877
  gqw(re-7)= 0.154684675126265853
  gqw(re-8)= 0.164276483745831675
  gqw(re-9)= 0.169142382963144267
 else if (gqn.eq.19) then
  gqk(re-1)= 0.992406843843584130
  gqk(re-2)= 0.960208152134829795
  gqk(re-3)= 0.903155903614817901
  gqk(re-4)= 0.822714656537142930
  gqk(re-5)= 0.720966177335229386
  gqk(re-6)= 0.600545304661680546
  gqk(re-7)= 0.464570741375960994
  gqk(re-8)= 0.316564099963629553
  gqk(re-9)= 0.160358645640225478
  gqk(re-10)= 0.0
 
  gqw(re-1)= 0.194617882297267140E-01
  gqw(re-2)= 0.448142267656994678E-01
  gqw(re-3)= 0.690445427376410320E-01
  gqw(re-4)= 0.914900216224504431E-01
  gqw(re-5)= 0.111566645547334364
  gqw(re-6)= 0.128753962539336297
  gqw(re-7)= 0.142606702173606104
  gqw(re-8)= 0.152766042065859725
  gqw(re-9)= 0.158968843393954451
  gqw(re-10)= 0.161054449848783754
 else if (gqn.eq.20) then
  gqk(re-1)= 0.993128599185095107
  gqk(re-2)= 0.963971927277913698
  gqk(re-3)= 0.912234428251326168
  gqk(re-4)= 0.839116971822219115
  gqk(re-5)= 0.746331906460150796
  gqk(re-6)= 0.636053680726514914
  gqk(re-7)= 0.510867001950827126
  gqk(re-8)= 0.373706088715419771
  gqk(re-9)= 0.227785851141645096
  gqk(re-10)= 0.765265211334972273E-01
 
  gqw(re-1)= 0.176140071391518681E-01
  gqw(re-2)= 0.406014298003870081E-01
  gqw(re-3)= 0.626720483341083323E-01
  gqw(re-4)= 0.832767415767049490E-01
  gqw(re-5)= 0.101930119817240816
  gqw(re-6)= 0.118194531961518134
  gqw(re-7)= 0.131688638449176554
  gqw(re-8)= 0.142096109318382652
  gqw(re-9)= 0.149172986472603242
  gqw(re-10)= 0.152753387130725754
 else if (gqn.eq.21) then
  gqk(re-1)= 0.993752170620389785
  gqk(re-2)= 0.967226838566306424
  gqk(re-3)= 0.920099334150400683
  gqk(re-4)= 0.853363364583317074
  gqk(re-5)= 0.768439963475677779
  gqk(re-6)= 0.667138804197412338
  gqk(re-7)= 0.551618835887219716
  gqk(re-8)= 0.424342120207438889
  gqk(re-9)= 0.288021316802401006
  gqk(re-10)= 0.145561854160894955
  gqk(re-11)= 0.0
 
  gqw(re-1)= 0.160172282577742686E-01
  gqw(re-2)= 0.369537897708525492E-01
  gqw(re-3)= 0.571344254268573923E-01
  gqw(re-4)= 0.761001136283792068E-01
  gqw(re-5)= 0.934444234560339593E-01
  gqw(re-6)= 0.108797299167148837
  gqw(re-7)= 0.121831416053728478
  gqw(re-8)= 0.132268938633337607
  gqw(re-9)= 0.139887394791073261
  gqw(re-10)= 0.144524403989969630
  gqw(re-11)= 0.146081133649690303
 else if (gqn.eq.22) then
  gqk(re-1)= 0.994294585482399240
  gqk(re-2)= 0.970060497835429025
  gqk(re-3)= 0.926956772187173983
  gqk(re-4)= 0.865812577720299958
  gqk(re-5)= 0.787816805979208445
  gqk(re-6)= 0.694487263186682635
  gqk(re-7)= 0.587640403506911269
  gqk(re-8)= 0.469355837986756952
  gqk(re-9)= 0.341935820892084352
  gqk(re-10)= 0.207860426688221578
  gqk(re-11)= 0.697392733197224751E-01
 
  gqw(re-1)= 0.146279952982718842E-01
  gqw(re-2)= 0.337749015848136103E-01
  gqw(re-3)= 0.522933351526838133E-01
  gqw(re-4)= 0.697964684245209743E-01
  gqw(re-5)= 0.859416062170672845E-01
  gqw(re-6)= 0.100414144442881270
  gqw(re-7)= 0.112932296080539132
  gqw(re-8)= 0.123252376810512224
  gqw(re-9)= 0.131173504787062994
  gqw(re-10)= 0.136541498346015533
  gqw(re-11)= 0.139251872855632342
 else if (gqn.eq.23) then
  gqk(re-1)= 0.994769334997552157
  gqk(re-2)= 0.972542471218115434
  gqk(re-3)= 0.932971086826016260
  gqk(re-4)= 0.876752358270441845
  gqk(re-5)= 0.804888401618839899
  gqk(re-6)= 0.718661363131950615
  gqk(re-7)= 0.619609875763646234
  gqk(re-8)= 0.509501477846007744
  gqk(re-9)= 0.390301038030290870
  gqk(re-10)= 0.264135680970344955
  gqk(re-11)= 0.133256824298466026
  gqk(re-12)= 0.0
 
  gqw(re-1)= 0.134118594871415770E-01
  gqw(re-2)= 0.309880058569792539E-01
  gqw(re-3)= 0.480376717310841278E-01
  gqw(re-4)= 0.642324214085256556E-01
  gqw(re-5)= 0.792814117767189075E-01
  gqw(re-6)= 0.929157660600352514E-01
  gqw(re-7)= 0.104892091464541898
  gqw(re-8)= 0.114996640222411114
  gqw(re-9)= 0.123049084306730075
  gqw(re-10)= 0.128905722188081689
  gqw(re-11)= 0.132462039404696807
  gqw(re-12)= 0.133654572186106213
 else if (gqn.eq.24) then
  gqk(re-1)= 0.995187219997021644
  gqk(re-2)= 0.974728555971310140
  gqk(re-3)= 0.938274552002732576
  gqk(re-4)= 0.886415527004401294
  gqk(re-5)= 0.820001985973903058
  gqk(re-6)= 0.740124191578554247
  gqk(re-7)= 0.648093651936975323
  gqk(re-8)= 0.545421471388839563
  gqk(re-9)= 0.433793507626044850
  gqk(re-10)= 0.315042679696163508
  gqk(re-11)= 0.191118867473616172
  gqk(re-12)= 0.640568928626053941E-01
 
  gqw(re-1)= 0.123412297999875385E-01
  gqw(re-2)= 0.285313886289334240E-01
  gqw(re-3)= 0.442774388174193220E-01
  gqw(re-4)= 0.592985849154368597E-01
  gqw(re-5)= 0.733464814110801611E-01
  gqw(re-6)= 0.861901615319533992E-01
  gqw(re-7)= 0.976186521041138705E-01
  gqw(re-8)= 0.107444270115966065
  gqw(re-9)= 0.115505668053725516
  gqw(re-10)= 0.121670472927803003
  gqw(re-11)= 0.125837456346828025
  gqw(re-12)= 0.127938195346752714
 else if (gqn.eq.25) then
  gqk(re-1)= 0.995556969790497792
  gqk(re-2)= 0.976663921459517637
  gqk(re-3)= 0.942974571228974434
  gqk(re-4)= 0.894991997878275103
  gqk(re-5)= 0.833442628760833970
  gqk(re-6)= 0.759259263037357690
  gqk(re-7)= 0.673566368473468402
  gqk(re-8)= 0.577662930241222838
  gqk(re-9)= 0.473002731445714919
  gqk(re-10)= 0.361172305809387695
  gqk(re-11)= 0.243866883720988331
  gqk(re-12)= 0.122864692610710341
  gqk(re-13)= 0.0
 
  gqw(re-1)= 0.113937985010262918E-01
  gqw(re-2)= 0.263549866150318245E-01
  gqw(re-3)= 0.409391567013068711E-01
  gqw(re-4)= 0.549046959758347913E-01
  gqw(re-5)= 0.680383338123570630E-01
  gqw(re-6)= 0.801407003350009944E-01
  gqw(re-7)= 0.910282619829641398E-01
  gqw(re-8)= 0.100535949067050615
  gqw(re-9)= 0.108519624474263013
  gqw(re-10)= 0.114858259145712072
  gqw(re-11)= 0.119455763535784104
  gqw(re-12)= 0.122242442990309827
  gqw(re-13)= 0.123176053726715515
 else if (gqn.eq.26) then
  gqk(re-1)= 0.995885701145616808
  gqk(re-2)= 0.978385445956471811
  gqk(re-3)= 0.947159066661714122
  gqk(re-4)= 0.902637861984306844
  gqk(re-5)= 0.845445942788497717
  gqk(re-6)= 0.776385948820678573
  gqk(re-7)= 0.696427260419957395
  gqk(re-8)= 0.606692293017618067
  gqk(re-9)= 0.508440714824505591
  gqk(re-10)= 0.403051755123486288
  gqk(re-11)= 0.292004839485956735
  gqk(re-12)= 0.176858820356889934
  gqk(re-13)= 0.592300934293129369E-01
 
  gqw(re-1)= 0.105513726173427809E-01
  gqw(re-2)= 0.244178510926317607E-01
  gqw(re-3)= 0.379623832943630224E-01
  gqw(re-4)= 0.509758252971478643E-01
  gqw(re-5)= 0.632740463295748540E-01
  gqw(re-6)= 0.746841497656597214E-01
  gqw(re-7)= 0.850458943134855677E-01
  gqw(re-8)= 0.942138003559136183E-01
  gqw(re-9)= 0.102059161094425407
  gqw(re-10)= 0.108471840528576668
  gqw(re-11)= 0.113361816546319771
  gqw(re-12)= 0.116660443485296764
  gqw(re-13)= 0.118321415279262029
 else if (gqn.eq.27) then
  gqk(re-1)= 0.996179262888988282
  gqk(re-2)= 0.979923475961501089
  gqk(re-3)= 0.950900557814704839
  gqk(re-4)= 0.909482320677491352
  gqk(re-5)= 0.856207908018294828
  gqk(re-6)= 0.791771639070508182
  gqk(re-7)= 0.717013473739423590
  gqk(re-8)= 0.632907971946494952
  gqk(re-9)= 0.540551564579456745
  gqk(re-10)= 0.441148251750026865
  gqk(re-11)= 0.335993903638508784
  gqk(re-12)= 0.226459365439536792
  gqk(re-13)= 0.113972585609529858
  gqk(re-14)= 0.0
 
  gqw(re-1)= 0.979899605129469845E-02
  gqw(re-2)= 0.226862315961806719E-01
  gqw(re-3)= 0.352970537574195109E-01
  gqw(re-4)= 0.474494125206146450E-01
  gqw(re-5)= 0.589835368598338391E-01
  gqw(re-6)= 0.697488237662459570E-01
  gqw(re-7)= 0.796048677730574611E-01
  gqw(re-8)= 0.884231585437570827E-01
  gqw(re-9)= 0.960887273700287425E-01
  gqw(re-10)= 0.102501637817746655
  gqw(re-11)= 0.107578285788533137
  gqw(re-12)= 0.111252488356845022
  gqw(re-13)= 0.113476346108965578
  gqw(re-14)= 0.114220867378956217
 
 else if (gqn.eq.28) then
  gqk(re-1)= 0.996442497573954977
  gqk(re-2)= 0.981303165370872810
  gqk(re-3)= 0.954259280628938722
  gqk(re-4)= 0.915633026392132510
  gqk(re-5)= 0.865892522574395307
  gqk(re-6)= 0.805641370917179356
  gqk(re-7)= 0.735610878013631453
  gqk(re-8)= 0.656651094038864791
  gqk(re-9)= 0.569720471811402063
  gqk(re-10)= 0.475874224955118330
  gqk(re-11)= 0.376251516089078641
  gqk(re-12)= 0.272061627635178382
  gqk(re-13)= 0.164569282133380540
  gqk(re-14)= 0.550792898840344117E-01
 
  gqw(re-1)= 0.912428259309441651E-02
  gqw(re-2)= 0.211321125927711427E-01
  gqw(re-3)= 0.329014277823043502E-01
  gqw(re-4)= 0.442729347590044148E-01
  gqw(re-5)= 0.551073456757167970E-01
  gqw(re-6)= 0.652729239669994216E-01
  gqw(re-7)= 0.746462142345691582E-01
  gqw(re-8)= 0.831134172289010320E-01
  gqw(re-9)= 0.905717443930330185E-01
  gqw(re-10)= 0.969306579979299782E-01
  gqw(re-11)= 0.102112967578060251
  gqw(re-12)= 0.106055765922846670
  gqw(re-13)= 0.108711192258294398
  gqw(re-14)= 0.110047013016474640
 else if (gqn.eq.29) then
  gqk(re-1)= 0.996679442260596238
  gqk(re-2)= 0.982545505261413044
  gqk(re-3)= 0.957285595778087917
  gqk(re-4)= 0.921180232953058731
  gqk(re-5)= 0.874637804920103012
  gqk(re-6)= 0.818185487615252671
  gqk(re-7)= 0.752462851734477134
  gqk(re-8)= 0.678214537602686462
  gqk(re-9)= 0.596281797138227931
  gqk(re-10)= 0.507592955124227641
  gqk(re-11)= 0.413152888174008470
  gqk(re-12)= 0.314031637867639823
  gqk(re-13)= 0.211352286166001213
  gqk(re-14)= 0.106278230132679244
  gqk(re-15)= 0.0
 
  gqw(re-1)= 0.851690387874670489E-02
  gqw(re-2)= 0.197320850561229638E-01
  gqw(re-3)= 0.307404922020933537E-01
  gqw(re-4)= 0.414020625186822880E-01
  gqw(re-5)= 0.515948269024979200E-01
  gqw(re-6)= 0.612030906570794897E-01
  gqw(re-7)= 0.701179332550518619E-01
  gqw(re-8)= 0.782383271357637994E-01
  gqw(re-9)= 0.854722573661721025E-01
  gqw(re-10)= 0.917377571392589131E-01
  gqw(re-11)= 0.969638340944079247E-01
  gqw(re-12)= 0.101091273759915293
  gqw(re-13)= 0.104073310077729880
  gqw(re-14)= 0.105876155097320734
  gqw(re-15)= 0.106479381718314892
 else if (gqn.eq.30) then
  gqk(re-1)= 0.996893484074649616
  gqk(re-2)= 0.983668123279747175
  gqk(re-3)= 0.960021864968307437
  gqk(re-4)= 0.926200047429274309
  gqk(re-5)= 0.882560535792052736
  gqk(re-6)= 0.829565762382768024
  gqk(re-7)= 0.767777432104826185
  gqk(re-8)= 0.697850494793315734
  gqk(re-9)= 0.620526182989243225
  gqk(re-10)= 0.536624148142020307
  gqk(re-11)= 0.447033769538089376
  gqk(re-12)= 0.352704725530877838
  gqk(re-13)= 0.254636926167889743
  gqk(re-14)= 0.153869913608583514
  gqk(re-15)= 0.514718425553179065E-01
 
  gqw(re-1)= 0.796819249616657205E-02
  gqw(re-2)= 0.184664683110913989E-01
  gqw(re-3)= 0.287847078833229422E-01
  gqw(re-4)= 0.387991925696270709E-01
  gqw(re-5)= 0.484026728305940526E-01
  gqw(re-6)= 0.574931562176197244E-01
  gqw(re-7)= 0.659742298821796719E-01
  gqw(re-8)= 0.737559747377051766E-01
  gqw(re-9)= 0.807558952294199633E-01
  gqw(re-10)= 0.868997872010830175E-01
  gqw(re-11)= 0.921225222377861780E-01
  gqw(re-12)= 0.963687371746445171E-01
  gqw(re-13)= 0.995934205867948230E-01
  gqw(re-14)= 0.101762389748404750
  gqw(re-15)= 0.102852652893557994
 else if (gqn.eq.31) then
  gqk(re-1)= 0.997087481819476928
  gqk(re-2)= 0.984685909665152459
  gqk(re-3)= 0.962503925092950019
  gqk(re-4)= 0.930756997896648453
  gqk(re-5)= 0.889760029948271081
  gqk(re-6)= 0.839920320146267496
  gqk(re-7)= 0.781733148416624890
  gqk(re-8)= 0.715776784586853343
  gqk(re-9)= 0.642706722924260454
  gqk(re-10)= 0.563249161407149312
  gqk(re-11)= 0.478193782044902316
  gqk(re-12)= 0.388385901608233330
  gqk(re-13)= 0.294718069981701580
  gqk(re-14)= 0.198121199335570625
  gqk(re-15)= 0.995553121523416740E-01
  gqk(re-16)= 0.0
 
  gqw(re-1)= 0.747083157924885010E-02
  gqw(re-2)= 0.173186207903102581E-01
  gqw(re-3)= 0.270090191849792424E-01
  gqw(re-4)= 0.364322739123857306E-01
  gqw(re-5)= 0.454937075272009855E-01
  gqw(re-6)= 0.541030824249167513E-01
  gqw(re-7)= 0.621747865610288650E-01
  gqw(re-8)= 0.696285832354105322E-01
  gqw(re-9)= 0.763903865987760888E-01
  gqw(re-10)= 0.823929917615889990E-01
  gqw(re-11)= 0.875767406084783651E-01
  gqw(re-12)= 0.918901138936415590E-01
  gqw(re-13)= 0.952902429123198419E-01
  gqw(re-14)= 0.977433353863284837E-01
  gqw(re-15)= 0.992250112266724327E-01
  gqw(re-16)= 0.997205447934266659E-01
 else if (gqn.eq.32) then
  gqk(re-1)= 0.997263861849481903
  gqk(re-2)= 0.985611511545268937
  gqk(re-3)= 0.964762255587506057
  gqk(re-4)= 0.934906075937739667
  gqk(re-5)= 0.896321155766052313
  gqk(re-6)= 0.849367613732569748
  gqk(re-7)= 0.794483795967942386
  gqk(re-8)= 0.732182118740290155
  gqk(re-9)= 0.663044266930215231
  gqk(re-10)= 0.587715757240761749
  gqk(re-11)= 0.506899908932229137
  gqk(re-12)= 0.421351276130635610
  gqk(re-13)= 0.331868602282127834
  gqk(re-14)= 0.239287362252137092
  gqk(re-15)= 0.144471961582796293
  gqk(re-16)= 0.483076656877382618E-01
 
  gqw(re-1)= 0.701861000946991335E-02
  gqw(re-2)= 0.162743947309059410E-01
  gqw(re-3)= 0.253920653092619235E-01
  gqw(re-4)= 0.342738629130204947E-01
  gqw(re-5)= 0.428358980222272451E-01
  gqw(re-6)= 0.509980592623756890E-01
  gqw(re-7)= 0.586840934785354262E-01
  gqw(re-8)= 0.658222227763619466E-01
  gqw(re-9)= 0.723457941088502532E-01
  gqw(re-10)= 0.781938957870700058E-01
  gqw(re-11)= 0.833119242269453331E-01
  gqw(re-12)= 0.876520930044037694E-01
  gqw(re-13)= 0.911738786957642794E-01
  gqw(re-14)= 0.938443990808049688E-01
  gqw(re-15)= 0.956387200792744169E-01
  gqw(re-16)= 0.965400885147281312E-01
 else if (gqn.eq.33) then
  gqk(re-1)= 0.997424694246455079E+00
  gqk(re-2)= 0.986455726230642371E+00
  gqk(re-3)= 0.966822909689992738E+00
  gqk(re-4)= 0.938694372611168615E+00
  gqk(re-5)= 0.902316767743433612E+00
  gqk(re-6)= 0.858009652676504198E+00
  gqk(re-7)= 0.806162356274166547E+00
  gqk(re-8)= 0.747230496449561854E+00
  gqk(re-9)= 0.681731959969742562E+00
  gqk(re-10)= 0.610242345836379263E+00
  gqk(re-11)= 0.533389904786347957E+00
  gqk(re-12)= 0.451850017272450333E+00
  gqk(re-13)= 0.366339257748073188E+00
  gqk(re-14)= 0.277609097152496820E+00
  gqk(re-15)= 0.186439298827991456E+00
  gqk(re-16)= 0.936310658547332281E-01
  gqk(re-17)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.660622784758776657E-02
  gqw(re-2)= 0.153217015129345439E-01
  gqw(re-3)= 0.239155481017491525E-01
  gqw(re-4)= 0.323003586323289890E-01
  gqw(re-5)= 0.404015413316692537E-01
  gqw(re-6)= 0.481477428187116821E-01
  gqw(re-7)= 0.554708466316636700E-01
  gqw(re-8)= 0.623064825303177791E-01
  gqw(re-9)= 0.685945728186565656E-01
  gqw(re-10)= 0.742798548439536216E-01
  gqw(re-11)= 0.793123647948873051E-01
  gqw(re-12)= 0.836478760670388566E-01
  gqw(re-13)= 0.872482876188440531E-01
  gqw(re-14)= 0.900819586606386863E-01
  gqw(re-15)= 0.921239866433172794E-01
  gqw(re-16)= 0.933564260655961736E-01
  gqw(re-17)= 0.937684461602098090E-01
 else if (gqn.eq.34) then
  gqk(re-1)= 0.997571753790841731E+00
  gqk(re-2)= 0.987227816406309078E+00
  gqk(re-3)= 0.968708262533344300E+00
  gqk(re-4)= 0.942162397405106877E+00
  gqk(re-5)= 0.907809677718324326E+00
  gqk(re-6)= 0.865934638334564744E+00
  gqk(re-7)= 0.816884227900933624E+00
  gqk(re-8)= 0.761064876629872988E+00
  gqk(re-9)= 0.698939113216262786E+00
  gqk(re-10)= 0.631021727080528616E+00
  gqk(re-11)= 0.557875500669746782E+00
  gqk(re-12)= 0.480106545190327028E+00
  gqk(re-13)= 0.398359277758645547E+00
  gqk(re-14)= 0.313311081339463393E+00
  gqk(re-15)= 0.225666691616449588E+00
  gqk(re-16)= 0.136152357259182899E+00
  gqk(re-17)= 0.455098219531024986E-01
  
  gqw(re-1)= 0.622914055590870121E-02
  gqw(re-2)= 0.144501627485954624E-01
  gqw(re-3)= 0.225637219854949410E-01
  gqw(re-4)= 0.304913806384458329E-01
  gqw(re-5)= 0.381665937963874893E-01
  gqw(re-6)= 0.455256115233531114E-01
  gqw(re-7)= 0.525074145726781433E-01
  gqw(re-8)= 0.590541358275244246E-01
  gqw(re-9)= 0.651115215540764847E-01
  gqw(re-10)= 0.706293758142558098E-01
  gqw(re-11)= 0.755619746600321440E-01
  gqw(re-12)= 0.798684443397720961E-01
  gqw(re-13)= 0.835130996998452030E-01
  gqw(re-14)= 0.864657397470357802E-01
  gqw(re-15)= 0.887018978356934329E-01
  gqw(re-16)= 0.902030443706409724E-01
  gqw(re-17)= 0.909567403302597721E-01
 else if (gqn.eq.35) then
  gqk(re-1)= 0.997706569099600205E+00
  gqk(re-2)= 0.987935764443851339E+00
  gqk(re-3)= 0.970437616039230289E+00
  gqk(re-4)= 0.945345148207827335E+00
  gqk(re-5)= 0.912854261359317687E+00
  gqk(re-6)= 0.873219125025222032E+00
  gqk(re-7)= 0.826749899092225293E+00
  gqk(re-8)= 0.773810252286912581E+00
  gqk(re-9)= 0.714814501556628845E+00
  gqk(re-10)= 0.650224364665890509E+00
  gqk(re-11)= 0.580545344749764602E+00
  gqk(re-12)= 0.506322773241488777E+00
  gqk(re-13)= 0.428137541517814524E+00
  gqk(re-14)= 0.346601554430813641E+00
  gqk(re-15)= 0.262352941209296031E+00
  gqk(re-16)= 0.176051061165989481E+00
  gqk(re-17)= 0.883713432756593198E-01
  gqk(re-18)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.588343342044307920E-02
  gqw(re-2)= 0.136508283483615152E-01
  gqw(re-3)= 0.213229799114830892E-01
  gqw(re-4)= 0.288292601088945763E-01
  gqw(re-5)= 0.361101158634636460E-01
  gqw(re-6)= 0.431084223261701122E-01
  gqw(re-7)= 0.497693704013535201E-01
  gqw(re-8)= 0.560408162123698303E-01
  gqw(re-9)= 0.618736719660803458E-01
  gqw(re-10)= 0.672222852690871064E-01
  gqw(re-11)= 0.720447947725599974E-01
  gqw(re-12)= 0.763034571554418450E-01
  gqw(re-13)= 0.799649422423242828E-01
  gqw(re-14)= 0.830005937288565004E-01
  gqw(re-15)= 0.853866533920992210E-01
  gqw(re-16)= 0.871044469971835739E-01
  gqw(re-17)= 0.881405304302753240E-01
  gqw(re-18)= 0.884867949071044407E-01
 else if (gqn.eq.36) then
  gqk(re-1)= 0.997830462484085468E+00
  gqk(re-2)= 0.988586478902212074E+00
  gqk(re-3)= 0.972027691049697662E+00
  gqk(re-4)= 0.948272984399507135E+00
  gqk(re-5)= 0.917497774515659059E+00
  gqk(re-6)= 0.879929800890397296E+00
  gqk(re-7)= 0.835847166992475410E+00
  gqk(re-8)= 0.785576230132206565E+00
  gqk(re-9)= 0.729489171593556640E+00
  gqk(re-10)= 0.668001236585521130E+00
  gqk(re-11)= 0.601567658135980565E+00
  gqk(re-12)= 0.530680285926245054E+00
  gqk(re-13)= 0.455863944433420321E+00
  gqk(re-14)= 0.377672547119689062E+00
  gqk(re-15)= 0.296684995344028646E+00
  gqk(re-16)= 0.213500892316865865E+00
  gqk(re-17)= 0.128736103809384717E+00
  gqk(re-18)= 0.430181984737086076E-01
  
  gqw(re-1)= 0.556571966424551038E-02
  gqw(re-2)= 0.129159472840655494E-01
  gqw(re-3)= 0.201815152977354549E-01
  gqw(re-4)= 0.272986214985685045E-01
  gqw(re-5)= 0.342138107703069544E-01
  gqw(re-6)= 0.408757509236452043E-01
  gqw(re-7)= 0.472350834902662206E-01
  gqw(re-8)= 0.532447139777596778E-01
  gqw(re-9)= 0.588601442453249094E-01
  gqw(re-10)= 0.640397973550155541E-01
  gqw(re-11)= 0.687453238357361857E-01
  gqw(re-12)= 0.729418850056530177E-01
  gqw(re-13)= 0.765984106458703629E-01
  gqw(re-14)= 0.796878289120719341E-01
  gqw(re-15)= 0.821872667043395538E-01
  gqw(re-16)= 0.840782189796623197E-01
  gqw(re-17)= 0.853466857393385958E-01
  gqw(re-18)= 0.859832756703944601E-01
 else if (gqn.eq.37) then
  gqk(re-1)= 0.997944582477913511E+00
  gqk(re-2)= 0.989185963214318953E+00
  gqk(re-3)= 0.973493030056485797E+00
  gqk(re-4)= 0.950972343262094455E+00
  gqk(re-5)= 0.921781437412463434E+00
  gqk(re-6)= 0.886124962155486084E+00
  gqk(re-7)= 0.844252987340555761E+00
  gqk(re-8)= 0.796459200509902265E+00
  gqk(re-9)= 0.743078833981965170E+00
  gqk(re-10)= 0.684486309130959425E+00
  gqk(re-11)= 0.621092608408924218E+00
  gqk(re-12)= 0.553342391861581850E+00
  gqk(re-13)= 0.481710877803205872E+00
  gqk(re-14)= 0.406700509318326076E+00
  gqk(re-15)= 0.328837429883707122E+00
  gqk(re-16)= 0.248667792791365444E+00
  gqk(re-17)= 0.166753930239851939E+00
  gqk(re-18)= 0.836704089547699037E-01
  gqk(re-19)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.527305727949792854E-02
  gqw(re-2)= 0.122387801003073747E-01
  gqw(re-3)= 0.191290444890840385E-01
  gqw(re-4)= 0.258860369905588271E-01
  gqw(re-5)= 0.324616398475218518E-01
  gqw(re-6)= 0.388096025019342358E-01
  gqw(re-7)= 0.448853646624372410E-01
  gqw(re-8)= 0.506462976548246249E-01
  gqw(re-9)= 0.560519879982755154E-01
  gqw(re-10)= 0.610645165232256426E-01
  gqw(re-11)= 0.656487228727512945E-01
  gqw(re-12)= 0.697724515557008457E-01
  gqw(re-13)= 0.734067772484879866E-01
  gqw(re-14)= 0.765262075705293171E-01
  gqw(re-15)= 0.791088618375294095E-01
  gqw(re-16)= 0.811366245084651772E-01
  gqw(re-17)= 0.825952722364371589E-01
  gqw(re-18)= 0.834745736258623444E-01
  gqw(re-19)= 0.837683609931398759E-01
 else if (gqn.eq.38) then
  gqk(re-1)= 0.998049930535687801E+00
  gqk(re-2)= 0.989739454266385765E+00
  gqk(re-3)= 0.974846328590153299E+00
  gqk(re-4)= 0.953466330933529504E+00
  gqk(re-5)= 0.925741332048584331E+00
  gqk(re-6)= 0.891855739004632220E+00
  gqk(re-7)= 0.852035021932362358E+00
  gqk(re-8)= 0.806544167605316886E+00
  gqk(re-9)= 0.755685903753970711E+00
  gqk(re-10)= 0.699798680379184468E+00
  gqk(re-11)= 0.639254415829681788E+00
  gqk(re-12)= 0.574456021047806908E+00
  gqk(re-13)= 0.505834717927931443E+00
  gqk(re-14)= 0.433847169432376667E+00
  gqk(re-15)= 0.358972440479435051E+00
  gqk(re-16)= 0.281708809790165104E+00
  gqk(re-17)= 0.202570453892116670E+00
  gqk(re-18)= 0.122084025337867275E+00
  gqk(re-19)= 0.407851479045781215E-01
  
  gqw(re-1)= 0.500288074963925727E-02
  gqw(re-2)= 0.116134447164688084E-01
  gqw(re-3)= 0.181565777096130906E-01
  gqw(re-4)= 0.245797397382327490E-01
  gqw(re-5)= 0.308395005451748624E-01
  gqw(re-6)= 0.368940815940247205E-01
  gqw(re-7)= 0.427031585046740089E-01
  gqw(re-8)= 0.482280618607590084E-01
  gqw(re-9)= 0.534320199103326746E-01
  gqw(re-10)= 0.582803991469972096E-01
  gqw(re-11)= 0.627409333921333939E-01
  gqw(re-12)= 0.667839379791402704E-01
  gqw(re-13)= 0.703825070668987751E-01
  gqw(re-14)= 0.735126925847433416E-01
  gqw(re-15)= 0.761536635484462432E-01
  gqw(re-16)= 0.782878446582109117E-01
  gqw(re-17)= 0.799010332435281800E-01
  gqw(re-18)= 0.809824937705970055E-01
  gqw(re-19)= 0.815250292803858245E-01
 else if (gqn.eq.39) then
  gqk(re-1)= 0.998147383066433025E+00
  gqk(re-2)= 0.990251536854686365E+00
  gqk(re-3)= 0.976098709333470982E+00
  gqk(re-4)= 0.955775212324652346E+00
  gqk(re-5)= 0.929409148486738279E+00
  gqk(re-6)= 0.897167119292992976E+00
  gqk(re-7)= 0.859252937999906319E+00
  gqk(re-8)= 0.815906297430143090E+00
  gqk(re-9)= 0.767401242931063599E+00
  gqk(re-10)= 0.714044435894534701E+00
  gqk(re-11)= 0.656173213432010960E+00
  gqk(re-12)= 0.594153454957277782E+00
  gqk(re-13)= 0.528377268660437438E+00
  gqk(re-14)= 0.459260512309135727E+00
  gqk(re-15)= 0.387240163971561746E+00
  gqk(re-16)= 0.312771559248186282E+00
  gqk(re-17)= 0.236325512461836029E+00
  gqk(re-18)= 0.158385339997837910E+00
  gqk(re-19)= 0.794438046087555250E-01
  gqk(re-20)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.475294469163459622E-02
  gqw(re-2)= 0.110347889391651383E-01
  gqw(re-3)= 0.172562290937251818E-01
  gqw(re-4)= 0.233693848321779515E-01
  gqw(re-5)= 0.293349559839033540E-01
  gqw(re-6)= 0.351151114981313184E-01
  gqw(re-7)= 0.406732768479340437E-01
  gqw(re-8)= 0.459743011089163503E-01
  gqw(re-9)= 0.509846652921290000E-01
  gqw(re-10)= 0.556726903409166946E-01
  gqw(re-11)= 0.600087360885957627E-01
  gqw(re-12)= 0.639653881386829104E-01
  gqw(re-13)= 0.675176309662311042E-01
  gqw(re-14)= 0.706430059706081304E-01
  gqw(re-15)= 0.733217534142687338E-01
  gqw(re-16)= 0.755369373228362145E-01
  gqw(re-17)= 0.772745525446822401E-01
  gqw(re-18)= 0.785236132873714798E-01
  gqw(re-19)= 0.792762225683681238E-01
  gqw(re-20)= 0.795276221394427968E-01
 else if (gqn.eq.40) then
  gqk(re-1)= 0.998237709710559473E+00
  gqk(re-2)= 0.990726238699457196E+00
  gqk(re-3)= 0.977259949983774412E+00
  gqk(re-4)= 0.957916819213791459E+00
  gqk(re-5)= 0.932812808278676631E+00
  gqk(re-6)= 0.902098806968874345E+00
  gqk(re-7)= 0.865959503212259452E+00
  gqk(re-8)= 0.824612230833311699E+00
  gqk(re-9)= 0.778305651426519529E+00
  gqk(re-10)= 0.727318255189927543E+00
  gqk(re-11)= 0.671956684614179678E+00
  gqk(re-12)= 0.612553889667980078E+00
  gqk(re-13)= 0.549467125095128073E+00
  gqk(re-14)= 0.483075801686178863E+00
  gqk(re-15)= 0.413779204371605480E+00
  gqk(re-16)= 0.341994090825758046E+00
  gqk(re-17)= 0.268152185007253574E+00
  gqk(re-18)= 0.192697580701371080E+00
  gqk(re-19)= 0.116084070675255210E+00
  gqk(re-20)= 0.387724175060507603E-01
  
  gqw(re-1)= 0.452127709853319439E-02
  gqw(re-2)= 0.104982845311530609E-01
  gqw(re-3)= 0.164210583819077993E-01
  gqw(re-4)= 0.222458491941671041E-01
  gqw(re-5)= 0.279370069800231949E-01
  gqw(re-6)= 0.334601952825480456E-01
  gqw(re-7)= 0.387821679744719330E-01
  gqw(re-8)= 0.438709081856729147E-01
  gqw(re-9)= 0.486958076350718985E-01
  gqw(re-10)= 0.532278469839372673E-01
  gqw(re-11)= 0.574397690993916007E-01
  gqw(re-12)= 0.613062424929287225E-01
  gqw(re-13)= 0.648040134566013615E-01
  gqw(re-14)= 0.679120458152332324E-01
  gqw(re-15)= 0.706116473912871412E-01
  gqw(re-16)= 0.728865823958042836E-01
  gqw(re-17)= 0.747231690579684277E-01
  gqw(re-18)= 0.761103619006258392E-01
  gqw(re-19)= 0.770398181642485830E-01
  gqw(re-20)= 0.775059479784248190E-01
 else if (gqn.eq.41) then
  gqk(re-1)= 0.998321588574771268E+00
  gqk(re-2)= 0.991167109699016224E+00
  gqk(re-3)= 0.978338673561083505E+00
  gqk(re-4)= 0.959906891730345935E+00
  gqk(re-5)= 0.935976987497853963E+00
  gqk(re-6)= 0.906685944758101048E+00
  gqk(re-7)= 0.872201511692440978E+00
  gqk(re-8)= 0.832721200401360995E+00
  gqk(re-9)= 0.788471145047409450E+00
  gqk(re-10)= 0.739704803069925809E+00
  gqk(re-11)= 0.686701502034950728E+00
  gqk(re-12)= 0.629764839072196270E+00
  gqk(re-13)= 0.569220941610215303E+00
  gqk(re-14)= 0.505416599199406402E+00
  gqk(re-15)= 0.438717277051406951E+00
  gqk(re-16)= 0.369505022640481517E+00
  gqk(re-17)= 0.298176277341824836E+00
  gqk(re-18)= 0.225139605633422835E+00
  gqk(re-19)= 0.150813354863992166E+00
  gqk(re-20)= 0.756232589891630558E-01
  gqk(re-21)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.430614035816524309E-02
  gqw(re-2)= 0.999993877390563564E-02
  gqw(re-3)= 0.156449384078183526E-01
  gqw(re-4)= 0.212010633687792598E-01
  gqw(re-5)= 0.266358992071100073E-01
  gqw(re-6)= 0.319182117316998068E-01
  gqw(re-7)= 0.370177167035077997E-01
  gqw(re-8)= 0.419051951959098154E-01
  gqw(re-9)= 0.465526483690140644E-01
  gqw(re-10)= 0.509334542946178193E-01
  gqw(re-11)= 0.550225192425789003E-01
  gqw(re-12)= 0.587964209498725646E-01
  gqw(re-13)= 0.622335425809660073E-01
  gqw(re-14)= 0.653141964535274983E-01
  gqw(re-15)= 0.680207367608765268E-01
  gqw(re-16)= 0.703376606208175881E-01
  gqw(re-17)= 0.722516968610225074E-01
  gqw(re-18)= 0.737518820272237696E-01
  gqw(re-19)= 0.748296231762215353E-01
  gqw(re-20)= 0.754787470927151538E-01
  gqw(re-21)= 0.756955356472987229E-01
 else if (gqn.eq.42) then
  gqk(re-1)= 0.998399618990062465E+00
  gqk(re-2)= 0.991577288340861229E+00
  gqk(re-3)= 0.979342508063748229E+00
  gqk(re-4)= 0.961759365338204608E+00
  gqk(re-5)= 0.938923557354987781E+00
  gqk(re-6)= 0.910959724904127466E+00
  gqk(re-7)= 0.878020569812172691E+00
  gqk(re-8)= 0.840285983261817027E+00
  gqk(re-9)= 0.797962053255487302E+00
  gqk(re-10)= 0.751279935689480483E+00
  gqk(re-11)= 0.700494590556171137E+00
  gqk(re-12)= 0.645883388869247788E+00
  gqk(re-13)= 0.587744597485109543E+00
  gqk(re-14)= 0.526395749931192092E+00
  gqk(re-15)= 0.462171912070421909E+00
  gqk(re-16)= 0.395423852042974810E+00
  gqk(re-17)= 0.326516124465411228E+00
  gqk(re-18)= 0.255825079342879236E+00
  gqk(re-19)= 0.183736806564854638E+00
  gqk(re-20)= 0.110645027208519917E+00
  gqk(re-21)= 0.369489431653518066E-01
  
  gqw(re-1)= 0.410599860464941520E-02
  gqw(re-2)= 0.953622030174815921E-02
  gqw(re-3)= 0.149224436973573928E-01
  gqw(re-4)= 0.202278695690523078E-01
  gqw(re-5)= 0.254229595261142131E-01
  gqw(re-6)= 0.304792406996026444E-01
  gqw(re-7)= 0.353690710975924638E-01
  gqw(re-8)= 0.400657351806920844E-01
  gqw(re-9)= 0.445435777719661377E-01
  gqw(re-10)= 0.487781407928031679E-01
  gqw(re-11)= 0.527462956991737036E-01
  gqw(re-12)= 0.564263693580185702E-01
  gqw(re-13)= 0.597982622275863851E-01
  gqw(re-14)= 0.628435580450027592E-01
  gqw(re-15)= 0.655456243649093773E-01
  gqw(re-16)= 0.678897033765219204E-01
  gqw(re-17)= 0.698629924925937562E-01
  gqw(re-18)= 0.714547142651710404E-01
  gqw(re-19)= 0.726561752438043273E-01
  gqw(re-20)= 0.734608134534676105E-01
  gqw(re-21)= 0.738642342321729622E-01
 else if (gqn.eq.43) then
  gqk(re-1)= 0.998472332242507421E+00
  gqk(re-2)= 0.991959557593243946E+00
  gqk(re-3)= 0.980278220980254966E+00
  gqk(re-4)= 0.963486613014079785E+00
  gqk(re-5)= 0.941671956847637937E+00
  gqk(re-6)= 0.914947907206138944E+00
  gqk(re-7)= 0.883453765218616849E+00
  gqk(re-8)= 0.847353716209314989E+00
  gqk(re-9)= 0.806835964136938588E+00
  gqk(re-10)= 0.762111747194955114E+00
  gqk(re-11)= 0.713414235268957087E+00
  gqk(re-12)= 0.660997313751498172E+00
  gqk(re-13)= 0.605134259639601213E+00
  gqk(re-14)= 0.546116316660084977E+00
  gqk(re-15)= 0.484251176785734594E+00
  gqk(re-16)= 0.419861376029269762E+00
  gqk(re-17)= 0.353282612864303902E+00
  gqk(re-18)= 0.284861998032913732E+00
  gqk(re-19)= 0.214956244860517981E+00
  gqk(re-20)= 0.143929809510713297E+00
  gqk(re-21)= 0.721529908745862925E-01
  gqk(re-22)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.391949025384484995E-02
  gqw(re-2)= 0.910399663740062842E-02
  gqw(re-3)= 0.142487564315766403E-01
  gqw(re-4)= 0.193199014236838419E-01
  gqw(re-5)= 0.242904566138388116E-01
  gqw(re-6)= 0.291344132614984562E-01
  gqw(re-7)= 0.338264920868600882E-01
  gqw(re-8)= 0.383422221941325941E-01
  gqw(re-9)= 0.426580571979818107E-01
  gqw(re-10)= 0.467514947543465967E-01
  gqw(re-11)= 0.506011927843905765E-01
  gqw(re-12)= 0.541870803188816072E-01
  gqw(re-13)= 0.574904619569102035E-01
  gqw(re-14)= 0.604941152499917020E-01
  gqw(re-15)= 0.631823804493960445E-01
  gqw(re-16)= 0.655412421263231043E-01
  gqw(re-17)= 0.675584022293649195E-01
  gqw(re-18)= 0.692233441936565969E-01
  gqw(re-19)= 0.705273877650853809E-01
  gqw(re-20)= 0.714637342525141495E-01
  gqw(re-21)= 0.720275019714212289E-01
  gqw(re-22)= 0.722157516937991101E-01
 else if (gqn.eq.44) then
  gqk(re-1)= 0.998540200636774067E+00
  gqk(re-2)= 0.992316392138515746E+00
  gqk(re-3)= 0.981151833077913649E+00
  gqk(re-4)= 0.965099650422493127E+00
  gqk(re-5)= 0.944239509118193876E+00
  gqk(re-6)= 0.918675259984175541E+00
  gqk(re-7)= 0.888534238286043054E+00
  gqk(re-8)= 0.853966595004710394E+00
  gqk(re-9)= 0.815144539645135335E+00
  gqk(re-10)= 0.772261479248756100E+00
  gqk(re-11)= 0.725531053660716907E+00
  gqk(re-12)= 0.675186070666122129E+00
  gqk(re-13)= 0.621477345903575840E+00
  gqk(re-14)= 0.564672453185471213E+00
  gqk(re-15)= 0.505054391388201851E+00
  gqk(re-16)= 0.442920174525411769E+00
  gqk(re-17)= 0.378579352014706827E+00
  gqk(re-18)= 0.312352466502786252E+00
  gqk(re-19)= 0.244569456928201534E+00
  gqk(re-20)= 0.175568014775516840E+00
  gqk(re-21)= 0.105691901708653405E+00
  gqk(re-22)= 0.352892369641353634E-01
  
  gqw(re-1)= 0.374540480311303476E-02
  gqw(re-2)= 0.870048136752464216E-02
  gqw(re-3)= 0.136195867555801708E-01
  gqw(re-4)= 0.184714817368149369E-01
  gqw(re-5)= 0.232314819020185692E-01
  gqw(re-6)= 0.278757828212809304E-01
  gqw(re-7)= 0.323812228120700374E-01
  gqw(re-8)= 0.367253478138087480E-01
  gqw(re-9)= 0.408865123103463110E-01
  gqw(re-10)= 0.448439840819699412E-01
  gqw(re-11)= 0.485780464483519595E-01
  gqw(re-12)= 0.520700960917051467E-01
  gqw(re-13)= 0.553027355637272788E-01
  gqw(re-14)= 0.582598598775960069E-01
  gqw(re-15)= 0.609267367015621786E-01
  gqw(re-16)= 0.632900797332036635E-01
  gqw(re-17)= 0.653381148791813832E-01
  gqw(re-18)= 0.670606389062939345E-01
  gqw(re-19)= 0.684490702693664743E-01
  gqw(re-20)= 0.694964918615726401E-01
  gqw(re-21)= 0.701976854735579248E-01
  gqw(re-22)= 0.705491577893542637E-01
 else if (gqn.eq.45) then
  gqk(re-1)= 0.998603645181936894E+00
  gqk(re-2)= 0.992649998447204140E+00
  gqk(re-3)= 0.981968715034540751E+00
  gqk(re-4)= 0.966608310396894432E+00
  gqk(re-5)= 0.946641690995628893E+00
  gqk(re-6)= 0.922163936719000321E+00
  gqk(re-7)= 0.893291671753242200E+00
  gqk(re-8)= 0.860162475960664308E+00
  gqk(re-9)= 0.822934220502086311E+00
  gqk(re-10)= 0.781784312593906350E+00
  gqk(re-11)= 0.736908848945490136E+00
  gqk(re-12)= 0.688521680771200573E+00
  gqk(re-13)= 0.636853394453223309E+00
  gqk(re-14)= 0.582150212569353176E+00
  gqk(re-15)= 0.524672820462916190E+00
  gqk(re-16)= 0.464695123919634634E+00
  gqk(re-17)= 0.402502943858541351E+00
  gqk(re-18)= 0.338392654250601888E+00
  gqk(re-19)= 0.272669769752377389E+00
  gqk(re-20)= 0.205647489783263693E+00
  gqk(re-21)= 0.137645205983253027E+00
  gqk(re-22)= 0.689869801631442653E-01
  gqk(re-23)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.358266315528324875E-02
  gqw(re-2)= 0.832318929621839523E-02
  gqw(re-3)= 0.130311049915827308E-01
  gqw(re-4)= 0.176775352579379222E-01
  gqw(re-5)= 0.222398475505789402E-01
  gqw(re-6)= 0.266962139675773792E-01
  gqw(re-7)= 0.310253749345150986E-01
  gqw(re-8)= 0.352066922016092271E-01
  gqw(re-9)= 0.392202367293025475E-01
  gqw(re-10)= 0.430468807091649933E-01
  gqw(re-11)= 0.466683877183736717E-01
  gqw(re-12)= 0.500674992379516182E-01
  gqw(re-13)= 0.532280167312689650E-01
  gqw(re-14)= 0.561348787597864066E-01
  gqw(re-15)= 0.587742327188420025E-01
  gqw(re-16)= 0.611335008310665354E-01
  gqw(re-17)= 0.632014400738199478E-01
  gqw(re-18)= 0.649681957507235591E-01
  gqw(re-19)= 0.664253484498423669E-01
  gqw(re-20)= 0.675659541636080135E-01
  gqw(re-21)= 0.683845773786698197E-01
  gqw(re-22)= 0.688773169776612931E-01
  gqw(re-23)= 0.690418248292319486E-01
 else if (gqn.eq.46) then
  gqk(re-1)= 0.998663042133817624E+00
  gqk(re-2)= 0.992962348906174075E+00
  gqk(re-3)= 0.982733669804166099E+00
  gqk(re-4)= 0.968021391853991720E+00
  gqk(re-5)= 0.948892363446089404E+00
  gqk(re-6)= 0.925433798806754138E+00
  gqk(re-7)= 0.897752711533941761E+00
  gqk(re-8)= 0.865975394866857928E+00
  gqk(re-9)= 0.830246837066065835E+00
  gqk(re-10)= 0.790730057075274106E+00
  gqk(re-11)= 0.747605359615665988E+00
  gqk(re-12)= 0.701069512020405461E+00
  gqk(re-13)= 0.651334846201997775E+00
  gqk(re-14)= 0.598628289712715200E+00
  gqk(re-15)= 0.543190330261802634E+00
  gqk(re-16)= 0.485273918388164527E+00
  gqk(re-17)= 0.425143313282828395E+00
  gqk(re-18)= 0.363072877020995499E+00
  gqk(re-19)= 0.299345822701870101E+00
  gqk(re-20)= 0.234252922206269643E+00
  gqk(re-21)= 0.168091179467103641E+00
  gqk(re-22)= 0.101162475305584254E+00
  gqk(re-23)= 0.337721900160522920E-01
  
  gqw(re-1)= 0.343030086810735147E-02
  gqw(re-2)= 0.796989822972424787E-02
  gqw(re-3)= 0.124798837709891534E-01
  gqw(re-4)= 0.169335140078361590E-01
  gqw(re-5)= 0.213099987541363785E-01
  gqw(re-6)= 0.255892863971297069E-01
  gqw(re-7)= 0.297518295522030719E-01
  gqw(re-8)= 0.337786279991064697E-01
  gqw(re-9)= 0.376513053573864082E-01
  gqw(re-10)= 0.413521901096781039E-01
  gqw(re-11)= 0.448643952773183641E-01
  gqw(re-12)= 0.481718951017120964E-01
  gqw(re-13)= 0.512595980071431506E-01
  gqw(re-14)= 0.541134153858570457E-01
  gqw(re-15)= 0.567203258439908653E-01
  gqw(re-16)= 0.590684345955467330E-01
  gqw(re-17)= 0.611470277246501104E-01
  gqw(re-18)= 0.629466210643946644E-01
  gqw(re-19)= 0.644590034671393836E-01
  gqw(re-20)= 0.656772742677805971E-01
  gqw(re-21)= 0.665958747684550906E-01
  gqw(re-22)= 0.672106136006780930E-01
  gqw(re-23)= 0.675186858490365999E-01
 else if (gqn.eq.47) then
  gqk(re-1)= 0.998718728584211402E+00
  gqk(re-2)= 0.993255210987767923E+00
  gqk(re-3)= 0.983451003071623253E+00
  gqk(re-4)= 0.969346787326564208E+00
  gqk(re-5)= 0.951003969257707915E+00
  gqk(re-6)= 0.928502693012360103E+00
  gqk(re-7)= 0.901941329438525119E+00
  gqk(re-8)= 0.871436015796896113E+00
  gqk(re-9)= 0.837120139899901861E+00
  gqk(re-10)= 0.799143754167741749E+00
  gqk(re-11)= 0.757672918445438492E+00
  gqk(re-12)= 0.712888973409064208E+00
  gqk(re-13)= 0.664987747390332840E+00
  gqk(re-14)= 0.614178699956373975E+00
  gqk(re-15)= 0.560684005934664387E+00
  gqk(re-16)= 0.504737583863578254E+00
  gqk(re-17)= 0.446584073104855872E+00
  gqk(re-18)= 0.386477764084667330E+00
  gqk(re-19)= 0.324681486337736025E+00
  gqk(re-20)= 0.261465459214974660E+00
  gqk(re-21)= 0.197106110279111818E+00
  gqk(re-22)= 0.131884866554515034E+00
  gqk(re-23)= 0.660869239163557054E-01
  gqk(re-24)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.328745384252837440E-02
  gqw(re-2)= 0.763861629584913163E-02
  gqw(re-3)= 0.119628484643115665E-01
  gqw(re-4)= 0.162353331464330647E-01
  gqw(re-5)= 0.204369381476687609E-01
  gqw(re-6)= 0.245492116596586520E-01
  gqw(re-7)= 0.285541507006435678E-01
  gqw(re-8)= 0.324342355151846298E-01
  gqw(re-9)= 0.361724965841750887E-01
  gqw(re-10)= 0.397525861225309779E-01
  gqw(re-11)= 0.431588486484795197E-01
  gqw(re-12)= 0.463763890865058689E-01
  gqw(re-13)= 0.493911377473608554E-01
  gqw(re-14)= 0.521899117800573784E-01
  gqw(re-15)= 0.547604727815294567E-01
  gqw(re-16)= 0.570915802932318028E-01
  gqw(re-17)= 0.591730409423390086E-01
  gqw(re-18)= 0.609957530087398420E-01
  gqw(re-19)= 0.625517462209217862E-01
  gqw(re-20)= 0.638342166057170451E-01
  gqw(re-21)= 0.648375562389451043E-01
  gqw(re-22)= 0.655573777665499763E-01
  gqw(re-23)= 0.659905335888101158E-01
  gqw(re-24)= 0.661351296236556280E-01
 else if (gqn.eq.48) then
  gqk(re-1)= 0.998771007252426068E+00
  gqk(re-2)= 0.993530172266350542E+00
  gqk(re-3)= 0.984124583722826740E+00
  gqk(re-4)= 0.970591592546247273E+00
  gqk(re-5)= 0.952987703160430688E+00
  gqk(re-6)= 0.931386690706554221E+00
  gqk(re-7)= 0.905879136715569522E+00
  gqk(re-8)= 0.876572020274247743E+00
  gqk(re-9)= 0.843588261624393487E+00
  gqk(re-10)= 0.807066204029442624E+00
  gqk(re-11)= 0.767159032515740469E+00
  gqk(re-12)= 0.724034130923814967E+00
  gqk(re-13)= 0.677872379632663780E+00
  gqk(re-14)= 0.628867396776513488E+00
  gqk(re-15)= 0.577224726083972461E+00
  gqk(re-16)= 0.523160974722232996E+00
  gqk(re-17)= 0.466902904750958747E+00
  gqk(re-18)= 0.408686481990716388E+00
  gqk(re-19)= 0.348755886292160755E+00
  gqk(re-20)= 0.287362487355455387E+00
  gqk(re-21)= 0.224763790394689050E+00
  gqk(re-22)= 0.161222356068891820E+00
  gqk(re-23)= 0.970046992094628219E-01
  gqk(re-24)= 0.323801709628694853E-01
  
  gqw(re-1)= 0.315334605230601760E-02
  gqw(re-2)= 0.732755390127637612E-02
  gqw(re-3)= 0.114772345792345954E-01
  gqw(re-4)= 0.155793157229435868E-01
  gqw(re-5)= 0.196161604573554978E-01
  gqw(re-6)= 0.235707608393248176E-01
  gqw(re-7)= 0.274265097083566459E-01
  gqw(re-8)= 0.311672278327983880E-01
  gqw(re-9)= 0.347772225647700606E-01
  gqw(re-10)= 0.382413510658302230E-01
  gqw(re-11)= 0.415450829434650185E-01
  gqw(re-12)= 0.446745608566943841E-01
  gqw(re-13)= 0.476166584924902353E-01
  gqw(re-14)= 0.503590355538544795E-01
  gqw(re-15)= 0.528901894851936394E-01
  gqw(re-16)= 0.551995036999841787E-01
  gqw(re-17)= 0.572772921004032209E-01
  gqw(re-18)= 0.591148396983956492E-01
  gqw(re-19)= 0.607044391658941376E-01
  gqw(re-20)= 0.620394231598922347E-01
  gqw(re-21)= 0.631141922862539090E-01
  gqw(re-22)= 0.639242385846478800E-01
  gqw(re-23)= 0.644661644359501573E-01
  gqw(re-24)= 0.647376968126839036E-01
 else if (gqn.eq.49) then
  gqk(re-1)= 0.998820150606634982E+00
  gqk(re-2)= 0.993788661944167595E+00
  gqk(re-3)= 0.984757895914212700E+00
  gqk(re-4)= 0.971762200901555184E+00
  gqk(re-5)= 0.954853658674137185E+00
  gqk(re-6)= 0.934100294755810467E+00
  gqk(re-7)= 0.909585655828073603E+00
  gqk(re-8)= 0.881408445573008570E+00
  gqk(re-9)= 0.849682119844165862E+00
  gqk(re-10)= 0.814534427359855484E+00
  gqk(re-11)= 0.776106894345446552E+00
  gqk(re-12)= 0.734554254237402837E+00
  gqk(re-13)= 0.690043824425132013E+00
  gqk(re-14)= 0.642754832419237254E+00
  gqk(re-15)= 0.592877694108900899E+00
  gqk(re-16)= 0.540613246991725971E+00
  gqk(re-17)= 0.486171941452492207E+00
  gqk(re-18)= 0.429772993341576537E+00
  gqk(re-19)= 0.371643501262284903E+00
  gqk(re-20)= 0.312017532119748864E+00
  gqk(re-21)= 0.251135178612577115E+00
  gqk(re-22)= 0.189241592461813379E+00
  gqk(re-23)= 0.126585997269671902E+00
  gqk(re-24)= 0.634206849826864510E-01
  gqk(re-25)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.302727898892328050E-02
  gqw(re-2)= 0.703509959008593363E-02
  gqw(re-3)= 0.110205510315939687E-01
  gqw(re-4)= 0.149621449356246699E-01
  gqw(re-5)= 0.188435958530892879E-01
  gqw(re-6)= 0.226492015874465193E-01
  gqw(re-7)= 0.263636189270656830E-01
  gqw(re-8)= 0.299718846205841265E-01
  gqw(re-9)= 0.334594667916219835E-01
  gqw(re-10)= 0.368123209630005985E-01
  gqw(re-11)= 0.400169457663736225E-01
  gqw(re-12)= 0.430604369812592619E-01
  gqw(re-13)= 0.459305393555955663E-01
  gqw(re-14)= 0.486156958878280337E-01
  gqw(re-15)= 0.511050943301447833E-01
  gqw(re-16)= 0.533887107082595611E-01
  gqw(re-17)= 0.554573496748035424E-01
  gqw(re-18)= 0.573026815301876727E-01
  gqw(re-19)= 0.589172757600273794E-01
  gqw(re-20)= 0.602946309531519620E-01
  gqw(re-21)= 0.614292009791929314E-01
  gqw(re-22)= 0.623164173200572713E-01
  gqw(re-23)= 0.629527074651955454E-01
  gqw(re-24)= 0.633355092964918154E-01
  gqw(re-25)= 0.634632814047905586E-01
 else if (gqn.eq.50) then
  gqk(re-1)= 0.998866404420071019E+00
  gqk(re-2)= 0.994031969432090601E+00
  gqk(re-3)= 0.985354084048005840E+00
  gqk(re-4)= 0.972864385106692375E+00
  gqk(re-5)= 0.956610955242808036E+00
  gqk(re-6)= 0.936656618944878172E+00
  gqk(re-7)= 0.913078556655791962E+00
  gqk(re-8)= 0.885967979523613169E+00
  gqk(re-9)= 0.855429769429945974E+00
  gqk(re-10)= 0.821582070859336211E+00
  gqk(re-11)= 0.784555832900399430E+00
  gqk(re-12)= 0.744494302226069049E+00
  gqk(re-13)= 0.701552468706822308E+00
  gqk(re-14)= 0.655896465685439245E+00
  gqk(re-15)= 0.607702927184950337E+00
  gqk(re-16)= 0.557158304514649982E+00
  gqk(re-17)= 0.504458144907463990E+00
  gqk(re-18)= 0.449806334974038713E+00
  gqk(re-19)= 0.393414311897565427E+00
  gqk(re-20)= 0.335500245419437293E+00
  gqk(re-21)= 0.276288193779531954E+00
  gqk(re-22)= 0.216007236876041647E+00
  gqk(re-23)= 0.154890589998145700E+00
  gqk(re-24)= 0.931747015600861150E-01
  gqk(re-25)= 0.310983383271887723E-01
  
  gqw(re-1)= 0.290862255315541647E-02
  gqw(re-2)= 0.675979919574548616E-02
  gqw(re-3)= 0.105905483836505540E-01
  gqw(re-4)= 0.143808227614859625E-01
  gqw(re-5)= 0.181155607134892739E-01
  gqw(re-6)= 0.217802431701246170E-01
  gqw(re-7)= 0.253606735700120794E-01
  gqw(re-8)= 0.288429935805349612E-01
  gqw(re-9)= 0.322137282235784791E-01
  gqw(re-10)= 0.354598356151462624E-01
  gqw(re-11)= 0.385687566125874071E-01
  gqw(re-12)= 0.415284630901479740E-01
  gqw(re-13)= 0.443275043388032738E-01
  gqw(re-14)= 0.469550513039486556E-01
  gqw(re-15)= 0.494009384494665249E-01
  gqw(re-16)= 0.516557030695805192E-01
  gqw(re-17)= 0.537106218889967235E-01
  gqw(re-18)= 0.555577448062121726E-01
  gqw(re-19)= 0.571899256477280057E-01
  gqw(re-20)= 0.586008498132224651E-01
  gqw(re-21)= 0.597850587042653980E-01
  gqw(re-22)= 0.607379708417704889E-01
  gqw(re-23)= 0.614558995903167204E-01
  gqw(re-24)= 0.619360674206836936E-01
  gqw(re-25)= 0.621766166553476066E-01
 else if (gqn.eq.51) then
  gqk(re-1)= 0.998909990848903084E+00
  gqk(re-2)= 0.994261260436752226E+00
  gqk(re-3)= 0.985915991735903052E+00
  gqk(re-4)= 0.973903368019323867E+00
  gqk(re-5)= 0.958267848613908235E+00
  gqk(re-6)= 0.939067544002962218E+00
  gqk(re-7)= 0.916373862309780240E+00
  gqk(re-8)= 0.890271218029527467E+00
  gqk(re-9)= 0.860856711182291900E+00
  gqk(re-10)= 0.828239763823064745E+00
  gqk(re-11)= 0.792541712099380957E+00
  gqk(re-12)= 0.753895354485375369E+00
  gqk(re-13)= 0.712444457577036561E+00
  gqk(re-14)= 0.668343221175370372E+00
  gqk(re-15)= 0.621755704600723158E+00
  gqk(re-16)= 0.572855216351303764E+00
  gqk(re-17)= 0.521823669366185738E+00
  gqk(re-18)= 0.468850904286041092E+00
  gqk(re-19)= 0.414133983226303626E+00
  gqk(re-20)= 0.357876456688409561E+00
  gqk(re-21)= 0.300287606335332025E+00
  gqk(re-22)= 0.241581666447798687E+00
  gqk(re-23)= 0.181977026957077626E+00
  gqk(re-24)= 0.121695421018888902E+00
  gqk(re-25)= 0.609611001505786787E-01
  gqk(re-26)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.279680717109035684E-02
  gqw(re-2)= 0.650033778325221755E-02
  gqw(re-3)= 0.101851912978219716E-01
  gqw(re-4)= 0.138326340064773828E-01
  gqw(re-5)= 0.174287147234007921E-01
  gqw(re-6)= 0.209599884017029198E-01
  gqw(re-7)= 0.244133005737816253E-01
  gqw(re-8)= 0.277757985941623417E-01
  gqw(re-9)= 0.310349712901602896E-01
  gqw(re-10)= 0.341786932041882180E-01
  gqw(re-11)= 0.371952689232605271E-01
  gqw(re-12)= 0.400734762854961721E-01
  gqw(re-13)= 0.428026079978803126E-01
  gqw(re-14)= 0.453725114076503136E-01
  gqw(re-15)= 0.477736262406229031E-01
  gqw(re-16)= 0.499970201500572337E-01
  gqw(re-17)= 0.520344219366972796E-01
  gqw(re-18)= 0.538782523130455632E-01
  gqw(re-19)= 0.555216520957385121E-01
  gqw(re-20)= 0.569585077202589968E-01
  gqw(re-21)= 0.581834739825921321E-01
  gqw(re-22)= 0.591919939229614833E-01
  gqw(re-23)= 0.599803157775033111E-01
  gqw(re-24)= 0.605455069347373351E-01
  gqw(re-25)= 0.608854648448562963E-01
  gqw(re-26)= 0.609989248412057747E-01
 else if (gqn.eq.52) then
  gqk(re-1)= 0.998951111103949851E+00
  gqk(re-2)= 0.994477590929215505E+00
  gqk(re-3)= 0.986446195651549562E+00
  gqk(re-4)= 0.974883884221744434E+00
  gqk(re-5)= 0.959831826933086552E+00
  gqk(re-6)= 0.941343853641359085E+00
  gqk(re-7)= 0.919486128916424228E+00
  gqk(re-8)= 0.894336890534495277E+00
  gqk(re-9)= 0.865986162846067886E+00
  gqk(re-10)= 0.834535432326734772E+00
  gqk(re-11)= 0.800097283430468265E+00
  gqk(re-12)= 0.762794995193744829E+00
  gqk(re-13)= 0.722762099749983067E+00
  gqk(re-14)= 0.680141904227167471E+00
  gqk(re-15)= 0.635086977695245802E+00
  gqk(re-16)= 0.587758604979578658E+00
  gqk(re-17)= 0.538326209285827084E+00
  gqk(re-18)= 0.486966745698096004E+00
  gqk(re-19)= 0.433864067718761737E+00
  gqk(re-20)= 0.379208269116093621E+00
  gqk(re-21)= 0.323195003434807837E+00
  gqk(re-22)= 0.266024783605001980E+00
  gqk(re-23)= 0.207902264156366062E+00
  gqk(re-24)= 0.149035508606949557E+00
  gqk(re-25)= 0.896352446489006444E-01
  gqk(re-26)= 0.299141097973388594E-01
  
  gqw(re-1)= 0.269131695004720759E-02
  gqw(re-2)= 0.625552396297328946E-02
  gqw(re-3)= 0.980263457946313477E-02
  gqw(re-4)= 0.133151149823408019E-01
  gqw(re-5)= 0.167800233963005599E-01
  gqw(re-6)= 0.201848915079813201E-01
  gqw(re-7)= 0.235175135539844350E-01
  gqw(re-8)= 0.267659537465031981E-01
  gqw(re-9)= 0.299185811471437829E-01
  gqw(re-10)= 0.329641090897192651E-01
  gqw(re-11)= 0.358916348350969902E-01
  gqw(re-12)= 0.386906783104237831E-01
  gqw(re-13)= 0.413512195005606084E-01
  gqw(re-14)= 0.438637342590007875E-01
  gqw(re-15)= 0.462192283727847694E-01
  gqw(re-16)= 0.484092697440753408E-01
  gqw(re-17)= 0.504260185663419278E-01
  gqw(re-18)= 0.522622553839065113E-01
  gqw(re-19)= 0.539114069327574427E-01
  gqw(re-20)= 0.553675696693022856E-01
  gqw(re-21)= 0.566255309023687425E-01
  gqw(re-22)= 0.576807874525268327E-01
  gqw(re-23)= 0.585295617718138289E-01
  gqw(re-24)= 0.591688154660430787E-01
  gqw(re-25)= 0.595962601712488119E-01
  gqw(re-26)= 0.598103657452911247E-01
 else if (gqn.eq.53) then
  gqk(re-1)= 0.998989947776328213E+00
  gqk(re-2)= 0.994681919308006868E+00
  gqk(re-3)= 0.986947035023371333E+00
  gqk(re-4)= 0.975810233714984432E+00
  gqk(re-5)= 0.961309694623136535E+00
  gqk(re-6)= 0.943495353464442243E+00
  gqk(re-7)= 0.922428603042812267E+00
  gqk(re-8)= 0.898182057875426931E+00
  gqk(re-9)= 0.870839297558241521E+00
  gqk(re-10)= 0.840494576545801286E+00
  gqk(re-11)= 0.807252498416895725E+00
  gqk(re-12)= 0.771227654925532025E+00
  gqk(re-13)= 0.732544230807510188E+00
  gqk(re-14)= 0.691335575601366825E+00
  gqk(re-15)= 0.647743743916510462E+00
  gqk(re-16)= 0.601919005713769106E+00
  gqk(re-17)= 0.554019328277067569E+00
  gqk(re-18)= 0.504209831657133734E+00
  gqk(re-19)= 0.452662219461845705E+00
  gqk(re-20)= 0.399554186953952784E+00
  gqk(re-21)= 0.345068808495722579E+00
  gqk(re-22)= 0.289393906451626326E+00
  gqk(re-23)= 0.232721403724272885E+00
  gqk(re-24)= 0.175246662155326222E+00
  gqk(re-25)= 0.117167809071955620E+00
  gqk(re-26)= 0.586850543002594635E-01
  gqk(re-27)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.259168372056725344E-02
  gqw(re-2)= 0.602427622694844470E-02
  gqw(re-3)= 0.944120228494081323E-02
  gqw(re-4)= 0.128260261442399957E-01
  gqw(re-5)= 0.161667252566874593E-01
  gqw(re-6)= 0.194517211076365680E-01
  gqw(re-7)= 0.226696730570699577E-01
  gqw(re-8)= 0.258094825107575108E-01
  gqw(re-9)= 0.288603236178236883E-01
  gqw(re-10)= 0.318116784590194851E-01
  gqw(re-11)= 0.346533725835339451E-01
  gqw(re-12)= 0.373756098034831591E-01
  gqw(re-13)= 0.399690058435401285E-01
  gqw(re-14)= 0.424246206345194957E-01
  gqw(re-15)= 0.447339891036736825E-01
  gqw(re-16)= 0.468891503407507099E-01
  gqw(re-17)= 0.488826750326984000E-01
  gqw(re-18)= 0.507076910692924737E-01
  gqw(re-19)= 0.523579072298729700E-01
  gqw(re-20)= 0.538276348687306769E-01
  gqw(re-21)= 0.551118075239334995E-01
  gqw(re-22)= 0.562059983817396955E-01
  gqw(re-23)= 0.571064355362668613E-01
  gqw(re-24)= 0.578100149917128298E-01
  gqw(re-25)= 0.583143113622560591E-01
  gqw(re-26)= 0.586175862327201619E-01
  gqw(re-27)= 0.587187941511647660E-01
 else if (gqn.eq.54) then
  gqk(re-1)= 0.999026666867340762E+00
  gqk(re-2)= 0.994875117018338528E+00
  gqk(re-3)= 0.987420637397343870E+00
  gqk(re-4)= 0.976686328857903296E+00
  gqk(re-5)= 0.962707645785923605E+00
  gqk(re-6)= 0.945530975164995735E+00
  gqk(re-7)= 0.925213359866651652E+00
  gqk(re-8)= 0.901822286284701402E+00
  gqk(re-9)= 0.875435454065569219E+00
  gqk(re-10)= 0.846140515970773111E+00
  gqk(re-11)= 0.814034785913567815E+00
  gqk(re-12)= 0.779224915346253932E+00
  gqk(re-13)= 0.741826538809184144E+00
  gqk(re-14)= 0.701963889719172895E+00
  gqk(re-15)= 0.659769387631983451E+00
  gqk(re-16)= 0.615383198331127312E+00
  gqk(re-17)= 0.568952768195209568E+00
  gqk(re-18)= 0.520632334385933038E+00
  gqk(re-19)= 0.470582412481382606E+00
  gqk(re-20)= 0.418969263255204394E+00
  gqk(re-21)= 0.365964340372191388E+00
  gqk(re-22)= 0.311743720834468163E+00
  gqk(re-23)= 0.256487520069997643E+00
  gqk(re-24)= 0.200379293606213288E+00
  gqk(re-25)= 0.143605427316255946E+00
  gqk(re-26)= 0.863545182632482339E-01
  gqk(re-27)= 0.288167481993418170E-01
  
  gqw(re-1)= 0.249748183576151877E-02
  gqw(re-2)= 0.580561101524013686E-02
  gqw(re-3)= 0.909936945550923575E-02
  gqw(re-4)= 0.123633281288471151E-01
  gqw(re-5)= 0.155863030359248613E-01
  gqw(re-6)= 0.187575276214693894E-01
  gqw(re-7)= 0.218664514228533932E-01
  gqw(re-8)= 0.249027414672081254E-01
  gqw(re-9)= 0.278563093105955346E-01
  gqw(re-10)= 0.307173424978707948E-01
  gqw(re-11)= 0.334763364643725081E-01
  gqw(re-12)= 0.361241258403834226E-01
  gqw(re-13)= 0.386519147821024686E-01
  gqw(re-14)= 0.410513061366448301E-01
  gqw(re-15)= 0.433143293095971729E-01
  gqw(re-16)= 0.454334667282769306E-01
  gqw(re-17)= 0.474016788064450176E-01
  gqw(re-18)= 0.492124273245284627E-01
  gqw(re-19)= 0.508596971461884179E-01
  gqw(re-20)= 0.523380161982985245E-01
  gqw(re-21)= 0.536424736475540689E-01
  gqw(re-22)= 0.547687362130579308E-01
  gqw(re-23)= 0.557130625605904498E-01
  gqw(re-24)= 0.564723157306249376E-01
  gqw(re-25)= 0.570439735587948485E-01
  gqw(re-26)= 0.574261370541126609E-01
  gqw(re-27)= 0.576175367071472058E-01
 else if (gqn.eq.55) then
  gqk(re-1)= 0.999061419564817976E+00
  gqk(re-2)= 0.995057977847411346E+00
  gqk(re-3)= 0.987868941198889017E+00
  gqk(re-4)= 0.977515735503989069E+00
  gqk(re-5)= 0.964031328593135006E+00
  gqk(re-6)= 0.947458868041211044E+00
  gqk(re-7)= 0.927851424720791851E+00
  gqk(re-8)= 0.905271800744000044E+00
  gqk(re-9)= 0.879792322419895245E+00
  gqk(re-10)= 0.851494606617154037E+00
  gqk(re-11)= 0.820469298559321047E+00
  gqk(re-12)= 0.786815781127622405E+00
  gqk(re-13)= 0.750641856348021719E+00
  gqk(re-14)= 0.712063399986637702E+00
  gqk(re-15)= 0.671203990319826249E+00
  gqk(re-16)= 0.628194512249928172E+00
  gqk(re-17)= 0.583172738026031867E+00
  gqk(re-18)= 0.536282885908343276E+00
  gqk(re-19)= 0.487675158187473978E+00
  gqk(re-20)= 0.437505260037174160E+00
  gqk(re-21)= 0.385933900740979274E+00
  gqk(re-22)= 0.333126278890024052E+00
  gqk(re-23)= 0.279251553200806635E+00
  gqk(re-24)= 0.224482300647845606E+00
  gqk(re-25)= 0.168993963646873219E+00
  gqk(re-26)= 0.112964288059329218E+00
  gqk(re-27)= 0.565727538183368045E-01
  gqk(re-28)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.240832361998037214E-02
  gqw(re-2)= 0.559863226656106411E-02
  gqw(re-3)= 0.877574610705806821E-02
  gqw(re-4)= 0.119251607198487593E-01
  gqw(re-5)= 0.150364583335118630E-01
  gqw(re-6)= 0.180996145207283979E-01
  gqw(re-7)= 0.211048016680160377E-01
  gqw(re-8)= 0.240423880097262073E-01
  gqw(re-9)= 0.269029614563965139E-01
  gqw(re-10)= 0.296773577651604989E-01
  gqw(re-11)= 0.323566892261863623E-01
  gqw(re-12)= 0.349323728735898634E-01
  gqw(re-13)= 0.373961578679655737E-01
  gqw(re-14)= 0.397401518743368473E-01
  gqw(re-15)= 0.419568463177186698E-01
  gqw(re-16)= 0.440391404216063376E-01
  gqw(re-17)= 0.459803639462839503E-01
  gqw(re-18)= 0.477742985512007098E-01
  gqw(re-19)= 0.494151977115520405E-01
  gqw(re-20)= 0.508978051244938501E-01
  gqw(re-21)= 0.522173715456320103E-01
  gqw(re-22)= 0.533696700016056264E-01
  gqw(re-23)= 0.543510093299107086E-01
  gqw(re-24)= 0.551582460025086055E-01
  gqw(re-25)= 0.557887941952840347E-01
  gqw(re-26)= 0.562406340710846289E-01
  gqw(re-27)= 0.565123182497717508E-01
  gqw(re-28)= 0.566029764445602629E-01
 else if (gqn.eq.56) then
  gqk(re-1)= 0.999094343801465445E+00
  gqk(re-2)= 0.995231226081069620E+00
  gqk(re-3)= 0.988293715540161322E+00
  gqk(re-4)= 0.978301709140256603E+00
  gqk(re-5)= 0.965285901905490240E+00
  gqk(re-6)= 0.949286479561962437E+00
  gqk(re-7)= 0.930352880247496516E+00
  gqk(re-8)= 0.908543620420655618E+00
  gqk(re-9)= 0.883926108327827809E+00
  gqk(re-10)= 0.856576433762748612E+00
  gqk(re-11)= 0.826579132142881923E+00
  gqk(re-12)= 0.794026922893866227E+00
  gqk(re-13)= 0.759020422705129039E+00
  gqk(re-14)= 0.721667834450188006E+00
  gqk(re-15)= 0.682084612694470627E+00
  gqk(re-16)= 0.640393106807007007E+00
  gqk(re-17)= 0.596722182770663689E+00
  gqk(re-18)= 0.551206824855534849E+00
  gqk(re-19)= 0.503987718384381722E+00
  gqk(re-20)= 0.455210814878459491E+00
  gqk(re-21)= 0.405026880927092436E+00
  gqk(re-22)= 0.353591032174954745E+00
  gqk(re-23)= 0.301062253867220520E+00
  gqk(re-24)= 0.247602909434337132E+00
  gqk(re-25)= 0.193378238635275201E+00
  gqk(re-26)= 0.138555846810376387E+00
  gqk(re-27)= 0.833051868224353870E-01
  gqk(re-28)= 0.277970352872752950E-01
  
  gqw(re-1)= 0.232385537577303215E-02
  gqw(re-2)= 0.540252224601552901E-02
  gqw(re-3)= 0.846906316330747234E-02
  gqw(re-4)= 0.115098243403837352E-01
  gqw(re-5)= 0.145150892780213942E-01
  gqw(re-6)= 0.174755129114016299E-01
  gqw(re-7)= 0.203819298824025850E-01
  gqw(re-8)= 0.232253515625648783E-01
  gqw(re-9)= 0.259969870583913812E-01
  gqw(re-10)= 0.286882684738235909E-01
  gqw(re-11)= 0.312908767473099730E-01
  gqw(re-12)= 0.337967671156118241E-01
  gqw(re-13)= 0.361981938723151195E-01
  gqw(re-14)= 0.384877342592476954E-01
  gqw(re-15)= 0.406583113847442321E-01
  gqw(re-16)= 0.427032160846671019E-01
  gqw(re-17)= 0.446161276526921835E-01
  gqw(re-18)= 0.463911333730020331E-01
  gqw(re-19)= 0.480227467936004473E-01
  gqw(re-20)= 0.495059246830473892E-01
  gqw(re-21)= 0.508360826177984837E-01
  gqw(re-22)= 0.520091091517417628E-01
  gqw(re-23)= 0.530213785240102806E-01
  gqw(re-24)= 0.538697618657143634E-01
  gqw(re-25)= 0.545516368708899163E-01
  gqw(re-26)= 0.550648959017621883E-01
  gqw(re-27)= 0.554079525032453238E-01
  gqw(re-28)= 0.555797463065146879E-01
 else if (gqn.eq.57) then
  gqk(re-1)= 0.999125565625262779E+00
  gqk(re-2)= 0.995395523678430316E+00
  gqk(re-3)= 0.988696577650221786E+00
  gqk(re-4)= 0.979047226709468310E+00
  gqk(re-5)= 0.966476085171886545E+00
  gqk(re-6)= 0.951020626447876438E+00
  gqk(re-7)= 0.932726961067101845E+00
  gqk(re-8)= 0.911649678521391338E+00
  gqk(re-9)= 0.887851678882220940E+00
  gqk(re-10)= 0.861403983262046702E+00
  gqk(re-11)= 0.832385521150439067E+00
  gqk(re-12)= 0.800882894547218283E+00
  gqk(re-13)= 0.766990119359450162E+00
  gqk(re-14)= 0.730808344744523386E+00
  gqk(re-15)= 0.692445551199517562E+00
  gqk(re-16)= 0.652016228280976828E+00
  gqk(re-17)= 0.609641032908715408E+00
  gqk(re-18)= 0.565446429269237383E+00
  gqk(re-19)= 0.519564311391187528E+00
  gqk(re-20)= 0.472131609517975781E+00
  gqk(re-21)= 0.423289881451563821E+00
  gqk(re-22)= 0.373184890086594279E+00
  gqk(re-23)= 0.321966168395378616E+00
  gqk(re-24)= 0.269786573161838850E+00
  gqk(re-25)= 0.216801828796124085E+00
  gqk(re-26)= 0.163170062591264153E+00
  gqk(re-27)= 0.109051332808787774E+00
  gqk(re-28)= 0.546071510016470293E-01
  gqk(re-29)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.224375387225035942E-02
  gqw(re-2)= 0.521653347471887508E-02
  gqw(re-3)= 0.817816006782141024E-02
  gqw(re-4)= 0.111157637323365342E-01
  gqw(re-5)= 0.140202707907540070E-01
  gqw(re-6)= 0.168829590234411235E-01
  gqw(re-7)= 0.196952706994880447E-01
  gqw(re-8)= 0.224488078907765717E-01
  gqw(re-9)= 0.251353509909180706E-01
  gqw(re-10)= 0.277468814021802554E-01
  gqw(re-11)= 0.302756048426938498E-01
  gqw(re-12)= 0.327139743663715787E-01
  gqw(re-13)= 0.350547127823129176E-01
  gqw(re-14)= 0.372908343244169743E-01
  gqw(re-15)= 0.394156654754807406E-01
  gqw(re-16)= 0.414228648708008795E-01
  gqw(re-17)= 0.433064422162143456E-01
  gqw(re-18)= 0.450607761613817520E-01
  gqw(re-19)= 0.466806310736416902E-01
  gqw(re-20)= 0.481611726616874708E-01
  gqw(re-21)= 0.494979824020197398E-01
  gqw(re-22)= 0.506870707249270150E-01
  gqw(re-23)= 0.517248889205183526E-01
  gqw(re-24)= 0.526083397291774654E-01
  gqw(re-25)= 0.533347865848186747E-01
  gqw(re-26)= 0.539020614832985134E-01
  gqw(re-27)= 0.543084714524987855E-01
  gqw(re-28)= 0.545528036047615017E-01
  gqw(re-29)= 0.546343287565841385E-01
 else if (gqn.eq.58) then
  gqk(re-1)= 0.999155200407386479E+00
  gqk(re-2)= 0.995551476597291041E+00
  gqk(re-3)= 0.989079008248442415E+00
  gqk(re-4)= 0.979755014694350224E+00
  gqk(re-5)= 0.967606202502924062E+00
  gqk(re-6)= 0.952667557518869113E+00
  gqk(re-7)= 0.934982137588258988E+00
  gqk(re-8)= 0.914600928564352933E+00
  gqk(re-9)= 0.891582692022030332E+00
  gqk(re-10)= 0.865993794074807366E+00
  gqk(re-11)= 0.837908013339373592E+00
  gqk(re-12)= 0.807406327913088306E+00
  gqk(re-13)= 0.774576681749652884E+00
  gqk(re-14)= 0.739513731020042142E+00
  gqk(re-15)= 0.702318571153908167E+00
  gqk(re-16)= 0.663098445332125253E+00
  gqk(re-17)= 0.621966435263078932E+00
  gqk(re-18)= 0.579041135130224927E+00
  gqk(re-19)= 0.534446309648847429E+00
  gqk(re-20)= 0.488310537216718465E+00
  gqk(re-21)= 0.440766839186839565E+00
  gqk(re-22)= 0.391952296330753125E+00
  gqk(re-23)= 0.342007653597995565E+00
  gqk(re-24)= 0.291076914311108992E+00
  gqk(re-25)= 0.239306924966153411E+00
  gqk(re-26)= 0.186846951835761332E+00
  gqk(re-27)= 0.133848250595466900E+00
  gqk(re-28)= 0.804636302141425175E-01
  gqk(re-29)= 0.268470123659420787E-01
  
  gqw(re-1)= 0.216772324962763311E-02
  gqw(re-2)= 0.503998161265015784E-02
  gqw(re-3)= 0.790197384999922689E-02
  gqw(re-4)= 0.107415535328788565E-01
  gqw(re-5)= 0.135502371129885141E-01
  gqw(re-6)= 0.163198742349711587E-01
  gqw(re-7)= 0.190424654618933896E-01
  gqw(re-8)= 0.217101561401457331E-01
  gqw(re-9)= 0.243152527249637755E-01
  gqw(re-10)= 0.268502431819824805E-01
  gqw(re-11)= 0.293078180441596857E-01
  gqw(re-12)= 0.316808912538092541E-01
  gqw(re-13)= 0.339626204934161333E-01
  gqw(re-14)= 0.361464268670875222E-01
  gqw(re-15)= 0.382260138458586085E-01
  gqw(re-16)= 0.401953854098681534E-01
  gqw(re-17)= 0.420488633295821382E-01
  gqw(re-18)= 0.437811035336401824E-01
  gqw(re-19)= 0.453871115148196599E-01
  gqw(re-20)= 0.468622567290261222E-01
  gqw(re-21)= 0.482022859454179012E-01
  gqw(re-22)= 0.494033355089621706E-01
  gqw(re-23)= 0.504619424799535796E-01
  gqw(re-24)= 0.513750546182856066E-01
  gqw(re-25)= 0.521400391836695509E-01
  gqw(re-26)= 0.527546905263702529E-01
  gqw(re-27)= 0.532172364465791428E-01
  gqw(re-28)= 0.535263433040584768E-01
  gqw(re-29)= 0.536811198633349168E-01
 else if (gqn.eq.59) then
  gqk(re-1)= 0.999183353909294913E+00
  gqk(re-2)= 0.995699640383245987E+00
  gqk(re-3)= 0.989442365133731183E+00
  gqk(re-4)= 0.980427573956715759E+00
  gqk(re-5)= 0.968680221681781695E+00
  gqk(re-6)= 0.954233009376951324E+00
  gqk(re-7)= 0.937126190353453570E+00
  gqk(re-8)= 0.917407438788155338E+00
  gqk(re-9)= 0.895131711743472169E+00
  gqk(re-10)= 0.870361094292882220E+00
  gqk(re-11)= 0.843164625816872348E+00
  gqk(re-12)= 0.813618107288211379E+00
  gqk(re-13)= 0.781803889862361046E+00
  gqk(re-14)= 0.747810645278640496E+00
  gqk(re-15)= 0.711733118677197352E+00
  gqk(re-16)= 0.673671864504937212E+00
  gqk(re-17)= 0.633732966238849782E+00
  gqk(re-18)= 0.592027740704030303E+00
  gqk(re-19)= 0.548672427808396201E+00
  gqk(re-20)= 0.503787866557718345E+00
  gqk(re-21)= 0.457499158253266425E+00
  gqk(re-22)= 0.409935317810418698E+00
  gqk(re-23)= 0.361228914169794468E+00
  gqk(re-24)= 0.311515700803013718E+00
  gqk(re-25)= 0.260934237342811570E+00
  gqk(re-26)= 0.209625503392036533E+00
  gqk(re-27)= 0.157732505587857841E+00
  gqk(re-28)= 0.105399879016344095E+00
  gqk(re-29)= 0.527734840883099451E-01
  gqk(re-30)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.209549228454068270E-02
  gqw(re-2)= 0.487223916826582792E-02
  gqw(re-3)= 0.763952945348736739E-02
  gqw(re-4)= 0.103858855009964276E-01
  gqw(re-5)= 0.131033663063449511E-01
  gqw(re-6)= 0.157843473130817209E-01
  gqw(re-7)= 0.184213427536098588E-01
  gqw(re-8)= 0.210069982884366381E-01
  gqw(re-9)= 0.235341053937143940E-01
  gqw(re-10)= 0.259956197312981897E-01
  gqw(re-11)= 0.283846802005348550E-01
  gqw(re-12)= 0.306946278361115363E-01
  gqw(re-13)= 0.329190242710450734E-01
  gqw(re-14)= 0.350516696364001484E-01
  gqw(re-15)= 0.370866198188714094E-01
  gqw(re-16)= 0.390182030161600329E-01
  gqw(re-17)= 0.408410355386867666E-01
  gqw(re-18)= 0.425500368110667340E-01
  gqw(re-19)= 0.441404435302979528E-01
  gqw(re-20)= 0.456078229405091351E-01
  gqw(re-21)= 0.469480851869622726E-01
  gqw(re-22)= 0.481574947146059043E-01
  gqw(re-23)= 0.492326806793620883E-01
  gqw(re-24)= 0.501706463429970814E-01
  gqw(re-25)= 0.509687774253939421E-01
  gqw(re-26)= 0.516248493908909104E-01
  gqw(re-27)= 0.521370336483746866E-01
  gqw(re-28)= 0.525039026478295173E-01
  gqw(re-29)= 0.527244338591280268E-01
  gqw(re-30)= 0.527980126219903811E-01
 else if (gqn.eq.60) then
  gqk(re-1)= 0.999210123227436187E+00
  gqk(re-2)= 0.995840525118838249E+00
  gqk(re-3)= 0.989787895222221770E+00
  gqk(re-4)= 0.981067201752598317E+00
  gqk(re-5)= 0.969701788765052752E+00
  gqk(re-6)= 0.955722255839996149E+00
  gqk(re-7)= 0.939166276116422782E+00
  gqk(re-8)= 0.920078476177627280E+00
  gqk(re-9)= 0.898510310810046064E+00
  gqk(re-10)= 0.874519922646898040E+00
  gqk(re-11)= 0.848171984785929811E+00
  gqk(re-12)= 0.819537526162145924E+00
  gqk(re-13)= 0.788693739932264215E+00
  gqk(re-14)= 0.755723775306585743E+00
  gqk(re-15)= 0.720716513355730393E+00
  gqk(re-16)= 0.683766327381355565E+00
  gqk(re-17)= 0.644972828489477013E+00
  gqk(re-18)= 0.604440597048510719E+00
  gqk(re-19)= 0.562278900753944710E+00
  gqk(re-20)= 0.518601400058570139E+00
  gqk(re-21)= 0.473525841761706923E+00
  gqk(re-22)= 0.427173741583078137E+00
  gqk(re-23)= 0.379670056576798254E+00
  gqk(re-24)= 0.331142848268448142E+00
  gqk(re-25)= 0.281722937423261766E+00
  gqk(re-26)= 0.231543551376029111E+00
  gqk(re-27)= 0.180739964873425335E+00
  gqk(re-28)= 0.129449135396944970E+00
  gqk(re-29)= 0.778093339495365410E-01
  gqk(re-30)= 0.259597723012476474E-01
  
  gqw(re-1)= 0.202681196887353294E-02
  gqw(re-2)= 0.471272992695384151E-02
  gqw(re-3)= 0.738993116334548880E-02
  gqw(re-4)= 0.100475571822881728E-01
  gqw(re-5)= 0.126781664768156801E-01
  gqw(re-6)= 0.152746185967851077E-01
  gqw(re-7)= 0.178299010142074013E-01
  gqw(re-8)= 0.203371207294571026E-01
  gqw(re-9)= 0.227895169439978999E-01
  gqw(re-10)= 0.251804776215210982E-01
  gqw(re-11)= 0.275035567499250753E-01
  gqw(re-12)= 0.297524915007890793E-01
  gqw(re-13)= 0.319212190192958850E-01
  gqw(re-14)= 0.340038927249464648E-01
  gqw(re-15)= 0.359948980510846547E-01
  gqw(re-16)= 0.378888675692436588E-01
  gqw(re-17)= 0.396806954523805586E-01
  gqw(re-18)= 0.413655512355848576E-01
  gqw(re-19)= 0.429388928359351468E-01
  gqw(re-20)= 0.443964787957876958E-01
  gqw(re-21)= 0.457343797161141596E-01
  gqw(re-22)= 0.469489888489124513E-01
  gqw(re-23)= 0.480370318199714666E-01
  gqw(re-24)= 0.489955754557561199E-01
  gqw(re-25)= 0.498220356905504300E-01
  gqw(re-26)= 0.505141845325089225E-01
  gqw(re-27)= 0.510701560698561124E-01
  gqw(re-28)= 0.514884515009812679E-01
  gqw(re-29)= 0.517679431749097571E-01
  gqw(re-30)= 0.519078776312203172E-01
 else if (gqn.eq.61) then
  gqk(re-1)= 0.999235597631362937E+00
  gqk(re-2)= 0.995974599815119932E+00
  gqk(re-3)= 0.990116745232516582E+00
  gqk(re-4)= 0.981676011284036809E+00
  gqk(re-5)= 0.970674258833183257E+00
  gqk(re-6)= 0.957140151912984294E+00
  gqk(re-7)= 0.941108986681361226E+00
  gqk(re-8)= 0.922622581382955165E+00
  gqk(re-9)= 0.901729162474001145E+00
  gqk(re-10)= 0.878483237214881085E+00
  gqk(re-11)= 0.852945450847663600E+00
  gqk(re-12)= 0.825182428108660049E+00
  gqk(re-13)= 0.795266599282359543E+00
  gqk(re-14)= 0.763276011172312141E+00
  gqk(re-15)= 0.729294123449464760E+00
  gqk(re-16)= 0.693409590894490946E+00
  gqk(re-17)= 0.655716032095070789E+00
  gqk(re-18)= 0.616311785197921402E+00
  gqk(re-19)= 0.575299651350830610E+00
  gqk(re-20)= 0.532786626502925187E+00
  gqk(re-21)= 0.488883622262251960E+00
  gqk(re-22)= 0.443705176538531587E+00
  gqk(re-23)= 0.397369154725755569E+00
  gqk(re-24)= 0.349996442204066838E+00
  gqk(re-25)= 0.301710628963030580E+00
  gqk(re-26)= 0.252637687169053549E+00
  gqk(re-27)= 0.202905642518058676E+00
  gqk(re-28)= 0.152644240230815159E+00
  gqk(re-29)= 0.101984606562274122E+00
  gqk(re-30)= 0.510589067079742151E-01
  gqk(re-31)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.196145336167059684E-02
  gqw(re-2)= 0.456092400601268346E-02
  gqw(re-3)= 0.715235499174884069E-02
  gqw(re-4)= 0.972546183035615137E-02
  gqw(re-5)= 0.122732635078117314E-01
  gqw(re-6)= 0.147890658849384096E-01
  gqw(re-7)= 0.172662929876137780E-01
  gqw(re-8)= 0.196984777461006871E-01
  gqw(re-9)= 0.220792731483191208E-01
  gqw(re-10)= 0.244024671875434328E-01
  gqw(re-11)= 0.266619985241506700E-01
  gqw(re-12)= 0.288519720881832663E-01
  gqw(re-13)= 0.309666743683978708E-01
  gqw(re-14)= 0.330005882759078564E-01
  gqw(re-15)= 0.349484075165332614E-01
  gqw(re-16)= 0.368050504231551834E-01
  gqw(re-17)= 0.385656732070083011E-01
  gqw(re-18)= 0.402256825909979437E-01
  gqw(re-19)= 0.417807477908884728E-01
  gqw(re-20)= 0.432268118124963935E-01
  gqw(re-21)= 0.445601020350832322E-01
  gqw(re-22)= 0.457771400531456624E-01
  gqw(re-23)= 0.468747507508083250E-01
  gqw(re-24)= 0.478500705850954597E-01
  gqw(re-25)= 0.487005550564112225E-01
  gqw(re-26)= 0.494239853467362267E-01
  gqw(re-27)= 0.500184741081782513E-01
  gqw(re-28)= 0.504824703867971930E-01
  gqw(re-29)= 0.508147636688183815E-01
  gqw(re-30)= 0.510144870386976743E-01
  gqw(re-31)= 0.510811194407861310E-01
 else if (gqn.eq.62) then
  gqk(re-1)= 0.999259859308777032E+00
  gqk(re-2)= 0.996102296316267455E+00
  gqk(re-3)= 0.990429971189290548E+00
  gqk(re-4)= 0.982255949097236680E+00
  gqk(re-5)= 0.971600723371651931E+00
  gqk(re-6)= 0.958491172973927386E+00
  gqk(re-7)= 0.942960401392328396E+00
  gqk(re-8)= 0.925047635636203736E+00
  gqk(re-9)= 0.904798122521093484E+00
  gqk(re-10)= 0.882263012831897564E+00
  gqk(re-11)= 0.857499231512070637E+00
  gqk(re-12)= 0.830569333604004978E+00
  gqk(re-13)= 0.801541346103976315E+00
  gqk(re-14)= 0.770488596055419639E+00
  gqk(re-15)= 0.737489525283156810E+00
  gqk(re-16)= 0.702627492222297145E+00
  gqk(re-17)= 0.665990561335479181E+00
  gqk(re-18)= 0.627671280646885488E+00
  gqk(re-19)= 0.587766447953086857E+00
  gqk(re-20)= 0.546376866300251063E+00
  gqk(re-21)= 0.503607089344755621E+00
  gqk(re-22)= 0.459565157240113820E+00
  gqk(re-23)= 0.414362323717125947E+00
  gqk(re-24)= 0.368112775046564367E+00
  gqk(re-25)= 0.320933341594194232E+00
  gqk(re-26)= 0.272943202696726117E+00
  gqk(re-27)= 0.224263585604165372E+00
  gqk(re-28)= 0.175017459249015739E+00
  gqk(re-29)= 0.125329223615896990E+00
  gqk(re-30)= 0.753243954962343337E-01
  gqk(re-31)= 0.251292914218204273E-01
  
  gqw(re-1)= 0.189920567951343180E-02
  gqw(re-2)= 0.441633345693080021E-02
  gqw(re-3)= 0.692604190183140032E-02
  gqw(re-4)= 0.941857942842010999E-02
  gqw(re-5)= 0.118873901170102655E-01
  gqw(re-6)= 0.143261918238066759E-01
  gqw(re-7)= 0.167288117901775620E-01
  gqw(re-8)= 0.190891766585733862E-01
  gqw(re-9)= 0.214013222776696203E-01
  gqw(re-10)= 0.236594072086829393E-01
  gqw(re-11)= 0.258577269540250683E-01
  gqw(re-12)= 0.279907281633140076E-01
  gqw(re-13)= 0.300530225739900035E-01
  gqw(re-14)= 0.320394005816246469E-01
  gqw(re-15)= 0.339448443794104840E-01
  gqw(re-16)= 0.357645406227678070E-01
  gqw(re-17)= 0.374938925822802388E-01
  gqw(re-18)= 0.391285317519635897E-01
  gqw(re-19)= 0.406643288824177773E-01
  gqw(re-20)= 0.420974044103838568E-01
  gqw(re-21)= 0.434241382580475219E-01
  gqw(re-22)= 0.446411789771246417E-01
  gqw(re-23)= 0.457454522145701939E-01
  gqw(re-24)= 0.467341684784151890E-01
  gqw(re-25)= 0.476048301841014432E-01
  gqw(re-26)= 0.483552379634780358E-01
  gqw(re-27)= 0.489834962205173280E-01
  gqw(re-28)= 0.494880179196995340E-01
  gqw(re-29)= 0.498675285949523941E-01
  gqw(re-30)= 0.501210695690438093E-01
  gqw(re-31)= 0.502480003752561535E-01
 else if (gqn.eq.63) then
  gqk(re-1)= 0.999282984029123744E+00
  gqk(re-2)= 0.996224012777970347E+00
  gqk(re-3)= 0.990728546892189477E+00
  gqk(re-4)= 0.982808810593727267E+00
  gqk(re-5)= 0.972484034697569943E+00
  gqk(re-6)= 0.959779449758941805E+00
  gqk(re-7)= 0.944726134041010135E+00
  gqk(re-8)= 0.927360920621843055E+00
  gqk(re-9)= 0.907726302778531391E+00
  gqk(re-10)= 0.885870328507853411E+00
  gqk(re-11)= 0.861846482364123756E+00
  gqk(re-12)= 0.835713554319502894E+00
  gqk(re-13)= 0.807535495773457290E+00
  gqk(re-14)= 0.777381262990372690E+00
  gqk(re-15)= 0.745324648317847616E+00
  gqk(re-16)= 0.711444099584845779E+00
  gqk(re-17)= 0.675822528114985888E+00
  gqk(re-18)= 0.638547105821365313E+00
  gqk(re-19)= 0.599709051877625354E+00
  gqk(re-20)= 0.559403409486284975E+00
  gqk(re-21)= 0.517728813290033174E+00
  gqk(re-22)= 0.474787247994803985E+00
  gqk(re-23)= 0.430683798795111594E+00
  gqk(re-24)= 0.385526394212247270E+00
  gqk(re-25)= 0.339425541974584688E+00
  gqk(re-26)= 0.292494058586251715E+00
  gqk(re-27)= 0.244846793245953515E+00
  gqk(re-28)= 0.196600346791507008E+00
  gqk(re-29)= 0.147872786357872155E+00
  gqk(re-30)= 0.987833564469454278E-01
  gqk(re-31)= 0.494521871161597501E-01
  gqk(re-32)= 0.000000000000000000E+00
  
  gqw(re-1)= 0.183987459557743158E-02
  gqw(re-2)= 0.427850834686345060E-02
  gqw(re-3)= 0.671029176596037343E-02
  gqw(re-4)= 0.912596867632676188E-02
  gqw(re-5)= 0.115193760768799568E-01
  gqw(re-6)= 0.138846126161152016E-01
  gqw(re-7)= 0.162158784103383358E-01
  gqw(re-8)= 0.185074644601614172E-01
  gqw(re-9)= 0.207537612580390024E-01
  gqw(re-10)= 0.229492710048898978E-01
  gqw(re-11)= 0.250886205533449938E-01
  gqw(re-12)= 0.271665743590972023E-01
  gqw(re-13)= 0.291780472082806795E-01
  gqw(re-14)= 0.311181166222201308E-01
  gqw(re-15)= 0.329820348837790989E-01
  gqw(re-16)= 0.347652406453560561E-01
  gqw(re-17)= 0.364633700854572682E-01
  gqw(re-18)= 0.380722675843495131E-01
  gqw(re-19)= 0.395879958915442767E-01
  gqw(re-20)= 0.410068457596661912E-01
  gqw(re-21)= 0.423253450208160853E-01
  gqw(re-22)= 0.435402670830273286E-01
  gqw(re-23)= 0.446486388259411809E-01
  gqw(re-24)= 0.456477478762922911E-01
  gqw(re-25)= 0.465351492453839957E-01
  gqw(re-26)= 0.473086713122690058E-01
  gqw(re-27)= 0.479664211379946387E-01
  gqw(re-28)= 0.485067890978837721E-01
  gqw(re-29)= 0.489284528205117600E-01
  gqw(re-30)= 0.492303804237476592E-01
  gqw(re-31)= 0.494118330399178907E-01
  gqw(re-32)= 0.494723666239314658E-01
 else if (gqn.eq.64) then
  gqk(re-1)= 0.999305041735771837E+00
  gqk(re-2)= 0.996340116771954998E+00
  gqk(re-3)= 0.991013371476744065E+00
  gqk(re-4)= 0.983336253884625644E+00
  gqk(re-5)= 0.973326827789910531E+00
  gqk(re-6)= 0.961008799652054102E+00
  gqk(re-7)= 0.946411374858402987E+00
  gqk(re-8)= 0.929569172131939792E+00
  gqk(re-9)= 0.910522137078503158E+00
  gqk(re-10)= 0.889315445995114140E+00
  gqk(re-11)= 0.865999398154092881E+00
  gqk(re-12)= 0.840629296252580427E+00
  gqk(re-13)= 0.813265315122797317E+00
  gqk(re-14)= 0.783972358943341274E+00
  gqk(re-15)= 0.752819907260531718E+00
  gqk(re-16)= 0.719881850171610882E+00
  gqk(re-17)= 0.685236313054233270E+00
  gqk(re-18)= 0.648965471254657200E+00
  gqk(re-19)= 0.611155355172393056E+00
  gqk(re-20)= 0.571895646202633334E+00
  gqk(re-21)= 0.531279464019894676E+00
  gqk(re-22)= 0.489403145707052789E+00
  gqk(re-23)= 0.446366017253464031E+00
  gqk(re-24)= 0.402270157963991848E+00
  gqk(re-25)= 0.357220158337668403E+00
  gqk(re-26)= 0.311322871990211414E+00
  gqk(re-27)= 0.264687162208767535E+00
  gqk(re-28)= 0.217423643740007194E+00
  gqk(re-29)= 0.169644420423993081E+00
  gqk(re-30)= 0.121462819296120655E+00
  gqk(re-31)= 0.729931217877993060E-01
  gqk(re-32)= 0.243502926634243631E-01
  
  gqw(re-1)= 0.178328072169718112E-02
  gqw(re-2)= 0.414703326056236471E-02
  gqw(re-3)= 0.650445796897804358E-02
  gqw(re-4)= 0.884675982636408571E-02
  gqw(re-5)= 0.111681394601308298E-01
  gqw(re-6)= 0.134630478967181499E-01
  gqw(re-7)= 0.157260304760249540E-01
  gqw(re-8)= 0.179517157756972079E-01
  gqw(re-9)= 0.201348231535296296E-01
  gqw(re-10)= 0.222701738083835726E-01
  gqw(re-11)= 0.243527025687106310E-01
  gqw(re-12)= 0.263774697150546203E-01
  gqw(re-13)= 0.283396726142592578E-01
  gqw(re-14)= 0.302346570724027139E-01
  gqw(re-15)= 0.320579283548518487E-01
  gqw(re-16)= 0.338051618371418006E-01
  gqw(re-17)= 0.354722132568824136E-01
  gqw(re-18)= 0.370551285402399774E-01
  gqw(re-19)= 0.385501531786161464E-01
  gqw(re-20)= 0.399537411327200095E-01
  gqw(re-21)= 0.412625632426231667E-01
  gqw(re-22)= 0.424735151236534866E-01
  gqw(re-23)= 0.435837245293231174E-01
  gqw(re-24)= 0.445905581637567883E-01
  gqw(re-25)= 0.454916279274180657E-01
  gqw(re-26)= 0.462847965813145898E-01
  gqw(re-27)= 0.469681828162097845E-01
  gqw(re-28)= 0.475401657148307108E-01
  gqw(re-29)= 0.479993885964579287E-01
  gqw(re-30)= 0.483447622348028641E-01
  gqw(re-31)= 0.485754674415032756E-01
  gqw(re-32)= 0.486909570091403135E-01
 end if

 ! Return the loop limits to be used in the order variable
 gqn = re - 1

 ! Reflection symmetry about 0 for the positions and weights
 do i = 1, gqn
   gqk(-i) = -gqk(i); gqw(-i) = gqw(i)
 end do

 ! Divide the weights by two to correspond to a unit integration
 gqw = gqw / 2.0d0

 return
 end subroutine gauss

      subroutine sortheap(n,ia,ra)

      integer n
      integer ia(n)
      real*8 ra(n)

      integer i, ir, j, l, iia
      real*8 rra

      if(n.lt.2) return

      l=n/2+1
      ir=n
   10 continue

        if(l.gt.1) then
          l=l-1
          rra=ra(l)
          iia=ia(l)
        else
          rra=ra(ir)
          iia=ia(ir)
          ra(ir)=ra(1)
          ia(ir)=ia(1)
          ir=ir-1
          if(ir.eq.1) then
            ra(1)=rra
            ia(1)=iia
            return
          endif
        endif

        i=l
        j=l+l

   20   if(j.le.ir) then
          if(j.lt.ir) then
            if(ia(j).lt.ia(j+1)) j=j+1
          endif
          if(iia.lt.ia(j)) then
            ra(i)=ra(j)
            ia(i)=ia(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif

          goto 20

        endif

        ra(i) = rra
        ia(i) = iia

      goto 10

      end



      subroutine heapsort(n,ia,ra)

      integer n
      integer ia(n)
      real ra(n)

      integer i, ir, j, l, iia
      real rra

      if(n.lt.2) return

      l=n/2+1
      ir=n
   10 continue

        if(l.gt.1) then
          l=l-1
          rra=ra(l)
          iia=ia(l)
        else
          rra=ra(ir)
          iia=ia(ir)
          ra(ir)=ra(1)
          ia(ir)=ia(1)
          ir=ir-1
          if(ir.eq.1) then
            ra(1)=rra
            ia(1)=iia
            return
          endif
        endif

        i=l
        j=l+l
        
   20   if(j.le.ir) then
          if(j.lt.ir) then
            if(ra(j).lt.ra(j+1)) j=j+1
          endif
          if(rra.lt.ra(j)) then
            ra(i)=ra(j)
            ia(i)=ia(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        
          goto 20

        endif

        ra(i) = rra
        ia(i) = iia

      goto 10

      end



      subroutine dheapsort(n,ia,ra)

      integer n
      integer ia(n)
      double precision ra(n)

      integer i, ir, j, l, iia
      double precision rra

      if(n.lt.2) return

      l=n/2+1
      ir=n
   10 continue

        if(l.gt.1) then
          l=l-1
          rra=ra(l)
          iia=ia(l)
        else
          rra=ra(ir)
          iia=ia(ir)
          ra(ir)=ra(1)
          ia(ir)=ia(1)
          ir=ir-1
          if(ir.eq.1) then
            ra(1)=rra
            ia(1)=iia
            return
          endif
        endif

        i=l
        j=l+l
        
   20   if(j.le.ir) then
          if(j.lt.ir) then
            if(ra(j).lt.ra(j+1)) j=j+1
          endif
          if(rra.lt.ra(j)) then
            ra(i)=ra(j)
            ia(i)=ia(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        
          goto 20

        endif

        ra(i) = rra
        ia(i) = iia

      goto 10

      end



      subroutine sides(tag,one,two,thr,x1,y1,x2,y2,x3,y3,flag,flip,topy)

      use CONTROL, only : disgal, axisym, refsym, tol

      implicit none
      integer tag(2)
      real x1, y1, x2, y2, x3, y3, loctol, cross, diag1, diag2, diag3, topy
      logical one, two, thr, flag, flip
      external cross
      
      ! Initialize boundary tag
      tag = 0; flip = .false.

      ! Set-up the appropriate cross-product tolerance measure
      if(axisym) then
        loctol = 0.01
      else
        loctol = tol
      end if

      ! If all the nodes are flagged as lying on the boundary ...
      if( one .and. two .and. thr ) then

        ! Careful of the bottom left corner
        if(axisym.and.refsym.and.(x1+y1.lt.tol)) then
          tag(1) = 2; tag(2) = 3

        else if(axisym.and.refsym.and.(x2+y2.lt.tol)) then
          tag(1) = 1; tag(2) = 3

        else if(axisym.and.refsym.and.(x3+y3.lt.tol)) then
          tag(1) = 1; tag(2) = 2

        else if(axisym.and.((abs(x1).lt.tol).or.(abs(x2).lt.tol).or.(abs(x3).lt.tol))) then

          ! Top left corner needs special treatment
          diag1 = (x2-x3)**2
          if(diag1.gt.tol*tol) diag1 = diag1 + (y2-y3)**2
          if((y2.ge.topy-tol).or.(y3.ge.topy-tol)) diag1 = diag1*0.9

          diag2 = (x3-x1)**2
          if(diag2.gt.tol*tol) diag2 = diag2 + (y3-y1)**2
          if((y3.ge.topy-tol).or.(y1.ge.topy-tol)) diag2 = diag2*0.9
          
          diag3 = (x1-x2)**2
          if(diag3.gt.tol*tol) diag3 = diag3 + (y1-y2)**2
          if((y1.ge.topy-tol).or.(y2.ge.topy-tol)) diag3 = diag3*0.9

          if((diag1.gt.diag2).and.(diag1.gt.diag3)) then
            tag(1) = 2; tag(2) = 3
          else if((diag2.gt.diag1).and.(diag2.gt.diag3)) then
            tag(1) = 1; tag(2) = 3
          else if((diag3.gt.diag1).and.(diag3.gt.diag2)) then
            tag(1) = 1; tag(2) = 2
          end if

          flip = .true.
          return

        else if(axisym.and.refsym.and.((abs(y1).lt.tol).or.(abs(y2).lt.tol).or.(abs(y3).lt.tol))) then

          ! Bottom right corner also needs special treatment
          if     (((abs(y1+y2).lt.tol).and.(x1.ge.x2)).or.((x1.ge.x3).and.(abs(y1+y3).lt.tol))) then
            tag(1) = 2; tag(2) = 3
          else if(((abs(y2+y1).lt.tol).and.(x2.ge.x1)).or.((x2.ge.x3).and.(abs(y2+y3).lt.tol))) then
            tag(1) = 1; tag(2) = 3
          else if(((abs(y3+y1).lt.tol).and.(x3.ge.x1)).or.((x3.ge.x2).and.(abs(y3+y2).lt.tol))) then
            tag(1) = 1; tag(2) = 2
          end if

          flip = .true.
          return

        end if

        if(      abs(cross(x3-x2,y3-y2,x1-x2,y1-y2)).gt.1.0-loctol ) then
          tag(1) = 1; tag(2) = 3
        else if( abs(cross(x1-x3,y1-y3,x2-x3,y2-y3)).gt.1.0-loctol ) then
          tag(1) = 1; tag(2) = 2
        else if( abs(cross(x3-x1,y3-y1,x2-x1,y2-y1)).gt.1.0-loctol ) then
          tag(1) = 2; tag(2) = 3
        endif

        ! ... and for the non-conforming case, is also flagged to be "flipped" ...
        flip = disgal

      ! Two diagonal nodes lying on the boundary
      else if( one.and.two.and.( abs(cross(x1-x3,y1-y3,x2-x3,y2-y3)).gt.1.0-loctol .and.flag ).and.(.not.axisym) ) then
        tag(1) = -1; tag(2) = -2
      else if( two.and.thr.and.( abs(cross(x3-x1,y3-y1,x2-x1,y2-y1)).gt.1.0-loctol .and.flag ).and.(.not.axisym) ) then
        tag(1) = -2; tag(2) = -3
      else if( thr.and.one.and.( abs(cross(x3-x2,y3-y2,x1-x2,y1-y2)).gt.1.0-loctol .and.flag ).and.(.not.axisym) ) then
        tag(1) = -1; tag(2) = -3

      ! One element edge only on boundary
      else if( two .and. thr ) then
        tag(1) =  1
        if(axisym.and.refsym) then
          flip = (x2+x3.eq.0.0)
          if((abs(y1-y3).lt.tol).or.((y1-y2)/x1.gt.tol)) then
            tag(2) = 12
          else
            tag(2) = 13
          end if
        end if
      else if( one .and. thr ) then
        tag(1) =  2
        if(axisym.and.refsym) then
          flip = (x1+x3.eq.0.0)
          if((abs(y2-y3).lt.tol).or.((y2-y1)/x2.gt.tol)) then
            tag(2) = 11
          else
            tag(2) = 13
          end if
        end if
      else if( one .and. two ) then
        tag(1) =  3
        if(axisym.and.refsym) then
          flip = (x1+x2.eq.0.0)
          if((abs(y3-y2).lt.tol).or.((y3-y1)/x3.gt.tol)) then
            tag(2) = 11
          else
            tag(2) = 12
          end if
        end if

      ! Only one node on boundary
      else if( one ) then
        tag(1) = -1
      else if( two ) then
        tag(1) = -2
      else if( thr ) then
        tag(1) = -3
      endif

      return
      end




      subroutine decoder(tag,flag)                             
 
      implicit none
      integer tag(2)
      logical flag

      if(flag) then
        tag(2) = floor( real(tag(1)) / 10.0 )
        tag(1) = modulo( tag(1) - 1 , 10 ) + 1 - 4
      else
        tag(1) = 10 * tag(2) + 4 + tag(1)
      endif

      return
      end       




      subroutine Matrix_Inverse(n,Matrix,Inverse)
 
      integer n, i, j, err
      double precision Matrix(n,n), Inverse(n,n)
      double precision, allocatable::MatrixStore(:,:)     
   
      allocate(MatrixStore(n,n),stat=err)
      if(err.ne.0) stop
      MatrixStore=Matrix
 
      call LU_Factorization(n,Matrix)
 
      do 40 i = 1, n
        do 30 j = 1, n
          if(i.eq.j) then
            Inverse(i,j)=1.0
          else
            Inverse(i,j)=0.0
          endif
   30   continue
   40 continue
      call Solver(n,Matrix,Inverse,n)
 
      Matrix=MatrixStore

      return
      end




      subroutine LU_FactorizationLight(n,A)

      use PLACE, only : cr

      integer n, prow, row, column, post, maxcol
      double precision A(n*n)

      maxcol=0
      !print*,'Factorizing ...'
      do 40 prow = 1, n
        !print*,"Factoring Row ",prow,'  ',n
        post=0
        do 10 column=n, 1, -1
          if(abs(A(cr(prow,column))).gt.0.1d-22) goto 15
          post=post+1
   10   continue
   15   if(maxcol.lt.n-post) maxcol=n-post
        do 30 row = prow +1, n
          if(abs(A(cr(row,prow))).lt.0.1d-22) goto 30
          if(abs(A(cr(prow,prow))).lt.0.1d-22) then
            print*,'oooooooooppps!'
            stop
          endif
          A(cr(row,prow))=A(cr(row,prow))/A(cr(prow,prow))
          do 20 column = prow+1, maxcol
            A(cr(row,column))=A(cr(row,column)) -A(cr(row,prow))*A(cr(prow,column))
   20     continue
   30   continue
   40 continue
      return
      end


    
      subroutine LU_Factorization(n,A)
 
      integer n, prow, row, column, post, maxcol
      double precision A(n,n)
                
      maxcol=0                                                    
      !print*,'Factorizing ...'
      do 40 prow = 1, n                                     
        !print*,"Factoring Row ",prow,'  ',n
        post=0
        do 10 column=n, 1, -1   
          if(abs(A(prow,column)).gt.0.1d-22) goto 15
          post=post+1   
   10   continue            
   15   if(maxcol.lt.n-post) maxcol=n-post
        do 30 row = prow +1, n                        
          if(abs(A(row,prow)).lt.0.1d-22) goto 30
          if(abs(A(prow,prow)).lt.0.1d-22) then
            print*,'oooooooooppps!'
            stop
          endif
          A(row,prow)=A(row,prow)/A(prow,prow)
          do 20 column = prow+1, maxcol
            A(row,column)=A(row,column) -A(row,prow)*A(prow,column)
   20     continue
   30   continue   
   40 continue
      return
      end
                                            
      complex function Det(n,A)
 
      integer n, i
      double precision A(n,n)
 
      Det=(1.0,0.0)
      do 10 i = 1, n
        Det = Det*A(i,i)
   10 continue
      return
      end
 
 
      subroutine Solver(n,A,B,numsols)
 
      integer n, row, column, sol, numsols
      double precision A(n,n), B(n,numsols)
 
      !print*,'Backsubstituting ...'
      do 50 sol = 1, numsols
        do 20 column = 1, n-1
          do 10 row = column +1, n
            B(row,sol)=B(row,sol) -A(row,column)*B(column,sol)
   10     continue
   20   continue
        do 40 row = n, 1, -1
          do 30 column = n, row +1, -1
            B(row,sol)=B(row,sol) -A(row,column)*B(column,sol)
   30     continue
          if(abs(A(row,row)).lt.0.1d-22) then
            print*,'oooooooooooooooooooooooooppps!'
            stop
          endif
          B(row,sol)=B(row,sol)/A(row,row)
   40   continue       
   50 continue
      return
      end

      subroutine SolverLight(n,A,B,numsols)

      use PLACE, only : cr

      integer n, row, column, sol, numsols
      double precision A(n*n), B(n,numsols)

      !print*,'Backsubstituting ...'
      do 50 sol = 1, numsols
        do 20 column = 1, n-1
          do 10 row = column +1, n
            B(row,sol)=B(row,sol) -A(cr(row,column))*B(column,sol)
   10     continue
   20   continue
        do 40 row = n, 1, -1
          do 30 column = n, row +1, -1
            B(row,sol)=B(row,sol) -A(cr(row,column))*B(column,sol)
   30     continue
          if(abs(A(cr(row,row))).lt.0.1d-22) then
            print*,'oooooooooooooooooooooooooppps!'
            stop
          endif
          B(row,sol)=B(row,sol)/A(cr(row,row))
   40   continue
   50 continue
      return
      end


      subroutine ludcmp(a,n,indx)
 
      integer n, indx(n), NMAX
      double precision d, a(n,n), TINY
      parameter (NMAX=500,TINY=1.0E-20)

      integer i, imax, j, k
      double precision aamax, dum, sum, vv(NMAX)

      d=1.0
      do 12 i=1,n
        aamax=0.0
        do 11 j=1,n
            if(abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
   11   continue
        if(aamax.eq.0.0) then
            print*,'An error has occured....... ', i, n
            stop
        endif
        vv(i)=1./aamax
   12 continue
      do 19 j=1,n
        print*,"Solving ",j," ",n
        do 14 i=1,j-1
            sum=a(i,j)
            do 13 k=1,i-1
                sum=sum-a(i,k)*a(k,j)
   13       continue
            a(i,j)=sum
   14   continue
        aamax=0.0
        do 16 i=j,n
            sum=a(i,j)
            do 15 k=1,j-1
                sum=sum-a(i,k)*a(k,j)
   15       continue
            a(i,j)=sum
            dum=vv(i)*abs(sum)
            if(dum.ge.aamax) then
                imax=i
                aamax=dum
            endif
   16   continue
        if(j.ne.imax) then
            do 17 k=1,n
                dum=a(imax,k)
                a(imax,k)=a(j,k)
                a(j,k)=dum
   17       continue
            d=-d
            vv(imax)=vv(j)
        endif
        if(imax.gt.n) then
            print*,"imax too big!!! ", imax, "  >  ",n
            stop
        endif
        indx(j)=imax
        if(a(j,j).eq.0.0) a(j,j)=TINY
        if(j.ne.n) then
            dum=1.0/a(j,j)
            do 18 i=j+1,n
                a(i,j)=a(i,j)*dum
   18       continue
        endif
   19 continue
      return
      END


      subroutine lubksb(a,n,indx,b)
      
      integer n,indx(n)
      double precision a(n,n), b(n)

      integer i,ii,j,ll
      double precision sum

      ii=0
      do 12 i=1,n
        ll=indx(i)
        sum=b(ll)
        b(ll)=b(i)
        if(ii.ne.0) then
            do 11 j=ii,i-1
                sum=sum-a(i,j)*b(j)
   11       continue
        else if(sum.ne.0.0) then
            ii=i
        endif
        b(i)=sum
   12 continue
      do 14 i=n,1,-1
        sum=b(i)
        do 13 j=i+1,n
            sum=sum-a(i,j)*b(j)
   13   continue
        b(i)=sum/a(i,i)
   14 continue
      return
      END
        

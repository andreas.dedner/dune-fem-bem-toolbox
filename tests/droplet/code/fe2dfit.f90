

      subroutine legfit(x,y)

      use CONTROL, only : lp, param, piontwo

      implicit none

      double precision, intent(inout)  :: x, y

      ! Local variables
      integer n
      double precision ang, pn, dpndx
      logical sx, sy
      external lgndr

      ! Record the original quadrant
      sx = x .lt. 0.0d0; sy = y .lt. 0.0d0

      ! Always work in first quadrant
      if(sx) x = -x
      if(sy) y = -y

      ang = atan2(y,x)
	  x = cos(piontwo - ang)

      y = 0.0d0

      do n = 1, lp
        call lgndr(2*n-2,x,pn,dpndx)
        y = y + param(n) * pn
      end do

      x = y * cos(ang)
      y = y * sin(ang)

      ! Restore original quadrant
      if(sx) x = -x
      if(sy) y = -y

      end subroutine legfit


      subroutine meanH(xm,ym,H)

      use CONTROL, only : lp, param, piontwo

      implicit none

      ! input    x:= cos(theta)
      !          lp:= number of Legendre polynomials
      !          param:= coefficients (use only even )
      ! output   H:= Mean_curvature at x

      double precision, intent(in)  :: xm, ym
      double precision, intent(out) :: H

      ! Local variables
      integer n, nn
      double precision x,y,r,dr,drdx,ddr, pn,dpn,dpndx,ddpn, num,denum,dummy,cotan_dr
      external lgndr

      x = atan2(ym,xm)
	  x = cos(piontwo - x)

      y = ( 1.0d0 - x ) * ( 1.0d0 + x )  ! sinus(theta)
      r = 0.0d0; dr = 0.0d0; drdx = 0.0d0; ddr = 0.0d0

      do n = 1, lp
        nn = 2 * n - 2
        ! return pn(x)  and d_pn/d_x
        call lgndr(nn,x,pn,dpndx)
        ! ddpn is second derivative of pn with respect to theta ...not cos(theta)
        ddpn = x * dpndx - dble(nn*(nn+1)) * pn
        ! dpn  is first derivative of pn with respect to theta
        dpn  = -sqrt(y) * dpndx
        ! radius
        r    = r    + param(n) * pn
        ! d(radius)/d(theta)
        dr   = dr   + param(n) * dpn
        ! d(radius)/d(x)
        drdx = drdx + param(n) * dpndx
        ! d2(radius)/d(theta)2
        ddr  = ddr  + param(n) * ddpn
      end do

      ! cotan * dr  = -cos * drdx
      cotan_dr = -x * drdx
      num   = r*r*r + 1.5d0 * r * dr * dr - 0.5d0 * cotan_dr * dr * dr -0.5d0 * r * r * ( cotan_dr + ddr )
      dummy = sqrt(r*r+dr*dr)
      denum = r*(r*r+dr*dr)*dummy
      !----------  Calculate H --------------
      H = num / denum

      end subroutine meanH


      

      subroutine fitsmooth(x,y)

      ! Nudges (x,y) coordinates onto the fit curve

      use CONTROL, only : zero, fitn, fita, fitb, fitan, fitbn, fitanbn

      implicit none

      double precision, intent(inout) :: x, y
      logical :: sx, sy
      double precision rn, zn

      ! Record the original quadrant
      sx = x .lt. 0.0d0; sy = y .lt. 0.0d0

      ! Always work in first quadrant
      if(sx) x = -x
      if(sy) y = -y

      ! Keep away from singularities
      if((x.lt.zero).and.(y.lt.zero)) then
        x = 0.0d0; y = 0.0d0
      else if(y.lt.zero) then
        x = fitb
      else if(x.lt.zero) then
        y = fita
      else if(y.lt.x) then
        rn = y**dble(fitn) / x**dble(fitn)
        rn = fitanbn / ( fitan + fitbn * rn )
        zn = fitan * ( 1.0d0 - rn / fitbn )
        x = rn ** ( 1.0d0 / dble(fitn) )
        y = zn ** ( 1.0d0 / dble(fitn) )
      else
        zn = x**dble(fitn) / y**dble(fitn)
        zn = fitanbn / ( fitbn + fitan * zn )
        rn = fitbn * ( 1.0d0 - zn / fitan )
        x = rn ** ( 1.0d0 / dble(fitn) )
        y = zn ** ( 1.0d0 / dble(fitn) )
      end if

      ! Restore original quadrant
      if(sx) x = -x
      if(sy) y = -y

      end subroutine fitsmooth



      subroutine fit(ma,Nt,rad,xt,a,chisq)

      use CONTROL, only : lp, legvals

      implicit none

      integer, intent(in) :: Nt, ma
      double precision, intent(in) :: rad(Nt), xt(Nt)
      double precision, intent(out) :: a(lp), chisq

      integer, allocatable :: ia(:)
      DOUBLE PRECISION, allocatable :: sig(:), covar(:,:), alpha(:,:)
      integer i, ios1
      double precision gammln, ecc, alamda,oldchisq,vol
      logical legstart

      a(1) = rad(1); a(2) = rad(Nt/2)

      ! Avoid fitting to a lemon if a / b < 2
      if((ma.eq.2).and.(a(1).lt.2.0*a(2))) then
        chisq = 1000.0d0
        return
      end if

      allocate(alpha(lp,lp),covar(lp,lp),ia(lp),sig(Nt),stat=ios1)
      if(ios1.ne.0) then
        print*,'ERROR in fit_operator: Insufficient memory for fitting!!!!!'
        stop
      endif

      sig = 1.0; ia = 1; a(3) = 1.6d0*dble(ma-2); alamda=-1.0d0; chisq=1.0d0; oldchisq=0.0d0

      ! Special starting values for Legendre poly fit
      if(ma.eq.lp) then
        ! inquire(file="legendre.coef",exist=legstart)
        legstart = legvals(lp) .gt. -9.0d0
        if(legstart) then
          ! open(45,file="legendre.coef")
          do i = 1, lp
            ! read(45,'(f18.9)') a(i)
            a(i) = legvals(i)
          end do
          ! close(45)
        else
          a = 0.0d0; a(1) = 1.0d0
        end if
      end if

      do i = 1, 220
!        vol = 1.5*a(1)*a(2)*a(2)*exp(gammln(1+1.0/a(3))+gammln(1+2.0/a(3))-gammln(1+3.0/a(3)))
!        ecc = sqrt(abs(1.0-a(2)*a(2)/a(1)/a(1)))
!        write(*,'("a=",f6.3," b=",f6.3," n=",f6.3," a/b=",f6.3," ecc=",f6.3," vol=",f6.3," chisq=",f6.3," alamda=",f6.3)'), &
!                         a(1),      a(2),      a(3),   a(1)/a(2),         ecc,         vol,         chisq,         alamda
        if(abs(oldchisq-chisq)<1.0d-13) exit
        oldchisq = chisq;
        call mrqmin(xt,rad,sig,Nt,a,ia,ma,covar,alpha,ma,chisq,alamda)
      end do

      ! Write-out special starting values for Legendre poly fit
      if(ma.eq.lp) then
        ! open(45,file="legendre.coef")
        do i = 1, lp
          ! write(45,'(f18.9)') a(i)
          legvals(i) = a(i)
        end do
        ! close(45)
      end if

      deallocate(alpha,covar,sig,ia)

      end subroutine fit



      subroutine abnfit(maxnid,List,X,Y)

      use CONTROL, only : piontwo, fita, fitb, fitn, fitan, fitbn, fitanbn, prefix, fish, relaxing, cflrorig
      use CONTROL, only : lp, sphereish, fitaa, fitbb, param, legendre, omeg, monocharge, cflr
      use CONTROL, only : inflex, inflexion, ejectQ, perfect
      use SIZES, only : maxnod

      implicit none

      integer maxnid, List(maxnid), i, j, Nt, Ntt, ios1, ma
      real X(maxnod), Y(maxnod), a, b, n, xx, yy, ang, xold, yold, xm, ym
      double precision, allocatable :: rad(:), xt(:)
      double precision chisq(2:4), plgndr
      external plgndr

      ! If sphereish, just use actual max coordinate mesh input values to speed things up
      !if(sphereish) then
      !  fita = fitaa; fitb = fitbb; fitn = 2.0
      !  fitan = dble(fita)**dble(fitn); fitbn = dble(fitb)**dble(fitn); fitanbn = fitan * fitbn
      !  return
      !end if

      ! Number of fit points arising from maxnid axisym ones
      Nt = 2 * ( maxnid - 1 )

      ! Allocate space for fitting arrays
      allocate(rad(Nt),xt(Nt),stat=ios1)
      if(ios1.ne.0) then
        print*,'Insufficient Memory !!!!!'
        stop
      endif

      ! Open fit display output file if fishing
      ! if(fish) call gmshOpen(33,'fit ')

      Nt = 0
      ! Loop the boundary nodes from the top of the drop to the (fictitious) bottom ...
      do i = maxnid, - ( maxnid - 2 ), -1
        if(i.eq.maxnid) then
          xold = X(List(i)); yold = Y(List(i))
          cycle
        else if(i.lt.1) then
          xx = X(List(2-i)); yy = -Y(List(2-i))
        else
          xx = X(List(i)); yy = Y(List(i))
        end if
        ! The mid-element coordinates
        xm = 0.5 * ( xold + xx ); ym = 0.5 * ( yold + yy )
        ! Current x,y become old x,y
        xold = xx; yold = yy
        ! Plot the coordinates to be used for the fit
        ! if(fish) call gmshSP(33,xm,ym,0.0d0)
        ! Processing for the fitting
        Nt = Nt + 1
	    xt(Nt)  = atan2(ym,xm)
	    rad(Nt) = sqrt(xm*xm+ym*ym); xt(Nt) = cos(piontwo - xt(Nt))
      end do

      ! Fit the lemon first (ma=2) and then the ellipsoid (ma=3) and then the poly peices (ma>3)
      do ma = 2, lp

        if((ma.gt.3).and.(ma.ne.lp)) cycle

        call fit(ma,Nt,rad,xt,param,chisq(min(ma,4)))

        ! No lemons if a / b < 2
        if(chisq(ma).gt.100.0d0) cycle

        ! Fit parameters left are those for the (a,b,n) super-ellipsoid
        if(ma.eq.3) then
          fita = param(1); fitb = param(2); fitn = param(3)
        end if

        Ntt = 100; a = param(1); b = param(2); n = param(3)

        ! Plot the fit curves if fishing
        if(fish) then
          do i = 0, Ntt
            ang = real(Ntt-i-Ntt/2) * 3.141727 / real(Ntt)
            if(ma.eq.2) then
              ! Lemon
              xx = ( a - b ) * cos(ang)
              yy = sign ( sqrt( abs( a*a - ( xx + b ) ** 2 ) ) , ang )
            else if(ma.eq.lp) then
              ! Legendre polynomial summation
              xx = cos(piontwo - ang)
              yy = 0.0
              do j = 1, lp
                yy = yy + real(param(j) * plgndr(2*j-2,0,dble(xx)))
              end do
              xx = yy * cos(ang)
              yy = yy * sin(ang)
            else
              ! a, b, n super-ellipsoid
              xx = b * cos(ang)
              yy = 1.0d0 - abs( xx / b )**n
              yy = sign( a * abs(yy) ** ( 1.0d0 / n ) , ang )
            end if
            ! if(i.gt.1) call gmshSQ(33,xold,yold,xx,yy,chisq(ma))
            xold = xx; yold = yy
          end do
        end if
      end do

      ! Close fit display output file if fishing
      ! if(fish) call gmshClose(33)

      deallocate(rad,xt)

      ! Some useful combinations
      fitan = dble(fita)**dble(fitn); fitbn = dble(fitb)**dble(fitn); fitanbn = fitan * fitbn

      ! Indicate explosion if lemon fit better than ellipsoid fit
      if( ( ( (.not.inflex) .and. ((chisq(2).lt.chisq(3)).or.(fitn.lt.1.40).or.(fita.gt.3.50*fitb)) &
            ) .or. (inflex  .and. inflexion ) &
              .or. ( (.not.perfect) .and. (fita.gt.3.50*fitb) ) &
          ) .and.(.not.relaxing) ) then
        omeg = omeg * ejectQ
        relaxing = .true.
        monocharge = .false.
        cflr = cflr * 0.45
        write(*,999,advance='no')
  999   format (' >>>---o---<<< ')
      else if(cflr.lt.cflrorig) then
        if(fita.lt.2.50*fitb) then
          cflr = cflrorig
        end if
      end if

      ! Stop Legendering when Legendre fit worse than spheroid fit
      legendre = legendre .and. (chisq(4).lt.chisq(3))

      end subroutine abnfit


	      SUBROUTINE mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,nca,chisq,alamda)
	      INTEGER ma,nca,ndata,ia(ma),MMAX
	      DOUBLE PRECISION alamda,chisq,a(ma),alpha(nca,nca),covar(nca,nca),sig(ndata),x(ndata),y(ndata)
	      PARAMETER (MMAX=30)
	!     USES covsrt,gaussj,mrqcof
	      INTEGER j,k,l,mfit
	      DOUBLE PRECISION ochisq,atry(MMAX),beta(MMAX),da(MMAX)
	      SAVE ochisq,atry,beta,da,mfit
	      if(alamda.lt.0.)then
	        mfit=0
	        do 11 j=1,ma
	          if (ia(j).ne.0) mfit=mfit+1
	11      continue
	        alamda=0.001
	        call mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nca,chisq)
	        ochisq=chisq
	        do 12 j=1,ma
	          atry(j)=a(j)
	12      continue
	      endif
	      do 14 j=1,mfit
	        do 13 k=1,mfit
	          covar(j,k)=alpha(j,k)
	13      continue
	        covar(j,j)=alpha(j,j)*(1.+alamda)
	        da(j)=beta(j)
	14    continue
	      call gaussj(covar,mfit,nca,da,1,1)
	      if(alamda.eq.0.)then
	        call covsrt(covar,nca,ma,ia,mfit)
	        return
	      endif
	      j=0
	      do 15 l=1,ma
	        if(ia(l).ne.0) then
	          j=j+1
	          atry(l)=a(l)+da(j)
	        endif
	15    continue
	      call mrqcof(x,y,sig,ndata,atry,ia,ma,covar,da,nca,chisq)
	      if(chisq.lt.ochisq)then
	        alamda=0.1*alamda
	        ochisq=chisq
	        do 17 j=1,mfit
	          do 16 k=1,mfit
	            alpha(j,k)=covar(j,k)
	16        continue
	          beta(j)=da(j)
	17      continue
	        do 18 l=1,ma
	          a(l)=atry(l)
	18      continue
	      else
	        alamda=10.*alamda
	        chisq=ochisq
	      endif
	      return
	      END
	 




	      SUBROUTINE covsrt(covar,npc,ma,ia,mfit)
	      INTEGER ma,mfit,npc,ia(ma)
	      DOUBLE PRECISION covar(npc,npc)
	      INTEGER i,j,k
	      REAL swap
	      do 12 i=mfit+1,ma
	        do 11 j=1,i
	          covar(i,j)=0.
	          covar(j,i)=0.
	11      continue
	12    continue
	      k=mfit
	      do 15 j=ma,1,-1
	        if(ia(j).ne.0)then
	          do 13 i=1,ma
	            swap=covar(i,k)
	            covar(i,k)=covar(i,j)
	            covar(i,j)=swap
	13        continue
	          do 14 i=1,ma
	            swap=covar(k,i)
	            covar(k,i)=covar(j,i)
	            covar(j,i)=swap
	14        continue
	          k=k-1
	        endif
	15    continue
	      return
	      END






	      SUBROUTINE gaussj(a,n,np,b,m,mp)
	      INTEGER m,mp,n,np,NMAX
	      DOUBLE PRECISION a(np,np),b(np,mp)
	      PARAMETER (NMAX=50)
	      INTEGER i,icol,irow,j,k,l,ll,indxc(NMAX),indxr(NMAX),ipiv(NMAX)
	      DOUBLE PRECISION big,dum,pivinv
	      do 11 j=1,n
	        ipiv(j)=0
	11    continue
	      do 22 i=1,n
	        big=0.
	        do 13 j=1,n
	          if(ipiv(j).ne.1)then
	            do 12 k=1,n
	              if (ipiv(k).eq.0) then
	                if (abs(a(j,k)).ge.big)then
	                  big=abs(a(j,k))
	                  irow=j
	                  icol=k
	                endif
	              else if (ipiv(k).gt.1) then
	                !print*,'singular matrix in gaussj'
                    stop
	              endif
	12          continue
	          endif
	13      continue
	        ipiv(icol)=ipiv(icol)+1
	        if (irow.ne.icol) then
	          do 14 l=1,n
	            dum=a(irow,l)
	            a(irow,l)=a(icol,l)
	            a(icol,l)=dum
	14        continue
	          do 15 l=1,m
	            dum=b(irow,l)
	            b(irow,l)=b(icol,l)
	            b(icol,l)=dum
	15        continue
	        endif
	        indxr(i)=irow
	        indxc(i)=icol
	        if (a(icol,icol).eq.0.) then
              !print*,'singular matrix in gaussj'
              stop
            end if
	        pivinv=1./a(icol,icol)
	        a(icol,icol)=1.
	        do 16 l=1,n
	          a(icol,l)=a(icol,l)*pivinv
	16      continue
	        do 17 l=1,m
	          b(icol,l)=b(icol,l)*pivinv
	17      continue
	        do 21 ll=1,n
	          if(ll.ne.icol)then
	            dum=a(ll,icol)
	            a(ll,icol)=0.
	            do 18 l=1,n
	              a(ll,l)=a(ll,l)-a(icol,l)*dum
	18          continue
	            do 19 l=1,m
	              b(ll,l)=b(ll,l)-b(icol,l)*dum
	19          continue
	          endif
	21      continue
	22    continue
	      do 24 l=n,1,-1
	        if(indxr(l).ne.indxc(l))then
	          do 23 k=1,n
	            dum=a(k,indxr(l))
	            a(k,indxr(l))=a(k,indxc(l))
	            a(k,indxc(l))=dum
	23        continue
	        endif
	24    continue
	      return
	      END
	 




	      SUBROUTINE mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nalp,chisq)
	      INTEGER ma,nalp,ndata,ia(ma),MMAX
	      DOUBLE PRECISION chisq,a(ma),alpha(nalp,nalp),beta(ma),sig(ndata),x(ndata),y(ndata)
	      PARAMETER (MMAX=30)
	      INTEGER mfit,i,j,k,l,m
	      DOUBLE PRECISION dy,sig2i,wt,ymod,dyda(MMAX)
	      mfit=0
	      do 11 j=1,ma
	        if (ia(j).ne.0) mfit=mfit+1
	11    continue
	      do 13 j=1,mfit
	        do 12 k=1,j
	          alpha(j,k)=0.
	12      continue
	        beta(j)=0.
	13    continue
	      chisq=0.
	      do 16 i=1,ndata
            if(ma.eq.2) then
              call superlemon(x(i),a,ymod,dyda)
            else if(ma.eq.3) then
              call superellips(x(i),a,ymod,dyda)
            else
              call superlegendre(x(i),a,ymod,dyda)
            end if
	        sig2i=1./(sig(i)*sig(i))
	        dy=y(i)-ymod
	        j=0
	        do 15 l=1,ma
	          if(ia(l).ne.0) then
	            j=j+1
	            wt=dyda(l)*sig2i
	            k=0
	            do 14 m=1,l
	              if(ia(m).ne.0) then
	                k=k+1
	                alpha(j,k)=alpha(j,k)+wt*dyda(m)
	              endif
	14          continue
	            beta(j)=beta(j)+dy*wt
	          endif
	15      continue
	        chisq=chisq+dy*dy*sig2i
	16    continue
	      do 18 j=2,mfit
	        do 17 k=1,j-1
	          alpha(k,j)=alpha(j,k)
	17      continue
	18    continue
	      return
	      END



      subroutine superlegendre(x,p,y,dydp)

      use CONTROL, only : lp

      double precision x,p(lp),y,dydp(lp),plgndr,dpn
      y = 0.0d0
      do i = 1, lp
        call lgndr(2*i-2,x,dydp(i),dpn)
!        dydp(i) =  plgndr(2*i-2,0,x)
        y = y + p(i) * dydp(i)
      end do

      end subroutine superlegendre



      subroutine superlemon(x,p,y,dydp)

      double precision x,p(3),y,dydp(3),A,B,c
      A=p(1); B=p(2)
      c=sqrt(B*B*(x*x-1.0d0)*(B*B*x*x-A*A))
      y=sqrt(A*A+B*B*(1.0d0-2.0d0*x*x)-2.0d0*c)
      dydp(1)=A*(1.0d0+B*B*(x*x-1.0d0)/c)/y
      dydp(2)=B*(1.0d0-2.0d0*x*x+(x*x-1.0d0)*(A*A-2.0d0*B*B*x*x)/c)/y

      end subroutine superlemon



	      FUNCTION gammln(xx)
	      DOUBLE PRECISION gammln,xx
	      INTEGER j
	      DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
	      SAVE cof,stp
	      DATA cof,stp/76.18009172947146d0,-86.50532032941677d0,&
	      24.01409824083091d0,-1.231739572450155d0,0.1208650973866179d-2,&
	      -0.5395239384953d-5,2.5066282746310005d0/
	      x=xx
	      y=x
	      tmp=x+5.5d0
	      tmp=(x+0.5d0)*log(tmp)-tmp
	      ser=1.000000000190015d0
	      do 11 j=1,6
	        y=y+1.d0
	        ser=ser+cof(j)/y
	11    continue
	      gammln=tmp+log(stp*ser/x)
	      return
	      END


      subroutine superellips(x,p,y,dydp)

      double precision x,p(3),y,dydp(3),a,b,n,f,c1,c2,dfdn
      a=p(1); b=p(2); n=p(3)
      c1=x*x/(a*a); c2=(1.0-x*x)/(b*b)
      f = c1**(0.5*n)+c2**(0.5*n)
      y = f**(-1/n)

      dydp(1)=(c1**(0.5*n)*f**(-(1+n)/n))/a
      dydp(2)=(c2**(0.5*n)*f**(-(1+n)/n))/b
      dfdn   = 0.5*c1**(0.5*n)*log(c1)+0.5*c2**(0.5*n)*log(c2)
      dydp(3)=f**(-1/n)*(log(f)/(n*n) -dfdn/(n*f))

      end subroutine superellips


// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <dune/common/version.hh>
#if DUNE_VERSION_NEWER(DUNE_COMMON,2,3)
#include <dune/common/parallel/mpihelper.hh>
#else
#include <dune/common/mpihelper.hh>
#endif
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/sgrid.hh>
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#include <dune/grid/geometrygrid.hh>

#include <dune/grid/io/file/gmshreader.hh>

#include <dune/grid-glue/extractors/extractorpredicate.hh>
#include <dune/grid-glue/extractors/codim0extractor.hh>
#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/merging/mixeddimoverlappingmerge.hh>

#include <dune/grid-glue/test/couplingtest.hh>

using namespace Dune;
using namespace Dune::GridGlue;

/** \brief Returns always true */
template <class GridView>
class AllElementsDescriptor
  : public ExtractorPredicate<GridView,0>
{
public:
  virtual bool contains(const typename GridView::Traits::template Codim<0>::EntityPointer& element, unsigned int subentity) const
  {
    return true;
  }
};


int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  MixedDimOverlappingMerge<3,2,3> mixedDimOverlappingMerge;

  // /////////////////////////////////////////////////////////
  //   Make a 3d unit cube grid and a 2d grid embedded in 3d
  // /////////////////////////////////////////////////////////

  typedef Dune::ALUGrid<3,3,Dune::simplex,Dune::nonconforming> GridType3d;

  const std::string gridfile = "hole3.dgf";

  std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< GridType3d > gridPtr( gridfile );
  GridType3d& grid0 = *gridPtr ;

  // do initial load balance
  grid0.loadBalance();

  // refine grid
  // Dune::Fem::GlobalRefine::apply( grid0, 1 );

  typedef Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> GridType2d;

  const std::string grydfile = "sphere-h-0.4.msh";

  std::cout << "Loading macro grid: " << grydfile << std::endl;

  std::unique_ptr<GridType2d> grydPtr( Dune::GmshReader<GridType2d>::read( grydfile, true, false ) );

  GridType2d& grid1 = *grydPtr;

  // ////////////////////////////////////////
  //   Set up an overlapping coupling
  // ////////////////////////////////////////

  typedef typename GridType3d::LeafGridView DomGridView;
  typedef typename GridType2d::LeafGridView TarGridView;

  typedef Codim0Extractor<DomGridView> DomExtractor;
  typedef Codim0Extractor<TarGridView> TarExtractor;

  AllElementsDescriptor<DomGridView> domdesc;
  AllElementsDescriptor<TarGridView> tardesc;

#if DUNE_VERSION_NEWER(DUNE_GRID,2,3)
  DomExtractor domEx(grid0.leafGridView(), domdesc);
  TarExtractor tarEx(grid1.leafGridView(), tardesc);
#else
  DomExtractor domEx(grid0.leafView(), domdesc);
  TarExtractor tarEx(grid1.leafView(), tardesc);
#endif
  typedef Dune::GridGlue::GridGlue<DomExtractor,TarExtractor> GlueType;

  // The following code is out-commented, because the test functionality
  // doesn't actually work yet.
  MixedDimOverlappingMerge<3,2,3> merger;
  GlueType glue(domEx, tarEx, &merger);

  glue.build();

  std::cout << "Gluing successful, " << glue.size() << " remote intersections found!" << std::endl;

  // ///////////////////////////////////////////
  //   Test the coupling
  // ///////////////////////////////////////////

  testCoupling(glue);
}

#ifndef MCF_FEMSCHEME_HH
#define MCF_FEMSCHEME_HH

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>

// local includes
#include "../../common/source/femscheme.hh"

// MCFScheme
//-----------

template < class ImplicitModel, class ExplicitModel >
struct MCFScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef ExplicitModel ExplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  MCFScheme( GridPartType &gridPart,
              const ImplicitModelType& implicitModel,
              const ExplicitModelType& explicitModel )
  : BaseType(gridPart, implicitModel),
    explicitModel_(explicitModel),
    explicitOperator_( explicitModel_, discreteSpace_ )
  {
  }

  DiscreteFunctionType &forcing()
  {
    return polution_;
  }
  const DiscreteFunctionType &forcing() const
  {
    return polution_;
  }

  void prepare()
  {
    // apply constraints, e.g. Dirichlet contraints, to the solution
    explicitOperator_.prepare( explicitModel_.dirichletBoundary(), solution_ );
    // apply explicit operator and also setup right hand side
    explicitOperator_( solution_, rhs_ );
    // apply constraints, e.g. Dirichlet contraints, to the result
    explicitOperator_.prepare( solution_, rhs_ );
  }

  void initialize ()
  {
     Dune::Fem::LagrangeInterpolation
          < typename ExplicitModelType::InitialFunctionType, DiscreteFunctionType > interpolation;
     interpolation( explicitModel_.initialFunction(), solution_ );
  }

  //! solve the system
  void solve ( bool assemble )
  {
    // make a record of the original positions to be used to see the differences later
    polution_.assign( solution_ );
    polution_ *= -1.0;
    // solve as per the base type
    BaseType::solve( assemble );
  }

  //! prepare the forcing from the MCF solution
  void prepforce ()
  {
    // the forcing is proportional to the distance moved
    // if "H" is the mean curvature, then the velocity for mean curvature flow
    // is = 2 H, and thus the distance moved in one timestep "k" is = 2 H k
    polution_ += solution_;

    // apply any scaling that might be necessary
    // divide by 2 to remove the 2 from the 2 H above
    polution_ *= 1.0 / 2.0;
  }

private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::solution_;
  using BaseType::polution_;
  using BaseType::implicitModel_;
  using BaseType::rhs_;
  const ExplicitModelType &explicitModel_;
  typename BaseType::EllipticOperatorType explicitOperator_; // the operator for the rhs
};

#endif // end #if MCF_FEMSCHEME_HH

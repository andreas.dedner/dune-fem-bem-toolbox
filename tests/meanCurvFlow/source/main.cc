#include <config.h>

// iostream includes
#include <iostream>

// include GeometryGrid
#include <dune/grid/geometrygrid.hh>

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include grid width
#include <dune/fem/misc/gridwidth.hh>

// include discrete function space
#include <dune/fem/space/lagrange.hh>

// include discrete function
#include <dune/fem/function/adaptivefunction.hh>

// include solvers
#include <dune/fem/solver/cginverseoperator.hh>
#include <dune/fem/solver/oemsolver.hh>

// include Lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>

// include norms
#include <dune/fem/misc/l2norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

// include eoc calc
#include <dune/fem/misc/femeoc.hh>
// include discrete function
#include <dune/fem/function/blockvectordiscretefunction/blockvectordiscretefunction.hh>

// include linear operators
//#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/operator/matrix/blockmatrix.hh>
// #include <dune/fem/operator/2order/dgmatrixsetup.hh>
// #include <dune/fem/operator/2order/lagrangematrixsetup.hh>

#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>

// local includes
#include "../../common/source/elliptic.hh"
#include "deformation.hh"
#include "mcf.hh"
#include "mcfmodel.hh"
#include "mcfscheme.hh"
#include "curvature.hh"
#include "../../common/source/rhs.hh"

// algorithm
// ---------

template <class HGridType>
void algorithm ( HGridType &grid, const double timeStep, int step, const int eocId )
{
  // choose type of discrete function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::OutsideDeformationType  DeformationType;

  //DeformationCoordFunction deformation;
  DeformationType deformation( grid );

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GrydType GrydType;
  GrydType geoGrid( grid, deformation );

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GrydPartType GrydPartType;
  // typedef Dune::Fem::LeafGridPart< GrydType > GridPartType;

  GrydPartType grydPart( geoGrid );

  // setup time provider
  const double endTime = Dune::Fem::Parameter::getValue< double >( "mcf.endtime", 2.0 );
  const double dtreducefactor = Dune::Fem::Parameter::getValue< double >("mcf.reducetimestepfactor", 1 );
  double timeStup = Dune::Fem::Parameter::getValue< double >( "mcf.timestep", 0.125 );
  double timeJump = Dune::Fem::Parameter::getValue< double >( "mcf.timejump", 0.125 );

  // timeStup *= pow(dtreducefactor,step);

  Dune::Fem::GridTimeProvider< GrydType > timeProvider( geoGrid );

  // initialize with fixed time step
  timeProvider.init( timeStup ) ;

  // type of the mathematical model used
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceOutsideType FunctionSpaceOutsideType;
  typedef TimeDependentCosinusProduct< FunctionSpaceOutsideType > ProblemType;

  ProblemType problem( timeProvider ) ;

  typedef MCFModel< FunctionSpaceOutsideType, GrydPartType > ModelType;

  // implicit model for left hand side
  ModelType implicitModel( problem, grydPart, true );

  // explicit model for right hand side
  ModelType explicitModel( problem, grydPart, false );

  // create mcf scheme
  typedef MCFScheme< ModelType, ModelType > SchemeType;
  SchemeType scheme( grydPart, implicitModel, explicitModel );

  // local function adapter just to get size of forcing movements
  typedef typename SchemeType::DiscreteFunctionType MCFDFT;
  typedef Curvature< MCFDFT > CurvatureType;
  typedef Dune::Fem::LocalFunctionAdapter< CurvatureType > CurvFunctionType;
  CurvatureType curvature( scheme.forcing(), timeStup );
  CurvFunctionType curvFunction( "curvature", curvature, grydPart );

  // initial data for surface
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::DiscreteFunctionSpaceOutsideType DiscreteHostFunctionSpaceType;
  typedef Problem::SurfaceEvolution< DiscreteHostFunctionSpaceType > SurfaceEvolutionType;
  typedef DeformationCoordFunction< SurfaceEvolutionType > DeformationCoordFunctionType;

  // all the discrete vertex stuff
  // typedef typename Dune::FemFemCoupling::Glue<HGridType>::HostGridPartType HostGridPartType;
  // HostGridPartType hGridPart( grid );

  // find initial condition
  SurfaceEvolutionType initialCondition;

  // interpolate initial condition
  DeformationCoordFunctionType deform( initialCondition );
  deformation.initialize( deform, scheme.solution() );

  // setup data output
  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GrydPartType > GridExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, grydPart, 5 );
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *, GridExactSolutionType * > IOTupleType;
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution );
  typedef Dune::Fem::DataOutput< GrydType, IOTupleType > DataOutputType;
  DataOutputType dataOutput( geoGrid, ioTuple, DataOutputParameters( step ) );

  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *, CurvFunctionType * > ForcingOutsideIOTupleType;
  ForcingOutsideIOTupleType forcingIoTuple( &(scheme.forcing()), &curvFunction );
  typedef Dune::Fem::DataOutput< GrydType, ForcingOutsideIOTupleType > ForcingOutsideDataOutputType;
  ForcingOutsideDataOutputType forcingDataOutput( geoGrid, forcingIoTuple, DataOutputParameters( step, "forcing" ) );


  // find grid width at start time
  // const double h = Dune::Fem::GridWidth::calcGridWidth( hGridPart );

  double minR,maxR;

  // time loop, increment with fixed time step
  for( ; timeProvider.time() < endTime; timeProvider.next( timeStup ) )
  {
    deformation.setTime( timeProvider.time(), scheme.solution() );

    scheme.prepare();

    // solve once (we need to assemble the system the first time the method is called)
    scheme.solve( true );

    // use the MCF solution to prepare the surface forcing
    scheme.prepforce();

    // write data
    // dataOutput.write( timeProvider );
    forcingDataOutput.write( timeProvider );

  // output error information
  const double radius = deformation.radius(minR,maxR);
  const double time = timeProvider.time();
  const double radias = std::sqrt( 1.0 - 4.0 * time );
  const double error = std::abs( radius - radias );
  std::cout << "error: " << radius << "  " << radias << "  " << time << "  " << error << " TS = " << timeStup << std::endl;

  timeStup += timeJump;
  deformation.initialize( deform, scheme.solution() );
  curvature.resetStep( timeStup );
  }

  // write data at end time
  deformation.setTime( timeProvider.time(), scheme.solution() );
  dataOutput.write( timeProvider );

  // write eoc data to cout
  // Dune::Fem::FemEoc::setErrors( eocId, error );
  // Dune::FemFemEoc::write( h, dfSpace.size(), 0, timeProvider.timeStep(), std::cout );

  return;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // initialise eoc file
  // Dune::FemEoc::initialize( "../output/mcf-eoc", "eoc", "mean curvature flow" );

  // add headers
  std::vector< std::string > femEocHeaders;
  femEocHeaders.push_back("radius error");

  // get eoc id
  // const int eocId = Dune::FemEoc::addEntry( femEocHeaders );
  const int eocId = 0;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;
  Dune::GridPtr< HGridType > grid( gridfile );
  grid->loadBalance();

  // initial grid refinement
  const float level = Dune::Fem::Parameter::getValue< float >( "mcf.level" );
  grid->globalRefine( std::ceil( level*Dune::DGFGridInfo< HGridType >::refineStepsForHalf() ) );

  double timeStep = Dune::Fem::Parameter::getValue< double >( "mcf.timestep", 0.125 );
  algorithm( *grid, timeStep, 0, eocId );

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "mcf.repeats", 0 );
  for( int step = 1; step <= repeats; ++step )
    {
      timeStep /= 4.0;
      grid->globalRefine( Dune::DGFGridInfo< HGridType >::refineStepsForHalf() );

      algorithm( *grid, timeStep, step, eocId );
    }
  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

#include <config.h>

// iostream includes
#include <iostream>
#include <complex>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "traits.hh"

#include <dune/fem/gridpart/leafgridpart.hh>

#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_simple.hh>

// include header of adaptive scheme
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>

#include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>
#include <dune/fem_bem_toolbox/bem_objects/sharedbemscheme.hh>
// #include "../../common/source/erfscheme.hh"
#include <dune/fem_bem_toolbox/coupling_schemes/gmresscheme.hh>

#include <dune/grid/io/file/gmshreader.hh>

#include "fembem.hh"
#include "bemmodel.hh"

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( 
    typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydType& gryd, 
    int step ) 
{
  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydType GrydType;
  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydPartType GrydPartType;
  GrydPartType grydPart(gryd);

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType, false>::FunctionSpaceOutsideType ExtractedSurfaceFunctionSpaceType;

  // type of the mathematical model used
  typedef LaplaceModel< ExtractedSurfaceFunctionSpaceType, GrydPartType > ExtractedSurfaceModelType;

  ExtractedSurfaceModelType implycitModel;

  // set up a timer for the looping
  time_t before;
  time(&before);

  typedef BemScheme< ExtractedSurfaceModelType > ExtractedSurfaceSchemeType;
  ExtractedSurfaceSchemeType schyme( grydPart, implycitModel, "surface" );

  // set up a timer for the looping
  time_t middle;
  time(&middle);

  schyme.prepare();
  schyme.solve();

  // determine how long it took to do the loop
  time_t after;
  time(&after);

  double seconds = difftime(after,before);
  double assembled = difftime(middle,before);

  std::cout << "Solved after a time " << seconds << " secs." << std::endl;
  std::cout << "Assembled after a time " << assembled << " secs." << std::endl;

  const int externalLevel = Dune::Fem::Parameter::getValue< int >( "external_grid.level" );

  if(externalLevel<-5) return 0.;

  auto dirichlet = implycitModel.dirichletData();
  typedef Dune::Fem::GridFunctionAdapter< decltype(dirichlet) , GrydPartType > GrydDirichletSolutionType;
  GrydDirichletSolutionType grydDirichletSolution("exact dirichlet", dirichlet, grydPart, 5 );
  auto neumann = implycitModel.exactNeumann();
  typedef Dune::Fem::GridFunctionAdapter< decltype(neumann) , GrydPartType > GrydNeumannSolutionType;
  GrydNeumannSolutionType grydNeumannSolution("exact neumann", neumann, grydPart, 5 );
  typedef Dune::tuple< const typename ExtractedSurfaceSchemeType::DiscreteFunctionType *,
                       const typename ExtractedSurfaceSchemeType::DiscreteFunctionType *,
                       GrydDirichletSolutionType *, GrydNeumannSolutionType * > SurfaceIOTupleType;
  typedef Dune::Fem::DataOutput< GrydType, SurfaceIOTupleType > SurfaceDataOutputType;
  SurfaceIOTupleType surfaceioTuple( &(schyme.solution()),
                                     &(schyme.rhs()),
                                     &grydDirichletSolution,
                                     &grydNeumannSolution) ; 
  SurfaceDataOutputType surfacedataOutput( gryd, surfaceioTuple, DataOutputParameters( step, "surface" ) );
  surfacedataOutput.write();

  // output bem solution on an extended grid
  const std::string externalName = Dune::Fem::Parameter::getValue< std::string >( "external_grid" );
  schyme.outputExternal( externalName, externalLevel, true );
  return 0.;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  typedef Dune::GridSelector::GridType  HGridType ;
  // create dune grid file with surface mesh
  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydType GrydType;
  // create dune grid file with surface mesh

  const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", GrydType::dimension );
  const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );

  Dune::GridFactory<GrydType> factory;
  if( Dune::Fem::MPIManager::rank() == 0 )
    Dune::GmshReader<GrydType>::read(factory, grydfile, false,false );
  GrydType *grydPtr = factory.createGrid();
  GrydType& gryd = *grydPtr ;

  algorithm<HGridType>( gryd, 0 );
  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

template <class FS, class GP>
struct LaplaceModel
{
  typedef LaplaceModel<FS,GP> ModelType;
  typedef GP GridPartType;
  typedef FS FunctionSpaceType;
  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;

  LaplaceModel()
  : incidentData_(*this)
  , dirichletData_(*this)
  , exactNeumann_(*this)
  {
  }
  bool hasDirichletBoundary() const
  {
    return true;
  }
  bool hasRobinBoundary () const
  {
    return false;
  }
  struct IncidentData
  {
    const ModelType &model_;
    IncidentData(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      res = RangeType(0);
    }
  };
  struct DirichletData
  {
    const ModelType &model_;
    DirichletData(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& point, RangeType& res ) const
    {
      double x = point[0], y = point[1], z = point[2];
      double r = sqrt(x*x + y*y + z*z);
      res[0] = 2 * x * z / (r * r * r * r * r) - y / (r * r * r);
    }
  };
  struct ExactNeumann
  {
    const ModelType &model_;
    ExactNeumann(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& point, RangeType& res ) const
    {
      double x = point[0], y = point[1], z = point[2];
      double r = sqrt(x*x + y*y + z*z);
      res[0] = -6 * x * z / (r * r * r * r * r * r) + 2 * y / (r * r * r * r);
    }
  };
  const IncidentData& incidentData() const
  {
    return incidentData_;
  }
  const DirichletData& dirichletData() const
  {
    return dirichletData_;
  }
  const ExactNeumann& exactNeumann() const
  {
    return exactNeumann_;
  }
  const bool doDoubleLayer() const
  {
    return true;
  };
  const double idOpCoeff() const
  {
    return -0.5;
  };
private:
  const IncidentData incidentData_;
  const DirichletData dirichletData_;
  const ExactNeumann exactNeumann_;
};


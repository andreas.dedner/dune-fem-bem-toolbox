#ifndef ALE_FEMSCHEME_HH
#define ALE_FEMSCHEME_HH

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
#include <string>
#include <boost/lexical_cast.hpp>

using boost::lexical_cast;
using std::string;

// local includes
#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>

// ALEScheme
//-----------

template < class ImplicitModel >
struct ALEScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  ALEScheme( GridPartType &gridPart,
              const ImplicitModelType& implicitModel )
  : BaseType(gridPart, implicitModel)
  , meshVelocity_( "mesh velocity", discreteSpace_ )
  , tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01 ) )
  {
  }

  DiscreteFunctionType &meshVelocity()
  {
    return meshVelocity_;
  }
  const DiscreteFunctionType &meshVelocity() const
  {
    return meshVelocity_;
  }

  template <class DF , class IOFT>
  void oi(unsigned int num, DF &solution,  IOFT &iof)
  {
    unsigned int span = DF::RangeType::dimension;
    typename DF::DofIteratorType end = solution.dend(); unsigned int i = 0;
    for( typename DF::DofIteratorType git = solution.dbegin();
	 git != end; ++git, ++i )
    {
      iof << num << "  " << std::floor(float(i)/float(span))+1 << " w " << 1+i%span << " xxx " << lexical_cast<string>(*git) << std::endl;
    };
  }
  
  template <class DF , class IOFT>
  void io(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    
    typename DF::DofIteratorType end = solution.dend();
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git )
    {
      getline(iof,line); *git = std::stod(line.erase(0,line.rfind("xxx")+4),&sz);
    };
  }
  
  template <class DF , class IOFT>
  void ioo(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz; unsigned int count = 0;
    
    typename DF::DofIteratorType end = solution.dend();
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git )
    {
      double prev = *git;
      getline(iof,line); *git = std::stod(line.erase(0,line.rfind("xxx")+4),&sz);
      if( std::abs( *git - prev ) > tol_ ) std::cout << "Mismatch in volume node coordinate pick-up: " << prev << " != " << *git << " at count: " << ++count << std::endl;
    };
  }
  /*
  void bug( const char* label )
  {
    buug( solution_, label );
  }
  
  template <class DF>
  void buug( DF &solution, const char* label )
  {
    unsigned int count = 0, ref = Dune::Fem::Parameter::getValue< unsigned int >( "bug.vol" , 0 );
    typename DF::DofIteratorType end = solution.dend();
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git )
    {
      if( ++count == ref )
      {
        std::cout << label << " Value at " << count << " is: " << *git << std::endl;
      };
    }
  }
  */
  void dump(const int snap)
  {
    std::ifstream isnapfile; std::ofstream osnapfile;
    if( snap < -1 ) osnapfile.open("snip.ale"); else if( snap > 0 ) osnapfile.open("snap.ale"); else if( snap < 0 ) isnapfile.open("snap.ale");

    if( snap > 0 ) // writing data out
    {
      oi( 1, solution_, osnapfile );
      oi( 2, meshVelocity_, osnapfile );
    }
    else if( snap < 0 ) // reading data in
    {
      ioo( solution_, isnapfile );
      io( meshVelocity_, isnapfile );
    };
    
    if( snap > 0 ) osnapfile.close(); else if( snap < 0 ) isnapfile.close();
  }
  
  //! sotup the right hand side and physical boundaries
  void prepare()
  {
    rhs_.clear();
    implicitModel_.fixSig();
  }

  void solve( bool assemble ) 
  {
    meshVelocity_.assign(solution_);
    BaseType::solve(assemble);
    meshVelocity_ -= solution_;
  }

  void nodeDump()
  {
    std::ofstream osnapfile;
    osnapfile.open("snap.vol");
    oi( 0, solution_, osnapfile );
    osnapfile.close();
  }
  
private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::solution_;
  using BaseType::rhs_;
  DiscreteFunctionType meshVelocity_;
  const double tol_;
  using BaseType::implicitModel_;
};

#endif // end #if ALE_FEMSCHEME_HH

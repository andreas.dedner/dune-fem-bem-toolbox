#ifndef DIVERGENCE_HH
#define DIVERGENCE_HH


template < class DFT >
struct Divergence
{
  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;
  typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;
  typedef typename FunctionSpaceType::RangeType RangeType;

  // constructor
  Divergence( const DFT &f1 )
  : lf1_( f1 )
  {}

  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    typename FunctionSpaceType::JacobianRangeType grad;
    lf1_.jacobian( x, grad );
    ret[0] = grad[0][0];
    for( int i = 1; i < dimensionworld; ++i )
      ret[ 0 ] += grad[ i ][ i ];
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf1_.init( entity );
  }

  private:
    typename DFT::LocalFunctionType lf1_;
};


#endif // end #if DIVERGENCE_HH

#ifndef HOSTCOORDS_HH
#define HOSTCOORDS_HH

// combines the three component forcing vector with the MSP normal derivative to be used inside
template < class DFT , class outDFT >
struct HostCoords// : public AddChargeForcing<DFT,DFT>
{
  //typedef AddChargeForcing<DFT,outDFT> BaseType;
  //typedef typename BaseType::RangeType RangeType;

  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;

  typedef Dune::Fem::FunctionSpace<double,double,DFT::DiscreteFunctionSpaceType::dimDomain,
                              outDFT::DiscreteFunctionSpaceType::dimRange> FunctionSpaceType;
  // range type that is required
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;
  // typedef typename FunctionSpaceType::EntityType EntityType;

  // constructor
  HostCoords( const int &fixing , const DFT &f1, const double &spun )
    : lf1_( f1 )
    , entity_(0)
    , wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0   ) )
    , tul_( 0.000001 )
    , fixing_( fixing != 0 )
    , tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01   ) )
    , spun_( spun )
  {
    for( unsigned int i = 0; i < 3; ++i ) { maxi_[ i ] = -99999.99; mini_[ i ] = 99999.99; };
  }

  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    typename DFT::RangeType phi;
    ret = 999999.9;
    lf1_.evaluate( x, phi );
    auto geo = entity_->geometry();
    const auto heo = entity_->impl().hostEntity().geometry();
    auto xx = geo.global(Dune::Fem::coordinate(x));
    // determine contact node candidate flag   // node on/off wall:  node connected to an element 
    if( ( ( phi[ 2 ] != 0 ) || ( fixing_ && ( xx[ 0 ] <= wall_ + tol_ ) ) ) && ( ( ( !fixing_ ) && ( phi[ 1 ] != 0 ) ) || ( ( phi[ 1 ] < 0 ) && ( phi[ 1 ] > -999000.0 ) ) ) ) //                    with a node on/off wall:
    {
      // if( ( ( xx[ 0 ] <= wall_ ) && ( !fixing_ ) ) || ( ( xx[ 0 ] > wall_ ) && fixing_ ) )         
        ret[ 0 ] = 1.0;                        //        ON                      OFF
      if( fixing_ ) for( unsigned int i = 0; i < 3; ++i ) { if( xx[ i ] > maxi_[ i ] ) maxi_[ i ] = xx[ i ]; if( xx[ i ] < mini_[ i ] ) mini_[ i ] = xx[ i ]; };
    }
    // else
    //  ret[ 0 ] = 2.0;                        //        OFF                     ON 
    else if( ( phi[ 1 ] != 0 ) && ( phi[ 2 ] == 0 ) )
      if( ( xx[ 0 ] <= wall_ + tol_ ) && ( phi[ 1 ] > 0 ) )
        ret[ 0 ] = 3.0;                        //        ON                      (ON)
      else
        ret[ 0 ] = 4.0;
    else if( phi[ 2 ] != 0 )
        ret[ 0 ] = 2.0;                        //        OFF                     (OFF)
    else
        ret[ 0 ] = 99.0;    // should never happen, so big flag if it does ...

    // if( ret[ 0 ] < 4.0 )
    {
      for( int i = 0; i < geo.corners(); ++i )
      { // only note host vertex coordinates for nodes in or close to the substrate wall
        if( ( xx - geo.corner(i) ).two_norm() < tul_ )
        {
	  auto heoc = heo.corner(i);
          ret[ 1 ] = heoc[ 1 ];
          ret[ 2 ] = std::cos(spun_) * heoc[ 2 ] - std::sin(spun_) * heoc[ 0 ];
	  ret[ 3 ] = std::cos(spun_) * heoc[ 0 ] + std::sin(spun_) * heoc[ 2 ];
        };
      };
    };
  }

  template< class Point >
  void jacobian ( const Point &x, JacobianRangeType &ret ) const
  {
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    typename DFT::JacobianRangeType phi;
    lf1_.jacobian( x, phi );
    int i = 0;
    for( ; i < dimensionworld; ++i )
      ret[ i ] = phi[ i ];
    for( ; i < dimRange; ++i )
      ret[ i ] = 0;
  }
  
  int fixval()
  {
    int retval = 0;
    if( fixing_ )
    {
      for( unsigned int i = 0; i < 3; ++i ) maxi_[ i ] = std::abs( maxi_[ i ] - mini_[ i ] );
      if( ( 2.0 * maxi_[ 0 ] < maxi_[ 1 ] ) && ( 2.0 * maxi_[ 0 ] < maxi_[ 2 ] ) ) retval = 1;
      else if( ( maxi_[ 1 ] < 2.0 * maxi_[ 0 ] ) && ( maxi_[ 1 ] < maxi_[ 2 ] ) ) retval = 2;
      else retval = 3;
    };
    return retval;
  }
  
  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf1_.init( entity );
    entity_ = &entity;
  }

  private:
    typename DFT::LocalFunctionType lf1_;
    const EntityType* entity_;
    double wall_;
    const double tul_;
    const bool fixing_;
    const double tol_;
    const double spun_;
    mutable Dune::FieldVector<double,3> maxi_;
    mutable Dune::FieldVector<double,3> mini_;
};
#endif

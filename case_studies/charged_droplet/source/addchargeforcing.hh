#ifndef ADDCHARGEFORCING_HH
#define ADDCHARGEFORCING_HH


template < class DFT , class outDFT >
struct AddChargeForcing
{
  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;

  typedef Dune::Fem::FunctionSpace<double,double,DFT::DiscreteFunctionSpaceType::dimDomain,
                              outDFT::DiscreteFunctionSpaceType::dimRange> FunctionSpaceType;
  // range type that is required
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  // constructor
  AddChargeForcing( const double &correction , const outDFT &f0  , const DFT &f1 )
    : ts_( Dune::Fem::Parameter::getValue< double >( "droplet.timestep", 0.125 ) ),
      lf0_( f0 ),
      lf1_( f1 ),
      entity_(0),
      wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) ),
      nety_( 0.0 ),
      netz_( 0.0 ),
      neta_( 0.0 ),
      area_( 0.0 ),
      tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01 ) ),
      fact_( correction )
  {}

  void netForce( const double factor, Dune::FieldVector<double,4> &drift ) const
  {
    if( wall_ < -400.0 ) return;
    drift[ 0 ] = neta_/3.0; drift[ 1 ] = nety_/(3.0*factor); drift[ 2 ] = netz_/(3.0*factor);
    nety_ = 0.0; netz_ = 0.0; neta_ = 0.0;
  }
  
  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    typename DFT::RangeType phi;
    typename outDFT::RangeType psi;
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    bool offwall = false, onwall = false;
    ret = 999999;
    lf0_.evaluate( x, psi );
    double len = psi.two_norm();
    lf1_.evaluate( x, phi );
    double mag = ts_ * fact_ * fact_ * phi[ 0 ] * phi[ 0 ] / 2.0;
    // std::cout << "Check " << len << "  " << phi[ 0 ] << "  " << mag << std::endl;

    auto geo = entity_->geometry();
    // whip-up net surface tension forces tangential to any substrate
    for( int i = 0; i < geo.corners(); ++i )
    {
      if( geo.corner(i)[ 0 ] > wall_ + tol_ ) offwall = true; else onwall = true;
    };
    if( onwall && ( !offwall ) )
    {
      nety_ += psi[ 1 ] * area_; netz_ += psi[ 2 ] * area_; neta_ += area_;
    };
    
    for( int i = 0; i < dimRange; ++i )
      ret[ i ] = - mag * psi[ i ] / len;
  }
  template< class Point >
  void jacobian ( const Point &x, JacobianRangeType &ret ) const
  {
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    typename DFT::JacobianRangeType phi;
    lf1_.jacobian( x, phi );
    int i = 0;
    for( ; i < dimensionworld; ++i )
      ret[ i ] = phi[ i ];
    for( ; i < dimRange; ++i )
      ret[ i ] = 0;
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf0_.init( entity );
    lf1_.init( entity );
    entity_ = &entity;
    area_ = entity.geometry().volume();
  }

  protected:
    typename outDFT::LocalFunctionType lf0_;
    typename DFT::LocalFunctionType lf1_;
    const EntityType* entity_;
    double fact_;
    double ts_;
    const double wall_;
    mutable double nety_;
    mutable double netz_;
    mutable double neta_;
    double area_;
    const double tol_;
};


#endif // end #if ADDCHARGEFORCING_HH

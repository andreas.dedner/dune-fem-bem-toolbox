

template <class FS, class GP>
struct CoulombModel
{
  typedef CoulombModel<FS,GP> ModelType;
  typedef GP GridPartType;
  typedef FS FunctionSpaceType;
  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;

  CoulombModel(double omega) 
  : omega_( omega )
  , amplitude_( Dune::Fem::Parameter::getValue< double >( "helmholtz.amplitude", 0 ) )
  , c_(1.)
  , incidentData_(*this)
  , dirichletData_(*this)
  , exactD_(*this)
  {}
  double omega() const
  {
    return omega_;
  }
  bool hasDirichletBoundary() const
  {
    return true;
  }
  bool hasRobinBoundary() const
  {
    return false;
  }
  struct IncidentData
  {
    const ModelType &model_;
    IncidentData(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      res = RangeType(0);
    }
  };
  struct DirichletData
  {
    const ModelType &model_;
    DirichletData(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      // set constant electric surface potential
      res = RangeType(1);
    }
  };
  struct ExactNeumann
  {
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      res = RangeType(0);
    }
  };
  struct ExactDirichlet
  {
    const ModelType &model_;
    ExactDirichlet(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      res = RangeType(0);
    }
  };
  const IncidentData& incidentData() const
  {
    return incidentData_;
  }
  const DirichletData& dirichletData() const
  {
    return dirichletData_;
  }
  const ExactDirichlet& exactDirichlet() const
  {
    return exactD_;
  }
  const bool doDoubleLayer() const
  {
    return false;
  };
  const double idOpCoeff() const
  {
    return 1.0;
  };
private:
  const double omega_;
  const double amplitude_;
  const double c_;
  const IncidentData incidentData_;
  const DirichletData dirichletData_;
  const ExactDirichlet exactD_;
};


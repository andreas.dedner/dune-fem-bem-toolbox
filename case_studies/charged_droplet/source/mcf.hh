#ifndef MCF_HH
#define MCF_HH

#include <cassert>
#include <cmath>

#include <dune/fem/function/common/function.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/quadrature/quadrature.hh>

#include <dune/fem_bem_toolbox/fem_objects/temporalprobleminterface.hh>

// EvolutionFunction
// -----------------

template< class FunctionSpace, class Impl >
struct EvolutionFunction
: public Dune::Fem::Function< FunctionSpace, Impl >
{
  explicit EvolutionFunction ( const double time = 0.0 )
  : time_( time )
  {}

  const double &time () const
  {
    return time_;
  }

  void setTime ( const double time )
  {
    time_ = time;
  }

private:
  double time_;
};

namespace Problem
{
  // SurfaceEvolution
  // ----------------
  template< class FunctionSpace >
  struct SurfaceEvolution
    : public EvolutionFunction< FunctionSpace, SurfaceEvolution< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    SurfaceEvolution(const double &deform)
    : de_( deform )
    , cluster_( Dune::Fem::Parameter::getValue< double >( "clu.ster", 1.0   ) )
    , d_( Dune::Fem::Parameter::getValue< double >( "hemi.disp", 1.0   ) )
    , wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) > -400.0 )
    , r_(1)
    , a_(1)
    , b_(1)
    , h_(1)
    {
      if( cluster_ > 5 )
      {
      // sine and cosine of requested starting contact angle given in degrees
      double s = std::sin( cluster_ * M_PI / 180.0 );
      double c = std::cos( cluster_ * M_PI / 180.0 );
      // radius of sphere giving spherical cap
      r_ = std::pow( 4.0 / ( 2.0 - 3.0 * c + c*c*c ) , 1.0/3.0 );
      // radius of spherical cap
      a_ = r_ * s;
      // radius minus height of spherical cap
      b_ = r_ * c;
      // height of spherical cap
      h_ = r_ - b_;
      // std::cout << "YYY " << s << "  " << c << "  " << r_ << "  " << a_ << "  " << b_ << "  " << h_ << "  " << d_ << std::endl;
      }
    }

    void evaluate( const DomainType &z, RangeType &y ) const
    {
      assert( dimRange == 3 );
      assert( time() < 1.0e-10 );
      DomainType x = z;

      // initial mesh a spherical cap with given contact angle
      if( cluster_ > 5 )
      {
        y[0] = x[0];
        y[0] = ( y[0] + d_ ) * h_ / ( 1.0 + d_ );
        double rat = std::sqrt( r_*r_ - std::pow( ( b_ + y[0] ), 2.0 ) );
        if(x[0]>0 && rat!=0) rat /= std::sqrt( 1.0 - x[0]*x[0] );
	y[1] = rat * x[1]; y[2] = rat * x[2];
      }
      else if( cluster_ < -5 )
      { // angle of node to be rotated in degrees
	double ang = 180.0 * std::atan2(x[0],x[2]) / M_PI;
	// get in range
	if( ang < 0.0 ) ang += 180.0;
	// above or below the point to be most rotated determines ratio of full rotation to use
	if( ang < -cluster_ )
	  ang = ang / -cluster_;
        else
	  ang = ( 180.0 - ang ) / ( 180.0 + cluster_ );
	// apply ratio of full rotation to use and return things to radians
	if( -cluster_ < 90.0 )
          ang *= ( 90.0 + cluster_ ) * M_PI / 180.0;
	else
          ang *= - ( -cluster_ - 90.0 ) * M_PI / 180.0;
        double s = std::sin( ang ), c = std::cos( ang );
	// apply the rotation
        y[0] = c * x[0] + s * x[2];
        y[1] = x[1];
        y[2] = -s * x[0] + c * x[2];
      }
      else if( std::abs( cluster_ - 1.0 ) > 0.001 )
      {
        double origlen = x.two_norm();
        y = x;
        if( origlen > 0.0001 && x[0] < 0 )
	{
          y[0] *= cluster_;
          double newlen = y.two_norm();
          y *= origlen / newlen;
        };
      }
      else if( wall_ && ( std::abs( de_ - 1.0 ) > 0.0001 ) )
      {
        y = x;
        y[2] += de_;
      }
      else if( de_ < 0 )
      {
        // scaling parameter
        double gamma = 1; // 4 * M_PI / 3;

        // base hypoteneuse (axi-sym about x axis)
        double baseHyp = std::sqrt( x[1]*x[1]+x[2]*x[2] );

        // angle phi about axisym axis
        double cosPhi = baseHyp > 1.0e-10 ? x[1] / baseHyp : 0;
        double sinPhi = baseHyp > 1.0e-10 ? x[2] / baseHyp : 0;

        // angle alpha over z-coordinate
        double cosAlpha = x[0] / x.two_norm();
        double sinAlpha = baseHyp / x.two_norm();

        // second spherical harmonic
        double P2 = 0.5 * ( 3 * cosAlpha * cosAlpha - 1 );

        // required radius
        double radius = x.two_norm() * gamma * ( - de_ + 0.9 * P2 );

        // the projected positions
        y[0] = radius * cosAlpha;
        y[2] = radius * sinAlpha * cosPhi;
        y[1] = radius * sinAlpha * sinPhi;
      }
      else
      {
        y = x;
        if( de_ < 900.0 )
        {
          // now apply the deformation
          y /= std::pow( de_ , 1.0/3.0 );
          y[2] *= de_;
        }
      }
      return;
    }

    void ReDe(const double &deform)
    {
      de_ = deform;
    }

    using EvolutionFunction< FunctionSpace, SurfaceEvolution< FunctionSpace > >::time;

private:
    double r_;
    double a_;
    double b_;
    double h_;
    double d_;
    const bool wall_;
    double de_;
    double cluster_;
  };
  // VolumeEvolution
  // ----------------
  template< class FunctionSpace >
  struct VolumeEvolution
    : public EvolutionFunction< FunctionSpace, VolumeEvolution< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    VolumeEvolution(const double &deform)
    : de_( deform )
    , cluster_( Dune::Fem::Parameter::getValue< double >( "clu.ster", 1.0   ) )
    , d_( Dune::Fem::Parameter::getValue< double >( "hemi.disp", 1.0   ) )
    , wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) > -400.0 )
    , r_(1)
    , a_(1)
    , b_(1)
    , h_(1)
    {
      if( cluster_ > 5 )
      {
      // sine and cosine of requested starting contact angle given in degrees
      double s = std::sin( cluster_ * M_PI / 180.0 );
      double c = std::cos( cluster_ * M_PI / 180.0 );
      // radius of sphere giving spherical cap
      r_ = std::pow( 4.0 / ( 2.0 - 3.0 * c + c*c*c ) , 1.0/3.0 );
      // radius of spherical cap
      a_ = r_ * s;
      // radius minus height of spherical cap
      b_ = r_ * c;
      // height of spherical cap
      h_ = r_ - b_;
      // std::cout << "XXX " << s << "  " << c << "  " << r_ << "  " << a_ << "  " << b_ << "  " << h_ << "  " << d_ << std::endl;
      }
    }

    void evaluate( const DomainType &z, RangeType &y ) const
    {
      assert( dimRange == 3 );
      assert( time() < 1.0e-10 );
      DomainType x = z;

      // initial mesh a spherical cap with given contact angle
      if( cluster_ > 5 )
      {
        y[0] = x[0];
        y[0] = ( y[0] + d_ ) * h_ / ( 1.0 + d_ );
        double rat = std::sqrt( r_*r_ - std::pow( ( b_ + y[0] ), 2.0 ) );
        if(x[0]>0 && rat!=0) rat /= std::sqrt( 1.0 - x[0]*x[0] );
	y[1] = rat * x[1]; y[2] = rat * x[2];
      }
      else if( cluster_ < -5 )
      { // angle of node to be rotated in degrees
	double ang = 180.0 * std::atan2(x[0],x[2]) / M_PI;
	// get in range
	if( ang < 0.0 ) ang += 180.0;
	// above or below the point to be most rotated determines ratio of full rotation to use
	if( ang < -cluster_ )
	  ang = ang / -cluster_;
        else
	  ang = ( 180.0 - ang ) / ( 180.0 + cluster_ );
	// apply ratio of full rotation to use and return things to radians
	if( -cluster_ < 90.0 )
          ang *= ( 90.0 + cluster_ ) * M_PI / 180.0;
	else
          ang *= - ( -cluster_ - 90.0 ) * M_PI / 180.0;
        double s = std::sin( ang ), c = std::cos( ang );
	// apply the rotation
        y[0] = c * x[0] + s * x[2];
        y[1] = x[1];
        y[2] = -s * x[0] + c * x[2];
      }
      else if( std::abs( cluster_ - 1.0 ) > 0.001 )
      {
        double origlen = x.two_norm();
        y = x;
        if( origlen > 0.0001 && x[0] < 0  )
	{
          y[0] *= cluster_;
          double newlen = y.two_norm();
          y *= origlen / newlen;
        };
      }
      else if( wall_ && ( std::abs( de_ - 1.0 ) > 0.0001 ) )
      {
        y = x;
        y[2] += de_;
      }
      else if( de_ < 0 )
      {
        // scaling parameter
        double gamma = 1; // 4 * M_PI / 3;

        // base hypoteneuse (axi-sym about x axis)
        double baseHyp = std::sqrt( x[1]*x[1]+x[2]*x[2] );

        // angle phi about axisym axis
        double cosPhi = baseHyp > 1.0e-10 ? x[1] / baseHyp : 0;
        double sinPhi = baseHyp > 1.0e-10 ? x[2] / baseHyp : 0;

        // angle alpha over z-coordinate
        double cosAlpha = x[0] / x.two_norm();
        double sinAlpha = baseHyp / x.two_norm();

        // second spherical harmonic
        double P2 = 0.5 * ( 3 * cosAlpha * cosAlpha - 1 );

        // required radius
        double radius = x.two_norm() * gamma * ( - de_ + 0.9 * P2 );

        // the projected positions
        y[0] = radius * cosAlpha;
        y[2] = radius * sinAlpha * cosPhi;
        y[1] = radius * sinAlpha * sinPhi;
      }
      else
      {
        y = x;
        if( de_ < 900.0 )
        {
          // now apply the deformation
          y /= std::pow( de_ , 1.0/3.0 );
          y[2] *= de_;
        }
      }
      return;
    }

    void ReDe(const double &deform)
    {
      de_ = deform;
    }

    using EvolutionFunction< FunctionSpace, VolumeEvolution< FunctionSpace > >::time;

private:
    double r_;
    double a_;
    double b_;
    double h_;
    double d_;
    const bool wall_;
    double de_;
    double cluster_;
  };
#if 0
  // RHSFunction
  // -----------

  template< class FunctionSpace >
  struct RHSFunction
  : public EvolutionFunction< FunctionSpace, RHSFunction< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    void evaluate( const DomainType &x, RangeType &phi ) const
    {
      phi = RangeType( 0 );
    }

    using EvolutionFunction< FunctionSpace, RHSFunction< FunctionSpace > >::time;
  };
#endif

}


template <class FunctionSpace>
class AleNiceMesh : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  AleNiceMesh()
  : elastic_( Dune::Fem::Parameter::getValue< double >( "ale.stic", 0.0 ) )
  , maxZ_( -1.0e10 )
  , minZ_( +1.0e10 )
  , mew_( 0.0 )
  , sigSq_( 0.0 )
  , siggy_( 0.0 )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    if( elastic_ < 0.0 ) { maxZ_ = std::max( maxZ_ , x[ 2 ] ); minZ_ = std::min( minZ_ , x[ 2 ] ); }
    /*
    phi  = M_PI*(4*dimDomain*M_PI*std::cos( M_PI*time() ) - std::sin( M_PI*time() ));
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    */
    phi = 0;
    if( sigSq_ > 0.0 ) phi[ 2 ] = (mew_-x[2]) * std::exp( -0.5 * (x[2]-mew_) * (x[2]-mew_) / sigSq_ ) / siggy_;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true ;
  }
  
  virtual void fixSig() const
  {
    mew_ = ( maxZ_ + minZ_ ) / 2.0;
    if( ( elastic_ < 0.0 ) && ( maxZ_ > minZ_ ) )
    {
      sigSq_ = elastic_ * ( maxZ_ - minZ_ );
      sigSq_ = 1.0 / ( sigSq_ * sigSq_ ); 
      siggy_ = sigSq_ * std::sqrt( 2.0 * M_PI * sigSq_ );
    }
    else
    {
      sigSq_ = -1.0; siggy_ = 1.0;
    }
    maxZ_ = -1.0e10; minZ_ = +1.0e10;
  }
  
  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    phi = std::cos( M_PI );
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::cos( M_PI )*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }
private:
  const double elastic_;
  mutable double maxZ_;
  mutable double minZ_;
  mutable double mew_;
  mutable double sigSq_;
  mutable double siggy_;
};


template <class FunctionSpace>
class Fluid : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  Fluid( const Dune::Fem::TimeProviderBase &timeProvider )
    : BaseType( timeProvider ),
      m_( 1.0 / Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
      n_( Dune::Fem::Parameter::getValue< double >( "non.linear", 0 ) ),
      k_( std::abs( Dune::Fem::Parameter::getValue< double >( "drop.kick", 0 ) ) ),
      g_( std::abs( Dune::Fem::Parameter::getValue< double >( "grav.ity", 0 ) ) ),
      wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0   ) ),
      tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01 ) ),
      slipLength_( Dune::Fem::Parameter::getValue< double >( "slip.length", 1.0 ) ),
      slipHeight_( Dune::Fem::Parameter::getValue< double >( "slip.height", 0.0 ) ),
      slipPower_( Dune::Fem::Parameter::getValue< double >( "slip.power", 1.0 ) ),
      ycen_( 0.0 ),
      zcen_( 0.0 ),
      rmax_( 0.0 ),
      adaptedLength_( 5.0 ),
      ga_( 0 ),
      gb_( Dune::Fem::Parameter::getValue< double >( "grav.ang", 0 ) ),
      slidestart_( Dune::Fem::Parameter::getValue< double >( "drop.kick", 0 ) < -0.0001 ),
      bodyFriction_( -std::min( 0.0 , Dune::Fem::Parameter::getValue< double >( "wall.friction", 100.0   ) ) ),
      mom_( 0.0 ),
      gravity_( false )
  {
    gravity_ = g_ > 0.0001;
    // wall tilt angle (in degrees) at origin
    ga_ = std::floor( gb_ );
    // increase in tilt angle per unit of increasing non-dimensional z-coordinate
    gb_  = 1000.0 * ( gb_ - ga_ );
    // in radians
    ga_ *= M_PI / 180.0;
    gb_ *= M_PI / 180.0;
}
  
  // receive linear momentum for body friction calculation
  void linmom( const double &mom )
  {
    mom_ = mom;
  }
  
  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    /*
    phi  = M_PI*(4*dimDomain*M_PI*std::cos( M_PI*time() ) - std::sin( M_PI*time() ));
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    */
    phi = 0;
    double ang = ga_ + gb_ * x[ 2 ];
    phi[0] = -g_*m_*std::cos(ang);
    phi[2] = -g_*m_*std::sin(ang);
    // body (force) friction proportional to linear momentum retards motion
    phi[2] -= bodyFriction_ * mom_;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return false ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return false ;
  }

  //! mass coefficient (default = 0)
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(m_);
  }

  //! mass coefficient (default = 0)
  virtual void M(const DomainType& x, RangeType &M) const
  {
    // distance of current point from splat centre
    double len = std::sqrt( (x[1]-ycen_)*(x[1]-ycen_)+(x[2]-zcen_)*(x[2]-zcen_) ), fade;
    // fade-out tangential velocity zapping away from substrate
    if( x[ 0 ] <= wall_ + tol_ )
      fade = 0.0;
    else if( x[ 0 ] >= wall_ + tol_ + slipHeight_ )
      fade = 1.0;
    else
      fade = ( x[ 0 ] - wall_ - tol_ ) * ( x[ 0 ] - wall_ - tol_ ) / ( slipHeight_ * slipHeight_ );
    // ratio of distance from splat edge with the slip length adapted by the current contact angle
    if( len <= rmax_ - adaptedLength_ )
      // just zap tangential velocity history completely (proxi for full no-slip condition on substrate)
      M = RangeType( m_ * fade );
    else // grade-out tangential velocity zapping towards splat edge from adapted sliplength inside of it
      M = RangeType( m_ * ( fade + ( 1.0 - fade ) * std::pow( ( len - ( rmax_ - adaptedLength_ ) ) / adaptedLength_ , slipPower_ ) ) );
    // perpendicular velocity history left untouched
    if( x[ 0 ] <= wall_ + tol_ )
      M[0] = 0.0;
    else
    M[0] = m_;
  }

  //! non-linear coefficient (default = 0)
  virtual void n(const DomainType& x, RangeType &n) const
  {
    if( x[0] < wall_ + tol_ ) 
      n = RangeType(0.0);
    else
    n = RangeType(n_*m_);
  }

  virtual void setCentre(double ycen,double zcen,double rmax, double angle)
  {
    ycen_ = ycen; zcen_ = zcen; rmax_ = rmax; adaptedLength_ = slipLength_ * std::cos( M_PI * angle / 361.0 );
  }

  //! the exact solution
  // corresponds to the first (quadrupole) mode of oscillation
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    if(gravity_)
    {
      phi = 0;
      double ang = ga_ + gb_ * x[ 2 ];
      // give a little initial linear (sliding) momentum to droplet to get things going a bit quicker
      if( slidestart_ )
        phi[2] += k_;
      else
      {
	phi[0] = -std::sqrt(k_)*std::cos(ang);
	phi[2] = -std::sqrt(k_)*std::sin(ang);
      };
    }
    else
    {
    // vertical velocity component - along z-axis
    phi[ 2 ] = 2 * k_ * x[2];
    // radial velocity component
    phi[ 1 ] =  - k_ * x[1];
    phi[ 0 ] =  - k_ * x[0];
    phi[ 3 ] =  x[0];
    }
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::cos( M_PI*time() )*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }

private:
  double m_;
  double n_;
  double k_;
  double g_;
  double ga_;
  double gb_;
  double wall_;
  const double tol_;
  const double slipLength_;
  const double slipHeight_;
  const double slipPower_;
  double ycen_;
  double zcen_;
  double rmax_;
  double adaptedLength_;
  double bodyFriction_;
  double mom_;
  bool slidestart_;
  bool gravity_;
};


template <class FunctionSpace>
class TimeDependentCosinusProduct : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  TimeDependentCosinusProduct( const Dune::Fem::TimeProviderBase &timeProvider )
    : BaseType( timeProvider ),
      cl_( 9.0e-8 * std::pow( 180.0 - Dune::Fem::Parameter::getValue< double >( "equi.ang", 90.0 ) , 4.0 ) ),
      wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0   ) ),
      tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01 ) ),
      def_( Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ) ),
      ts_( Dune::Fem::Parameter::getValue< double >( "droplet.timestep", 0.125 ) ),
      choice_( 0 ),
      ycen_( 0.0 ),
      zcen_( 0.0 ),
      bias_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0   ) - 0.2 ),
      zapCLForce_( true ),
      reach_( std::pow( 10.0 , -10.0 ) ),
      weight_( Dune::Fem::Parameter::getValue< double >( "cl.weight", 1.0 ) ),
      // assume ellipsoid alligned along x-axis
      fita_( def_ / std::pow( def_ , 1.0/3.0 ) ),
      fitb_( 1.0  / std::pow( def_ , 1.0/3.0 ) )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi  = M_PI*(4*dimDomain*M_PI*std::cos( M_PI*time() ) - std::sin( M_PI*time() ));
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
  }

  //! contact line angle introduction
  virtual void cl(const DomainType& x, RangeType &cl) const
  {
    if( ( !zapCLForce_ ) && ( x[ 0 ] > bias_ ) )
    {
      cl[ 0 ] = 0.0;
      cl[ 1 ] = x[ 1 ] - ycen_;
      cl[ 2 ] = x[ 2 ] - zcen_;
      double len = std::sqrt( cl[ 1 ] * cl[ 1 ] + cl[ 2 ] * cl[ 2 ] );
      cl *= cl_ * sina_ / len;
      cl[ 0 ] = - cosa_ * cl_;
      if( x[ 0 ] <= wall_ + tol_ )
	cl[ 0 ] = 0.0;
      else
        cl /= 1.0 + ( x[ 0 ] - wall_ - tol_ ) * ( x[ 0 ] - wall_ - tol_ ) * std::pow( 10.0 , weight_ ) * reach_ * reach_;
    }
    else
      cl = RangeType(0.0);
  }

  virtual void setChoice(const int &val)
  {
    choice_ = val;
  }

  virtual void setCentre(double ycen,double zcen,double angle,double bias,double area,bool zapCLForce)
  {
    ycen_ = ycen; zcen_ = zcen; cosa_ = std::cos(M_PI * angle / 180.0); sina_ = std::sin(M_PI * angle / 180.0); bias_ = bias; zapCLForce_ = zapCLForce;
    reach_ = area;
  }

  //! the exact solution
  virtual void u(const DomainType& xx,
                 RangeType& phi) const
  {
    // radial distance
    double x = std::sqrt( xx[ 1 ]*xx[ 1 ] + xx[ 0 ]*xx[ 0 ] );

    // The y to go with the given x
    double y = x / fitb_;
    y = y * y;
    y = fita_ * sqrt( 1.0 - y );

    // std::cout << "Mean " << x << "  " << y << "  " << xx[ 0 ] << std::endl;

    // Constructions
    double r1 = fitb_ * fitb_; double r2 = fita_ * fita_;

    double H = r1 * r2; double K = r1;

    r1 = x * x / ( r1 * r1 ); r2 = y * y / ( r2 * r2 );

    r2 = sqrt( r1 + r2 );

    // Principle radii of Curvature
    r1 = H * r2 * r2 * r2;
    r2 = K * r2;

    // Mean Curvature - the Algebraic Mean
    phi = ( r1 + r2 ) / ( 2.0 * r1 * r2 );

    // Gaussian Curvature - the Geometric Mean
    // K = 1.0d0 / sqrt( r1 * r2 );

    /*
            ! Use spheroid analytic form when close to a sphere
            else if(sphereish) then
              ! Get spheroid Mean and Gaussian curvatures
              call fitHK(X_g(3),ek,Ex(j))
              ek = 2.0d0 * ek; Ex(j) = 2.0d0 * Ex(j) * ( omeg / fourpi ) ** 2


    phi = 5.6;
    phi = std::cos( M_PI*time() );
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    */
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::cos( M_PI*time() )*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }
private:
  double wall_;
  double tol_;
  double cl_;
  double def_;
  double fita_;
  double fitb_;
  int choice_;
  mutable double reach_;
  const double weight_;
  double ts_;
  double ycen_;
  double zcen_;
  double cosa_;
  double sina_;
  double bias_;
  bool zapCLForce_;
  using BaseType::timeProvider;
};


#endif // #ifndef MCF_HH

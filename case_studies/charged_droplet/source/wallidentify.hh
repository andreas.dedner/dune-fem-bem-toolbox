#ifndef WALLIDENTIFY_HH
#define WALLIDENTIFY_HH

// receives MSP normal derivative from the outside, and prepares it for the inside
template < class DFT >
struct WallIdentify
{
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  typedef Dune::Fem::FunctionSpace<double,double,DFT::DiscreteFunctionSpaceType::dimDomain,
                              DFT::DiscreteFunctionSpaceType::dimRange> FunctionSpaceType;
  // range type that is required
  typedef typename FunctionSpaceType::RangeType RangeType;
  // constructor
  WallIdentify( const bool &fixing , int &fixid , const DFT &f1, Dune::FieldVector<Dune::FieldVector<double,3>,3> & worst, double &spun, double &avesiz, const bool &skewtiny, double &bias )
    : wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0   ) )
    , record_( Dune::Fem::Parameter::getValue< double >( "ex.tra", 10000.0   ) )
    , worst_( worst )
    , fixing_( fixing )
    , fixid_( fixid )
    , area_( 0.0 )
    , tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01   ) )
    , spun_( spun )
    , sum_( 0.0 )
    , bias_( bias )
    , avesiz_( avesiz )
    , num_( 0 )
    , skewtiny_( skewtiny )
  {
    if( !fixing_ && !skewtiny_ )
    {
      record_ = ( 1.0 - record_ ) * avesiz_;
      avesiz_ = 0.0;
    }
  }

  const double &area() const
  {
    return area_;
  }
  
  void span()
  {
    spun_ += sum_ / double(num_);
    if( !fixing_ && !skewtiny_ ) avesiz_ /= double(num_);
  }
  
  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    auto geo = entity_->geometry();
    bool offwall = false, onwall = false, bad = false, w, ww, www;
    double longest = -1.0;
    double shortest =-1.0;
    double midest = -1.0;
    auto xx = geo.corner(0);
    auto yy = geo.corner(0);
    auto zz = geo.corner(0);
    const auto geoc = geo.center();
    const auto heoc = entity_->impl().hostEntity().geometry().center();
    // tally up for an average spin measure
    double ne = std::atan2( geoc[0] , geoc[2] ) - spun_;
    // std::cout << ne * 180.0 / M_PI << "  ";
    double ol = std::atan2( heoc[0] , heoc[2] );
    // std::cout << ol * 180.0 / M_PI << "  ";
    while( ne < -M_PI ) ne += 2 * M_PI;
    // std::cout << ne * 180.0 / M_PI << "  ";
    num_ += 1;
    if( ne - ol > M_PI )
    {
      // std::cout << "AAA " << ( ne - ol - 2 * M_PI ) * 180.0 / M_PI << std::endl;
      sum_ += ne - ol - 2 * M_PI;
    }
    else if( ne - ol < -M_PI )
    {
      // std::cout << "BBB " << ( ne - ol + 2 * M_PI ) * 180.0 / M_PI << std::endl;
      sum_ += ne - ol + 2 * M_PI;
    }
    else
    {
      // std::cout << "CCC " << ( ne - ol ) * 180.0 / M_PI << std::endl;
      sum_ += ne - ol;
    };
    for( int i = 0; i < geo.corners(); ++i )
    {
      bool fixable = geo.corner(i)[ 0 ] > bias_;
      // original
      if( geo.corner(i)[ 0 ] > wall_ + tol_ )
	offwall = true;
      else
	onwall = true;
      // consider worst point to be "in the wall" when fixing
      if( ( geo.corner(i) - worst_[0] ).two_norm() < 5.0 * tol_ )
	bad = true;
      // when not fixing, find the longest and shortest edges ...
      if( ! fixing_ )
      {
        double leng = ( geo.corner((i+1)%3) - geo.corner((i+2)%3) ).two_norm();
        if( leng > longest ) { shortest = midest; midest = longest; longest = leng; zz = yy; yy = xx; xx = geo.corner(i); www = ww; ww = w; w = fixable; }
        else if( leng > midest ) { shortest = midest; midest = leng; zz = yy; yy = geo.corner(i); www = ww; ww = fixable; }
	else if( leng > shortest ) { shortest = leng; zz = geo.corner(i); www = fixable; }
      };
    // end loop over corners
    };
    
    // accumulate contact area (if any)
    if( onwall && ( !offwall ) ) area_ += geo.volume();
    
    if( fixing_ && bad )
      ret[ 0 ] = -1000000.0;
    else if( ( ( onwall && offwall ) && fixing_ ) || offwall )
      ret[ 0 ] = 1000.0;
    else
      ret[ 0 ] = 0.0;
    // when not fixing, keep track of the worst point (opp. longest edge of most degenerate/tiniest element)
    if( ! fixing_ )
    {
      if( skewtiny_ ) // fixing the most skewed element
      {
	ne = longest / ( shortest + midest );
        if( ne > record_ )
	{
	  record_ = ne; fixid_ = num_;
	  if( w )
	  { worst_[0] = xx; worst_[1] = yy; worst_[2] = zz; }
	  else if( ww )
	  { worst_[0] = yy; worst_[1] = zz; worst_[2] = xx; }
	  else
	  { worst_[0] = zz; worst_[1] = xx; worst_[2] = yy; }
	};
      }
      else  // fixing the tinyest element
      {
	ne = geo.volume(); // / entity_->impl().hostEntity().geometry().volume();
	avesiz_ += ne;
        if( ne < record_ )
	{
	  record_ = ne; fixid_ = num_;
	  if( w )
	  { worst_[0] = xx; worst_[1] = yy; worst_[2] = zz; }
	  else if( ww )
	  { worst_[0] = yy; worst_[1] = zz; worst_[2] = xx; }
	  else
	  { worst_[0] = zz; worst_[1] = xx; worst_[2] = yy; }
	};
      };
    };
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    entity_ = &entity;
  }

  private:
    const EntityType* entity_;
    const double wall_;
    mutable double record_;
    Dune::FieldVector<Dune::FieldVector<double,3>,3> &worst_;
    const bool fixing_;
    const double tol_;
    mutable double area_;
    int &fixid_;
    double &spun_;
    mutable double sum_;
    double &avesiz_;
    mutable unsigned int num_;
    const bool skewtiny_;
    const double bias_;
};
#endif

#ifndef CURVATURE_HH
#define CURVATURE_HH


template < class DFT , class augDFT >
struct Curvature
{
  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename augDFT::DiscreteFunctionSpaceType AugDiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;
  typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;
  typedef typename AugDiscreteFunctionSpaceType::FunctionSpaceType AugFunctionSpaceType;
  typedef typename FunctionSpaceType::RangeType RangeType;

  // constructor
  Curvature( const DFT &f1, const augDFT &f2, const DFT &f3, const double &ts )
  : lf1_( f1 ),
    lf2_( f2 ),
    lf3_( f3 ),
    def_( Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ) ),
    // assume ellipsoid alligned along x-axis
    fita_( def_ / std::pow( def_ , 1.0/3.0 ) ),
    fitb_( 1.0  / std::pow( def_ , 1.0/3.0 ) ),
    surfTensCoeff_( 1.0 / Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
    ts_( ts ),
    Oh_( Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
    ent_( 0 )
  {}

  template< class Point >
  void evaluate ( const Point &xx, RangeType &ret ) const
  {
    typename FunctionSpaceType::RangeType phi;
    typename AugFunctionSpaceType::RangeType psi;
    typename FunctionSpaceType::RangeType pie;

    auto xxx = ent_->geometry().global(Dune::Fem::coordinate(xx));

    lf1_.evaluate( xx, phi );
    lf2_.evaluate( xx, psi );
    lf3_.evaluate( xx, pie );
    
    double x = phi[ 0 ]; double y = phi[ 1 ]; double z = phi[ 2 ];
    int flg = std::floor( psi[ 0 ] ); // double ny = psi[ 1 ]; double nz = psi[ 2 ];
    double mean = std::sqrt( x*x + y*y + z*z ) / ( 2.0 * ts_ * surfTensCoeff_ );

    ret = 0;
    // ret[ 0 ] = mean;

    if( pie[ 1 ] > 0 ) ret[ 0 ] = 1.0; else if( pie[ 1 ] < 0 ) ret[ 0 ] = -1.0; else ret[ 0 ] = 0.0;
    
    ret[ 1 ] = double( flg );
    
    // if( pie[ 2 ] > 0 ) ret[ 2 ] = 1.0; else if( pie[ 2 ] < 0 ) ret[ 2 ] = -1.0; else ret[ 2 ] = 0.0;
    
    if( flg == 1 )
      ret[ 2 ] = 90.0 + std::abs( std::atan( x / std::sqrt( y*y + z*z ) ) ) * 180.0 / M_PI;
  }

  template< class Point >
  void evaluatee ( const Point &xx, RangeType &ret ) const
  {
    typename FunctionSpaceType::RangeType phi;
    lf1_.evaluate( xx, phi );
    ret[ 0 ] = phi[ 0 ] * phi[ 0 ];
    for( int i = 1; i < dimensionworld; ++i )
      ret[ 0 ] += phi[ i ] * phi[ i ];
    ret[ 0 ] = std::sqrt( ret[ 0 ] ) * 2.0 * Oh_ / ts_;

    auto xxx = ent_->geometry().global(Dune::Fem::coordinate(xx));

    // radial distance
    double x = std::sqrt( std::pow(xxx[ 1 ],2.0) + std::pow(xxx[ 0 ],2.0) );

    // The y to go with the given x
    double y = x / fitb_;
    y = y * y;
    y = fita_ * sqrt( 1.0 - y );

    // std::cout << "Meann " << x << "  " << y << "  " << xxx[ 0 ] << "  " << xxx[ 1 ] << "  " << xxx[ 2 ] << std::endl;

    // Constructions
    double r1 = fitb_ * fitb_; double r2 = fita_ * fita_;

    double H = r1 * r2; double K = r1;

    r1 = x * x / ( r1 * r1 ); r2 = y * y / ( r2 * r2 );

    r2 = sqrt( r1 + r2 );

    // Principle radii of Curvature
    r1 = H * r2 * r2 * r2;
    r2 = K * r2;

    // Mean Curvature - the Algebraic Mean
    ret[ 1 ] = ( r1 + r2 ) / ( r1 * r2 );
    ret[ 2 ] = ret[ 1 ] - ret[ 0 ];
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf1_.init( entity );
    lf2_.init( entity );
    lf3_.init( entity );
    ent_ = &entity;
  }

  private:
    typename DFT::LocalFunctionType lf1_;
    typename augDFT::LocalFunctionType lf2_;
    typename DFT::LocalFunctionType lf3_;
private:
  double def_;
  double fita_;
  double fitb_;
  double ts_;
  double Oh_;
  const EntityType* ent_;
  double surfTensCoeff_;
};


#endif // end #if CURVATURE_HH

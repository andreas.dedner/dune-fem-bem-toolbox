#ifndef Traction_FEMSCHEME_HH
#define Traction_FEMSCHEME_HH

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parallel/mpicollectivecommunication.hh>
#include <dune/fem/operator/projection/vtxprojection.hh>
#include <string>
#include <boost/lexical_cast.hpp>

using boost::lexical_cast;
using std::string;

// local includes
#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include "addpressure.hh"
#include "addchargeforcing.hh"
#include "subsposnadapt.hh"
#include "hostcoords.hh"

// TractionScheme
//-----------

template < class ImplicitModel, class ExplicitModel, class MonitorType, SolverType solver=istl >
struct TractionScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef ExplicitModel ExplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  typedef typename BaseType::LinearInverseOperatorType LinearInverseOperatorType;

  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< AugmentedFunctionSpaceType, GridPartType, POLORDER > AugmentedDiscreteFunctionSpaceType;
  typedef Solvers<AugmentedDiscreteFunctionSpaceType,solver,true> AugmentedSolverType;
  typedef typename AugmentedSolverType::DiscreteFunctionType AugmentedDiscreteFunctionType;

  TractionScheme( GridPartType &gridPart,
              const ImplicitModelType& implicitModel,
	      const ExplicitModelType& explicitModel, const MonitorType& monitor )
  : BaseType(gridPart, implicitModel),
    explicitModel_(explicitModel),
    Rhs_( "forcing", discreteSpace_ ),
    forcing_( "forcing", discreteSpace_ ),
    Oh_(  Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
    chi_( Dune::Fem::Parameter::getValue< double >( "fiss.ility", 0.0   ) ),
    diag_( Dune::Fem::Parameter::getValue< bool >( "diag.nostics", false ) ),
    monitor_(monitor),
    surfTensCoeff_( 1.0 / Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
    explicitOperator_( explicitModel_, discreteSpace_, BaseType::constraints_ ),
    firstTime_( true )
  {
    // set all DoF to zero
    Rhs_.clear();
  }

  DiscreteFunctionType &Rhs()
  {
    return Rhs_;
  }
  const DiscreteFunctionType &Rhs() const
  {
    return Rhs_;
  }

  DiscreteFunctionType &forcing()
  {
    return forcing_;
  }
  const DiscreteFunctionType &forcing() const
  {
    return forcing_;
  }

  template <class DF , class IOFT>
  void oi(unsigned int num, DF &solution,  IOFT &iof)
  {
    unsigned int span = DF::RangeType::dimension;
    typename DF::DofIteratorType end = solution.dend(); unsigned int i = 0;
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git, ++i )
    {
      iof << num << "  " << std::floor(float(i)/float(span))+1 << " w " << 1+i%span << " xxx " << lexical_cast<string>(*git) << std::endl;
    };
  }
  
  template <class DF , class IOFT>
  void io(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    
    typename DF::DofIteratorType end = solution.dend();
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git )
    {
      getline(iof,line); *git = std::stod(line.erase(0,line.rfind("xxx")+4),&sz);
    };
  }
  
  void dump(const int snap)
  {
    std::ifstream isnapfile; std::ofstream osnapfile;
    if( snap < -1 ) osnapfile.open("snip.tra"); else if( snap > 0 ) osnapfile.open("snap.tra"); else if( snap < 0 ) isnapfile.open("snap.tra");

    if( snap > 0 ) // writing data out
    {
      oi( 1, solution_, osnapfile );
      oi( 2, rhs_, osnapfile );
      oi( 3, forcing_, osnapfile );
      oi( 4, Rhs_, osnapfile );
    }
    else if( snap < 0 ) // reading data in
    {
      io( solution_, isnapfile );
      io( rhs_, isnapfile );
      io( forcing_, isnapfile );
      io( Rhs_, isnapfile ); 
    };
    
    if( snap > 0 ) osnapfile.close(); else if( snap < 0 ) isnapfile.close();
  }
  
  void wipesol()
  {
    Rhs_.clear();
  } 

  bool notFirstTime()
  {
    bool ret = true;
    if( firstTime_ )
    {
      ret = false;
      firstTime_ = false;
    };
    return ret;
  }

  void nodeDump()
  {
    std::ofstream osnapfile;
    osnapfile.open("snap.sur");
    oi( 0, solution_, osnapfile );
    osnapfile.close();
  }

  void prepare()
  {
    // apply explicit operator and also setup right hand side
    explicitOperator_( solution_, rhs_ );
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void couple(OtherDiscreteFunctionType &otherSolution_)
  {
    if (!constraintsSetup_)
    {
      constraints().updateCouplingDofs();
      constraintsSetup_ = true;
    }
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = otherSolution_.space().gridPart();

    typedef AddPressure< OtherDiscreteFunctionType, DiscreteFunctionType > AddPressureType;
    typedef Dune::Fem::LocalFunctionAdapter< AddPressureType > AddPressureFunctionType;
    AddPressureType addpressure( otherSolution_ );
    AddPressureFunctionType addPressureFunction( "addpressure", addpressure, otherGridPart_ );

    constraints()( forcing_, addPressureFunction, Rhs_ );
  }

  void initialize ()
  {
     Dune::Fem::LagrangeInterpolation
          < typename ExplicitModelType::InitialFunctionType, DiscreteFunctionType > interpolation;
     interpolation( explicitModel_.initialFunction(), solution_ );
  }

  void explode()
  {
    // reset fissility after explosion
    chi_ = Dune::Fem::Parameter::getValue< double >( "fiss.ulity", 0.0   );
    std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << "BOOM !!!" << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl;
  }

  //! solve the system
  void solve ( bool assemble )
  {
    // make a record of the original positions to be used to see the differences later
    forcing_.assign( solution_ );
    forcing_ *= -1.0;
    // solve as per the base type
    BaseType::solve( assemble );

    // the forcing is proportional to the distance moved
    // if "H" is the mean curvature, then the velocity for mean curvature flow
    // is = 2 H, and thus the distance moved in one timestep "k" is = 2 H k
    forcing_ += solution_;

    // apply any scaling that might be necessary
    forcing_ *= surfTensCoeff_ * explicitModel_.timeStepRatio();

#if !HAVE_BEMPP
    // the coupling should have set the Rhs to contain the fluid velocites on the boundary as Dirichlet boundary conditions
    // put these into the solution, which should then remain untouched to be used by the nudge after the return
    // solution_.assign( Rhs_ );
    // NB: now the solution holds fluid velocities
#endif
  }

#if HAVE_BEMPP
  template <class OtherDiscreteFunctionType, class TempDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void charge(OtherDiscreteFunctionType &otherSolution,OtherDiscreteFunctionType &flagSolution,TempDiscreteFunctionType &tempSolution, int &fixing, const double spun, Dune::FieldVector<double,4> &drift)
  {
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart = otherSolution.space().gridPart();
    
    typedef typename OtherGridPartType::template Codim<0>::IteratorType OtherIteratorType;
    typedef typename OtherDiscreteFunctionType::RangeType OtherRangeType;

    double localCharge = 0.0;
    // determine the total charge on the surface from using a constant potential of unity
    const OtherIteratorType end = otherGridPart.template end<0>();
    for( OtherIteratorType it = otherGridPart.template begin<0>(); it != end; ++it )
    {
      double area = it->geometry().volume();
      OtherRangeType phi;
      otherSolution.evaluate( it->geometry().center() , phi );
      localCharge += area * phi[ 0 ];
    }

    typedef typename TempDiscreteFunctionType::GridPartType TempGridPartType;
    TempGridPartType tempGridPart = tempSolution.space().gridPart();

    Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> sumComm(Dune::MPIHelper::getCommunicator());
    double totalCharge = sumComm.sum(localCharge);

    // total charge correction factor
    double chargeCorrectionFactor = 8.0 * M_PI * std::sqrt( chi_ / Oh_ ) / totalCharge;

    typedef AddChargeForcing< OtherDiscreteFunctionType, DiscreteFunctionType > AddChargeForcingType;
    typedef Dune::Fem::LocalFunctionAdapter< AddChargeForcingType > AddChargeForcingFunctionType;
    AddChargeForcingType addForcing( chargeCorrectionFactor, forcing_, otherSolution );
    AddChargeForcingFunctionType addChargeForcingFunction( "addchargeforcing", addForcing, otherGridPart );

    Dune::Fem::VtxProjection
          < AddChargeForcingFunctionType, DiscreteFunctionType > interpolation;
    interpolation( addChargeForcingFunction, solution_ );

    // Note: forcing now holds the SQUARE of the surface charge density
    // note the net tangential forces from MCF surface tension on the substrate
    addForcing.netForce( surfTensCoeff_ * explicitModel_.timeProvider().deltaT(), drift );

    rhs_.clear();

    // put host node coordinates in
    typedef HostCoords< DiscreteFunctionType, AugmentedDiscreteFunctionType > HostCoordsType;
    typedef Dune::Fem::LocalFunctionAdapter< HostCoordsType > HostCoordsFunctionType;
    HostCoordsType hostCoords( fixing, rhs_, spun );
    HostCoordsFunctionType hostCoordsFunction( "hostCoords", hostCoords, gridPart_ );

    Dune::Fem::LagrangeInterpolation
          < HostCoordsFunctionType, AugmentedDiscreteFunctionType > interpolationFive;

    interpolationFive( hostCoordsFunction, rhs_ );

    fixing = hostCoords.fixval();
  }

  void augment()
  {
    if( diag_ )
      solution_.assign( Rhs_ );
    else
    {
      // now that the forcing variable has finished his work as a normal direction setter
      // combine the two magnetic forcing bits with the surface tension forcing
      forcing_ += solution_;

      // the coupling should have set the Rhs to contain the fluid velocites on the boundary as Dirichlet boundary conditions
      // now put these into the solution, which should then remain untouched to be used later by the nudge
      // NB: "nudge" has actually already received these velocities directly from the Rhs_ vector itself, so this is just to
      // restore original design ...
      // solution_.assign( Rhs_ );
      // NB: now the solution holds fluid velocities

      typedef SubsPosnAdapt< AugmentedDiscreteFunctionType, DiscreteFunctionType > SubsPosnAdaptType;
    typedef Dune::Fem::LocalFunctionAdapter< SubsPosnAdaptType > SubsPosnAdaptFunctionType;
    SubsPosnAdaptType spa( rhs_, Rhs_ );
    SubsPosnAdaptFunctionType spaFunction( "spa", spa, gridPart_ );

    Dune::Fem::LagrangeInterpolation
          < SubsPosnAdaptFunctionType, DiscreteFunctionType > interpolationSix;

    interpolationSix( spaFunction, solution_ );

    double chargeCorrectionFactor = chi_;
    // now finally combine the three components of the fully formed surface forcing vector
    // with the scalar temperature normal derivative to be used to drive the temperature inside in the fourth component position
    typedef AugmentWithTemperature< DiscreteFunctionType, AugmentedDiscreteFunctionType > AugmentedType;
    typedef Dune::Fem::LocalFunctionAdapter< AugmentedType > AugmentedFunctionType;
    AugmentedType aug( chargeCorrectionFactor, forcing_, rhs_ );
    AugmentedFunctionType augFunction( "aug", aug, gridPart_ );

    Dune::Fem::LagrangeInterpolation
          < AugmentedFunctionType, AugmentedDiscreteFunctionType > interpolationTwo;

    interpolationTwo( augFunction, fullForcing_ );
    }
  }
#endif

private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::constraints;
  using BaseType::solution_;
  DiscreteFunctionType forcing_;
  DiscreteFunctionType Rhs_;
  using BaseType::implicitModel_;
  using BaseType::rhs_;
  using BaseType::implicitOperator_;
  using BaseType::linearOperator_;
  using BaseType::solverEps_;
  using BaseType::constraintsSetup_;
  const ExplicitModelType &explicitModel_;
  typename BaseType::EllipticOperatorType explicitOperator_; // the operator for the rhs
  // Ohnesorge number
  double Oh_;
  // Fissility
  double chi_;
  // Surface tension coefficient
  double surfTensCoeff_;
  const MonitorType &monitor_;
  // diagnostics
  const bool diag_;
  bool firstTime_;
};

#endif // end #if Traction_FEMSCHEME_HH

#ifndef NAVIER_STOKES_MODEL_HH
#define NAVIER_STOKES_MODEL_HH

#include <dune/fem/solver/timeprovider.hh>

#include <dune/fem_bem_toolbox/fem_objects/temporalprobleminterface.hh>
#include <dune/fem_bem_toolbox/fluid_models/stokesmodel.hh>

template< class FunctionSpace, class GridPart, class MeshVelDFT >
struct NavierStokesModel : public StokesModel<FunctionSpace,GridPart>
{
  typedef StokesModel<FunctionSpace,GridPart> BaseType;
  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef TemporalProblemInterface< FunctionSpaceType > ProblemType ;

  typedef typename BaseType::ProblemType InitialFunctionType;

  typedef Dune::Fem::TimeProviderBase TimeProviderType;

  //! constructor taking problem reference, time provider,
  //! time step factor( either theta or -(1-theta) ),
  //! flag for the right hand side
  NavierStokesModel( const ProblemType& problem,
             const GridPart &gridPart,
             const bool implicit,
             const double &timestep, const MeshVelDFT &meshVelocity )
    : BaseType(problem,gridPart),
      timeProvider_(problem.timeProvider()),
      diag_( Dune::Fem::Parameter::getValue< bool >( "diag.nostics", false ) ),
      implicit_( implicit ),
      timestep_(timestep),
      meshVelocity_(meshVelocity),
      walled_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) > -400.0 ),
      angmom_( 0 ),
      timeStepFactor_( 0 )
  {
    // get theta for theta scheme
    const double theta = Dune::Fem::Parameter::getValue< double >( "ns.theta", 0.5 );
    if (implicit)
    {
      timeStepFactor_ = theta ;
    }
    else
    {
      timeStepFactor_ = -( 1.0 - theta ) ;
    }
  }

  // the linearization of the source function
  template< class Entity, class Point >
  void linSource ( const RangeType& uBar,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    const DomainType xGlobal = entity.geometry().global( coordinate( x ) );
    RangeType m;
    RangeType M;
    problem_.m(xGlobal,m);
    problem_.M(xGlobal,M);

    RangeType g;
    problem_.f(xGlobal,g);

    BaseType::linSource(uBar,entity,x,value,gradient,flux);

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      flux[ localRow ] *= timeStepFactor_;
    }

    if( ! implicit_ )
    {
      if(diag_)
        angmom_ += entity.geometry().volume() / 4;
      else if( walled_ ) // linear momentum in z-direction if wall present
      {
        // Determine Mesh Quality
	auto g = entity.geometry();
        double a0 = ( g.corner(3) - g.corner(0) ).two_norm() * ( g.corner(1) - g.corner(2) ).two_norm();
        double a1 = ( g.corner(3) - g.corner(1) ).two_norm() * ( g.corner(2) - g.corner(0) ).two_norm();
        double a2 = ( g.corner(3) - g.corner(2) ).two_norm() * ( g.corner(0) - g.corner(1) ).two_norm();
        double s = ( a0 + a1 + a2 ) / 2.0;
        double q = std::pow( s * ( s - a0 ) * ( s - a1 ) * ( s - a2 ) , 1.5 );
        double qual = M_PI * q / ( 162.0 * std::pow( entity.geometry().volume() , 4.0 ) );
	if( qual > angmom_ ) angmom_ = qual;
        // angmom_ += value[ 2 ] * entity.geometry().volume() / 4;
      }
      else
        angmom_ += ( xGlobal[ 2 ]*value[ 0 ] - xGlobal[ 0 ]*value[ 2 ] ) * entity.geometry().volume() / 4;
      Dune::FieldVector<double,dimDomain> uMinusV;
      typedef typename MeshVelDFT::LocalFunctionType LocalFunctionType;
      const LocalFunctionType meshVelLocal = meshVelocity_.localFunction( entity );
      typename LocalFunctionType::RangeType meshVel;
      meshVelLocal.evaluate( x, meshVel );
      for( unsigned int i = 0; i < dimDomain; ++i )
      {
        // NB: mesh velocity already minused (by ALE scheme)
        uMinusV[ i ] = value[ i ] + meshVel[ i ] / timestep_;
      }
      BaseType::advectionSource(uMinusV,entity,x,value,gradient,flux);
    }

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      if( ! implicit_ ) flux[ localRow ] += g[ localRow ];
      flux[ localRow ] *= timeProvider_.deltaT();
      if( implicit_ ) 
      flux[ localRow ] += m[ localRow ] * value[ localRow ];
      else
        flux[ localRow ] += M[ localRow ] * value[ localRow ];
    }

    flux[ dimDomain ] *= timeStepFactor_ * timeProvider_.deltaT();
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void diffusiveFlux ( const Entity &entity,
                       const Point &x,
                       const RangeType &value,
                       const JacobianRangeType &gradient,
                       JacobianRangeType &flux ) const
  {
    flux = 0.0;
  }

  // linearization of diffusiveFlux
  template< class Entity, class Point >
  void linDiffusiveFlux ( const RangeType& uBar,
                          const JacobianRangeType& gradientBar,
                          const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          const JacobianRangeType &gradient,
                          JacobianRangeType &flux ) const
  {
    BaseType::linDiffusiveFlux(uBar,gradientBar,entity,x,value,gradient,flux);

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      flux[ localRow ] *= timeStepFactor_ * timeProvider_.deltaT();
    }
    flux[ dimDomain ] *= timeStepFactor_ * timeProvider_.deltaT();
  }

  //! return reference to Problem's time provider
  const TimeProviderType & timeProvider() const
  {
    return timeProvider_;
  }

  double angMom(const bool &reset) const
  {
    double pass = angmom_;
    if(reset) angmom_ = 0.0;
    return pass;
  }

  void amSet(const double &am) const
  {
    angmom_ = am;
  }

  const InitialFunctionType &initialFunction() const
  {
    return problem_;
  }

protected:
  using BaseType::problem_;
  const TimeProviderType &timeProvider_;
  bool implicit_;
  double timeStepFactor_;
  mutable double angmom_;
  const double &timestep_;
  const MeshVelDFT &meshVelocity_;
  const bool diag_;
  const bool walled_;
public:
  static const bool applyBoundaryForcing = true;
};
#endif // #ifndef NAVIER_STOKES_MODEL_HH

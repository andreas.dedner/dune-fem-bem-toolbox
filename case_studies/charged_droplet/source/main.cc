#include <config.h>

// iostream includes
#include <iostream>

#include <sys/stat.h>
#include <unistd.h>
#include <string>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

// include GeometryGrid
#include <dune/grid/geometrygrid.hh>

// include grid part
#if HAVE_BEMPP && CHARGED
#include <dune/fem/gridpart/leafgridpart.hh>
#else
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#endif

// include grid width
#include <dune/fem/misc/gridwidth.hh>

// include discrete function space
#include <dune/fem/space/lagrange.hh>

// include discrete function
#include <dune/fem/function/adaptivefunction.hh>

// include solvers
#include <dune/fem/solver/cginverseoperator.hh>
#include <dune/fem/solver/oemsolver.hh>

// include Lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>

// include norms
#include <dune/fem/misc/l2norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

// include eoc calc
#include <dune/fem/misc/femeoc.hh>
// include discrete function
#include <dune/fem/function/blockvectordiscretefunction/blockvectordiscretefunction.hh>

// include linear operators
//#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/operator/matrix/blockmatrix.hh>
#include <dune/fem/operator/2order/dgmatrixsetup.hh>
#include <dune/fem/operator/2order/lagrangematrixsetup.hh>

#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>

#include "traits.hh"

// local includes
#include "bemmodel.hh"

#include <dune/fem_bem_toolbox/fem_objects/elliptic.hh>
#include "deformation.hh"
#include "volumedeformation.hh"
#include "mcf.hh"
#include "tractionmodel.hh"
#include "alemodel.hh"
#include "navierstokesmodel.hh"
#include "tractionscheme.hh"
#include "alescheme.hh"
#include "nsscheme.hh"
#include "divergence.hh"
#include "curvature.hh"
#include <dune/fem_bem_toolbox/fem_objects/rhs.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_zoltan.hh>

#if HAVE_BEMPP && CHARGED
#include "fembem.hh"
#include <dune/fem_bem_toolbox/bem_objects/dropbemscheme.hh>
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/common/parallel/mpihelper.hh>

// algorithm
// ---------

template <class HGridType>
void algorithm ( HGridType &grid, const double timeStep, int step, const int eocId )
{
  // choose type of discrete function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::InsideDeformationType  InsideDeformationType;

  //DeformationCoordFunction deformation;
  InsideDeformationType insideDeformation( grid );

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GridType GridType;
  GridType insideGeoGrid( grid, insideDeformation );

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GridPartType GridPartType;

  GridPartType gridPart( insideGeoGrid );

  // setup time provider
  const double endTime = Dune::Fem::Parameter::getValue< double >( "droplet.endtime", 2.0 );
  const double dtreducefactor = Dune::Fem::Parameter::getValue< double >("droplet.reducetimestepfactor", 1 );
  double timeStup = Dune::Fem::Parameter::getValue< double >( "droplet.timestep", 0.125 );

  timeStup *= pow(dtreducefactor,step);

  Dune::Fem::GridTimeProvider< GridType > insideTimeProvider( insideGeoGrid );

  // initialize with fixed time step
  insideTimeProvider.init( timeStup ) ;

  // type of the mathematical model used
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceType FunctionSpaceInsideType;
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceOutsideType FunctionSpaceOutsideType;
  typedef AleNiceMesh< FunctionSpaceInsideType > InsideProblemType;

  InsideProblemType insideProblem;

  typedef AleModel< FunctionSpaceInsideType, GridPartType > ALEModelType;

  // implicit model for left hand side
  ALEModelType insideImplicitModel( insideProblem, gridPart );

  // create ALE scheme
  typedef ALEScheme< ALEModelType > ALESchemeType;
  ALESchemeType aleScheme( gridPart, insideImplicitModel );

  // initial data for volume
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::DiscreteFunctionSpaceInsideType DiscreteInsideHostFunctionSpaceType;
  typedef Problem::VolumeEvolution< DiscreteInsideHostFunctionSpaceType > VolumeEvolutionType;
  typedef VolumeDeformationCoordFunction< VolumeEvolutionType > VolumeDeformationCoordFunctionType;

  // find initial condition
  VolumeEvolutionType insideInitialCondition(1000.0);

  // interpolate initial condition
  VolumeDeformationCoordFunctionType insideDeform( insideInitialCondition );
  insideDeformation.initialize( insideDeform, aleScheme.solution() );

  // setup data output
  typedef Dune::Fem::GridFunctionAdapter< InsideProblemType, GridPartType > InsideGridExactSolutionType;
  InsideGridExactSolutionType insideGridExactSolution("inside exact solution", insideProblem, gridPart, 5 );
  typedef Dune::tuple< const typename ALESchemeType::DiscreteFunctionType *, const typename ALESchemeType::DiscreteFunctionType *, InsideGridExactSolutionType * > InsideIOTupleType;
  InsideIOTupleType insideIoTuple( &(aleScheme.solution()), &(aleScheme.rhs()), &insideGridExactSolution );
  typedef Dune::Fem::DataOutput< GridType, InsideIOTupleType > InsideDataOutputType;
  InsideDataOutputType insideDataOutput( insideGeoGrid, insideIoTuple, DataOutputParameters( step, "volume" ) );

  // flag for whether or not to output vtu files
  const unsigned int vtuout = Dune::Fem::Parameter::getValue< unsigned int >( "vtu.output", 0 );

  // special vtu flag to output volume data just to check the partitioning (via Zoltan or otherwise ...)
  if( vtuout == -99 )
  {
    insideDataOutput.write(   insideTimeProvider );
    return;
  }

  typedef typename Dune::FemFemCoupling::Glue< HGridType,false >::OutsideHostGridType OutsideHostGridType;

  // create dune grid file with surface mesh
  OutsideHostGridType* grydd = 0;
  OutsideHostGridType* fullGrydd = 0;

  // if requested, use a previously created surface grid
  if( ( ! Dune::Fem::Parameter::getValue< bool >( "surface.use", false ) ) && ( Dune::Fem::MPIManager::size() == 1 ) )
  {
    Dune::FemFemCoupling::SurfaceExtractor<GridPartType , OutsideHostGridType > surfaceExtractor( gridPart );
    surfaceExtractor.extractSurface();
    // dump out internal volume node order
    aleScheme.nodeDump();
  }

    const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", OutsideHostGridType::dimension );
    const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );
    Dune::GridPtr< OutsideHostGridType > grydPtr( grydfile );
    grydd = grydPtr.release();
    // duplicate surface grid for BEM operators which will NOT be partitioned
    Dune::GridPtr< OutsideHostGridType > fullGrydPtr( grydfile, Dune::MPIHelper::getLocalCommunicator() );
    fullGrydd = fullGrydPtr.release();

  OutsideHostGridType& gryd = *grydd ;

#if HAVE_BEMPP && CHARGED
  // duplicate surface grid for BEM operators which will NOT be partitioned
  OutsideHostGridType& fullGryd = *fullGrydd ;
#endif

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<OutsideHostGridType> SurfLoadBalancer;
  SurfLoadBalancer sldb(gryd);
#if HAVE_BEMPP && CHARGED
  SurfLoadBalancer fsldb(fullGryd,true);
#endif

  // do initial load balance
  if ( sldb.repartition() )
    gryd.repartition( sldb );
  else
    gryd.loadBalance();

#if HAVE_BEMPP && CHARGED
  // dummey load balance that puts whole grid on node zero
  if ( fsldb.repartition() )
    fullGryd.repartition( fsldb );
  else
    fullGryd.loadBalance();
#endif

  // choose type of discrete function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::OutsideHostDiscreteFunctionType OutsideHostDiscreteFunctionType;
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::OutsideDeformationType  OutsideDeformationType;

  //DeformationCoordFunction deformation;
  OutsideDeformationType outsideDeformation( gryd, timeStup );
#if HAVE_BEMPP && CHARGED
  OutsideDeformationType fullOutsideDeformation( fullGryd, timeStup );
#endif

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GrydType GrydType;
  GrydType outsideGeoGrid( gryd, outsideDeformation );
#if HAVE_BEMPP && CHARGED
  GrydType fullOutsideGeoGrid( fullGryd, fullOutsideDeformation );
#endif

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GrydPartType GrydPartType;

  GrydPartType grydPart( outsideGeoGrid );
#if HAVE_BEMPP && CHARGED
  GrydPartType fullGrydPart( fullOutsideGeoGrid );
#endif

  Dune::Fem::GridTimeProvider< GrydType > outsideTimeProvider( outsideGeoGrid );

  // initialize with fixed time step
  outsideTimeProvider.init( timeStup ) ;

  // type of the mathematical model used
  typedef TimeDependentCosinusProduct< FunctionSpaceOutsideType > OutsideProblemType;

  OutsideProblemType outsideProblem( outsideTimeProvider ) ;

  typedef TractionModel< FunctionSpaceOutsideType, GrydPartType > TractionModelType;

  // implicit model for left hand side
  TractionModelType outsideImplicitModel( outsideProblem, grydPart, true );

  // explicit model for right hand side
  TractionModelType outsideExplicitModel( outsideProblem, grydPart, false );

  // initial data for surface
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::DiscreteFunctionSpaceOutsideType DiscreteOutsideHostFunctionSpaceType;
  typedef Problem::SurfaceEvolution< DiscreteOutsideHostFunctionSpaceType > SurfaceEvolutionType;
  typedef SurfaceDeformationCoordFunction< SurfaceEvolutionType > SurfaceDeformationCoordFunctionType;

  // find initial condition
  SurfaceEvolutionType outsideInitialCondition(1000.0);

  // interpolate initial condition
  SurfaceDeformationCoordFunctionType outsideDeform( outsideInitialCondition );
  outsideDeformation.initialize( outsideDeform );
#if HAVE_BEMPP && CHARGED
  fullOutsideDeformation.initialize( outsideDeform );
#endif

  typedef typename OutsideDeformationType::Monitor MonitorType;

  // create traction scheme
  typedef TractionScheme< TractionModelType, TractionModelType, MonitorType > TractionSchemeType;
  TractionSchemeType tractionScheme( grydPart, outsideImplicitModel, outsideExplicitModel, outsideDeformation.monitor() );

  // initialize solution
  outsideDeformation.initializeSolution( tractionScheme.solution() );

  if( ( ! Dune::Fem::Parameter::getValue< bool >( "surface.use", false ) ) && ( Dune::Fem::MPIManager::size() == 1 ) )
  {
    tractionScheme.nodeDump();
    return;
  }
  
  // setup data output
  typedef Dune::Fem::GridFunctionAdapter< OutsideProblemType, GrydPartType > OutsideGridExactSolutionType;
  OutsideGridExactSolutionType outsideGridExactSolution("outside exact tension solution", outsideProblem, grydPart, 5 );

  // local function adapter just to get size of forcing movements
  typedef typename TractionSchemeType::DiscreteFunctionType TDFT;
  typedef Curvature< TDFT > CurvatureType;
  typedef Dune::Fem::LocalFunctionAdapter< CurvatureType > CurvFunctionType;
  CurvatureType curvature( tractionScheme.forcing(), tractionScheme.rhs(), timeStup );
  CurvFunctionType curvFunction( "curvature", curvature, grydPart );

  typedef Dune::tuple< const typename TractionSchemeType::DiscreteFunctionType *, OutsideGridExactSolutionType * > OutsideIOTupleType;
  typedef Dune::tuple< const typename TractionSchemeType::DiscreteFunctionType *, CurvFunctionType *, OutsideGridExactSolutionType * > ForcingOutsideIOTupleType;
  OutsideIOTupleType outsideIoTuple( &(tractionScheme.solution()), &outsideGridExactSolution );
  ForcingOutsideIOTupleType forcingIoTuple( &(tractionScheme.forcing()), &curvFunction, &outsideGridExactSolution );
  typedef Dune::Fem::DataOutput< GrydType, OutsideIOTupleType > OutsideDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, ForcingOutsideIOTupleType > ForcingOutsideDataOutputType;
  OutsideDataOutputType outsideDataOutput( outsideGeoGrid, outsideIoTuple, DataOutputParameters( step, "surface" ) );
  ForcingOutsideDataOutputType forcingDataOutput( outsideGeoGrid, forcingIoTuple, DataOutputParameters( step, "forcing" ) );

  double minR,maxR;

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::AugmentedFunctionSpaceInsideType AugmentedFunctionSpaceInsideType;

  // type of the mathematical model used
  typedef Fluid< AugmentedFunctionSpaceInsideType > NSInsideProblemType;

  NSInsideProblemType nsInsideProblem( insideTimeProvider ) ;

  typedef NavierStokesModel< AugmentedFunctionSpaceInsideType, GridPartType, typename ALESchemeType::DiscreteFunctionType > NavierStokesModelType;

  // implicit model for left hand side
  NavierStokesModelType nsInsideImplicitModel( nsInsideProblem, gridPart, true, timeStup, aleScheme.meshVelocity() );

  // explicit model for right hand side
  NavierStokesModelType nsInsideExplicitModel( nsInsideProblem, gridPart, false, timeStup, aleScheme.meshVelocity() );

  // create NS scheme
  typedef NSScheme< NavierStokesModelType, NavierStokesModelType > NSSchemeType;
  NSSchemeType nsScheme( gridPart, nsInsideImplicitModel, nsInsideExplicitModel );


  // set-up Navier-Stokes data output
  typedef Dune::Fem::GridFunctionAdapter< NSInsideProblemType, GridPartType > NSInsideGridExactSolutionType;
  NSInsideGridExactSolutionType nsInsideGridExactSolution("NS inside exact solution", nsInsideProblem, gridPart, 5 );

  typedef typename NSSchemeType::DiscreteFunctionType DFT;
  typedef Divergence< DFT > DivergenceType;
  typedef Dune::Fem::LocalFunctionAdapter< DivergenceType > DivFunctionType;
  DivergenceType divergence( nsScheme.solution() );
  DivFunctionType divFunction( "divergence", divergence, gridPart );

  typedef Dune::tuple< const typename NSSchemeType::DiscreteFunctionType *, const typename NSSchemeType::DiscreteFunctionType *, DivFunctionType *, NSInsideGridExactSolutionType * > NsInsideIOTupleType;

  NsInsideIOTupleType nsInsideIoTuple( &(nsScheme.solution()), &(nsScheme.rhs()), &divFunction, &nsInsideGridExactSolution );
  typedef Dune::Fem::DataOutput< GridType, NsInsideIOTupleType > NsInsideDataOutputType;
  NsInsideDataOutputType nsInsideDataOutput( insideGeoGrid, nsInsideIoTuple, DataOutputParameters( step, "fluid" ) );

#if HAVE_BEMPP && CHARGED
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::ChargeFunctionSpaceOutsideType ChargeFunctionSpaceOutsideType;

  typedef CoulombModel< ChargeFunctionSpaceOutsideType, GrydPartType > SurfaceChargeModelType;

  SurfaceChargeModelType chargeModel( 0 );

  typedef DiffusionModel< ChargeFunctionSpaceOutsideType, GrydPartType > DummeySurfaceChargeModelType;

  typedef typename DummeySurfaceChargeModelType::ProblemType SurfaceChargeProblemType ;

  SurfaceChargeProblemType* chargeProblemPtr = new CoulombProblem< ChargeFunctionSpaceOutsideType > ();

  assert( chargeProblemPtr );
  SurfaceChargeProblemType& chargeProblem = *chargeProblemPtr ;

  typedef DropBemScheme< SurfaceChargeModelType > SurfaceChargeSchemeType;

  SurfaceChargeSchemeType chargeScheme( grydPart, fullGrydPart, chargeModel, sldb, "surface charge" );

  // setup data output
  typedef Dune::Fem::GridFunctionAdapter< SurfaceChargeProblemType, GrydPartType > SurfaceChargeExactSolutionType;
  SurfaceChargeExactSolutionType qExactSolution("outside exact charge solution", chargeProblem, grydPart, 5 );
  SurfaceChargeExactSolutionType qFullExactSolution("full outside exact charge solution", chargeProblem, fullGrydPart, 5 );

  typedef Dune::tuple< const typename SurfaceChargeSchemeType::DiscreteFunctionType *, const typename SurfaceChargeSchemeType::DiscreteFunctionType *, SurfaceChargeExactSolutionType * > ChargeIOTupleType;
  typedef Dune::tuple< const typename SurfaceChargeSchemeType::P1DiscreteFunctionType *, const typename SurfaceChargeSchemeType::NudgeVectorP1DiscreteFunctionType * > FullDirDataIOTupleType;
  typedef Dune::tuple< const typename SurfaceChargeSchemeType::DiscreteFunctionType *, const typename SurfaceChargeSchemeType::DiscreteFunctionType *, SurfaceChargeExactSolutionType * > FullChargeIOTupleType;
  typedef Dune::tuple< const typename SurfaceChargeSchemeType::P1DiscreteFunctionType * > DofRankIOTupleType;
  ChargeIOTupleType chargeIoTuple( &(chargeScheme.solution()), &(chargeScheme.rhs()), &qExactSolution );
  FullDirDataIOTupleType fullDirDataIoTuple( &(chargeScheme.fullDirData()), &(chargeScheme.fullVectorDirData()) );
  FullChargeIOTupleType fullChargeIoTuple( &(chargeScheme.fullSolution()), &(chargeScheme.fullRhs()), &qFullExactSolution );
  DofRankIOTupleType dofRankIoTuple( &(chargeScheme.dofRank()) );
  typedef Dune::Fem::DataOutput< GrydType, ChargeIOTupleType > ChargeDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, FullDirDataIOTupleType > FullDirDataDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, FullChargeIOTupleType > FullChargeDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, DofRankIOTupleType > DofRankDataOutputType;

  ChargeDataOutputType qDataOutput( outsideGeoGrid, chargeIoTuple, DataOutputParameters( step, "charge" ) );
  FullDirDataDataOutputType fullDirDataDataOutput( fullOutsideGeoGrid, fullDirDataIoTuple, DataOutputParameters( step, "full-dir-data" ) );
  FullChargeDataOutputType qFullDataOutput( fullOutsideGeoGrid, fullChargeIoTuple, DataOutputParameters( step, "full-charge" ) );
  DofRankDataOutputType dofRankDataOutput( fullOutsideGeoGrid, dofRankIoTuple, DataOutputParameters( step, "dofrank" ) );

  if( vtuout > 50 ) dofRankDataOutput.write( outsideTimeProvider );

#endif

  // initialize fluid velocities with quadrupole
  nsScheme.initialize();

  // set-up a more interesting initial condition
  insideInitialCondition.ReDe(Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ));
  outsideInitialCondition.ReDe(Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ));

  // interpolate the new more interesting initial condition
  insideDeformation.initialize( insideDeform, aleScheme.solution() );
  outsideDeformation.initialize( outsideDeform );
#if HAVE_BEMPP && CHARGED
  fullOutsideDeformation.initialize( outsideDeform );
#endif

  // initialize solution
  outsideDeformation.initializeSolution( tractionScheme.solution() );

  // set up a timer for the looping
  time_t before;
  time(&before);

  struct stat buffer;
  string name = "spot";

  double snaptime = Dune::Fem::Parameter::getValue< double >( "snap.time", 100000.0 );
  double snapjump = Dune::Fem::Parameter::getValue< double >( "snap.jump", 0.5 );
  double snapqual = Dune::Fem::Parameter::getValue< double >( "snap.qual", 100.0 );

  // Dune::FemFemCoupling::SurfaceExtractor<GridPartType , OutsideHostGridType > surfaceExtractor( gridPart );

  const bool diag = Dune::Fem::Parameter::getValue< bool >( "diag.nostics", false );
  typedef Dune::Fem::L2Norm< GrydPartType > NurmType;
  NurmType nurm( grydPart );
  double error; int al=0;

  // determine base line errors for reference
  double error1 = nurm.distance( qExactSolution, chargeScheme.dirichlet() );
  outsideProblem.setChoice(1);
  double error2 = nurm.distance( outsideGridExactSolution, tractionScheme.Rhs() );
  outsideProblem.setChoice(2);
  double error3 = nurm.distance( outsideGridExactSolution, tractionScheme.Rhs() );
  outsideProblem.setChoice(0);

  bool spot = (stat (name.c_str(), &buffer) == 0);
  unsigned int sample = std::round( 12.0 * endTime );

  // time loop, increment with fixed time step
  for( ; outsideTimeProvider.time() < endTime; outsideTimeProvider.next( timeStup ), al++ )
    {                                     // Surface Forcings
      // keep track of the droplet volume
      // double vol = surfaceExtractor.volume();
      // std::cout << "Volume = " << vol << "  " << 0.75*vol/M_PI << "  " << std::pow(0.75*vol/M_PI,1.0/3.0) << std::endl;
      // vol = std::pow(0.75*vol/M_PI,1.0/3.0);

    bool allowed = ( endTime < 4.0 ) || ( al%sample == 0 );
  
#if HAVE_BEMPP && CHARGED
    // setup the right hand side
    chargeScheme.prepare();
    chargeScheme.solve( true );

    if( diag )
    {
      chargeProblem.setNow(outsideTimeProvider.time());
      error = nurm.distance( qExactSolution, chargeScheme.dirichlet() );
      if( Dune::Fem::MPIManager::rank() == 0 )
	std::cout << "Error at time: " << outsideTimeProvider.time() << " is " << error << " ( " << 100.0 * error / error1 << " % ) ";
    }

    // solve once (we need to assemble the system the first time the method is called)
    chargeScheme.couple(nsScheme.solution());
#endif

    // use adapted surface mesh for MCF calculations
    outsideDeformation.move( tractionScheme.solution() );

    if( spot || ( ( vtuout > 2 ) && allowed ) )
    outsideDataOutput.write( outsideTimeProvider );

    // setup the right hand side
    tractionScheme.prepare();

    // solve once (we need to assemble the system the first time the method is called)
    tractionScheme.solve( true );

    int fixing = chargeScheme.wall( outsideDeformation.plane(), outsideDeformation.bias() );

    // restore surface mesh to original
    if( tractionScheme.notFirstTime() && ( !diag ) )
    outsideDeformation.move( tractionScheme.Rhs() );

    // just pulls velocity data off volume NS solution, so can be done after solving
    tractionScheme.couple(nsScheme.solution());

#if HAVE_BEMPP && CHARGED
    // apply any charge forcing
    tractionScheme.charge(chargeScheme.solution(),chargeScheme.rhs(),chargeScheme.dirichlet(),fixing,chargeScheme.spun(),chargeScheme.drift());
#endif

    if( diag )
    {
      outsideProblem.setChoice(1);
      error = nurm.distance( outsideGridExactSolution, tractionScheme.solution() );
      if( Dune::Fem::MPIManager::rank() == 0 )
        std::cout << " and " << error/timeStup << " ( " << 100.0 * error / error2 << " % ) ";
      outsideProblem.setChoice(2);
      error = nurm.distance( outsideGridExactSolution, tractionScheme.rhs() );
      if( Dune::Fem::MPIManager::rank() == 0 )
        std::cout << " and " << error/timeStup << " ( " << 100.0 * error / error3 << " % ) " << std::endl;
      tractionScheme.wipesol();
      outsideProblem.setChoice(0);
    }
    int snapval = 0; // take a snapshot of mesh + program state flag
    // take snapshot for remeshing if mesh quality has deteriorated (qual value too high)
    if( ( al > 3 ) && ( nsScheme.angMomNoReset() > snapqual ) && ( snapqual > 0.0 ) )
    {
      std::cout << "Terminating for remeshing ..." << std::endl;
      snapval = 2;
    }
    else if( std::abs( outsideTimeProvider.time() - std::abs( snaptime ) ) < 0.0001 )
    {
      snapval = 1; // snap flag non-zero if snap time
      if( ( snapjump > 0.0001 ) && ( snaptime > -0.0001 ) ) snaptime += snapjump;
    };
    // if( std::abs( outsideTimeProvider.time() - std::abs( snaptime ) - timeStup ) < 0.00001 ) snapval = 2; // snap flag non-zero if snap time
    if( ( snaptime < 0.0 ) && ( snapval == 1 ) )
    {
      snapval = -1; // negative on snaptime flags a read (NOT write) of snap data for debugging or whatever
      if( snapjump > 0.0001 ) snaptime = snapjump - snaptime;
    };
    const int snap = snapval; // use a const int to avoid accidents ...

    // turn the fluid velocities now held in tractionScheme Rhs into positions
    outsideDeformation.nudge( timeStup, tractionScheme.Rhs(), tractionScheme.forcing(), tractionScheme.fullForcing(), chargeScheme.drift(), fixing,chargeScheme.spun(), snap ); // , vol );

    if( spot || ( ( vtuout > 2 ) && allowed ) )
    forcingDataOutput.write( outsideTimeProvider );

    // outsideProblem.setCentre( outsideDeformation.cpYave() , outsideDeformation.cpZave(), outsideDeformation.bias(), outsideDeformation.zapCLForce() );

    tractionScheme.augment();
    nsInsideProblem.linmom( outsideDeformation.linmom() );
    
    double reset = outsideDeformation.reset();

    if( reset > -10000.0 )
    { // allow occasional mesh resetting if ellipsoidal for very long runs
      outsideInitialCondition.ReDe(reset);
      outsideDeformation.initialize( outsideDeform );
#if HAVE_BEMPP && CHARGED
      fullOutsideDeformation.initialize( outsideDeform );
#endif
      outsideDeformation.initializeSolution( tractionScheme.Rhs() );
      chargeScheme.zapSpun();
      nsScheme.wipesol();
    }

#if HAVE_BEMPP && CHARGED
    // copy the surface velocities on the ranked surface mesh to the full one
    chargeScheme.collate(tractionScheme.Rhs());
#endif

    if( snap != 0 ) nsScheme.predump(snap);

    // calculate the ellipsoid fit
    bool boom = outsideDeformation.abnfit(nsScheme.angMom(),fixing);

    outsideProblem.setCentre( outsideDeformation.cpYave() , outsideDeformation.cpZave(), outsideDeformation.currAngle(), outsideDeformation.bias(), outsideDeformation.area(), outsideDeformation.zapCLForce() );
    nsInsideProblem.setCentre( outsideDeformation.cpYave() , outsideDeformation.cpZave(), outsideDeformation.cpRmax(), outsideDeformation.curAng() );
    
#if HAVE_BEMPP && CHARGED
    // reset fissility after "explosion"
    if(boom) tractionScheme.explode();
#endif

    // setup the right hand side
    aleScheme.prepare();
    aleScheme.couple(tractionScheme.Rhs());

    // ALE solve to get nice distribution of interior nodes
    // avoid solving if just about to read-in data anyway (allows consistency checking to work properly)
    if( snap >= 0 ) aleScheme.solve( true );

    if( snap != 0 )
    { // data dumps
      chargeScheme.dump(snap);
      aleScheme.dump(snap);
      tractionScheme.dump(snap);
      nsScheme.dump(snap);
    };

    // update vertex positions of deforming volume + surface grid using the new surface solution
    insideDeformation.setTime(   insideTimeProvider.time(), aleScheme.solution() );
    outsideDeformation.setTime( outsideTimeProvider.time(), tractionScheme.Rhs() );
#if HAVE_BEMPP && CHARGED
    fullOutsideDeformation.setTime( outsideTimeProvider.time(), chargeScheme.fullVectorDirData() );
#endif

    if( reset > -10000.0 )
    { // allow occasional mesh resetting if ellipsoidal for very long runs
      insideInitialCondition.ReDe(reset);
      insideDeformation.initialize( insideDeform, aleScheme.solution() );
    }
                                         // Fluid Calculation
    // setup the right hand side
    nsScheme.prepare();
    
    if( spot || ( ( vtuout > 3 ) && allowed ) )
    fullForcingDataOutput.write( outsideTimeProvider );
    
    nsScheme.couple(tractionScheme.fullForcing());

    // Calculate new fluid velocities
    nsScheme.solve( true );

    if(spot && ( Dune::Fem::MPIManager::rank() == 0 ) ) remove( name.c_str() );
    
    if( false ) // outsideDeformation.peaked() )
    {
      insideInitialCondition.ReDe(outsideDeformation.ratio());
      outsideInitialCondition.ReDe(outsideDeformation.ratio());
      insideDeformation.initialize( insideDeform, aleScheme.solution() );
      outsideDeformation.initialize( outsideDeform );
      outsideDeformation.initializeSolution( tractionScheme.Rhs() );
    }
    // write data
#if HAVE_BEMPP && CHARGED
    if( spot || ( ( vtuout > 3 ) && allowed ) )
    qDataOutput.write(       outsideTimeProvider );
    if( spot || ( ( vtuout > 3 ) && allowed ) )
    qFullDataOutput.write( outsideTimeProvider );
#endif
    if( spot || ( ( vtuout > 3 ) && allowed ) )
    {
    insideDataOutput.write(   insideTimeProvider );
    // outsideDataOutput.write( outsideTimeProvider );
    }
#if HAVE_BEMPP && CHARGED
    if( spot || ( ( vtuout > 3 ) && allowed ) )
    fullDirDataDataOutput.write( outsideTimeProvider );
#endif
    if( spot || ( ( vtuout > 1 ) && allowed ) )
    nsInsideDataOutput.write( insideTimeProvider );
    // abort if remeshing required
    if( snapval == 2 ) break;
    // keep the time providers in sync
    insideTimeProvider.next( timeStup );
  }

  // determine how long it took to do the loop
  time_t after;
  time(&after);

  double seconds = difftime(after,before);

  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Solved after a time " << seconds << " secs." << std::endl;

#if HAVE_BEMPP && CHARGED
  delete chargeProblemPtr;
#endif
  return;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // add headers
  std::vector< std::string > femEocHeaders;
  femEocHeaders.push_back("radius error");

  // get eoc id
  const int eocId = 0;

  Dune::GridFactory<HGridType> factory;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  if( Dune::Fem::MPIManager::rank() == 0 )
  {
    std::cout << "Loading macro grid: " << gridfile << std::endl;
    Dune::GmshReader<HGridType>::read(factory, gridfile, true, true );
  }

  // Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType *gridPtr = factory.createGrid();

  HGridType& grid = *gridPtr ;

  // create special load balancer for 12 node case
#if HAVE_ZOLTAN
  typedef ZoltanLoadBalanceHandle<HGridType> BulkLoadBalancer;
#else
  typedef SimpleLoadBalanceHandle<HGridType> BulkLoadBalancer;
#endif
  BulkLoadBalancer bldb(grid);

  // do initial load balance
  if ( bldb.repartition() )
    grid.repartition( bldb );
  else
    grid.loadBalance();

  // initial grid refinement
  const float level = Dune::Fem::Parameter::getValue< float >( "droplet.level" );
  Dune::Fem::GlobalRefine::apply( grid, std::ceil( level*Dune::DGFGridInfo< HGridType >::refineStepsForHalf() ) );

  double timeStep = Dune::Fem::Parameter::getValue< double >( "droplet.timestep", 0.125 );
  algorithm( grid, timeStep, 0, eocId );

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

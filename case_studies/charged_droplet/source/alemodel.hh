#ifndef ALE_MODEL_HH
#define ALE_MODEL_HH

#include <dune/fem_bem_toolbox/fem_objects/model.hh>

template< class FunctionSpace, class GridPart >
struct AleModel : public DiffusionModel<FunctionSpace,GridPart>
{
  typedef DiffusionModel<FunctionSpace,GridPart> BaseType;

  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef ProblemInterface< FunctionSpaceType > ProblemType ;

  typedef typename BaseType::ProblemType InitialFunctionType;

  typedef Dune::Fem::TimeProviderBase TimeProviderType;

  //! constructor taking problem reference, time provider,
  //! time step factor( either theta or -(1-theta) ),
  //! flag for the right hand side
  AleModel( const ProblemType& problem,
             const GridPart &gridPart )
    : BaseType(problem,gridPart),
      elastic_( Dune::Fem::Parameter::getValue< double >( "ale.stic", 0.0 ) )
  {}

  template< class Entity, class Point >
  void source ( const Entity &entity,
                const Point &x,
                const RangeType &value,
                const JacobianRangeType &gradient,
                RangeType &flux ) const
  {
    linSource( value, entity, x, value, gradient, flux );
  }

  // the linearization of the source function
  template< class Entity, class Point >
  void linSource ( const RangeType& uBar,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    const DomainType xGlobal = entity.geometry().global( coordinate( x ) );
    RangeType f;
    BaseType::problem_.f(xGlobal,f);
    for (unsigned int i=0;i<flux.size();++i)
      flux[i] = f[i];
  }
  
  void fixSig() const
  {
    BaseType::problem_.fixSig();
  }
  
  //! return the diffusive flux
  template< class Entity, class Point >
  void diffusiveFlux ( const Entity &entity,
                       const Point &x,
                       const RangeType &value,
                       const JacobianRangeType &gradient,
                       JacobianRangeType &flux ) const
  {
    linDiffusiveFlux( value, gradient, entity, x, value, gradient, flux );
  }

  // linearization of diffusiveFlux
  template< class Entity, class Point >
  void linDiffusiveFlux ( const RangeType& uBar,
                          const JacobianRangeType& gradientBar,
                          const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          const JacobianRangeType &gradient,
                          JacobianRangeType &flux ) const
  {
    /*
    auto geo = entity.geometry();
    // Using     http://www2.washjeff.edu/users/mwoltermann/Dorrie/70.pdf       for cheap circumsphere radius calculation
    for( int i = 0; i < geo.corners()-1; ++i )
      flux[0][ i ] = ( geo.corner(3) - geo.corner(i) ).two_norm() * ( geo.corner((i+1)%3) - geo.corner((i+2)%3) ).two_norm();

    // Heron's Rule for area of triangle from edge lengths (and then cubed ...)
    double p = ( flux[0][0] + flux[0][1] + flux[0][2] ) / 2.0;
    p = std::pow( p * ( p - flux[0][0] ) * ( p - flux[0][1] ) * ( p - flux[0][2] ) , 1.5 );
    p = std::min( p , 200.0 );
    // ratio of volume of circumscribed sphere of tetrahedron to volume of tetrahedron 
    p = M_PI * p / ( 162.0 * std::pow( geo.volume() , 4.0 ) );
    */
    flux = 0.0;

    static const int dimDomain = RangeType::dimension;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      // velocity equations -- localRow = 0, 1, 2 = dimDomain - 1
      flux[ localRow ] += gradient[ localRow ];
      // flux[ localRow ][ localRow ] -= value[ dimDomain+1 ];
      if( elastic_ > 0.0 )
      {
      for( unsigned int localCol = 0; localCol < dimDomain; ++localCol )
      {
        flux[ localRow ][ localRow ] += elastic_ * gradient[ localCol ][ localCol ];
      }
      }
    }
  }

protected:
  double elastic_;
};
#endif // #ifndef ALE_MODEL_HH

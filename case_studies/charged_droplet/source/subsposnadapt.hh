#ifndef SUBSPOSNADAPT_HH
#define SUBSPOSNADAPT_HH

// combines the three component forcing vector with the MSP normal derivative to be used inside
template < class DFT , class outDFT >
struct SubsPosnAdapt// : public AddChargeForcing<DFT,DFT>
{
  //typedef AddChargeForcing<DFT,outDFT> BaseType;
  //typedef typename BaseType::RangeType RangeType;

  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;

  typedef Dune::Fem::FunctionSpace<double,double,DFT::DiscreteFunctionSpaceType::dimDomain,
                              outDFT::DiscreteFunctionSpaceType::dimRange> FunctionSpaceType;
  // range type that is required
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  // constructor
  SubsPosnAdapt( const DFT &f0  , const outDFT &f1 )
    : lf0_( f0 ),
      lf1_( f1 )
  {}

  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    typename outDFT::RangeType phi;
    typename DFT::RangeType psi;
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    ret = 999999;
    lf0_.evaluate( x, psi );
    lf1_.evaluate( x, phi );
    if( std::floor( psi[ 0 ] ) == 3 )
    {
      for( int i = 0; i < dimRange; ++i )
        ret[ i ] = psi[ i+1 ];
    }
    else
      for( int i = 0; i < dimRange; ++i )
        ret[ i ] = phi[ i ];
  }
  template< class Point >
  void jacobian ( const Point &x, JacobianRangeType &ret ) const
  {
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    typename DFT::JacobianRangeType phi;
    lf1_.jacobian( x, phi );
    int i = 0;
    for( ; i < dimensionworld; ++i )
      ret[ i ] = phi[ i ];
    for( ; i < dimRange; ++i )
      ret[ i ] = 0;
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf0_.init( entity );
    lf1_.init( entity );
  }

  private:
    typename DFT::LocalFunctionType lf0_;
    typename outDFT::LocalFunctionType lf1_;
};
#endif

#ifndef CHARGED_DROPLET_TRAITS_HH
#define CHARGED_DROPLET_TRAITS_HH

#include <dune/grid/sgrid.hh>


#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif


#include <dune/grid-glue/extractors/extractorpredicate.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim0extractor.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/lagrangeinterpolation.hh>
#include <dune/fem/space/lagrange.hh>
#include <dune/fem_bem_toolbox/data_communication/surfaceExtractor.hh>

#include <dune/grid-glue/extractors/codim0extractor.hh>
#include "deformation.hh"
#include "volumedeformation.hh"
#include <dune/fem/space/combinedspace/combineddiscretefunctionspace.hh>


namespace Dune
{
  namespace FemFemCoupling
  {
    // general definition of template class without implementation
    template <class Grid, bool special>
    struct Glue
    {
      // Grid Types
      typedef typename Dune::Fem::LeafGridPart<Grid> InsideHostGridPartType;

      typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, Grid::dimensionworld > InsideHostFunctionSpaceType;

      typedef Dune::Fem::LagrangeDiscreteFunctionSpace< InsideHostFunctionSpaceType, InsideHostGridPartType, 1 > InsideDiscreteHostFunctionSpaceType;

      typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< InsideDiscreteHostFunctionSpaceType > InsideHostDiscreteFunctionType;

      typedef VolumeDeformationDiscreteFunction< InsideHostDiscreteFunctionType > InsideDeformationType ;

      typedef Dune::GeometryGrid< Grid, InsideDeformationType > GridType;

      typedef typename Dune::Fem::LeafGridPart<GridType> GridPartType;
      typedef typename ExtractedSurfaceGrid< Grid >::type OutsideHostGridType;
      typedef Dune::Fem::LeafGridPart< OutsideHostGridType > OutsideHostGridPartType;
      typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, Grid::dimensionworld > OutsideHostFunctionSpaceType;

      typedef Dune::Fem::LagrangeDiscreteFunctionSpace< OutsideHostFunctionSpaceType, OutsideHostGridPartType, 1 > OutsideDiscreteHostFunctionSpaceType;

      // Function Spaces
      typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, Grid::dimensionworld > FunctionSpaceType;
      // Position Space for ALE Scheme
      typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, GridType::dimensionworld > FunctionSpaceInsideType;
      typedef Dune::Fem::LagrangeDiscreteFunctionSpace<   FunctionSpaceInsideType, GridPartType, POLORDER   > DiscreteFunctionSpaceInsideType;
      // Velocity + Pressure Space for NS scheme
      typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, GridType::dimensionworld+1 > AugmentedFunctionSpaceInsideType;

      typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< OutsideDiscreteHostFunctionSpaceType > OutsideHostDiscreteFunctionType;

      typedef SurfaceDeformationDiscreteFunction< OutsideHostDiscreteFunctionType > OutsideDeformationType ;

      typedef Dune::GeometryGrid< OutsideHostGridType, OutsideDeformationType > GrydType;

      typedef typename Dune::Fem::LeafGridPart< GrydType > GrydPartType;

      typedef Dune::Fem::FunctionSpace< double, double, GrydType::dimensionworld, GrydType::dimensionworld > FunctionSpaceOutsideType;

      #if HAVE_BEMPP
      typedef Dune::Fem::FunctionSpace< double, double, GrydType::dimensionworld, 1 > ChargeFunctionSpaceOutsideType;
      #endif

      typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceOutsideType, GrydPartType, POLORDER > DiscreteFunctionSpaceOutsideType;

      // Grid views
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
      typedef typename Dune::Fem::GridPart2GridView< GridPartType > VolDomGridView;
      typedef typename Dune::Fem::GridPart2GridView< GrydPartType > SurfDomGridView;
#else
      typedef typename Dune::Fem::GridPartView< GridPartType > VolDomGridView;
      typedef typename Dune::Fem::GridPartView< GrydPartType > SurfDomGridView;
#endif
      typedef typename std::conditional<special,SurfDomGridView,VolDomGridView>::type DomGridView;
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
      typedef typename Dune::Fem::GridPart2GridView< GrydPartType > TarGridView;
#else
      typedef typename Dune::Fem::GridPartView< GrydPartType > TarGridView;
#endif

      // Extractors
      typedef Dune::GridGlue::Codim1Extractor<DomGridView> VolDomExtractor;
      typedef Dune::GridGlue::Codim0Extractor<DomGridView> SurfDomExtractor;
      typedef typename std::conditional<special,SurfDomExtractor,VolDomExtractor>::type DomExtractor;

      typedef Dune::GridGlue::Codim0Extractor<TarGridView> TarExtractor;

      // for volume to surface gluing
      typedef Dune::GridGlue::GridGlue<DomExtractor,TarExtractor> GlueType;
    };


  } // namespace FemFemCoupling

} // namespace Dune

#endif

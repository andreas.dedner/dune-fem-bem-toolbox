#ifndef NS_FEMSCHEME_HH
#define NS_FEMSCHEME_HH

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>

// local includes
#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include "addpressure.hh"
#include <string>
#include <boost/lexical_cast.hpp>

using boost::lexical_cast;
using std::string;

// NSScheme
//-----------

template < class ImplicitModel, class ExplicitModel >
struct NSScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef ExplicitModel ExplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

  NSScheme( GridPartType &gridPart,
              const ImplicitModelType& implicitModel,
              const ExplicitModelType& explicitModel )
  : BaseType(gridPart, implicitModel),
    explicitModel_(explicitModel),
    explicitOperator_( explicitModel_, discreteSpace_, BaseType::constraints_ )
  {
  }

  template <class DF , class IOFT>
  void oi(unsigned int num, DF &solution,  IOFT &iof)
  {
    unsigned int span = DF::RangeType::dimension;
    typename DF::DofIteratorType end = solution.dend(); unsigned int i = 0;
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git, ++i )
    {
      iof << num << "  " << std::floor(float(i)/float(span))+1 << " w " << 1+i%span << " xxx " << lexical_cast<string>(*git) << std::endl;
    };
  }
  
  template <class DF , class IOFT>
  void io(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    
    typename DF::DofIteratorType end = solution.dend();
    for( typename DF::DofIteratorType git = solution.dbegin();
	       git != end; ++git )
    {
      getline(iof,line); *git = std::stod(line.erase(0,line.rfind("xxx")+4),&sz);
    };
  }
  
  void predump(const int snap)
  {
    std::ifstream isnapfile; std::ofstream osnapfile;
    if( snap < -1 ) osnapfile.open("snip.pre"); else if( snap > 0 ) osnapfile.open("snap.pre"); else if( snap < 0 ) isnapfile.open("snap.pre");

    if( snap > 0 ) // writing data out
    {
      double am = explicitOperator_.angMom(false);
      oid( am, osnapfile );
    }
    else if( snap < 0 ) // reading data in
    {
      double am;
      iod( am, isnapfile );
      explicitOperator_.amSet(am);
    };
    
    if( snap > 0 ) osnapfile.close(); else if( snap < 0 ) isnapfile.close();
  }
  
  void dump(const int snap)
  {
    std::ifstream isnapfile; std::ofstream osnapfile;
    if( snap < -1 ) osnapfile.open("snip.nav"); else if( snap > 0 ) osnapfile.open("snap.nav"); else if( snap < 0 ) isnapfile.open("snap.nav");

    if( snap > 0 ) // writing data out
    {
      oi( 1, solution_, osnapfile );
      oi( 2, rhs_, osnapfile );
    }
    else if( snap < 0 ) // reading data in
    {
      io( solution_, isnapfile );
      io( rhs_, isnapfile );
    };
    
    if( snap > 0 ) osnapfile.close(); else if( snap < 0 ) isnapfile.close();
  }
  
  void wipesol()
  {
    solution_.clear();
  }
  
  template <class DF , class IOFT>
  void oid(DF &solution,  IOFT &iof)
  {
    iof << lexical_cast<string>(solution) << std::endl;
  }
  
  template <class DF , class IOFT>
  void iod(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stod(line,&sz);
  }
  
  void prepare()
  {
    // apply explicit operator and also setup right hand side
    explicitOperator_( solution_, rhs_ );
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void couple(const OtherDiscreteFunctionType &otherSolution_)
  {
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = otherSolution_.space().gridPart();

    typedef AddPressure< OtherDiscreteFunctionType, DiscreteFunctionType > AddPressureType;
    typedef Dune::Fem::LocalFunctionAdapter< AddPressureType > AddPressureFunctionType;
    AddPressureType addpressure( otherSolution_ );
    AddPressureFunctionType addPressureFunction( "addpressure", addpressure, otherGridPart_ );

    BaseType::couple( addPressureFunction );
    // constraints()( solution_, addPressureFunction, rhs_ );
  }

  void initialize ()
  {
     Dune::Fem::LagrangeInterpolation
          < typename ExplicitModelType::InitialFunctionType, DiscreteFunctionType > interpolation;
     interpolation( explicitModel_.initialFunction(), solution_ );
  }

  double angMom() const
  {
    return explicitOperator_.angMom(true);
  }

  double angMomNoReset() const
  {
    return explicitOperator_.angMom(false);
  }

private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::constraints;
  using BaseType::solution_;
  using BaseType::implicitModel_;
  using BaseType::implicitOperator_;
  using BaseType::linearOperator_;
  using BaseType::solverEps_;
  using BaseType::rhs_;
  const ExplicitModelType &explicitModel_;
  typename BaseType::EllipticOperatorType explicitOperator_; // the operator for the rhs
};

#endif // end #if NS_FEMSCHEME_HH

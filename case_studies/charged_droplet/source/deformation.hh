#ifndef SURFACE_DEFORMATION_HH
#define SURFACE_DEFORMATION_HH

#include <dune/grid/geometrygrid/coordfunction.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parallel/mpicollectivecommunication.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <algorithm>    // std::sort
#include <vector>       // std::vector
#include <dune/fem/function/blockvectorfunction.hh>
#include "nr.h"
#include <string>
#include <boost/lexical_cast.hpp>

using boost::lexical_cast;
using std::string;

using namespace std;

struct myclass {
  template < class FitPointType >
  bool operator() (FitPointType i,FitPointType j) { return (i[2]<j[2]);}
} myobject;

  static std::vector<Dune::FieldVector<double,3>> * fitPointsGlobal_ = 0;

  void superplane(const DP ix, Vec_I_DP &aa, DP &y, Vec_O_DP &dyda)
  {
    DP x1 = (*fitPointsGlobal_)[ std::floor(ix) ][ 0 ];
    DP x2 = (*fitPointsGlobal_)[ std::floor(ix) ][ 1 ];
    y=0.0;
    if(aa.size()!=3) cerr << "superplane: wrong # of parameters\n";

    DP a=aa[0]; DP b=aa[1]; DP c=aa[2];
    y = a*x1+b*x2+c;

    dyda[0]=x1;
    dyda[1]=x2;
    dyda[2]=1.0;
  }

  void superellips(const DP x, Vec_I_DP &aa, DP &y, Vec_O_DP &dyda)
  {
    y=0.0;
    if(aa.size()!=3) cerr << "superellips: wrong # of parameters\n";

    DP a=aa[0]; DP b=aa[1]; DP n=aa[2];
    DP c1=x*x/(a*a); DP c2=(1.0-x*x)/(b*b);
    DP f = std::pow(c1,0.5*n) + std::pow(c2,0.5*n);
    y = std::pow(f,-1.0/n);

    dyda[0]=(std::pow(c1,0.5*n)*std::pow(f,-(1.0+n)/n))/a;
    dyda[1]=(std::pow(c2,0.5*n)*std::pow(f,-(1.0+n)/n))/b;
    DP dfdn = 0.5*std::pow(c1,0.5*n)*std::log(c1)+0.5*std::pow(c2,0.5*n)*std::log(c2);
    dyda[2]=std::pow(f,-1.0/n)*(std::log(f)/(n*n) -dfdn/(n*f));
  }

// SurfaceDeformationCoordFunction
// ------------------------

template < class SurfaceEvolution >
class SurfaceDeformationCoordFunction
  : public Dune::AnalyticalCoordFunction< double, 3, 3, SurfaceDeformationCoordFunction< SurfaceEvolution > >
{
  typedef Dune::AnalyticalCoordFunction< double, 3, 3, SurfaceDeformationCoordFunction< SurfaceEvolution > > BaseType;

public:
  typedef SurfaceEvolution SurfaceEvolutionType;
  typedef Dune::Fem::FunctionSpace< double, double, 3, 3 > FunctionSpaceType;

  typedef typename SurfaceEvolutionType::DomainType DomainVector;
  typedef typename SurfaceEvolutionType::RangeType RangeVector;
  typedef double RangeFieldType;

  SurfaceDeformationCoordFunction ( SurfaceEvolution &surfaceEvolution )
    : surfaceEvolution_( surfaceEvolution )
  {}

  void evaluate ( const DomainVector &x, RangeVector &y ) const
  {
    surfaceEvolution_.evaluate( x, y );
  }

  void setTime( const double time )
  {
    surfaceEvolution_.setTime( time );
  }

private:
  SurfaceEvolution& surfaceEvolution_;
};

template <class DiscreteFunctionType>
class SurfaceDeformationDiscreteFunction
: public Dune::DiscreteCoordFunction< double, 3, SurfaceDeformationDiscreteFunction< DiscreteFunctionType > >
{
public:
class Monitor
{
public:
  Monitor(const double step)
  : step_( step )
  , dura_( Dune::Fem::Parameter::getValue< double >( "moni.time", 0.0 ) )
  , tol_( Dune::Fem::Parameter::getValue< double >( "moni.tol", -1.0 ) )
  , freeFloating_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) < -900.0 )
  , bodyFriction_( Dune::Fem::Parameter::getValue< double >( "wall.friction", 100.0 ) < 0.0 )
  , store_(0)
  , sture_(0)
  , size_(0)
  , posn_(0)
  , multi_(1.0)
  , toggle_(-10001.0)
  { 
    size_ = std::ceil( dura_ / step_ );
    if( size_ > 1 )
    {
      store_ = new double[size_]; // holds raw input data given to be monitored
      sture_ = new double[size_]; // holds cyclically smoothed monitor data
      for( int i = 0; i < size_; ++i )
      {
        store_[ i ] = - 999.9;
        sture_[ i ] = - 999.9;
      };
    }
    else
      multi_ = 0.0;
  }
  ~Monitor()
  {
    if( size_ > 1 ) delete(store_);
    if( size_ > 1 ) delete(sture_);
    store_ = 0;
    sture_ = 0;
  }
public:
  double check(double value)
  {
    double smoothed = 0.0; int skip = 0; double dermom = 0.0;
    if( size_ > 1 )
    {
      bool passed = true;
      if( std::abs( value ) > tol_ )
      {
        for( int i = 0; i < size_; ++i )
	{
          if( std::abs( value - store_[ i ] ) > tol_ ) passed = false;
	  if( store_[ i ] < -900.0 ) skip++;
	  else smoothed += store_[ i ];
	};
      }
      else
        passed = false;
      if( passed )
      {
        for( int i = 0; i < size_; ++i )
          store_[ i ] = - 999.9;
        multi_ += 1.0;
        if( freeFloating_ ) toggle_ = value;
      };
      // cyclically smoothed momentum = average value in entire stored history
      if( skip < size_ ) smoothed /= double( size_ - skip );
      store_[posn_] = value;
      if( sture_[ posn_ ] > -900.0 ) dermom = ( smoothed - sture_[posn_] ) / dura_;
      sture_[posn_] = smoothed;
      posn_++;
      posn_%=size_;
    }
    else
      multi_ += step_;
    // keep smoothed momentum available ...
    if( ! freeFloating_ ) toggle_ = smoothed;
    if ( bodyFriction_ )
      return smoothed;
    else
      return dermom;
  }
  double reset()
  {
    double temp = -10001.0;
    if( freeFloating_ )
    {
      temp = toggle_;
      toggle_ = -10001.0;
    };
    return temp;
  }
  double multi() const
  {
    return multi_;
  }
  bool body() const
  {
    return bodyFriction_;
  }
  double mom() const
  {
    return toggle_;
  }
  
  template <class DF , class IOFT>
  void oi(DF &solution,  IOFT &iof)
  {
    iof << lexical_cast<string>(solution) << std::endl;
  }
  
  template <class DF , class IOFT>
  void io(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stod(line,&sz);
  }
  
  template <class DF , class IOFT>
  void ioI(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stoi(line,&sz);
  }
  
  void dump(const int snap)
  {
    std::ifstream isnapfile; std::ofstream osnapfile;
    if( snap < -1 ) osnapfile.open("snip.mon"); else if( snap > 0 ) osnapfile.open("snap.mon"); else if( snap < 0 ) isnapfile.open("snap.mon");

    if( snap > 0 ) // writing data out
    {
      if( size_ > 1 ) for( int i = 0; i < size_; ++i ) oi( store_[ i ], osnapfile );
      if( size_ > 1 ) for( int i = 0; i < size_; ++i ) oi( sture_[ i ], osnapfile );
      oi( posn_, osnapfile );
      oi( multi_, osnapfile );
      oi( toggle_, osnapfile );
    }
    else if( snap < 0 ) // reading data in
    {
      if( size_ > 1 ) for( int i = 0; i < size_; ++i ) io( store_[ i ], isnapfile );
      if( size_ > 1 ) for( int i = 0; i < size_; ++i ) io( sture_[ i ], isnapfile );
      ioI( posn_, isnapfile );
      io( multi_, isnapfile );
      io( toggle_, isnapfile );
    };
    
    if( snap > 0 ) osnapfile.close(); else if( snap < 0 ) isnapfile.close();
  }

private:
  const double step_;
  const double dura_;
  const double tol_;
  double *store_;
  double *sture_;
  int size_;
  int posn_;
  double multi_;
  double toggle_;
  const bool freeFloating_;
  const bool bodyFriction_;
};

  typedef Dune::DiscreteCoordFunction< double, 3, SurfaceDeformationDiscreteFunction< DiscreteFunctionType > > BaseType;

  typedef typename DiscreteFunctionType :: GridType GridType ;
  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType ;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;

  typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType ;
  typedef typename DiscreteFunctionType :: RangeType  RangeType ;

  typedef typename Dune::FieldVector<double,GridPartType::dimensionworld> FitPointType;

protected:
  typedef typename DiscreteFunctionSpaceType::BlockMapperType BlockMapperType;
  static const unsigned int blockSize = DiscreteFunctionSpaceType::localBlockSize;

  typedef typename DiscreteFunctionType::DofBlockPtrType DofBlockPtrType;

public:
  SurfaceDeformationDiscreteFunction ( GridType &grid, double &timeStep )
  : gridPart_( grid ),
    space_( gridPart_ ),
    fittol_( Dune::Fem::Parameter::getValue< double >( "fit.tol", 1.0e-4 ) ),
    timeStore_( 0.0 ),
    fitCount_( 0 ),
    track_( 0 ),
    de_( Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ) ),
    critRatio_( Dune::Fem::Parameter::getValue< double >( "critical.ratio", 2.5   ) ),
    boomed_( false ),
    retVal_( 0 ),
    ejectQ_( 0.71 ),
    ejectP_( 0.005 ),
    ejectG_( -0.5 ),
    ejectK_( 1.0 ),
    ejectC_( 0.0 ),
    small_( false ),
    snall_( false ),
    initiate_( Dune::Fem::Parameter::getValue< double >( "initi.ate", 4   ) ),
    wonk_( Dune::Fem::Parameter::getValue< double >( "initi.wonk", 1.1   ) ),
    fitres_( std::pow( 10.0, Dune::Fem::Parameter::getValue< double >( "fit.res", 3.0   ) ) ),
    wallFact_( Dune::Fem::Parameter::getValue< double >( "wall.fact", 1.0   ) ),
    wallStick_( Dune::Fem::Parameter::getValue< double >( "wall.stick", 0.0   ) ),
    wallFriction_( std::max( 0.0 , Dune::Fem::Parameter::getValue< double >( "wall.friction", 100.0   ) ) / 1.0e10 ),
    slipLength_( Dune::Fem::Parameter::getValue< double >( "slip.length", 1.0 ) ),
    wallDamp_( Dune::Fem::Parameter::getValue< double >( "wall.damp", 1.0   ) ),
    wall_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0   ) ),
    equiAngle_( Dune::Fem::Parameter::getValue< double >( "equi.ang", 90.0   ) ),
    cluster_( Dune::Fem::Parameter::getValue< double >( "clu.ster", 1.0   ) ),
    summitFlag_( 0 ),
    buildUp_( 0.0 ),
    prevRatio_( 0.0 ),
    cpYave_( 0.0 ),
    cpZave_( 0.0 ),
    cpYmax_( 0.0 ),
    cpZmax_( 0.0 ),
    surfTensCoeff_( 1.0 / Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
    zapCLForce_( true ),
    top_( 1.0e5 ),
    prevTop_( 0.0 ),
    mid_( 1.0e5 ),
    prevMid_( 0.0 ),
    monitor_( timeStep ),
    bot_( -1.0e5 ),
    prevBot_( 0.0 ),
    corrX_( 0.0 ),
    corrY_( 0.0 ),
    corrZ_( 0.0 ),
    speed_( 0.0 ),
    summitAng_( Dune::Fem::Parameter::getValue< double >( "summit.angle", 10.0 ) ),
    totArea_( 0.0 ),
    totAveX_( 0.0 ),
    totAveY_( 0.0 ),
    totAveZ_( 0.0 ),
    usePlaneInterpolation_( false ),
    plane_( 5 ),
    planeA_( 0.0 ),
    planeB_( 0.0 ),
    planeC_( 0.0 ),
    point_( 0.0 ),
    ga_( 0.0 ),
    gb_( Dune::Fem::Parameter::getValue< double >( "grav.ang", 0 ) ),
    tol_( Dune::Fem::Parameter::getValue< double >( "fix.tol", 0.01 ) ),
    marker_(-1),
    bias_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) ),
    biasFactor_( Dune::Fem::Parameter::getValue< double >( "wall.loc", -1000.0 ) ),
    cosa_( 0.0 ),
    RR_( 0.0 ),
    hh_( 0.0 ),
    dca_( 0.0 ),
    curAng_( 45.0 ),
    moveStore_( 0.0 ),
    fixing_( 0 )
  {
    if( wall_ > -400.0 )
    {
      // using a requested starting contact angle given in degrees, so must be calibrating ...
      if( cluster_ > 5 ) equiAngle_ = cluster_;
      // desired contact area: http://mathworld.wolfram.com/SphericalCap.html
      cosa_ = std::cos( M_PI * equiAngle_ / 180.0 );   // cosine of desired contact angle
      double sina = std::sin( M_PI * equiAngle_ / 180.0 );   // sine of desired contact angle
      RR_ = std::pow( 4.0 / ( 2.0 - 3.0 * cosa_ + cosa_ * cosa_ * cosa_ ) , 1.0 / 3.0 );
      hh_ = ( 1.0 - cosa_ ) * RR_;
      dca_ = M_PI * hh_ * ( 2.0 * RR_ - hh_ ); // desired contact area
      // square of cosine of summit angle
      summitAng_ = 1.0 - std::pow( std::cos( M_PI * summitAng_ / 180.0 ) , 2.0 );
      // set-up wall stickiness
      wallStick_ *= ( 1.0 + cosa_ ) * timeStep;
    }	
    fitPointsGlobal_ = new std::vector<FitPointType>;
    vertices_ = new DiscreteFunctionType( "deformation", space_ );
    // open file to hold the fit data
    if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_.open("fit.dat");
    ratioStore_.assign(3,0);

    // wall tilt angle (in degrees) at origin
    ga_ = std::floor( gb_ );
    // increase in tilt angle per unit of increasing non-dimensional z-coordinate
    gb_  = 1000.0 * ( gb_ - ga_ );
  }

  ~SurfaceDeformationDiscreteFunction()
  {
    delete(fitPointsGlobal_); fitPointsGlobal_ = 0;
    if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_.close();
  }

  // return linear momentum if needed
  const double linmom() const
  {
    return monitor_.mom();
  }

  const Monitor &monitor() const
  { 
    return monitor_;
  }

  template < class RangeType, class DFunctionType >
  struct CoordFunctor
  {
    RangeType& y_;
    DFunctionType * function_;

    CoordFunctor( RangeType& y, DFunctionType* function )
      : y_( y )
      , function_( function )
    {
    }

    template <class GlobalKey>
    void operator () ( const size_t local, const GlobalKey& globalKey )
    {
      DofBlockPtrType blockPtr = function_->block( globalKey );
      for( unsigned int j = 0; j < blockSize; ++j )
      {
        y_[ j ] = (*blockPtr)[ j ];
      }
    }

    void fill(RangeType& y)
    {
      for( unsigned int j = 0; j < blockSize; ++j )
      {
        y[ j ] = y_[ j ];
      }
    }
  };

  template< class HostEntity , class RangeVector >
  void evaluate ( const HostEntity &hostEntity, unsigned int corner,
                  RangeVector &y ) const
  {
    CoordFunctor< RangeVector, DiscreteFunctionType > coord ( y, vertices_);

    vertices_->space().blockMapper().mapEachEntityDof( hostEntity, coord );
    coord.fill ( y );
  }

  template <class RangeVector>
  void evaluate ( const typename GridType :: template Codim<0>::Entity &entity,
		              unsigned int corner,
                  RangeVector &y ) const
  {
    typedef typename GridType::ctype  ctype;
    enum { dim = GridType::dimension };

    const Dune::ReferenceElement< ctype, dim > &refElement
      = Dune::ReferenceElements< ctype, dim >::general( entity.type() );

    LocalFunctionType localVertices = vertices_->localFunction( entity );

    localVertices.evaluate( refElement.position( corner, dim ), y );
  }

  template <class DF>
  void setTime( const double time, DF &solution )
  {
    const typename DiscreteFunctionType::DofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    timeStore_ = time;
    for( typename DiscreteFunctionType::DofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
      *it = *git;
  }

  template <class DF>
  void move( DF &solution )
  {
    const typename DiscreteFunctionType::DofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::DofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
      *it = *git;
  }

  template <class GF>
  void initialize(const GF& gf)
  {
    typedef Dune::Fem::LagrangeInterpolation< GF, DiscreteFunctionType > VertexInterpolationType;
    VertexInterpolationType interpolation;
    interpolation( gf, *vertices_ );
  }

  template <class DF>
  void initializeSolution(DF &solution)
  {
    const typename DiscreteFunctionType::ConstDofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::ConstDofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
      *git = *it;
  }

  int plane() const
  {
    return plane_;
  }
  
  template <class DF , class ADF>
  void nudge(double & ts, DF &solution, DF &forcing, ADF &flag, Dune::FieldVector<double,4> &drift, int &fixing, const double &spun, const int snap) // , double &volrad)
  {
    Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> maxMinComm(Dune::MPIHelper::getCommunicator());
    prevTop_ = maxMinComm.max(top_); prevBot_ = maxMinComm.min(bot_); prevMid_ = maxMinComm.max(mid_);
    // will need an average gradient of nodes around the tip
    double gradientAccumulator = 0;
    // gradient accumulator count
    int gac = 0;
    top_ = 0.0; bot_ = 0.0; mid_ = 0.0; if( plane_ == 3 ) fixing_ = fixing;
    // no changing fix coordinates midway through a fix
    if( fixing != 0 ) fixing = fixing_;
    unsigned int count = 0; if( plane_ == 0 ) { marker_ = -1; fixing_ = 0; };
    initiate_--;

    // find the drop centre
    totAveX_ = maxMinComm.sum(drift[0]); totAveY_ = maxMinComm.sum(drift[1]);
    totAveZ_ = maxMinComm.sum(drift[2]); totArea_ = maxMinComm.sum(drift[3]);

    string line; std::string::size_type sz; std::ifstream isnapmesh; std::ofstream osnapmesh;
    if( snap < -1 ) osnapmesh.open("snip.msh"); else if( snap > 0 ) osnapmesh.open("snap.msh"); else if( snap < 0 ) isnapmesh.open("snap.msh");

    // calculate and reset averaging
    if( prevMid_ < 90000 )
    {
      // if( wall_ > -900.0 ) speed_ = -std::sqrt( corrX_*corrX_ + corrY_*corrY_ + corrZ_*corrZ_ ) / ts;
      if( wall_ > -400.0 )
      {
	bias_ = biasFactor_ - 2.0 * tol_;
	biasFactor_ = 0.0;
      }
      else if( totArea_ > 0.0 )
      {
      corrX_ = - totAveX_ / totArea_; corrY_ = - totAveY_ / totArea_; corrZ_ = - totAveZ_ / totArea_;
      }
      else
      {
        corrX_ = 0.0; corrY_ = 0.0; corrZ_ = 0.0;
      }
      // if( wall_ > -900.0 ) speed_ += std::sqrt( corrX_*corrX_ + corrY_*corrY_ + corrZ_*corrZ_ ) / ts;
    }

    double push = 0; bool noLeaveSurface = false; double nx, ny, nz, na, nb; bool toBeFixed;
    // Surface tension ratio between fluid/gas and fluid/solid at triple line contact point (CP)
    bool onContactLine = false; double cpYsum = 0.0, cpZsum = 0.0, cpYmax = 0.0, cpZmax = 0.0, ratio = 0.0; 
    // keep averages of mean curvature and contact angle going
    double avCang = 0.0; double avMean = 0.0; double avCount = 0.0;

    double yExtra = 0.0; double zExtra = 0.0; int flg;

    // int rank = Dune::Fem::MPIManager::rank();

    const typename DiscreteFunctionType::ConstDofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    typename DF::DofIteratorType hit = forcing.dbegin();
    typename DF::DofIteratorType hitp = forcing.dbegin();
    typename DF::DofIteratorType hitpp = forcing.dbegin();
    typename DiscreteFunctionType::ConstDofIteratorType itpp = vertices_->dbegin();
    typename ADF::DofIteratorType kit = flag.dbegin();
    typename ADF::DofIteratorType kitp = flag.dbegin();
    typename ADF::DofIteratorType kitpp = flag.dbegin();
    typename ADF::DofIteratorType kitppp = flag.dbegin();
    ++itpp; ++itpp; ++hitp; ++hitpp; ++hitpp; ++kitp; ++kitpp; ++kitpp; ++kitppp; ++kitppp; ++kitppp;
    for( typename DiscreteFunctionType::ConstDofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git, ++hit )
    // const typename DF::DofIteratorType end = solution.dend();
    // typename DiscreteFunctionType::DofIteratorType it = vertices_->dbegin();
    // for( typename DF::DofIteratorType git = solution.dbegin();
    //         git != end; ++it, ++git )
    {
      // parallel runs can come in here with *apparently* unset vertex positions
      // ( "silenced" parts of full grid, done for the grid partitioning, still appear here, and with "inf"s ...)
      if( *it > 1.0e9 ) continue;

      // std::cout << "ABC  " << *git << "  " << *it;

      track_++;
      // determine (x-axis directed) wall reaction force proportional to wall penetration distance
      if( track_ == 1 )
      {
        double x = *hit; double y = *hitp; double z = *hitpp; flg = std::floor(*kit); ny = *kitp; nz = *kitpp; toBeFixed =  false; count++;
        nx = *kitppp;
	if( fixing_ == 2 ){na=nz;nb=nx;} else if( fixing_ == 3 ){na=nx;nb=ny;} else {na=ny;nb=nz;};
	summitFlag_ = 0; // flag for if node at summit of drop, and if so, its nature (convex = +1 or concave = -1)
	if( summitAng_*x*x > y*y + z*z ){ if( x < 0 ) summitFlag_ = 1; else summitFlag_ = -1; };
        if( flg == 1 ) // on the contact line
        {
          onContactLine = true;
	  if( count == marker_ ) { toBeFixed = true; marker_ = -1; };
          if( zapCLForce_ ) *hit = 0.0;
          if( fixing == 0 ){avCount += 1.0; }; // just for output purposes
          // if( (planeA_[1]==0.0)&&(planeB_[1]==0.0)&&(planeC_[1]==0.0) )
	  // {
	  yExtra = 0.0; zExtra = 0.0;
          // fit plane points
	  if( fixing == 0 ){ point_[ 0 ] = ny; point_[ 1 ] = nz; }
          else             { point_[ 0 ] = na; point_[ 1 ] = nb; };// y|z-coordinate of corres. host point for plane fitting
        }
        else if( ( flg == 4 ) || ( count == marker_ ) ) // degenerate element node to be moved
	{
          noLeaveSurface = false; onContactLine = false; toBeFixed = true; if( flg == 4 ) marker_ = count; else
	  {
	    marker_ = -1;
	    if( flg == 3 )
	    {
            if( (planeA_[3]==0.0)&&(planeB_[3]==0.0)&&(planeC_[3]==0.0) )
              *kit = 5.0;
            else
	    {
              // set-up adapted substrate wall position
              *kitpp  = planeA_[2] * ny + planeB_[2] * nz + planeC_[2];
              *kitppp = planeA_[3] * ny + planeB_[3] * nz + planeC_[3];
	      double yyy = std::min(1.0,std::max(0.0,std::abs(*kitpp -cpYave_)/cpYmax_));
	      double zzz = std::min(1.0,std::max(0.0,std::abs(*kitppp-cpZave_)/cpZmax_));
              ratio      = std::min(1.0,std::max(0.0,   1.0 - yyy*yyy - zzz*zzz       ));

	      // double yyyy = wallDamp_ * ( *kitpp  - cpYave_ - corrY_ ) * cpZmax_ / cpYmax_;
	      // double zzzz = wallDamp_ * ( *kitppp - cpZave_ - corrZ_ ) * cpYmax_ / cpZmax_;
              *kitp = bias_; // - std::max( 0.0, RR_ - hh_ + std::sqrt( std::max( 0.0, RR_*RR_ - yyyy*yyyy - zzzz*zzzz ) ) );
            }
	    }
	  }
	}  
        else if( flg == 3 ) // on substrate inside contact line
	{ 
          noLeaveSurface = true; onContactLine = false;
	  // zap any artificial vertical force from any displacement of surface for traction calcs
	  *hit = 0.0;
        
          if( (planeA_[3]==0.0)&&(planeB_[3]==0.0)&&(planeC_[3]==0.0) )
            *kit = 5.0;
          else
	  {
            // set-up adapted substrate wall position
            *kitpp  = planeA_[2] * ny + planeB_[2] * nz + planeC_[2];
            *kitppp = planeA_[3] * ny + planeB_[3] * nz + planeC_[3];
	    double yyy = std::min(1.0,std::max(0.0,std::abs(*kitpp -cpYave_)/cpYmax_));
	    double zzz = std::min(1.0,std::max(0.0,std::abs(*kitppp-cpZave_)/cpZmax_));
            ratio      = std::min(1.0,std::max(0.0,   1.0 - yyy*yyy - zzz*zzz       ));

	    // double yyyy = wallDamp_ * ( *kitpp  - cpYave_ - corrY_ ) * cpZmax_ / cpYmax_;
	    // double zzzz = wallDamp_ * ( *kitppp - cpZave_ - corrZ_ ) * cpYmax_ / cpZmax_;
            *kitp = bias_; // - std::max( 0.0, RR_ - hh_ + std::sqrt( std::max( 0.0, RR_*RR_ - yyyy*yyyy - zzzz*zzzz ) ) );
          }
        }
        // not on or in substrate, but still with x-component
	else
	{
          push = 0; onContactLine = false;
          noLeaveSurface = ( *git < 0.0 ) && ( fixing == 0 ) && ( *it < wall_ );
        };
	// keep track of the deepest substrate penetration
	if( *it < biasFactor_ ) biasFactor_ = *it;
        if( *it < wall_ ) 
	{
	  noLeaveSurface = true;
          // reaction force from the substrate surface wall
          push = wallFact_ * ( wall_ - *it ); // -std::min( 0.0 , *git );
        };
	if( push != 0 )
          *hit += push * ts;
	// add surface stick force to exactly balance any upwards forces at the contact line,
	// but only up to a critical value dependent on the desired sessile drop equilibrium angle 
	if( onContactLine && ( *hit > 0.0 ) && ( *hit < wallStick_ ) )
	  *hit = 0.0;
        // accumulate contact area of drop with substrate
        // if( noLeaveSurface ) contArea += area;
        
        ++hitp;++hitpp;++kit;++kitp;++kitpp;++kitppp;
        ++hitp;++hitpp;++kit;++kitp;++kitpp;++kitppp;
        ++hitp;++hitpp;++kit;++kitp;++kitpp;++kitppp;
                       ++kit;++kitp;++kitpp;++kitppp;
      }
      // on or off substrate surface with y or z (tangential) component
      else 
      {
        // apply tension ratio if on contact line
        if( zapCLForce_ && onContactLine ) *hit = 0.0;
        // if with y or z component on the contact line
        if( onContactLine && ( fixing == 0 ) )
	{
          // build-up an average y and z coordinate for contact line points
          if( track_ == 2 ) { cpYsum += *it; cpYmax = std::max(cpYmax,std::abs(*it-cpYave_)); }
          else              { cpZsum += *it; cpZmax = std::max(cpZmax,std::abs(*it-cpZave_)); }
        }
        if( track_ == 2 ) { ++itpp; ++itpp; ++itpp; };
      };

      // surface reaction force should never be able to push a node off of the surface, nor should a 
      // node be allowed off if MCF force less than a critical amount based on the dynamic contact angle
      if( ( noLeaveSurface || ( ( fixing != 0 ) && ( *it < wall_ + tol_ ) ) ) && ( track_ == 1 ) )
	*git = std::min( wall_ + tol_ / 2.0 , *it + ts * *git );
      else if( noLeaveSurface && ( flg != 2 ) && ( flg != 1 ) && ( track_ == plane_+2 ) && usePlaneInterpolation_ )
      {
	double fVal = *it + ts * *git; // value if just allowed to move with the local fluid velocity
        double pVal = planeA_[plane_] * ny + planeB_[plane_] * nz + planeC_[plane_]; // use y, z coords from y, z fit plane
        *git = ( 1.0 - ratio ) * fVal + ratio * pVal; // move from using plane value to fluid value away from contact centre
      }
      else if( toBeFixed && ( track_ == fixing_ ) )
      {
	if( fixing == fixing_ )
	{
          moveStore_[track_-1] += ts * *git;
	  *git = *it;
	}
	else
	{
	  *git = *it + moveStore_[track_-1] + ts * *git;
          moveStore_[track_-1] = 0.0;
	};
      }
      else if( toBeFixed )
      {
	// store all velocity induced mesh node movements for application *after* everything has been fixed
	moveStore_[track_-1] += ts * *git;
	// std::cout << "gggg " << marker_ << "  " << count << " == " << track_ << "   " << plane_ << "   " << fixing_;
	if( ( ( plane_ + fixing_ - 1 ) % 3 == track_ - 1 ) && usePlaneInterpolation_ )
	{
          *git  = planeA_[plane_] * na + planeB_[plane_] * nb + planeC_[plane_];
	  // std::cout << "   hhhh " << na << "   " << nb << " ++ " << *git << " ++ ";
	}
	else
	{
	  // if nodal coordinate temporarily frozen for fixing ... 
	  *git = *it;   // (new position = old position)
	};
	
	if( fixing != fixing_ )
	{
	  *git += moveStore_[track_-1];
          moveStore_[track_-1] = 0.0;
	};
	// std::cout << std::endl;
      }
      else if( ( ( *it >= prevTop_ ) || ( *it <= prevBot_ ) ) && ( initiate_ > 0 ) && small_ && snall_ )
	// initial tiny surface imperfections to get explosion axis aligned with z-axis through charge build-up at poles
        *git = *it + wonk_ * ts * *git;
      else
	*git = *it + ts * *git;

      // summits definitely not near the wall
      if( ( *git < wall_ + 10.0*tol_ ) && ( track_ == 1 ) ) summitFlag_ = 0;

      bool bob = *git > 1.0e+05;

      if( snap > 0 )
      {
        oi( count, (track_-1)*10+1, *git, osnapmesh ); oi( count, (track_-1)*10+2, point_[0], osnapmesh ); oi( count, (track_-1)*10+3, point_[1], osnapmesh ); oi( count, (track_-1)*10+4, flg, osnapmesh ); oiL( count, (track_-1)*10+5, onContactLine, osnapmesh ); oi( count, (track_-1)*10+6, fixing, osnapmesh ); oi( count, (track_-1)*10+7, plane_, osnapmesh );	oi( count, (track_-1)*10+8, yExtra, osnapmesh ); oi( count, (track_-1)*10+9, zExtra, osnapmesh ); oi( count, (track_-1)*10+10, summitFlag_, osnapmesh );
      }
      else if( snap < 0 )
      {
	double prev = *git;
        io( *git, isnapmesh ); io( point_[0], isnapmesh ); io( point_[1], isnapmesh ); ioI( flg, isnapmesh ); ioL( onContactLine, isnapmesh ); ioI( fixing, isnapmesh ); ioI( plane_, isnapmesh ); io( yExtra, isnapmesh ); io( zExtra, isnapmesh ); ioI( summitFlag_, isnapmesh );
	if( std::abs( *git - prev ) > tol_ ) std::cout << "Mismatch in surface node coordinate pick-up: " << track_ << "   " << prev << " != " << *git << std::endl;
      }

      // std::cout << "  " << *git << "  " << ts << "  " << bob << "  " << rank << std::endl;

      // pick-up the coordinates for the (potential) fit points
      if( track_ == 1 )
      {
	if(      ( summitFlag_ > 0 ) && ( ( *git > top_ ) || ( top_ < tol_ ) ) ) top_ = *git; // if a convex summit, keep track of the highest value there
	else if( ( summitFlag_ < 0 ) && ( ( *git < top_ ) || ( top_ < tol_ ) ) ) top_ = *git; // but if concave, then track the *lowest* value *at the summit*
        // x-coordinate
	//if( ( *git > top_ ) && ( wall_ > -400.0 ) ) bot_ = *git;
        if( wall_ < -900.0 ) { *git += corrX_; point_[ 0 ] = *git; }
        else if( onContactLine && ( flg != 4 ) && ( ( ( fixing == 2 ) && ( plane_ == 4 ) ) || ( ( fixing == 3 ) && ( plane_ == 3 ) ) ) )
	{
	  point_[ 2 ] = *git;
          fitPoints_.push_back(point_);
          // std::cout << "Scat  " << point_[ 0 ] << "  " << point_[ 1 ] << "  " << point_[ 2 ] << "  " << gridPart_.comm().rank() << std::endl;
          fitCount_++;
        }
        else if( wall_ < -400.0 ) { *git += corrX_; }
      }
      else if( track_ == 2 )
      {
        // y-coordinate
        if( wall_ < -900.0 ) { *git += corrY_; point_[ 1 ] = *git; }
        else if( onContactLine && ( flg != 4 ) && ( ( plane_ == 1 ) || ( plane_ == 5 ) || ( ( plane_ == 3 ) && ( fixing == 0 ) ) || ( ( fixing == 1 ) && ( plane_ == 3 ) ) || ( ( fixing == 3 ) && ( plane_ == 4 ) ) ) )
	{
	  point_[ 2 ] = *git; // fitting plane to y-coordinates
          if( plane_ == 1 ) point_[ 2 ] += yExtra;
          fitPoints_.push_back(point_);
          // std::cout << "Scat  " << point_[ 0 ] << "  " << point_[ 1 ] << "  " << point_[ 2 ] << "  " << gridPart_.comm().rank() << std::endl;
          fitCount_++;
        }
        else if( wall_ < -400.0 ) { *git += corrY_; }
      }
      else
      { // z-coordinate
        /*
        // keep track of the biggest and smallest (new) raw coordinate values
	if( wall_ > -400.0 )
	{
	  if( ( point_[1] * point_[1] + point_[2] * point_[2] < 0.01 ) && ( bot_ > top_ ) ) top_ = bot_;
	}
	else
	{
        if( *git > top_ ) top_ = *git;
        if( *git < bot_ ) bot_ = *git;
	}
        */
        if( wall_ < -900.0 ) { *git += corrZ_;

        // all points are used in the fitting
	point_[ 1 ] = std::sqrt( point_[1] * point_[1] + point_[0] * point_[0] );
        if( point_[ 1 ] > mid_ ) mid_ = point_[ 1 ];
        point_[ 1 ] = std::round( fitres_ * point_[ 1 ] ) / fitres_;

        // following line makes removing duplicates easier
        point_[ 0 ] = 0;

        // build up average of tip gradients
        if( ( *git > 0.75 * prevTop_ ) && ( *git < prevTop_ ) )
	{
          gradientAccumulator += ( *git - prevTop_ ) / point_[ 1 ];
          gac++;
        }
        else if( ( *git < 0.75 * prevBot_ ) && ( *git > prevBot_ ) )
	{
          gradientAccumulator -= ( *git - prevBot_ ) / point_[ 1 ];
          gac++;
        }

        // Accomodate tip shape change at explosion
        if( retVal_ )
	{
          if( std::abs(*git) > ejectC_ / ejectK_ )
          {
            double r = point_[ 1 ];

            // Compute the scaled displacement to use
            double d = 4.0 * ejectG_ * ejectK_ * r + 2.0 * ejectG_ * ejectG_;
            d = ( 4.0 * ejectK_ * ejectK_ * r * r - ejectG_ * ejectG_ ) / d;
            d = 1.0 + d;
            // Apply it to the z coordinate
            if( *git > 0 )
              *git = *git - d * ( *git - ejectC_ / ejectK_ );
            else
              *git = *git - d * ( *git + ejectC_ / ejectK_ );
          }
        }
        point_[ 2 ]  = *git;
        point_[ 2 ] = std::round( fitres_ * point_[ 2 ] ) / fitres_;

         // add point to list of fitpoints
	 if( !bob && ( point_.two_norm() > 0.1 ) )
	 {
         fitPoints_.push_back(point_);
         // std::cout << "Scat  " << point_[ 2 ] << "  " << point_[ 1 ] << "  " << gridPart_.comm().rank() << std::endl;
         fitCount_++;
         }
	                      }
        else if( onContactLine && ( flg != 4 ) && ( ( plane_ == 0 ) || ( plane_ == 2 ) || ( ( fixing == 1 ) && ( plane_ == 4 ) ) || ( ( fixing == 2 ) && ( plane_ == 3 ) ) ) )
	{
	  point_[ 2 ] = *git; // fitting plane to z-coordinates
          if( plane_ == 2 ) point_[ 2 ] += zExtra;
          fitPoints_.push_back(point_);
          // std::cout << "Scit  " << point_[ 0 ] << "  " << point_[ 1 ] << "  " << point_[ 2 ] << "  " << gridPart_.comm().rank() << std::endl;
          fitCount_++;
        }
        else if( wall_ < -400.0 ) { *git += corrZ_; }
        track_ = 0;
      } // z-coordinate

      // ... and whether the previous two values looked at were zeroish
      snall_ = small_;
      small_ = std::abs(*git) < 3.0e-2;

    } // end of loop over vertices

    if( snap > 0 ) osnapmesh.close(); else if( snap < 0 ) isnapmesh.close();

    double totCount = 0.0;
    
    if( fixing == 0 )
    {
    totCount = maxMinComm.sum( avCount );
    cpYave_   = maxMinComm.sum( cpYsum );
    cpZave_   = maxMinComm.sum( cpZsum );
    cpYmax_   = maxMinComm.max( cpYmax );
    cpZmax_   = maxMinComm.max( cpZmax );
    cpRmax_   = std::sqrt( cpYmax_*cpYmax_ + cpZmax_*cpZmax_ );
    if(totCount<0.5) totCount = 1.0;

    // store average mean curvature of free surface not in contact with substrate
    cpYave_ /= totCount; cpZave_ /= totCount;

    // determine estimate of tip gradient
    double totalGradientAccumulator = maxMinComm.sum(gradientAccumulator);
    int totalGac = maxMinComm.sum(gac);
    if( totalGac > 0 )
      ejectG_ = totalGradientAccumulator / double( totalGac );
    else
      ejectG_ = -0.005;
    
    };
    
    if( snap != 0 ) { dump(snap,ts); monitor_.dump(snap); };

    if( fixing == 0 )
    {
    if( wall_ > -400.0 )
    {
      double multi = monitor_.multi();
      double ang = ga_ + gb_ * cpZave_;

      if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_ << "Calibration " << timeStore_+ts << "   " << cpYave_ << "   " << cpZave_ << "   " << totCount << "   " << cpYsum << "   " << cpZsum << "   " << ang << "   " << "   " << totArea_ << "   " << bias_ << "   " << spun * 180.0 / M_PI << " # " << cpYmax_ << "  " << cpZmax_ << "  " << prevTop_ << "  " << curAng_ << "  " << buildUp_ << std::endl; // "   " << volrad << std::endl;
    }

    }
    totalFitCount_ = maxMinComm.sum(fitCount_);

#if 1
    // Send the locally collected fit points to the root process
    // std::vector<int> sizes(fitPoints_.size()); // gridPart_.comm().size());

    int share = fitPoints_.size();
    // gridPart_.comm().template allgather<int>(&share, 1, sizes.data());
    int globSize = maxMinComm.sum(share);

    // std::cout << "WWW " << rank << "  " << gridPart_.comm().rank() << "  " << share << "  " << globSize << std::endl;

    // std::vector<int> offsets(sizes.size());

    // for (size_t k = 0; k < offsets.size(); ++k)
    // offsets[k] = std::accumulate(sizes.begin(), sizes.begin() + k, 0);

    if (gridPart_.comm().rank() == 0)
      fitPointsGlobal_->resize(globSize); // std::accumulate(sizes.begin(), sizes.end(), 0));

    maxMinComm.gather(fitPoints_.data(),
                          fitPointsGlobal_->data(),
                          fitPoints_.size(),
                          0);

    // std::cout << "\n\nZZZZZZZZZZZZZZZZZZZ\n\n" << std::endl;
#endif
  }

  // indicates whether a max or min of a / b has been reached
  bool peaked()
  {
    bool one = ( ratioStore_[ 1 ] > ratioStore_[ 0 ] ) && ( ratioStore_[ 1 ] > ratioStore_[ 2 ] );
    bool two = ( ratioStore_[ 1 ] < ratioStore_[ 0 ] ) && ( ratioStore_[ 1 ] < ratioStore_[ 2 ] );

    if( one || two ) std::cout << "PEAKED !!!" << std::endl;
    return one || two;
  }

  double cpYave() const
  {
    return cpYave_;
  }
  
  double cpZave() const
  {
    return cpZave_;
  }

  double cpRmax() const
  {
    return cpRmax_;
  }

  bool zapCLForce() const
  {
    return zapCLForce_;
  }

  double bias() const
  {
    return bias_ + tol_;
  }
  
  double area() const
  {
    return totArea_ / dca_;
  }

  double currAngle()
  {
    // contact angle calculated from current contact area
    double top = 179.0, bot = 1.0, test;
    for( unsigned int i = 0; i < 15; ++i )
    {
      if( i == 0 )
	test = top;
      else if ( i == 1 )
	test = bot;
      else
        test = ( top + bot ) / 2.0;
      double cosa = std::cos( M_PI * test / 180.0 );    
      double RR = std::pow( 4.0 / ( 2.0 - 3.0 * cosa + cosa * cosa * cosa ) , 1.0 / 3.0 );
      double hh = ( 1.0 - cosa ) * RR;
      double value = totArea_ - M_PI * hh * ( 2.0 * RR - hh );
      // std::cout << i  << "  " << top << "  " << bot << "  " << test << "  " << value << std::endl;
      if( std::abs( value ) < tol_ )
	break;
      else if( value < 0.0 )
	bot = test;
      else
	top = test;
    }
    curAng_ = test;
    double currRatio = totArea_ / dca_;
    if( currRatio > prevRatio_ ) buildUp_ += wallFriction_ / ( dca_ * ( currRatio - prevRatio_ ) );
    if( buildUp_ > 1.0 ) buildUp_ = 1.0;
    // std::cout << "Build-up: " << buildUp_ << "  " << wallFriction_ << "  " << currRatio << "  " << prevRatio_ << "  " << totArea_ << std::endl;
    prevRatio_ = currRatio;
    return buildUp_ * test / 2.0;
  }
  
  double curAng() const
  {
    return curAng_;
  }

  double reset()
  {
    return monitor_.reset();
  }

  double ratio()
  {
    return ratioStore_[ 1 ];
  }

  short int abnfit(double angmom, const int &fixing)
  {
    // plane fitting if a (substrate) wall is present
    if( wall_ > -900.0 )
      planefitter(angmom,fixing);
    else
      abnfitter(angmom);

    return retVal_ != 0;
  }
  
  void abnfitter(double angmom)
  {
    Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> comm(Dune::MPIHelper::getCommunicator());
    short int localRetVal = 0;

    double totangmom = comm.sum( angmom );
#if 1
    if( Dune::Fem::MPIManager::rank() == 0 )
    {

    // for (int i=0;i<fitPointsGlobal_->size();i++)
    // {
    //    std::cout << "Before " << (*fitPointsGlobal_)[ i ][ 2 ] << "  " << (*fitPointsGlobal_)[ i ][ 1 ] << std::endl;
    // }

    // std::cout << "\n\n\n Before " << fitPointsGlobal_->size() << std::endl;

    // remove the duplicates
    std::sort(fitPointsGlobal_->begin(), fitPointsGlobal_->end(), myobject);

    // for (int i=0;i<fitPointsGlobal_->size();i++)
    // {
    //    std::cout << "Middle " << (*fitPointsGlobal_)[ i ][ 2 ] << "  " << (*fitPointsGlobal_)[ i ][ 1 ] << std::endl;
    // }

    // std::cout << "\n\n\n Middle " << fitPointsGlobal_->size() << std::endl;

    auto last = std::unique(fitPointsGlobal_->begin(), fitPointsGlobal_->end());
    fitPointsGlobal_->erase(last, fitPointsGlobal_->end());

    // for (int i=0;i<fitPointsGlobal_->size();i++)
    // {
    //    std::cout << "After  " << (*fitPointsGlobal_)[ i ][ 2 ] << "  " << (*fitPointsGlobal_)[ i ][ 1 ] << std::endl;
    // }

    // std::cout << "\n\n\n After  " << fitPointsGlobal_->size() << std::endl;

    int NPT = fitPointsGlobal_->size(), MA = 3;
#else
    int NPT = fitPoints_.size(), MA = 3;
#endif
    Vec_DP x(NPT),y(NPT),sig(NPT);
    Vec_BOOL ia(MA);
    Vec_DP a(MA);
    Mat_DP covar(MA,MA),alpha(MA,MA);
    DP alamda,chisq=0.0,ochisq=1000.0;
    double xm = 0.0, ym = 0.0;

    // set-up the radius and angle info needed for the fitting
    for (int i=0;i<NPT;i++)
    {
#if 1
       double xx = (*fitPointsGlobal_)[ i ][ 2 ];
       double yy = (*fitPointsGlobal_)[ i ][ 1 ];
#else
       double xx = fitPoints_[ i ][ 2 ];
       double yy = fitPoints_[ i ][ 1 ];
#endif
       // std::cout << "Gath  " << xx << "  " << yy << "  " << gridPart_.comm().rank() << std::endl;
       double rr = std::sqrt( xx*xx + yy*yy );
       double ang = std::atan2( yy , xx );

       x[i]=std::cos( M_PI / 2.0 - ang );
       y[i]=rr;
       sig[i]=1.0;

       // keep track of biggest x and y values for initializing the fitting
       if( ( abs(yy) < 1e-2 ) || ( de_ > 0 ) ) xm = std::max( xm , std::abs(xx) );
       if( ( abs(xx) < 1e-2 ) || ( de_ > 0 ) ) ym = std::max( ym , std::abs(yy) );
    }

    // initial guesses for the "a", "b" and "n" parameters
    a[0] = ym;
    a[1] = xm;
    a[2] = 2.0;

    for (int i=0;i<MA;i++) ia[i]=true;

    alamda = -1.0;

    if( de_ > 0.0 )
    {
    // perform the least-squared fitting
    for( unsigned int i = 0; i< 220; ++i )
    {
      // std::cout << "CHI " << a[0] << "  " << a[1] << "  " <<  a[2] << "  " <<  chisq << std::endl;
      if( std::abs( ochisq - chisq ) < 1e-13 ) break;
      ochisq = chisq;
      NR::mrqmin(x,y,sig,a,ia,covar,alpha,chisq,superellips,alamda);
    }
    }
    double etc  = 2.0 * prevMid_ / ( prevTop_ - prevBot_ );
    double rat = 1.0 / etc;

    monitor_.check(rat);
    double multi = monitor_.multi();

    // write fit data to output file
    if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_ << timeStore_ << "  " << a[0] << "  " << a[1] << "  " <<  a[2] << "  " << NPT << "  " << totalFitCount_ << "  " << chisq << "  " << rat << "  " << prevTop_ << "  " << prevBot_ << "  " << prevMid_ << "  " << ejectG_ << std::endl;

    fitPointsGlobal_->clear();

    // keep a short history of the fit ratio
    for( unsigned int i = 2; i > 0; --i )
      ratioStore_[ i ] = ratioStore_[ i-1 ];
    ratioStore_[ 0 ] = a[ 1 ] / a[ 0 ];

    bool boom = a[1] / a[0] > critRatio_;

    if( boom && ( ! boomed_ ) ) localRetVal = 1;

    if( boom ) boomed_ = true;

    } // end of rank 0 stuff

    retVal_ = comm.sum(localRetVal);

    // reset fit points array
    track_ = 0;
    fitPoints_.clear();
    fitCount_ = 0;

    if( retVal_ )
    {
      // Set-up the transformation parameters to the canonical tip geometry
      ejectK_ = 0.25 * std::pow( ejectG_ * ejectG_ * ejectG_ * ejectG_ / ejectP_ , 1.0/3.0 );
      ejectC_ = ( prevTop_ - prevBot_ ) * ejectK_ / 2.0 - ejectG_ * ejectG_ / 2.0;

      std::cout << "Eject parameters on rank " << Dune::Fem::MPIManager::rank() << " are: Top = " << prevTop_
                << " Bot = " << prevBot_ << " P = " << ejectP_ << " G = " << ejectG_
                << " K = " << ejectK_ << " C = " << ejectC_ << " Q = " << ejectQ_ << std::endl;
    }
  }

  double radius(double &max, double &min)
  {
    max = 0.0;
    min = 1.0e6;
    double average = 0.;
    double count = 0;
    const DiscreteFunctionSpaceType &dfSpace = vertices_->space();

    const IteratorType end = dfSpace.end();
    for( IteratorType it = dfSpace.begin(); it != end; ++it )
      {
	const EntityType &entity = *it;

	for( unsigned int corner = 0; corner < 3; ++corner )
	  {
	    RangeType y;
	    evaluate( entity, corner, y );

	    double r = y.two_norm();
	    average += r;
	    ++count;

	    if( r > max )
	      max = r;
	    if( r < min )
	      min = r;
	  }
      }
    average /= count;

    if( max - min > 1.0e-8 )
      {
	std::cout << "not circular! (max: " << max << " min: " << min << " )" << std::endl;
      }

    return average;
  }

  void planefitter(double angmom, const int &fixing)
  {
    Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> comm(Dune::MPIHelper::getCommunicator());

    double rough;
    // if using body friction, give z-position rather than momentum to monitor
    if( monitor_.body() )
      rough = cpZave_;
    else
      rough = comm.sum( angmom );
    
    short int localRetVal = 0;
    int MA = 3; Vec_DP a(MA); a[0]=0.0;a[1]=0.0;a[2]=0.0;
    DP alamda,chisq=0.0,ochisq=1000.0;
    double plA = 0.0, plB = 0.0, plC = 0.0;
    // update identity of plane being fitted
    if( ( fixing == 0 ) && ( plane_ == 3 ) )
      plane_ = 0;
    else
    plane_ = (plane_+1)%6;
    // once fitting for last plane has been done, can use the contact line forces
    if( zapCLForce_ ) if( (planeA_[3]!=0.0) || (planeB_[3]!=0.0) || (planeC_[3]!=0.0) )
      zapCLForce_ = false;
    // only rank zero does the plane fitting
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::sort(fitPointsGlobal_->begin(), fitPointsGlobal_->end(), myobject);
      auto last = std::unique(fitPointsGlobal_->begin(), fitPointsGlobal_->end());
      fitPointsGlobal_->erase(last, fitPointsGlobal_->end());
      int NPT = fitPointsGlobal_->size();

      if( NPT > 2 ) // need at least 3 points to set a plane
      {

      Vec_DP x(NPT),y(NPT),sig(NPT);
      Vec_BOOL ia(MA);
      Mat_DP covar(MA,MA),alpha(MA,MA);
      double xm = 0.0, ym = 0.0;

      for (int i=0;i<NPT;i++)
      {
        x[i]=double(i);
        y[i]=(*fitPointsGlobal_)[ i ][ 2 ];
        sig[i]=1.0;
      }

    // initial guesses for the "a", "b" and "c" parameters based on the previous values
    a[0] = planeA_[ plane_ ];
    a[1] = planeB_[ plane_ ];
    a[2] = planeC_[ plane_ ];

    for (int i=0;i<MA;i++) ia[i]=true;

    alamda = -1.0;

    // perform the least-squared fitting
    for( unsigned int i = 0; i< 220; ++i )
    {
      if( std::abs( ochisq - chisq ) < 1e-13 ) break;
      ochisq = chisq;
      NR::mrqmin(x,y,sig,a,ia,covar,alpha,chisq,superplane,alamda);
      if( std::isnan( chisq ) || std::isnan( -chisq ) ) break;
    };
    if( std::isnan( chisq ) || std::isnan( -chisq ) )
    {
      plA = planeA_[ plane_ ]; plB = planeB_[ plane_ ]; plC = planeC_[ plane_ ];
    }
    else
    {
    plA = a[0]; plB = a[1]; plC = a[2];
    };
      }

    double dermom = monitor_.check( rough );
    double multi = monitor_.multi();
    
    // write fit data to output file
    if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_ << timeStore_ << "  " << " || " << corrX_ << "  " << corrY_ << "  " << corrZ_ << "  " << totArea_ << "  " << dermom << "  " << speed_ << "  " << a[0] << "  " << a[1] << "  " <<  a[2] << "  " << NPT << "  " << totalFitCount_ << "  " << chisq << "  " << rough << "  " << std::endl;
    fitPointsGlobal_->clear();

    } // end of rank 0 stuff
 
    planeA_[ plane_ ] = comm.sum(plA); planeB_[ plane_ ] = comm.sum(plB); planeC_[ plane_ ] = comm.sum(plC);
    usePlaneInterpolation_=(planeA_[plane_]!=0.0)||(planeB_[plane_]!=0.0)||(planeC_[plane_]!=0.0);

    // reset fit points array
    track_ = 0;
    fitPoints_.clear();
    fitCount_ = 0;
  }

  template <class IOFT>
  void oiL(unsigned int num, unsigned int nim, bool &solution,  IOFT &iof)
  {
    if(solution) iof << num << " w " << nim << " xxx " << "1" << std::endl; else iof << num << " w " << nim << " xxx " << "0" << std::endl;
  }
  
  template <class IOFT>
  void ioL(bool &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stoi(line.erase(0,line.rfind("xxx")+4),&sz) == 1;
  }
  
  template <class DF , class IOFT>
  void oi(unsigned int num, unsigned int nim, DF &solution,  IOFT &iof)
  {
    iof << num << " w " << nim << " xxx " << lexical_cast<string>(solution) << std::endl;
  }
  
  template <class DF , class IOFT>
  void io(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stod(line.erase(0,line.rfind("xxx")+4),&sz);
  }
  
  template <class DF , class IOFT>
  void ioI(DF &solution,  IOFT &iof)
  {
    string line; std::string::size_type sz;
    getline(iof,line); solution = std::stoi(line.erase(0,line.rfind("xxx")+4),&sz);
  }
  
  void dump(const int snap, const double ts)
  {
    std::ifstream isnapfile; std::ofstream osnapfile;
    if( snap < -1 ) osnapfile.open("snip.def"); else if( snap > 0 ) osnapfile.open("snap.def"); else if( snap < 0 ) isnapfile.open("snap.def");

    if( snap > 0 ) // writing data out
    {
      double tim = timeStore_ + ts;
      oi( 0, 0, tim, osnapfile );
      oi( 0, 0, track_, osnapfile );
      oi( 0, 0, top_, osnapfile );
      oi( 0, 0, prevTop_, osnapfile );
      oi( 0, 0, mid_, osnapfile );
      oi( 0, 0, prevMid_, osnapfile );
      oi( 0, 0, bot_, osnapfile );
      oi( 0, 0, prevBot_, osnapfile );
      oi( 0, 0, totAveX_, osnapfile );
      oi( 0, 0, totAveY_, osnapfile );
      oi( 0, 0, totAveZ_, osnapfile );
      oi( 0, 0, totArea_, osnapfile );
      oi( 0, 0, corrX_, osnapfile );
      oi( 0, 0, corrY_, osnapfile );
      oi( 0, 0, corrZ_, osnapfile );
      oi( 0, 0, speed_, osnapfile );
      oi( 0, 0, curAng_, osnapfile );
      oi( 0, 0, buildUp_, osnapfile );
      oi( 0, 0, prevRatio_, osnapfile );
      oi( 0, 0, cpYave_, osnapfile );
      oi( 0, 0, cpZave_, osnapfile );
      oi( 0, 0, cpYmax_, osnapfile );
      oi( 0, 0, cpZmax_, osnapfile );
      // zapCLForce_ = false;
      // std::cout << "OOOOOOOOOOOOOOOOO zapCLForce_ = " << zapCLForce_ << std::endl;
      oiL( 0, 0, zapCLForce_, osnapfile );
      // usePlaneInterpolation_ = true;
      // std::cout << "OOOOOOOOOOOOOOOOO usePlaneInterpolation_ = " << usePlaneInterpolation_ << std::endl;
      oiL( 0, 0, usePlaneInterpolation_, osnapfile );
      oi( 0, 0, plane_, osnapfile );
      oi( 0, 0, bias_, osnapfile );
      oi( 0, 0, biasFactor_, osnapfile );
      for( unsigned int i = 0; i < 6; ++i )
      oi( 0, 0, planeA_[ i ], osnapfile );
      for( unsigned int i = 0; i < 6; ++i )
      oi( 0, 0, planeB_[ i ], osnapfile );
      // planeC_[ 5 ] = 0.1234;
      // std::cout << "OOOOOOOOOOOOOOOOO planeC_[ 5 ] = " << planeC_[ 5 ] << std::endl;
      for( unsigned int i = 0; i < 6; ++i )
      oi( 0, 0, planeC_[ i ], osnapfile );
      oi( 0, 0, marker_, osnapfile );
      oi( 0, 0, fixing_, osnapfile );
      for( unsigned int i = 0; i < 3; ++i )
      oi( 0, 0, moveStore_[ i ], osnapfile );
    }
    else if( snap < 0 ) // reading data in
    {
      double tim;
      io( tim, isnapfile );
      timeStore_ = tim - ts;
      ioI( track_, isnapfile );
      io( top_, isnapfile );
      io( prevTop_, isnapfile );
      io( mid_, isnapfile );
      io( prevMid_, isnapfile );
      io( bot_, isnapfile );
      io( prevBot_, isnapfile );
      io( totAveX_, isnapfile );
      io( totAveY_, isnapfile );
      io( totAveZ_, isnapfile );
      io( totArea_, isnapfile );
      io( corrX_, isnapfile );
      io( corrY_, isnapfile );
      io( corrZ_, isnapfile );
      io( speed_, isnapfile );
      io( curAng_, isnapfile );
      io( buildUp_, isnapfile );
      io( prevRatio_, isnapfile );
      io( cpYave_, isnapfile );
      io( cpZave_, isnapfile );
      io( cpYmax_, isnapfile );
      io( cpZmax_, isnapfile );
      ioL( zapCLForce_, isnapfile );
      // std::cout << "IIIIIIIIIIIIIIIIII zapCLForce_ = " << zapCLForce_ << std::endl;
      ioL( usePlaneInterpolation_, isnapfile );
      // std::cout << "IIIIIIIIIIIIIIIIII usePlaneInterpolation_ = " << usePlaneInterpolation_ << std::endl;
      ioI( plane_, isnapfile );
      io( bias_, isnapfile );
      io( biasFactor_, isnapfile );
      for( unsigned int i = 0; i < 6; ++i )
      io( planeA_[ i ], isnapfile );
      for( unsigned int i = 0; i < 6; ++i )
      io( planeB_[ i ], isnapfile );
      for( unsigned int i = 0; i < 6; ++i )
      io( planeC_[ i ], isnapfile );
      // std::cout << "IIIIIIIIIIIIIIIIIIIIII planeC_[ 5 ] = " << planeC_[ 5 ] << std::endl;
      ioI( marker_, isnapfile );
      ioI( fixing_, isnapfile );
      for( unsigned int i = 0; i < 3; ++i )
      io( moveStore_[ i ], isnapfile );
    };
    
    if( snap > 0 ) osnapfile.close(); else if( snap < 0 ) { isnapfile.close(); std::remove("snap.def"); };
  }

  void adapt()
  {
    DUNE_THROW(Dune::NotImplemented, "adapt method on coordinate function for GeometryGrid not implemented");
  }

protected:
  GridPartType gridPart_;
  DiscreteFunctionSpaceType space_;
  DiscreteFunctionType* vertices_;
private:
  // tolerance for deciding whether a potential fit point is on the appropriate coordinate plane
  const double fittol_;
  // index to keep track of which (potential) fit point coordinate is being picked-up
  unsigned int track_;
  // stores to pick up full 3-D (potential) fit point coordinates from the solution vector
  FitPointType point_;
  // store of points to be used by the fitting
  std::vector<FitPointType> fitPoints_;
  // std::vector<FitPointType> fitPointsGlobal_;
  // output stream
  std::ofstream fitOutput_;
  // keep a record of the time for outputing with the fit data
  double timeStore_;
  // record how many fit points were found
  unsigned int fitCount_;
  unsigned int totalFitCount_;
  // history of the ratios
  std::vector<double> ratioStore_;
  double de_;
  double top_;
  double prevTop_;
  double mid_;
  double prevMid_;
  double bot_;
  double prevBot_;
  double summitAng_;
  double dca_;
  double curAng_;
  Monitor monitor_;
  double critRatio_;
  double buildUp_;
  double prevRatio_;
  bool boomed_;
  short int retVal_;

  // Multiplier to reduce charge on prolate explosion
  double ejectQ_;

  // Percentage of initial fluid volume to be lost at explosion
  double ejectP_;

  // Gradient of droplet surface at tip just before explosion
  double ejectG_;

  // Scale factor to transform to canonical tip geometry
  double ejectK_;

  // Vertical displacement to transform to canonical tip geometry
  double ejectC_;

  bool small_;
  bool snall_;
  int initiate_;
  double wonk_;
  double fitres_;
  // droplet centering stuff
  double totAveX_;
  double totAveY_;
  double totAveZ_;
  double totArea_;
  double corrX_;
  double corrY_;
  double corrZ_;
  double speed_;
  // wall stuff
  double wall_;
  double wallFact_;
  double wallStick_;
  double wallFriction_;
  double slipLength_;
  double wallDamp_;
  double equiAngle_;
  double cluster_;
  int summitFlag_;
  double cpYave_;
  double cpZave_;
  double cpYmax_;
  double cpZmax_;
  double cpRmax_;
  double surfTensCoeff_;
  bool zapCLForce_;
  bool usePlaneInterpolation_;
  int plane_;
  double bias_;
  double biasFactor_;
  Dune::FieldVector<double,6> planeA_;
  Dune::FieldVector<double,6> planeB_;
  Dune::FieldVector<double,6> planeC_;
  double ga_;
  double gb_;
  const double tol_;
  int marker_;
  int fixing_;
  double cosa_;
  double RR_;
  double hh_;
  Dune::FieldVector<double,3> moveStore_;
};

#endif // #ifndef SURFACE_DEFORMATION_HH

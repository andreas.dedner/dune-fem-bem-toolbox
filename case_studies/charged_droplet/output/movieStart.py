#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML Partitioned Unstructured Grid Reader'
s0001fluidpoisson000000 = XMLPartitionedUnstructuredGridReader(FileName=[

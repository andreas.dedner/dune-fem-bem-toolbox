
# create a new 'Plane'
planenumber = Plane()

# Properties modified on planenumber
planenumber.Origin = [-1.005, 4.0, value]
planenumber.Point1 = [-1.005, -4.0, value]
planenumber.Point2 = [-1.005, 4.0, velue]

# show data in view
planenumberDisplay = Show(planenumber, renderView1)

# trace defaults for the display properties.
planenumberDisplay.Representation = 'Surface'
planenumberDisplay.ColorArrayName = [None, '']
# planenumberDisplay.OSPRayScaleArray = 'Normals'
# planenumberDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
planenumberDisplay.SelectOrientationVectors = 'None'
planenumberDisplay.ScaleFactor = 0.6000000000000001
planenumberDisplay.SelectScaleArray = 'None'
planenumberDisplay.GlyphType = 'Arrow'

# change solid color
planenumberDisplay.DiffuseColor = [farbe, couleur, 0.0]

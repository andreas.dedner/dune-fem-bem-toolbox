
# update the view to ensure updated data information
renderView1.Update()


# set active source
SetActiveSource(s0001fluidpoisson000000)

# get camera animation track for the view
cameraAnimationCue1 = GetCameraTrack(view=renderView1)

# create keyframes for this animation track

# create a key frame
keyFrame71519 = CameraKeyFrame()
keyFrame71519.Position = [5.754077876170891, 10.507078317910189, 14.002278185487933]
keyFrame71519.FocalPoint = [-1.8455249458237495, -2.4049908703276017, 6.268369281739732]
keyFrame71519.ViewUp = [0.8849977004089349, -0.3161532791379883, -0.34179844113335933]
keyFrame71519.ParallelScale = 2.0357978696104055
keyFrame71519.PositionPathPoints = [0.0, 3.670552222434203, -3.3951504211718415, 1.3683505286127757, 4.73565765956984, -0.8373549798040476, 2.2140376638193566, 3.991902829833577, 2.0402816031999103, 2.214037663819356, 1.7233767988877757, 4.138599960402628, 1.3683505286127753, -1.203420593810163, 4.6561137985705106, 2.220446049250313e-16, -3.6705522224342, 3.3951504211718397, -1.3683505286127746, -4.735657659569837, 0.8373549798040474, -2.214037663819355, -3.9919028298335744, -2.040281603199909, -2.2140376638193544, -1.7233767988877746, -4.138599960402625, -1.3683505286127742, 1.203420593810162, -4.656113798570507]
keyFrame71519.FocalPathPoints = [0.0, 0.0, 0.0]
keyFrame71519.ClosedPositionPath = 1

# create a key frame
keyFrame71520 = CameraKeyFrame()
keyFrame71520.KeyTime = 1.0
keyFrame71520.Position = [5.754077876170891, 10.507078317910189, 14.002278185487933]
keyFrame71520.FocalPoint = [-1.8455249458237495, -2.4049908703276017, 6.268369281739732]
keyFrame71520.ViewUp = [0.8849977004089349, -0.3161532791379883, -0.34179844113335933]
keyFrame71520.ParallelScale = 2.0357978696104055

# initialize the animation track
cameraAnimationCue1.Mode = 'Follow-data'
cameraAnimationCue1.KeyFrames = [keyFrame71519, keyFrame71520]
cameraAnimationCue1.DataSource = s0001fluidpoisson000000

# animationScene1.Play()

# animationScene1.GoToFirst()

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [7.59960282199464, 12.91206918823779, 7.733908903748201]

renderView1.CameraViewUp = [0.8849977004089349, -0.3161532791379883, -0.34179844113335933]
renderView1.CameraParallelScale = 2.0357978696104055

# update the view to ensure updated data information
renderView1.Update()

# current camera placement for renderView1
renderView1.CameraPosition = [0.35567774386733114, 5.690510665360319, 3.1223879322025283]
renderView1.CameraViewUp = [0.9896671121593392, 0.016306726155493906, -0.14245384442788328]
renderView1.CameraParallelScale = 2.0357978696104055


# save animation
SaveAnimation('new.png', renderView1, ImageResolution=[918, 522],
    FrameWindow=[0, 5000])

])
s0001fluidpoisson000000.CellArrayStatus = ['volume', 'rank']
s0001fluidpoisson000000.PointArrayStatus = ['solution0', 'solution1', 'solution2', 'solution3', 'solution4', 'rhs0', 'rhs1', 'rhs2', 'rhs3', 'rhs4', 'divergence0', 'divergence1', 'divergence2', 'divergence3', 'divergence4', 'NS inside exact solution0', 'NS inside exact solution1', 'NS inside exact solution2', 'NS inside exact solution3', 'NS inside exact solution4']

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [905, 528]

# get color transfer function/color map for 'solution0'
solution0LUT = GetColorTransferFunction('solution0')

# show data in view
s0001fluidpoisson000000Display = Show(s0001fluidpoisson000000, renderView1)

# trace defaults for the display properties.
s0001fluidpoisson000000Display.ColorArrayName = ['POINTS', 'solution0']
s0001fluidpoisson000000Display.LookupTable = solution0LUT
s0001fluidpoisson000000Display.GlyphType = 'Arrow'
s0001fluidpoisson000000Display.ScalarOpacityUnitDistance = 0.1384371961970866

# reset view to fit data
renderView1.ResetCamera()

# show color bar/color legend
s0001fluidpoisson000000Display.SetScalarBarVisibility(renderView1, True)

# get opacity transfer function/opacity map for 'solution0'
solution0PWF = GetOpacityTransferFunction('solution0')

# create a new 'Clip'
clip1 = Clip(Input=s0001fluidpoisson000000)
clip1.ClipType = 'Plane'
clip1.Scalars = ['POINTS', 'solution0']
clip1.Value = -0.001919364556670189

# Properties modified on clip1
clip1.Invert = 1

# init the 'Plane' selected for 'ClipType'
clip1.ClipType.Origin = [0.0010580122470855713, 0.000643998384475708, 9.000301361083984e-06]

# Properties modified on clip1.ClipType
clip1.ClipType.Normal = [0.0, 1.0, 0.0]

# show data in view
clip1Display = Show(clip1, renderView1)
# trace defaults for the display properties.
clip1Display.ColorArrayName = ['POINTS', 'solution0']
clip1Display.LookupTable = solution0LUT
clip1Display.GlyphType = 'Arrow'
clip1Display.ScalarOpacityUnitDistance = 0.13727365576868253

# hide data in view
Hide(s0001fluidpoisson000000, renderView1)

# show color bar/color legend
# s0001fluidpoisson000000Display.SetScalarBarVisibility(renderView1, False)

clip1Display.SetScalarBarVisibility(renderView1, False)

# set scalar coloring
ColorBy(clip1Display, ('POINTS', 'solution2'))

# rescale color and/or opacity maps used to include current data range
clip1Display.RescaleTransferFunctionToDataRange(True)

# show color bar/color legend
clip1Display.SetScalarBarVisibility(renderView1, False)

# get color transfer function/color map for 'solution3'
solution3LUT = GetColorTransferFunction('solution2')

# get opacity transfer function/opacity map for 'solution3'
solution3PWF = GetOpacityTransferFunction('solution2')

# Rescale transfer function
solution3LUT.RescaleTransferFunction(-1.0, 1.0)

# Rescale transfer function
solution3PWF.RescaleTransferFunction(-1.0, 1.0)

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=clip1)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [-0.1914626742999834, -4.692844012325429, -0.9335781363646458]
renderView1.CameraFocalPoint = [0.0010580122470855713, 0.0006439983844757078, 9.000301361083915e-06]
renderView1.CameraViewUp = [-0.08313059855480956, -0.19113268962739977, 0.9780376263414998]
renderView1.CameraParallelScale = 1.2395643325170254

# update the view to ensure updated data information
renderView1.Update()

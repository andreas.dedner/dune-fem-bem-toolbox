"""
sudo apt update
sudo apt install -y numpy
sudo apt install -y libatlas-base-dev
sudo apt install -y libatlas3-base
sudo apt-get install -y python-scipy
sudo apt-get install -y python-numpy
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o get-pip.py
python get-pip.py
pip install PyDistMesh
"""
import matplotlib
matplotlib.use('Agg')

print('3-D Unit ball')
import distmesh as dm
import numpy as np
import itertools
import distmesh.mlcompat as ml
import math
"""
Example: (Uniform Mesh on Unit Sphere)
    fd=@(p) dsphere(p,0,0,0,1);
    [p,t]=distmeshsurface(fd,@huniform,0.2,1.1*[-1,-1,-1;1,1,1]);
"""
fd = lambda p: np.sqrt((p**2).sum(1))-1.0
p, t = dm.distmeshnd(fd, dm.huniform, 0.2, (-1,-1,-1, 1,1,1))
"""
Example: (Uniform Mesh on Torus)
    fd=@(p) (sum(p.^2,2)+.8^2-.2^2).^2-4*.8^2*(p(:,1).^2+p(:,2).^2);
    [p,t]=distmeshsurface(fd,@huniform,0.1,[-1.1,-1.1,-.25;1.1,1.1,.25]);
""
fd = lambda p: (p[:,0]**2+p[:,1]**2+p[:,2]**2+0.8**2-0.2**2)**2-4*0.8**2*(p[:,1]**2+p[:,2]**2)
""
fd = lambda p: np.sqrt(p[:,0]**2+p[:,1]**2+p[:,2]**2)-1.0
""
fd = lambda p: (p[:,0]**2+p[:,1]**2+p[:,2]**2+0.8**2-0.2**2)**2-4.0*(0.8**2)*(p[:,0]**2+p[:,1]**2)
p, t = dm.distmeshnd(fd, dm.huniform, 0.08, (-1.1,-1.1,-0.25, 1.1,1.1,0.25))
"""

def simpvol(p, t):
    """Signed volumes of the simplex elements in the mesh."""
    dim = p.shape[1]
    if dim == 1:
        d01 = p[t[:,1]]-p[t[:,0]]
        return d01
    elif dim == 2:
        d01 = p[t[:,1]]-p[t[:,0]]
        d02 = p[t[:,2]]-p[t[:,0]]
        return (d01[:,0]*d02[:,1]-d01[:,1]*d02[:,0])/2
    elif dim == 3:
        d01 = p[t[:,1]]-p[t[:,0]]
        d02 = p[t[:,2]]-p[t[:,0]]
        d03 = p[t[:,3]]-p[t[:,0]]
        e01 = d01[:,0]*d02[:,1]-d01[:,1]*d02[:,0]
        e02 = d01[:,0]*d02[:,2]-d01[:,2]*d02[:,0]
        e03 = d01[:,1]*d02[:,2]-d01[:,2]*d02[:,1]
        return (d03[:,0]*e03-d03[:,1]*e02+d03[:,2]*e01)/6
    else:
        raise NotImplementedError

def norm(a):
    return np.sqrt(a[:,0]*a[:,0]+a[:,1]*a[:,1]+a[:,2]*a[:,2])
    
def simpqual(p, t):
    """Determine Mesh Quality"""
    a0 = norm( p[t[:,3]] - p[t[:,0]] ) * norm( p[t[:,1]] - p[t[:,2]] )
    a1 = norm( p[t[:,3]] - p[t[:,1]] ) * norm( p[t[:,2]] - p[t[:,0]] )
    a2 = norm( p[t[:,3]] - p[t[:,2]] ) * norm( p[t[:,0]] - p[t[:,1]] )
    s = ( a0 + a1 + a2 ) / 2
    q = ( s * ( s - a0 ) * ( s - a1 ) * ( s - a2 ) ) ** 1.5
    return math.pi * q / ( 162 * simpvol(p,t)**4 )

def simpvul(p, e):
    """Signed volumes of the simplex elements in the mesh."""
    dim = p.shape[1]
    if dim == 3:
        d01 = p[e[:,1]]-p[e[:,0]]
        d02 = p[e[:,2]]-p[e[:,0]]
        cen = (p[e[:,0]]+p[e[:,1]]+p[e[:,2]])/3
        e01 = d01[:,0]*d02[:,1]-d01[:,1]*d02[:,0]
        e02 = d01[:,0]*d02[:,2]-d01[:,2]*d02[:,0]
        e03 = d01[:,1]*d02[:,2]-d01[:,2]*d02[:,1]
        return ((cen[:,0]-0.8*cen[:,0]/norm(cen))*e03-(cen[:,1]-0.8*cen[:,1]/norm(cen))*e02+cen[:,2]*e01)/6
    else:
        raise NotImplementedError

"""Fix Orientation of Tetras"""

ptol=2e-13
snap = (p.max(0)-p.min(0)).max()*ptol
_, ix, jx = ml.unique_rows(np.round(p/snap)*snap, True, True)

p = p[ix]
t = jx[t]

flip = simpvol(p,t)<0
t[flip, :2] = t[flip, 1::-1]


q = simpqual(p,t)
print('Quality: %f\n' % q.max())

"""Boundary Edge Extractor"""

dim = t.shape[1]-1
vertices = set(range(dim+1))
faces = list(itertools.combinations(vertices, dim))

edges = np.vstack((t[:,face] for face in faces))
edges.sort(1)
_, ix, jx = ml.unique_rows(edges, True, True)
vec, _ = np.histogram(jx, np.arange(max(jx)+2))
qx = (vec == 1).nonzero()
e = edges[ix[qx]]



"""Fix Orientation of Surface Patches"""

snip = (p.max(0)-p.min(0)).max()*ptol
_, ix, jx = ml.unique_rows(np.round(p/snip)*snip, True, True)

p = p[ix]
e = jx[e]

flip = simpvul(p,e)<0
e[flip, :2] = e[flip, 1::-1]




"""Output Mesh"""

with open("distmesh.msh", "w") as tf:
        num_vertices = p.shape[0]
        num_triangles = t.shape[0]
        num_edges = e.shape[0]
        tot_num = num_edges + num_triangles

        tf.write('$MeshFormat\n')
        tf.write('2.2 0 8\n')
        tf.write('$EndMeshFormat\n')
        tf.write('$Nodes\n')
        tf.write('%d\n' % num_vertices)

        for i in range(0, num_vertices):
            tf.write('%d %f %f %f\n' 
                        % (i+1, p[i,0], p[i,1], p[i,2]))


        tf.write('$EndNodes\n')
        tf.write('$Elements\n')
            
        tf.write('%d\n' % tot_num)

        for i in range(0, num_edges):
            tf.write('%d 2 2 0 60 %d %d %d\n'
                % (i+1, e[i,0]+1, e[i,1]+1, e[i,2]+1))

        for i in range(0, num_triangles):
            tf.write('%d 4 2 0 4000 %d %d %d %d\n'
                % (num_edges+i+1, t[i,0]+1, t[i,1]+1, t[i,2]+1, t[i,3]+1))

        tf.write('$EndElements\n')

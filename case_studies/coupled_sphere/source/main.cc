#include <config.h>

// iostream includes
#include <iostream>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "traits.hh"

// include grid part
#include <dune/fem/gridpart/leafgridpart.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/grid/io/file/gmshreader.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_simple.hh>

// include header of adaptive scheme
#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include <dune/fem_bem_toolbox/coupling_schemes/jacobischeme.hh>
#include <dune/fem_bem_toolbox/coupling_schemes/gaussseidelscheme.hh>

#include "bulksurfproblem.hh"

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( HGridType &grid, int step )
{
  //! extract surface
  typedef typename Dune::FemFemCoupling::Glue< HGridType,false >::GrydType SurfaceGridType;
  std::shared_ptr<SurfaceGridType> grydp;

  if( Dune::Fem::Parameter::getValue< bool >( "surface.use", false ) )
  {
    const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", SurfaceGridType::dimension );
    const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );
    std::cout << "... from file" << std::endl;
    Dune::GridPtr< SurfaceGridType > grydPtr( grydfile );
    grydp = std::shared_ptr<SurfaceGridType>( grydPtr.release() );
    // grydp = tmp;
  }
  else
  {
    std::stringstream dgfBlock;
    dgfBlock << "PROJECTION\n"
             << "  function p(x) = x / |x|\n"
             << "  default p\n"
             << "#";
    typedef Dune::FemFemCoupling::SurfaceExtractor< Dune::Fem::LeafGridPart<HGridType>, SurfaceGridType > SurfaceExtractorType ;
    Dune::Fem::LeafGridPart<HGridType> gp(grid) ;
    SurfaceExtractorType surfaceExtractor( gp );
    surfaceExtractor.extractSurface( dgfBlock.str() );
    Dune::GridPtr< SurfaceGridType > grydPtr( "surface.dgf" );
    grydp = std::shared_ptr<SurfaceGridType>( grydPtr.release() );
    // grydp = tmp;
    return 0;
  }
  SurfaceGridType& surfaceGrid = *grydp ;

  std::cout << "do initial load balancing" << std::endl;
  typedef SimpleLoadBalanceHandle<HGridType> BulkLoadBalancer;
  BulkLoadBalancer bldb(grid);
  grid.repartition( bldb );

  std::cout << "loadbalance surface grid" << std::endl;
  typedef SimpleLoadBalanceHandle<SurfaceGridType> SurfLoadBalancer;
  SurfLoadBalancer sldb(surfaceGrid);
  surfaceGrid.repartition( sldb );

  // create surface gridpart, model, and finite element scheme
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GridPartType GridPartType;
  GridPartType gridPart(grid);
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceType FunctionSpaceType;
  typedef DiffusionModel< FunctionSpaceType, GridPartType> ModelType;
  typedef typename ModelType::ProblemType ProblemType ;
  const ProblemType& problem = BulkProblem< FunctionSpaceType > ();
  ModelType implicitModel( problem, gridPart );
  typedef FemScheme< ModelType, istl > SchemeType;
  SchemeType scheme( gridPart, implicitModel,"bulk" );

  typedef typename Dune::FemFemCoupling::Glue< HGridType,false >::GrydPartType GrydPartType;
  GrydPartType surfacePart(surfaceGrid);

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceOutsideType ExtractedSurfaceFunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< ExtractedSurfaceFunctionSpaceType, GrydPartType > ExtractedSurfaceModelType;

  typedef typename ExtractedSurfaceModelType::ProblemType ExtractedSurfaceProblemType ;
  const ExtractedSurfaceProblemType& problim = SurfaceProblem< ExtractedSurfaceFunctionSpaceType > ();

  ExtractedSurfaceModelType implycitModel( problim, surfacePart );

  typedef FemScheme< ExtractedSurfaceModelType, istl > ExtractedSurfaceSchemeType;
  ExtractedSurfaceSchemeType schyme( surfacePart, implycitModel,"surface" );

  // JacobiCouplingScheme<decltype(scheme), decltype(schyme)> ffScheme( scheme, schyme );
  GaussSeidelCouplingScheme<decltype(scheme), decltype(schyme)> ffScheme( scheme, schyme );

  // set up a timer for the looping
  time_t before;
  time(&before);

  ffScheme.prepare();
  ffScheme.solve(false);

  // determine how long it took to do the loop
  time_t after;
  time(&after);

  double seconds = difftime(after,before);

  // some output and error computation
  typedef typename ModelType::ExactSolutionType GridExactSolutionType;
  typedef typename ExtractedSurfaceModelType::ExactSolutionType GrydExactSolutionType;
  GridExactSolutionType gridExactSolution = implicitModel.exactSolution();
  GrydExactSolutionType surfaceExactSolution = implycitModel.exactSolution();

  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *,
                          decltype(gridExactSolution) * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step ) );

  typedef Dune::tuple< const typename ExtractedSurfaceSchemeType::DiscreteFunctionType *, GrydExactSolutionType *  > SurfaceIOTupleType;
  typedef Dune::Fem::DataOutput< SurfaceGridType, SurfaceIOTupleType > SurfaceDataOutputType;
  SurfaceIOTupleType surfaceioTuple( &(schyme.solution()), &surfaceExactSolution ) ; // tuple with pointers
  SurfaceDataOutputType surfacedataOutput( surfaceGrid, surfaceioTuple, DataOutputParameters( step, "surface" ) );

  // write initial solve
  dataOutput.write();
  surfacedataOutput.write();

  auto error = ffScheme.error();

  if( Dune::Fem::MPIManager::rank() == 0 )
  {
    std::cout << "Converged at iteration " << ffScheme.n() << " to an error of (" 
            << error.first << "," << error.second  << " ) after a loop time " << seconds << " secs." << std::endl;
  }

  return error.first + error.second;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  Dune::GridFactory<HGridType> factory;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  if( Dune::Fem::MPIManager::rank() == 0 )
  {
    std::cout << "Loading macro grid: " << gridfile << std::endl;
    Dune::GmshReader<HGridType>::read(factory, gridfile, true, true );
  }

  // setup EOC loop
  int repeats = Dune::Fem::Parameter::getValue< int >( "poisson.repeats", 0 );
  if (Dune::Fem::MPIManager::size()>1 && repeats>0)
  {
    std::cout << "cannot do eoc loop in parallel due to parallel boundary projection problem in ALUGrid" << std::endl;
    repeats = 0;
  }

  // calculate first step
  double oldError;
  // Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType *gridPtr = factory.createGrid();

  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  oldError = algorithm( grid, (repeats > 0) ? 0 : -1 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    double newError = 0;
    grid.globalRefine(1);
    newError = algorithm( grid, step );

    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }
  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

#ifndef RANNER_SPHERE_TRAITS_HH
#define RANNER_SPHERE_TRAITS_HH

#include <dune/grid/sgrid.hh>


#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif



#include <dune/grid-glue/extractors/extractorpredicate.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim0extractor.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/lagrangeinterpolation.hh>
#include <dune/fem/space/lagrange.hh>
#include <dune/fem_bem_toolbox/data_communication/surfaceExtractor.hh>


namespace Dune
{
  namespace FemFemCoupling
  {
    // general definition of template class without implementation
    template <class Grid, bool special>
    struct Glue
    {
      typedef typename Dune::Fem::LeafGridPart< Grid > GridPartType;
      // !!!!!!!!!!!!! why two types for the same thing?
      typedef typename ExtractedSurfaceGrid< Grid >::type OutsideHostGridType;
      typedef typename ExtractedSurfaceGrid< Grid >::type GrydType;

      typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, 1 > FunctionSpaceType;

      typedef typename Dune::Fem::LeafGridPart< GrydType > GrydPartType;

      typedef Dune::Fem::FunctionSpace< double, double, GrydType::dimensionworld, 1 > FunctionSpaceOutsideType;

      typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceOutsideType, GrydPartType, POLORDER > DiscreteFunctionSpaceOutsideType;

      // Grid views
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
      typedef typename Dune::Fem::GridPart2GridView< GridPartType > VolDomGridView;
      typedef typename Dune::Fem::GridPart2GridView< GrydPartType > SurfDomGridView;
#else
      typedef typename Dune::Fem::GridPartView< GridPartType > VolDomGridView;
      typedef typename Dune::Fem::GridPartView< GrydPartType > SurfDomGridView;
#endif
      typedef typename std::conditional<special,SurfDomGridView,VolDomGridView>::type DomGridView;
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
      typedef typename Dune::Fem::GridPart2GridView< GrydPartType > TarGridView;
#else
      typedef typename Dune::Fem::GridPartView< GrydPartType > TarGridView;
#endif

      // Extractors
      typedef Dune::GridGlue::Codim1Extractor<DomGridView> VolDomExtractor;
      typedef Dune::GridGlue::Codim0Extractor<DomGridView> SurfDomExtractor;
      typedef typename std::conditional<special,SurfDomExtractor,VolDomExtractor>::type DomExtractor;

      typedef Dune::GridGlue::Codim0Extractor<TarGridView> TarExtractor;

      // for volume to surface gluing
      typedef Dune::GridGlue::GridGlue<DomExtractor,TarExtractor> GlueType;
    };


  } // namespace FemFemCoupling

} // namespace Dune

#endif

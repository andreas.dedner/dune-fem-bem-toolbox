#ifndef BULKSURF_PROBLEM_HH
#define BULKSURF_PROBLEM_HH

#include <cassert>
#include <cmath>

#include <dune/fem_bem_toolbox/fem_objects/probleminterface.hh>

template <class FunctionSpace>
class BulkProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;

public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  BulkProblem() {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    const double Xx = x[0], Yy = x[1]; 
    phi = std::exp(-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(3. + 4.*Xx - 4.*std::pow(Xx,2) + 4.*Yy - 4.*std::pow(Yy,2));
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(1);
  }

  //! the exact bulk solution
  virtual void u(const DomainType& x,
                 RangeType& ret) const
  {
    ret = std::exp( - x[0] * (x[0] - 1.0) - x[1] * (x[1] - 1.0)  );
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret[0][0] = 0.;
    ret[0][1] = 0.;
    ret[0][2] = 0.;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return false ;
  }

  //! return true if Robin boundary is present (default is false)
  virtual bool hasRobinBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return false ;
  }
};

template <class FunctionSpace>
class SurfaceProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  typedef typename FunctionSpace::HessianRangeType HessianRangeType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& ret) const
  {
    const double Xx = x[0], Yy = x[1], Zz = x[2];
    ret = std::exp(-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(1.*Xx - 2.*std::pow(Xx,2) + 1.*Yy - 2.*std::pow(Yy,2)) + std::exp(-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(3. + 4.*std::pow(Xx,5) - 4.*std::pow(Xx,6) + 4.*std::pow(Yy,5) - 4.*std::pow(Yy,6) + std::pow(Yy,4)*(13. - 4.*std::pow(Zz,2)) + std::pow(Xx,4)*(13. + 4.*Yy - 12.*std::pow(Yy,2) - 4.*std::pow(Zz,2)) + Yy*(8. - 2.*std::pow(Zz,2)) + std::pow(Yy,3)*(-10. + 4.*std::pow(Zz,2)) + std::pow(Xx,3)*(-10. - 2.*Yy + 8.*std::pow(Yy,2) + 4.*std::pow(Zz,2)) + std::pow(Yy,2)*(-14. + 5.*std::pow(Zz,2)) + std::pow(Xx,2)*(-14. + 8.*std::pow(Yy,3) - 12.*std::pow(Yy,4) + 5.*std::pow(Zz,2) + std::pow(Yy,2)*(26. - 8.*std::pow(Zz,2)) + Yy*(-10. + 4.*std::pow(Zz,2))) + Xx*(8. - 2.*std::pow(Yy,3) + 4.*std::pow(Yy,4) - 2.*std::pow(Zz,2) + Yy*(4. - 2.*std::pow(Zz,2)) + std::pow(Yy,2)*(-10. + 4.*std::pow(Zz,2)))) + std::exp(-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(4. + 19.*Xx - 34.*std::pow(Xx,2) - 12.*std::pow(Xx,7) + 8.*std::pow(Xx,8) + 19.*Yy - 34.*std::pow(Yy,2) - 56.*std::pow(Yy,3) + 60.*std::pow(Yy,4) + 45.*std::pow(Yy,5) + 12.*Xx*std::pow(Yy,5) - 36.*std::pow(Xx,2)*std::pow(Yy,5) - 38.*std::pow(Yy,6) - 12.*Xx*std::pow(Yy,6) + 32.*std::pow(Xx,2)*std::pow(Yy,6) - 12.*std::pow(Yy,7) + 8.*std::pow(Yy,8) - 2.*Xx*std::pow(Zz,2) + 8.*std::pow(Xx,2)*std::pow(Zz,2) - 2.*Yy*std::pow(Zz,2) + 8.*std::pow(Yy,2)*std::pow(Zz,2) + 21.*std::pow(Yy,3)*std::pow(Zz,2) - 22.*std::pow(Yy,4)*std::pow(Zz,2) - 12.*std::pow(Yy,5)*std::pow(Zz,2) + 8.*std::pow(Yy,6)*std::pow(Zz,2) + std::pow(Xx,2)*std::pow(Yy,2)*(120. - 44.*std::pow(Zz,2)) + std::pow(Xx,2)*std::pow(Yy,3)*(88. - 24.*std::pow(Zz,2)) + Xx*std::pow(Yy,4)*(43. - 12.*std::pow(Zz,2)) + std::pow(Xx,5)*(45. + 12.*Yy - 36.*std::pow(Yy,2) - 12.*std::pow(Zz,2)) + Xx*Yy*(24. - 8.*std::pow(Zz,2)) + std::pow(Xx,6)*(-38. - 12.*Yy + 32.*std::pow(Yy,2) + 8.*std::pow(Zz,2)) + Xx*std::pow(Yy,3)*(-32. + 12.*std::pow(Zz,2)) + std::pow(Xx,2)*Yy*(-52. + 19.*std::pow(Zz,2)) + Xx*std::pow(Yy,2)*(-52. + 19.*std::pow(Zz,2)) + std::pow(Xx,2)*std::pow(Yy,4)*(-114. + 24.*std::pow(Zz,2)) + std::pow(Xx,3)*(-56. + 24.*std::pow(Yy,3) - 36.*std::pow(Yy,4) + 21.*std::pow(Zz,2) + std::pow(Yy,2)*(88. - 24.*std::pow(Zz,2)) + Yy*(-32. + 12.*std::pow(Zz,2))) + std::pow(Xx,4)*(60. - 36.*std::pow(Yy,3) + 48.*std::pow(Yy,4) - 22.*std::pow(Zz,2) + Yy*(43. - 12.*std::pow(Zz,2)) + std::pow(Yy,2)*(-114. + 24.*std::pow(Zz,2))));
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(1);
  }

  //! the exact surface solution derives from the chosen bulk interior solution
  virtual void u(const DomainType& p, RangeType& phi) const
  {
    const double Xx = p[0], Yy = p[1], Zz = p[2];
    phi = std::exp(-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(1. + 1.*Xx - 2.*Xx*Xx + (1. - 2.*Yy)*Yy);
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x, JacobianRangeType& ret) const
  {
    ret[0][0] = 0.;
    ret[0][1] = 0.;
    ret[0][2] = 0.;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return false ;
  }

  //! return true if Robin boundary is present (default is false)
  virtual bool hasRobinBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return false ;
  }
};

#endif

template <class FS, class GP>
struct LaplaceModel
{
  typedef GP GridPartType;
  typedef FS FunctionSpaceType;
  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;

  void evaluate( const DomainType& x, RangeType& res ) const
  {
    double r = x.two_norm();
    res[0] = 2 * x[0] * x[2] / (r * r * r * r * r) - x[1] / (r * r * r);
    std::cout << "Laplacemodel: " << res[0] << std::endl;
  }
  bool hasDirichletBoundary() const
  {
    return true;
  }
  bool hasRobinBoundary() const
  {
    return false;
  }
  struct ExactNeumannData
  {
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      double r = x.two_norm();
      res[0] = -6 * x[0] * x[2] / (r * r * r * r * r * r) + 2 * x[1] / (r * r * r * r);
    }
  };
  struct Exact
  {
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& p, RangeType& res ) const
    {
      double x = p[0], y = p[1], z = p[2];
      double r = p.two_norm();
      res[0] = -6 * x * z / (r * r * r * r * r * r) + 2 * y / (r * r * r * r);
    }
  };
  const bool doDoubleLayer() const
  {
    return true;
  };
  const double idOpCoeff() const
  {
    return -0.5;
  };
};
template <class FS, class GP>
struct HelmholtzModel
{
  typedef HelmholtzModel<FS,GP> ModelType;
  typedef GP GridPartType;
  typedef FS FunctionSpaceType;
  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;

  HelmholtzModel(double omega) 
  : omega_( omega )
  , amplitude_( Dune::Fem::Parameter::getValue< double >( "helmholtz.amplitude", 0 ) )
  , direction_( DomainType(0) )
  , c_(1.)
  , incidentData_(*this)
  , dirichletData_(*this)
  , exactD_(*this)
  {
    direction_[0] = 1.0;
    direction_[1] = 2.0;
    double len = direction_.two_norm();
    direction_ /= len;
  }
  double omega() const
  {
    return omega_;
  }
  bool hasDirichletBoundary() const
  {
    return true;
  }
  bool hasRobinBoundary() const
  {
    return false;
  }
  struct IncidentData
  {
    const ModelType &model_;
    IncidentData(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      double r = x.dot( model_.direction_ );
      double real = std::cos( model_.omega_ * r );
      double imag = std::sin( model_.omega_ * r );
      res = std::complex<double>(real,imag);
      res *= model_.amplitude_;
    }
  };
  struct DirichletData
  {
    const ModelType &model_;
    DirichletData(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      res = RangeType(0);
    }
  };
  struct ExactNeumann
  {
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      res = RangeType(0);
    }
  };
  struct ExactDirichlet
  {
    const ModelType &model_;
    ExactDirichlet(const ModelType &model) : model_(model) {}
    typedef FS FunctionSpaceType;
    void evaluate( const DomainType& x, RangeType& res ) const
    {
      res = RangeType(0);
    }
  };
  const IncidentData& incidentData() const
  {
    return incidentData_;
  }
  const DirichletData& dirichletData() const
  {
    return dirichletData_;
  }
  const ExactDirichlet& exactDirichlet() const
  {
    return exactD_;
  }
  const bool doDoubleLayer() const
  {
    return true;
  };
  const double idOpCoeff() const
  {
    return -0.5;
  };
private:
  const double omega_;
  const double amplitude_;
  DomainType direction_;
  const double c_;
  const IncidentData incidentData_;
  const DirichletData dirichletData_;
  const ExactDirichlet exactD_;
};


#include <config.h>

// iostream includes
#include <iostream>
#include <complex>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "traits.hh"

// include grid part
#include <dune/fem/gridpart/leafgridpart.hh>

#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_simple.hh>

// include header of adaptive scheme
#include <dune/fem_bem_toolbox/data_communication/fem_fem_coupling.hh>
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>

#include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>
#include <dune/fem_bem_toolbox/bem_objects/sharedbemscheme.hh>
// #include "../../common/source/erfscheme.hh"
#include <dune/fem_bem_toolbox/coupling_schemes/gmresscheme.hh>
#include <dune/fem_bem_toolbox/coupling_schemes/jacobischeme.hh>
#include <dune/fem_bem_toolbox/coupling_schemes/gaussseidelscheme.hh>

#include <dune/grid/io/file/gmshreader.hh>

#include "fembem.hh"
#include "bemmodel.hh"

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType> //  , class PlotGridType>
double algorithm ( HGridType &grid, int step ) // , PlotGridType &plotGryd
{
  // set up a timer for the looping
  time_t before;
  time(&before);

  // we want to solve the problem on the leaf elements of the grid
  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GridPartType GridPartType;
  GridPartType gridPart( grid );

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType, false>::FunctionSpaceType FunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< FunctionSpaceType, GridPartType > ModelType;

  typedef typename ModelType::ProblemType ProblemType ;
  ProblemType* problemPtr = 0 ;
  const std::string problemNames [] = { "bulkprob", "surfprob", "bulkwave", "surfwave" };
  const int problemNumber = Dune::Fem::Parameter::getEnum("volume.problem", problemNames, 0 );
  switch ( problemNumber )
  {
    case 0:
#if COMPLEX
      problemPtr = new HelmholtzFundamental< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
#else
      problemPtr = new BulkBemProblem< FunctionSpaceType > ();
#endif
      break ;
    case 1:
#if COMPLEX
      problemPtr = new SurfaceHelmholtzFundamental< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
      break ;
    case 2:
      problemPtr = new WaveScatter< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
      break ;
    case 3:
      problemPtr = new SurfaceWaveScatter< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
#else
      problemPtr = new SurfaceBemProblem< FunctionSpaceType > ();
#endif
      break ;
    otherwise: 
      std::cout << "wrong problem number chooses ... abording" << std::endl;
      abort();
  }
  assert( problemPtr );
  ProblemType& problem = *problemPtr ;

  // implicit model for left hand side
  ModelType implicitModel( problem, gridPart );
  // ModelType ImplicitModel( problem, GridPart );

  // create adaptive scheme
  typedef FemScheme< ModelType, istl > SchemeType;
  SchemeType scheme( gridPart, implicitModel,"bulk" );

  // create dune grid file with surface mesh
  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydType GrydType;
  // create dune grid file with surface mesh
  GrydType* grydd = 0;
  GrydType* fullGrydd = 0;

  // if requested, use a previously created surface grid
  if( Dune::Fem::Parameter::getValue< bool >( "surface.use", false ) )
  {
    const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", GrydType::dimension );
    const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );
    Dune::GridPtr< GrydType > grydPtr( grydfile );
    grydd = grydPtr.release();
    // duplicate surface grid for BEM operators which will NOT be partitioned
    Dune::GridPtr< GrydType > fullGrydPtr( grydfile, Dune::MPIHelper::getLocalCommunicator() );
    fullGrydd = fullGrydPtr.release();
  }
  else
  {
    Dune::FemFemCoupling::SurfaceExtractor<GridPartType , GrydType > surfaceExtractor( gridPart );
    surfaceExtractor.extractSurface();
    return 0;
  }
  GrydType& gryd = *grydd ;
  GrydType& fullGryd = *fullGrydd ;

  typedef SimpleLoadBalanceHandle<GrydType> SurfLoadBalancer;
  SurfLoadBalancer sldb(gryd);
  SurfLoadBalancer fsldb(fullGryd,true);

  // do initial load balance
  if ( sldb.repartition() )
    gryd.repartition( sldb );
  else
    gryd.loadBalance();

  // dummey load balance that puts whole grid on node zero
  if ( fsldb.repartition() )
    fullGryd.repartition( fsldb );
  else
    fullGryd.loadBalance();

  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydPartType GrydPartType;
  GrydPartType grydPart(gryd);
  GrydPartType fullGrydPart(fullGryd);

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType, false>::FunctionSpaceOutsideType ExtractedSurfaceFunctionSpaceType;

  // type of the mathematical model used
  typedef HelmholtzModel< ExtractedSurfaceFunctionSpaceType, GrydPartType > ExtractedSurfaceModelType;

  ExtractedSurfaceModelType implycitModel( Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ) );

#if 0
  typedef BemScheme< ExtractedSurfaceModelType > ExtractedSurfaceSchemeType;
  ExtractedSurfaceSchemeType schyme( grydPart, implycitModel, "surface" );
#else
  typedef SharedBemScheme< ExtractedSurfaceModelType > ExtractedSurfaceSchemeType;
  ExtractedSurfaceSchemeType schyme( grydPart, fullGrydPart, implycitModel, sldb, "surface" );
#endif

  auto problim = implycitModel.exactDirichlet();
  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GridPartType > GridExactSolutionType;
  typedef Dune::Fem::GridFunctionAdapter< decltype(problim) , GrydPartType > GrydExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, gridPart, 5 );
  GrydExactSolutionType grydExactSolution("exact solution", problim, grydPart, 5 );
  GrydExactSolutionType fullGrydExactSolution("exact solution", problim, fullGrydPart, 5 );

  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *,
                       const typename SchemeType::DiscreteFunctionType *,
                       GridExactSolutionType * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &(scheme.rhs()), &gridExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step ) );

  typedef Dune::tuple< const typename ExtractedSurfaceSchemeType::DiscreteFunctionType *,
                       const typename ExtractedSurfaceSchemeType::DiscreteFunctionType *,
                       GrydExactSolutionType *  > SurfaceIOTupleType;
  // typedef Dune::tuple< const typename PlotSurfaceErfSchemeType::P1DiscreteFunctionType * > PlotSurfaceIOTupleType;
  typedef Dune::tuple< const typename ExtractedSurfaceSchemeType::P1DiscreteFunctionType * > DofRankIOTupleType;
  typedef Dune::Fem::DataOutput< GrydType, SurfaceIOTupleType > SurfaceDataOutputType;
  SurfaceIOTupleType surfaceioTuple( &(schyme.solution()),
                                     &(schyme.rhs()),
                                     &grydExactSolution ) ; // tuple with pointers
  // PlotSurfaceIOTupleType plotSurfaceioTuple( &(plotSchyme.rhs()) ) ; // tuple with pointers
  SurfaceDataOutputType surfacedataOutput( gryd, surfaceioTuple, DataOutputParameters( step, "surface" ) );
  // PlotSurfaceDataOutputType erfDataOutput( plotGryd, plotSurfaceioTuple, DataOutputParameters( step, "erf surface" ) );

  /*
  typedef Dune::Fem::DataOutput< GrydType, DofRankIOTupleType > DofRankDataOutputType;
  DofRankIOTupleType dofRankIoTuple( &(schyme.dofRank()) );
  DofRankDataOutputType dofRankDataOutput( fullGryd, dofRankIoTuple, DataOutputParameters( step, "dofrank" ) );
  dofRankDataOutput.write( );
  */

  GMResCouplingScheme<decltype(scheme), decltype(schyme)> fbScheme( scheme, schyme );
  // For experimental purposes
  // JacobiCouplingScheme<decltype(scheme), decltype(schyme)> fbScheme( scheme, schyme );
  // GaussSeidelCouplingScheme<decltype(scheme), decltype(schyme)> fbScheme( scheme, schyme );

  // setup the right hand side
  fbScheme.prepare();

  // solve once
  fbScheme.solve();

  // determine how long it took to do the loop
  time_t after;
  time(&after);

  double seconds = difftime(after,before);

  if( Dune::Fem::MPIManager::rank() == 0 )
  {
    std::cout << "Converged at iteration " << fbScheme.n() << " after a time " << seconds << " secs." << std::endl;
  }

  const int externalLevel = Dune::Fem::Parameter::getValue< int >( "external_grid.level" );

  if(externalLevel<-5) return 0.;

  // write final output ((float)timer)/CLOCKS_PER_SEC
  dataOutput.write();
  surfacedataOutput.write();

  // output bem solution on an extended grid
  const std::string externalName = Dune::Fem::Parameter::getValue< std::string >( "external_grid" );
  schyme.outputExternal( externalName, externalLevel, true );
  return 0.;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << "  " << std::endl; // < plotGridFile <

  // determine type of grid file
  bool dgfFile = gridfile.find("dgf") != std::string::npos;
  bool gmshFile = gridfile.find("msh") != std::string::npos;

  // check file type is meaningful
  if( !dgfFile && !gmshFile )
  {
    std::cout << "Unknown file type " << dgfFile << "  " << gmshFile << std::endl;
    std::abort();
  }

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType& grid = *gridPtr ;

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<HGridType> BulkLoadBalancer;
  BulkLoadBalancer bldb(grid);

  // do initial load balance
  if ( bldb.repartition() )
    grid.repartition( bldb );
  else
  grid.loadBalance();

  // plotGrid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "poisson.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );
  // Dune::Fem::GlobalRefine::apply( plotGrid, level * refineStepsForHalf );

  // calculate first step
  double oldError = algorithm( grid, 0 ); // , plotGrid

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

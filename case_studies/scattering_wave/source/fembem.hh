#ifndef FEMBEM_HH
#define FEMBEM_HH


#include <cassert>
#include <cmath>

#if COMPLEX
#include "helmholtz.hh"
#include "../../../case_studies/scattering_wave/source/wavescat.hh"
#endif

// a reentrant corner problem with a 270 degree corner
template <class FunctionSpace>
class BulkBemProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;

public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  BulkBemProblem() {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& p,
                 RangeType& phi) const
  {
    phi = 0;
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(1);
  }

  //! the exact bulk solution
  virtual void u(const DomainType& p,
                 RangeType& ret) const
  {
    ret = RangeType(0);
    return;
    double x = p[0], y = p[1], z = p[2];
    double r = p.two_norm();
    ret[0] = 2 * x * z / (r * r * r * r * r) - y / (r * r * r);
    // ret[0] = -1 / r;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret[0][0] = 2.*x[0];
    ret[0][1] = 2.*x[1];
    ret[0][2] = 2.*x[2];
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  bool hasRobinBoundary() const
  {
    return false;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    // assert( x.two_norm() < 0.5 );
    return true ;
  }
};

// not really used....
template <class FunctionSpace>
class SurfaceBemProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  typedef typename FunctionSpace::HessianRangeType HessianRangeType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& ret) const
  {
    ret = 0;
    abort();
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = 0;
    abort();
  }

  //! the exact normal derivative on the surface derives from the chosen bulk interior solution
  virtual void u(const DomainType& p, RangeType& phi) const
  {
    double x = p[0], y = p[1], z = p[2];
    double r = p.two_norm();
    phi[0] = -6 * x * z / (r * r * r * r * r * r) + 2 * y / (r * r * r * r);
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x, JacobianRangeType& ret) const
  {
    ret[0][0] = 2.;
    ret[0][1] = 2.;
    ret[0][2] = 2.;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  bool hasRobinBoundary() const
  {
    return false;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true ;
  }
};

#endif

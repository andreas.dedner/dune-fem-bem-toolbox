##### Verbosity #####
# number of process that prints output (-1 no output)
fem.verboserank: 0
# true for iteration output, false means no output
fem.solver.verbose: false
fem.solver.gmres.restart: 50

##### Parameters for preconditioned linear solver
# preconditioning method: none, ssor, sor, ilu-0, ilu-n, gauss-seidel, jacobi, amg-ilu-0
istl.preconditioning.method: none
istl.preconditioning.iterations: 1
istl.preconditioning.relaxation: 1.2
# enable simple Jacobi preconditioning (for non istl inverse operators)
fem.preconditioning: false

#### Parameters for input ######
# macro grid files
fem.io.macroGridFile_1d: ../data/circle.dgf
fem.io.macroGrydFile_1d: surface.dgf
fem.io.macroGridFile_2d: ../data/unitcube-2d.dgf
fem.io.macroGrydFile_2d: surface.dgf
# fem.io.macroGridFile_2d: ../data/corner.dgf
# fem.io.macroGridFile_2d: ../data/sphere.dgf
fem.io.macroGrydFile_2d: surface.dgf
# fem.io.macroGridFile_3d: ../data/sphereInSphere.msh
fem.io.macroGridFile_3d: ../data/unitcube-3d.tmp.dgf
# fem.io.macroGridFile_3d: ../data/sphere-3d.dgf
fem.io.macroGrydFile_3d: surface.dgf

# fem.io.outputformat: sub-vtk-cell
# fem.io.subsamplinglevel: 2

surface.use: 1

external_grid: ../data/external-3d.dgf
# external_grid: ../data/sphere.dgf
external_grid.level: -1


#### Parameters for output ######
# path for output
fem.prefix: ../output
# time interval for data output
fem.io.savestep: 0.1
# number of calls to write() after which a file is produced
fem.io.savecount: 1
# output format (vtk-cell, vtk-vertex, sub-vtk-cell, sub-vtk-vertex, gnuplot)
fem.io.outputformat: vtk-cell
# print partitioning for parallel runs: none, rank, rank+thread, rank/thread
fem.io.partitioning: rank

#### Parameters for fem-fem-coupling ######
# outer boundary radius of inner region
coupling.inner: 0.0
# inner boundary radius of outer region
coupling.outer: 0.0
# alpha value for Dirichlet steps
coupling.alpha: 1.0
# beta value for Neumann steps
coupling.beta: 0.0
# gamma value for Robin steps
coupling.gamma: 0.0
# Successive over-relaxation
coupling.sor: 1.0

##### Problem setup parameters #####
# level of initial global refinement
poisson.level: 0
# number of EOC steps to be performed
poisson.repeats: 0
# tolerance for linear solver
poisson.solvereps: 1e-6
# valid are: cos | sin | bulkprob | surfprob | sphere
volume.problem: bulkwave
surface.problem: surfwave
# volume.problem: bulkprob
# surface.problem: surfprob

helmholtz.omega: autoomega

helmholtz.shift: 0.0

helmholtz.limit: 50

helmholtz.squash: 0.0

helmholtz.amplitude: 0.1

helmholtz.boxwave: 1.0

##### parameters used for afem example #####
# valid are: none | maximum | equidistribution | uniform
adaptation.strategy: maximum
# tolerance for error estimator
adaptation.tolerance: 0.01

dirdata.warp: 1.0

fembem.useGMRes: true


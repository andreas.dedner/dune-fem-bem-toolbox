##### Verbosity #####
# number of process that prints output (-1 no output)
fem.verboserank: -1
# true for iteration output, false means no output
fem.solver.verbose: false

##### Parameters for preconditioned linear solver
# preconditioning method: none, ssor, sor, ilu-0, ilu-n, gauss-seidel, jacobi, amg-ilu-0
istl.preconditioning.method: jacobi
istl.preconditioning.iterations: 1
istl.preconditioning.relaxation: 1.2
# enable simple Jacobi preconditioning (for non istl inverse operators)
fem.preconditioning: false

#### Parameters for input ######
# macro grid files
fem.io.macroGridFile_2d: ../data/unitcube-2d.dgf
fem.io.macroGridFile_3d: ../data/unitcube-3d.dgf

#### Parameters for output ######
# path for output
fem.prefix: ../output
# number of calls to write() after which a file is produced
fem.io.savecount: 1
# output format (vtk-cell, vtk-vertex, sub-vtk-cell, sub-vtk-vertex, gnuplot)
fem.io.outputformat: vtk-cell
# print partitioning for parallel runs: none, rank, rank+thread, rank/thread
fem.io.partitioning: rank

##### Problem setup parameters #####
# level of initial global refinement
poisson.level: 0
# number of EOC steps to be performed
poisson.repeats: 0
# tolerance for linear solver
poisson.solvereps: 1e-12
# valid are: cos | sin | corner | curvedridges | sphere
poisson.problem: cos

